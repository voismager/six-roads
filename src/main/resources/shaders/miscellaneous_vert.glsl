#version 330

#define precision lowp float;

layout (location = 0) in vec3 loc_position;
layout (location = 1) in vec2 loc_texCoord;
layout (location = 3) in mat4 loc_modelMatrix;

out vec2 o_textCoord;

uniform int u_instanced;
uniform mat4 u_modelMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_projectionMatrix;

void main() {
    mat4 modelMatrix;

    if (u_instanced > 0) {
        modelMatrix = loc_modelMatrix;
    } else {
        modelMatrix = u_modelMatrix;
    }

    gl_Position = u_projectionMatrix * u_viewMatrix * modelMatrix * vec4(loc_position, 1.0);
    o_textCoord = loc_texCoord;
}
