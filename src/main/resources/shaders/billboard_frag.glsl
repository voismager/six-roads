#version 330

#define precision lowp float;

in vec2 o_texture_coordinate;
in vec3 o_colour;
in float o_filter;

out vec4 fragColor;

uniform sampler2D u_texture;
uniform bool u_textured;
uniform vec4 u_filter;

void main() {
    vec4 colour;

    if (u_textured) {
        colour = texture(u_texture, o_texture_coordinate);
        colour.a *= u_filter.a;
    } else {
        colour = vec4(o_colour, u_filter.a);
    }

    colour.rgb = clamp(colour.rgb - (vec3(1, 1, 1) - u_filter.rgb) * o_filter, 0, 1);

    fragColor = colour;
}