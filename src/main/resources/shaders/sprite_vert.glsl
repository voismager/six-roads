#version 330

#define precision lowp float;

layout (location = 0) in vec3 loc_position;
layout (location = 1) in vec2 loc_texCoord;

out vec2 o_texCoord;

uniform mat4 u_modelMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_projectionMatrix;

void main() {
    mat4 modelMatrix = u_modelMatrix;
    mat4 modelViewMatrix = u_viewMatrix * modelMatrix;
    vec4 mvPos = modelViewMatrix * vec4(loc_position, 1.0);
    o_texCoord = loc_texCoord;
    gl_Position = u_projectionMatrix * mvPos;
}
