#version 330

layout (location = 0) in vec2 loc_position;
layout (location = 1) in vec2 loc_texture_coordinate;
layout (location = 1) in vec3 loc_colour;
layout (location = 2) in float loc_filter;

uniform mat4 u_projectionMatrix;
uniform mat4 u_viewMatrix;
uniform vec3 u_cameraRight;
uniform vec3 u_cameraUp;

uniform vec3 u_center;
uniform vec2 u_size;
uniform bool u_textured;

out vec2 o_texture_coordinate;
out vec3 o_colour;
out float o_filter;

void main() {
    vec3 pos = u_center + u_cameraRight * loc_position.x * u_size.x + u_cameraUp * loc_position.y * u_size.y;
    gl_Position = u_projectionMatrix * u_viewMatrix * vec4(pos, 1.0);

    if (u_textured) {
        o_texture_coordinate = loc_texture_coordinate;
    } else {
        o_colour = loc_colour;
    }

    o_filter = loc_filter;
}
