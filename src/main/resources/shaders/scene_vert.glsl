#version 330

#define precision lowp float;

const int NUM_CASCADES = 2;

struct Material {
    bool hasTexture;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float reflectance;
};

layout (location = 0) in vec3 loc_position;
layout (location = 1) in vec2 loc_texCoord;
layout (location = 2) in vec3 loc_normal;
layout (location = 3) in mat4 loc_modelMatrix;

out vec2 o_textCoord;
out vec3 o_normal;
out vec3 o_mvPos;
out vec4 o_mlightViewVertexPos[NUM_CASCADES];

uniform int u_instanced;
uniform mat4 u_modelMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_projectionMatrix;
uniform mat4 u_lightViewMatrix[NUM_CASCADES];
uniform mat4 u_orthoProjectionMatrix[NUM_CASCADES];
uniform Material u_material;

void main() {
    mat4 modelMatrix;

    if (u_instanced > 0) {
        modelMatrix = loc_modelMatrix;
    } else {
        modelMatrix = u_modelMatrix;
    }

    mat4 modelViewMatrix = u_viewMatrix * modelMatrix;
    vec4 mvPos = modelViewMatrix * vec4(loc_position, 1.0);
    gl_Position = u_projectionMatrix * mvPos;

    o_textCoord = loc_texCoord;
    o_normal = normalize(modelViewMatrix * vec4(loc_normal, 0.0)).xyz;
    o_mvPos = mvPos.xyz;

    for (int i = 0; i < NUM_CASCADES; i++) {
        o_mlightViewVertexPos[i] = u_orthoProjectionMatrix[i] * u_lightViewMatrix[i] * modelMatrix * vec4(loc_position, 1.0);
    }
}
