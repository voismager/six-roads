#version 330

#define precision lowp float;

const int NUM_CASCADES = 2;

in vec2 o_textCoord;
in vec3 o_normal;
in vec3 o_mvPos;
in vec4 o_mlightViewVertexPos[NUM_CASCADES];

out vec4 fragColor;

struct DirectionalLight {
    vec3 colour;
    vec3 direction;
    float intensity;
};

struct Material {
    bool hasTexture;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float reflectance;
};

uniform sampler2D u_texture;
uniform sampler2D u_shadowMap_0;
uniform sampler2D u_shadowMap_1;

uniform float u_specularPower;
uniform Material u_material;
uniform DirectionalLight u_directionalLight;
uniform vec3 u_ambientLight;
uniform float u_cascadeFarPlanes[NUM_CASCADES];

vec4 ambientC;
vec4 diffuseC;
vec4 specularC;

void setupColors(Material material) {
    if (material.hasTexture) {
        ambientC = texture(u_texture, o_textCoord);
        diffuseC = ambientC;
        specularC = ambientC;
    } else {
        ambientC = material.ambient;
        diffuseC = material.diffuse;
        specularC = material.specular;
    }
}

float calcShadow(vec4 position, int idx) {
    vec3 projCoords = position.xyz;
    projCoords = projCoords * 0.5 + 0.5;
    float bias = 0.0008;

    float textDepth;

    if (idx == 0) {
        textDepth = texture(u_shadowMap_0, projCoords.xy).r;
    } else {
        textDepth = texture(u_shadowMap_1, projCoords.xy).r;
    }

    if (projCoords.z - bias < textDepth) {
        return 1.0;
    } else {
        return 0.0;
    }
}

vec4 calcLightColour(vec3 lightColour, float lightIntensity, vec3 position, vec3 toLightSource, vec3 normal) {
    vec4 diffuseColour = vec4(0, 0, 0, 0);
    vec4 specColour = vec4(0, 0, 0, 0);

    //Diffuse light
    float diffuseFactor = max(dot(normal, toLightSource), 0.0);
    diffuseColour = u_material.diffuse * vec4(lightColour, 1.0) * lightIntensity * diffuseFactor;

    //Specular light
    vec3 cameraDirection = normalize(-position);
    vec3 reflectedLight = normalize(reflect(-toLightSource, normal));
    float specularFactor = max(dot(cameraDirection, reflectedLight), 0.0);
    specularFactor = pow(specularFactor, u_specularPower);
    specColour = u_material.specular * specularFactor * u_material.reflectance * vec4(lightColour, 1.0);

    return (diffuseColour + specColour);
}

vec4 calcDirectionalLight(DirectionalLight light, vec3 position, vec3 normal) {
    return calcLightColour(light.colour, light.intensity, position, normalize(light.direction), normal);
}

void main() {
    setupColors(u_material);

    float shadow = 0.0;
    for (int i = 0; i < NUM_CASCADES; i++) {
        if ( abs(o_mvPos.z) < u_cascadeFarPlanes[i] ) {
            shadow = calcShadow(o_mlightViewVertexPos[i], i);
            break;
        }
    }

    vec4 diffuseSpecularComp = calcDirectionalLight(u_directionalLight, o_mvPos, o_normal);

    fragColor = clamp(ambientC * vec4(u_ambientLight, 1.0) + diffuseSpecularComp * shadow, 0.0, 1.0);
}