#version 330

#define precision lowp float;

in vec2 o_textCoord;

out vec4 fragColor;

uniform sampler2D u_texture;
uniform bool u_textured;
uniform vec4 u_color;

void main() {
    if (u_textured) {
        fragColor = texture(u_texture, o_textCoord);
    } else {
        fragColor = u_color;
    }
}