#version 330

out vec4 fragColor;

void main() {
    gl_FragDepth = gl_FragCoord.z;
}
