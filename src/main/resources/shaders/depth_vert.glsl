#version 330

layout (location = 0) in vec3 o_position;
layout (location = 3) in mat4 loc_modelMatrix;

uniform int u_instanced;
uniform mat4 u_modelMatrix;
uniform mat4 u_orthoProjectionMatrix;
uniform mat4 u_lightViewMatrix;

void main() {
    mat4 modelMatrix;

    if (u_instanced > 0) {
        modelMatrix = loc_modelMatrix;
    } else {
        modelMatrix = u_modelMatrix;
    }

    gl_Position = u_orthoProjectionMatrix * u_lightViewMatrix * modelMatrix * vec4(o_position, 1.0);
}
