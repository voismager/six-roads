#version 330

#define precision lowp float;

in vec2 o_texCoord;

out vec4 fragColor;

struct Material {
    bool hasTexture;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float reflectance;
};

uniform sampler2D u_texture;
uniform Material u_material;

void main() {
    vec4 color;

    if (u_material.hasTexture) {
        color = texture(u_texture, o_texCoord);
    } else {
        color = u_material.ambient + u_material.diffuse + u_material.specular * u_material.reflectance;
    }

    fragColor = color;
}