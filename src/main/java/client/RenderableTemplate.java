package client;

import common.Template;
import engine.graphics.core.Model;
import engine.types.vectors.Vector;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.ObjectSet;

public class RenderableTemplate extends Template {
    public Model model;

    public RenderableTemplate(short id, String name, ObjectSet<String> labels, Object2ObjectMap<String, Vector> attributes) {
        super(id, name, labels, attributes);
    }

    public RenderableTemplate(RenderableTemplate template) {
        super(template);
        this.model = template.model;
    }
}
