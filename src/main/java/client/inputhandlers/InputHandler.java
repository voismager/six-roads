package client.inputhandlers;

import common.gameobjects.Puppeteer;
import engine.Cleanable;

public interface InputHandler extends Cleanable {
    void init(Puppeteer me);

    void onLoopUpdate(byte currentMouseState);
}
