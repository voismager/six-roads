package client.inputhandlers;

import common.fsm.FSM;
import common.gameobjects.Puppeteer;
import engine.Window;
import scripting.GlobalEnvironment;

import java.util.Collections;
import java.util.Map;

import static org.lwjgl.glfw.GLFW.*;

public class WASD implements InputHandler {
    private final GlobalEnvironment env;
    private final FSM fsm;
    private final boolean[] wasd;

    public WASD(GlobalEnvironment env, FSM fsm) {
        this.env = env;
        this.fsm = fsm;
        this.wasd = new boolean[4];
    }

    @Override
    public void init(Puppeteer me) {
        glfwSetKeyCallback(Window.getHandle(), (windowHandle, key, scancode, action, mods) -> {
            switch (key) {
                case GLFW_KEY_W:
                    if (action == GLFW_PRESS) this.wasd[0] = true;
                    if (action == GLFW_RELEASE) this.wasd[0] = false;
                    break;
                case GLFW_KEY_D:
                    if (action == GLFW_PRESS) this.wasd[1] = true;
                    if (action == GLFW_RELEASE) this.wasd[1] = false;
                    break;
                case GLFW_KEY_S:
                    if (action == GLFW_PRESS) this.wasd[2] = true;
                    if (action == GLFW_RELEASE) this.wasd[2] = false;
                    break;
                case GLFW_KEY_A:
                    if (action == GLFW_PRESS) this.wasd[3] = true;
                    if (action == GLFW_RELEASE) this.wasd[3] = false;
                    break;
            }
        });

        final Map<?, ?> params = Collections.singletonMap("puppeteer", me);
        this.fsm.init(me.getGamemode().rootScript, params);
    }

    @Override
    public void onLoopUpdate(byte currentMouseState) {
        if (wasd[0]) {
            if (wasd[1]) fsm.onEvent(1);
            else if (wasd[3]) fsm.onEvent(7);
            else fsm.onEvent(0);
        }
        else if (wasd[2]) {
            if (wasd[1]) fsm.onEvent(3);
            else if (wasd[3]) fsm.onEvent(5);
            else fsm.onEvent(4);
        }
        else if (wasd[1]) fsm.onEvent(2);
        else if (wasd[3]) fsm.onEvent(6);
    }

    @Override
    public void cleanup() {
        glfwSetKeyCallback(Window.getHandle(), null);
    }
}
