package client.inputhandlers;

import common.fsm.FSM;
import scripting.GlobalEnvironment;

public class InputHandlers {
    public static boolean exists(String name) {
        switch (name) {
            default: return false;
            case "WASD": return true;
        }
    }

    public static InputHandler get(String name, FSM fsm, GlobalEnvironment env) {
        switch (name) {
            default: throw new IllegalArgumentException();
            case "WASD": return new WASD(env, fsm);
        }
    }
}
