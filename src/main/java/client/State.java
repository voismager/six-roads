package client;

import client.processor.Command;

public interface State {
    void handle(Command command);

    void enter();

    void leave();

    void update();

    void render();
}
