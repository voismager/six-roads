package client;

import common.server.properties.*;
import common.server.requests.*;
import common.server.responses.*;
import server.message.Header;
import server.property.CustomPropertyKey;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Settings {
    public static final String VERSION = "0.0.6";

    public static int WINDOW_WIDTH;
    public static int WINDOW_HEIGHT;
    public static boolean FULLSCREEN;
    public static boolean V_SYNC;
    public static float CAMERA_ANGLE;
    public static String SERVICE_SCHEME;
    public static String SERVICE_ADDRESS;
    public static int SERVICE_PORT;
    public static String SERVICE_TYPE;

    public static void init(InputStream stream) throws IOException {
        Properties properties = new Properties();
        properties.load(stream);

        WINDOW_WIDTH = Integer.parseInt(properties.getProperty("WINDOW_WIDTH"));
        WINDOW_HEIGHT = Integer.parseInt(properties.getProperty("WINDOW_HEIGHT"));
        FULLSCREEN = Boolean.parseBoolean(properties.getProperty("FULLSCREEN"));
        V_SYNC = Boolean.parseBoolean(properties.getProperty("V_SYNC"));
        CAMERA_ANGLE = Float.parseFloat(properties.getProperty("CAMERA_ANGLE"));
        SERVICE_SCHEME = properties.getProperty("SERVICE_SCHEME");
        SERVICE_ADDRESS = properties.getProperty("SERVICE_ADDRESS");
        SERVICE_PORT = Integer.parseInt(properties.getProperty("SERVICE_PORT"));
        SERVICE_TYPE = properties.getProperty("SERVICE_TYPE");

        init();
    }

    private static void init() {
        Header[] ini = new Header[] {
                GmInnerTransitionRequest.HEADER, GmNextTurnRequest.HEADER, GmOuterTransitionRequest.HEADER,
                StartGameRequest.HEADER, StopPlayingRequest.HEADER,
                GameEndedResponse.HEADER, GameStartedResponse.HEADER, GmKillPuppeteerResponse.HEADER,
                GmNextTurnResponse.HEADER, GmRunWatcherScriptResponse.HEADER
        };

        for (Header h : ini) {
            System.out.println(h + " is loaded.");
        }

        CustomPropertyKey[] propertyKeys = new CustomPropertyKey[] {
                MapProperty.KEY,
                MaxPlayersProperty.KEY, ModuleProperty.KEY,
                LoadedModulesProperty.KEY, VersionProperty.KEY
        };

        for (CustomPropertyKey p : propertyKeys) {
            server.property.Properties.registerProperty(p);
        }
    }
}
