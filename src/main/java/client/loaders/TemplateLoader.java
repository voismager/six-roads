package client.loaders;

import client.RenderableTemplate;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import common.loaders.AttributeLoader;
import it.unimi.dsi.fastutil.objects.ObjectArraySet;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;

public class TemplateLoader {
    public static Short2ObjectMap<RenderableTemplate> load(JsonArray jsonArray) {
        final Short2ObjectMap<RenderableTemplate> templates = new Short2ObjectOpenHashMap<>();

        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            final short id = jsonObject.get("id").getAsShort();
            final String name = jsonObject.get("name").getAsString();
            final JsonElement attributes = jsonObject.get("attributes");
            templates.put(id, new RenderableTemplate(
                    id, name, toStringSet(jsonObject.getAsJsonArray("labels")),
                    AttributeLoader.load(attributes.getAsJsonArray()))
            );
        }

        return templates;
    }

    private static ObjectSet<String> toStringSet(JsonArray jsonArray) {
        ObjectSet<String> result = new ObjectArraySet<>();
        for (JsonElement jsonElement : jsonArray)
            result.add(jsonElement.getAsString());
        return result;
    }
}
