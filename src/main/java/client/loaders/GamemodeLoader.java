package client.loaders;

import common.Gamemode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import common.fsm.ScriptStorage;
import it.unimi.dsi.fastutil.shorts.Short2ObjectArrayMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

public class GamemodeLoader {
    public static Short2ObjectMap<Gamemode> load(JsonArray jsonArray, ScriptStorage scripts) {
        Short2ObjectMap<Gamemode> rules = new Short2ObjectArrayMap<>();

        for (JsonElement jsonElement : jsonArray) {
            Gamemode o = loadRule(jsonElement.getAsJsonObject(), scripts);
            rules.put(o.key, o);
        }

        return rules;
    }

    private static Gamemode loadRule(JsonObject r, ScriptStorage scripts) {
        short key = r.getAsJsonPrimitive("id").getAsShort();
        JsonPrimitive rootScript = r.getAsJsonPrimitive("rootScript");
        return new Gamemode(key, scripts.getScript(rootScript.getAsString()));
    }
}
