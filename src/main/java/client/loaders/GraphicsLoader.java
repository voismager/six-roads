package client.loaders;

import client.model.Module;
import client.RenderableTemplate;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import engine.graphics.core.ModelCache;
import org.joml.Vector3f;

public class GraphicsLoader {
    public static void load(Module module, JsonObject jsonObject) {
        loadTemplatesMeshes(module, jsonObject.getAsJsonArray("templates"));
    }

    private static void loadTemplatesMeshes(Module module, JsonArray jsonArray) {
        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            RenderableTemplate template = module.nameToTemplate.get(jsonObject.get("name").getAsString());
            if (template != null) loadTemplateModel(template, jsonObject.get("model").getAsJsonObject());
        }
    }

    private static void loadTemplateModel(RenderableTemplate template, JsonObject jsonObject) {
        final String meshName = jsonObject.get("mesh").getAsString();

        if (meshName.equals("hexagon")) {
            template.model = ModelCache.loadHexagonModel(parseColor(jsonObject.getAsJsonArray("color")));
        }
    }

    private static Vector3f parseColor(JsonArray jsonArray) {
        float r = jsonArray.get(0).getAsFloat();
        float g = jsonArray.get(1).getAsFloat();
        float b = jsonArray.get(2).getAsFloat();
        return new Vector3f(r, g, b);
    }
}
