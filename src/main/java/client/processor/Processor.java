package client.processor;

import client.MainApplication;
import client.State;
import client.processor.commands.ActivateExecutor;
import client.processor.commands.DeactivateExecutor;
import client.processor.commands.Null;
import client.states.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static client.processor.CommandType.*;

public final class Processor implements CommandExecutor, Runnable {
    private static final Logger logger = LogManager.getLogger(Processor.class);

    private static final EnumMap<CommandType, CommandExecutor> EVENT_HANDLER_MAP = new EnumMap<>(CommandType.class);
    private static final BlockingQueue<Command> COMMANDS = new LinkedBlockingQueue<>();
    private static final Processor INSTANCE = new Processor();

    private boolean active;

    public static void launch(State state) {
        checkEvents(INSTANCE);
        INSTANCE.start();

        checkEvents(MainApplication.INSTANCE);
        MainApplication.INSTANCE.start(state);
    }

    public static void fire(Command command) {
        COMMANDS.offer(command);
    }

    public static void fireAndWait(Command command) {
        COMMANDS.offer(command);
        try {
            command.await();
        } catch (InterruptedException e) { command.markAsFailed(); }
    }

    private static void checkEvents(CommandExecutor commandExecutor) {
        for (CommandType t : commandExecutor.getAllowedCommands()) {
            EVENT_HANDLER_MAP.putIfAbsent(t, commandExecutor);
        }
    }

    @Override
    public void run() {
        try {
            while (active) {
                Command command = COMMANDS.take();
                CommandExecutor executor = EVENT_HANDLER_MAP.get(command.type());

                if (executor != null && executor.isActive()) {
                    executor.offer(command);
                }
            }

        } catch (InterruptedException e) {
            shutdown();

        } catch (RuntimeException e) {
            logger.warn("Command execution exception", e);
        }
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void start(Object... args) {
        this.active = true;
        new Thread(this,"processor").start();
    }

    @Override
    public void stop() {
        this.active = false;
        fire(new Null());
    }

    @Override
    public void offer(Command command) {
        switch (command.type()) {
            case ACTIVATE_EXECUTOR:
                ActivateExecutor e0 = (ActivateExecutor) command;
                if (!e0.executor.isActive()) {
                    try {
                        checkEvents(e0.executor);
                        e0.executor.start(e0.args);
                        command.markAsSucceed();
                    } catch (RuntimeException e) {
                        command.markAsFailed();
                        logger.error("Activation failed", e);
                    }
                } else {
                    command.markAsFailed();
                    logger.error("Activation failed: already running");
                }
                return;

            case DEACTIVATE_EXECUTOR:
                DeactivateExecutor e1 = (DeactivateExecutor) command;
                if (e1.executor.isActive()) {
                    try {
                        e1.executor.stop();
                        command.markAsSucceed();
                    } catch (RuntimeException e) {
                        command.markAsFailed();
                    }
                } else {
                    command.markAsFailed();
                }
                return;

            case SHUTDOWN:
                shutdown();
                return;
        }
    }

    @Override
    public EnumSet<CommandType> getAllowedCommands() {
        return EnumSet.of(SHUTDOWN, ACTIVATE_EXECUTOR, DEACTIVATE_EXECUTOR);
    }

    private void shutdown() {
        EVENT_HANDLER_MAP.values().stream()
                .distinct()
                .filter(CommandExecutor::isActive)
                .forEach(CommandExecutor::stop);
    }
}
