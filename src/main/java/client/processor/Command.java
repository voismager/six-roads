package client.processor;

public abstract class Command {
    private boolean succeed, done;
    private boolean blocked;
    private Throwable cause;

    public abstract CommandType type();

    public synchronized void await() throws InterruptedException {
        blocked = true;
        while (!isDone()) wait();
    }

    public void markAsFailed() {
        succeed = false;
        done = true;

        if (blocked) {
            synchronized (this) {
                notify();
            }
        }
    }

    public void markAsFailed(Throwable cause) {
        this.markAsFailed();
        this.cause = cause;
    }

    public void markAsSucceed() {
        succeed = true;
        done = true;

        if (blocked) {
            synchronized (this) {
                notify();
            }
        }
    }

    public Throwable getCause() {
        return cause;
    }

    public final boolean isDone() {
        return done;
    }

    public final boolean isSucceed() {
        return succeed;
    }
}
