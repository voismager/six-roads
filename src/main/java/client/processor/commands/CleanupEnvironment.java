package client.processor.commands;

import client.logic.ClEnvironment;
import client.processor.CommandType;
import client.processor.Command;

public class CleanupEnvironment extends Command {
    public ClEnvironment environment;

    public CleanupEnvironment(ClEnvironment environment) {
        this.environment = environment;
    }

    @Override
    public CommandType type() {
        return CommandType.CLEANUP_ENVIRONMENT;
    }
}
