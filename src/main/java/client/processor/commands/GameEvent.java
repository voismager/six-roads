package client.processor.commands;

import client.processor.Command;
import client.processor.CommandType;
import server.message.Message;

public class GameEvent extends Command {
    public Message response;

    public GameEvent(Message response) {
        this.response = response;
    }

    @Override
    public CommandType type() {
        return CommandType.GAME_EVENT;
    }
}
