package client.processor.commands;

import client.processor.CommandExecutor;
import client.processor.CommandType;
import client.processor.Command;

public class DeactivateExecutor extends Command {
    public CommandExecutor executor;

    public DeactivateExecutor(CommandExecutor executor) {
        this.executor = executor;
    }

    @Override
    public CommandType type() {
        return CommandType.DEACTIVATE_EXECUTOR;
    }
}
