package client.processor.commands;

import client.processor.CommandExecutor;
import client.processor.CommandType;
import client.processor.Command;

public class ActivateExecutor extends Command {
    public CommandExecutor executor;
    public Object[] args;

    public ActivateExecutor(CommandExecutor executor, Object... args) {
        this.executor = executor;
        this.args = args;
    }

    @Override
    public CommandType type() {
        return CommandType.ACTIVATE_EXECUTOR;
    }
}
