package client.processor.commands;

import client.processor.CommandType;
import client.processor.Command;

public class Shutdown extends Command {
    @Override
    public CommandType type() {
        return CommandType.SHUTDOWN;
    }
}
