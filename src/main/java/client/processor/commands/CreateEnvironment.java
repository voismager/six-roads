package client.processor.commands;

import client.logic.ClEnvironment;
import client.processor.Command;
import client.processor.CommandType;
import client.CreateEnvironmentPack;

public class CreateEnvironment extends Command {
    public CreateEnvironmentPack pack;
    public ClEnvironment environment;

    public CreateEnvironment(CreateEnvironmentPack pack) {
        this.pack = pack;
    }

    @Override
    public CommandType type() {
        return CommandType.CREATE_ENVIRONMENT;
    }
}
