package client.processor.commands;

import client.State;
import client.processor.CommandType;
import client.processor.Command;

public class ChangeState extends Command {
    public State state;

    public ChangeState(State state) {
        this.state = state;
    }

    @Override
    public CommandType type() {
        return CommandType.CHANGE_STATE;
    }
}
