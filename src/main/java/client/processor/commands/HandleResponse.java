package client.processor.commands;

import client.processor.Command;
import client.processor.CommandType;
import server.message.Message;

public class HandleResponse extends Command {
    public Message response;

    public HandleResponse(Message response) {
        this.response = response;
    }

    @Override
    public CommandType type() {
        return CommandType.HANDLE_RESPONSE;
    }
}
