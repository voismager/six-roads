package client.processor.commands;

import client.processor.Command;
import client.processor.CommandType;
import server.message.Message;

public class SendRequest extends Command {
    public Message request;

    public SendRequest(Message request) {
        this.request = request;
    }

    @Override
    public CommandType type() {
        return CommandType.SEND_REQUEST;
    }
}
