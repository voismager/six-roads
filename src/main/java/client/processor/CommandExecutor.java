package client.processor;

import java.util.EnumSet;

public interface CommandExecutor {
    boolean isActive();

    void start(Object... args);

    void stop();

    void offer(Command command);

    EnumSet<CommandType> getAllowedCommands();
}
