package client.render;

import engine.gameobjects.GameObject;
import engine.graphics.renderer.Shader;
import engine.structures.ObjOpenArray;
import engine.support.loaders.FilesLoader;

import java.util.HashMap;
import java.util.Map;

import static engine.graphics.transformations.Transformations.getPerspectiveProjectionMatrix;
import static engine.graphics.transformations.Transformations.getViewMatrix;

public class RendererMiscellaneous {
    private final static Shader SHADER;

    static {
        Shader shader;
        try {
            shader = createShader();
        } catch (Exception e) {
            shader = null;
        }

        SHADER = shader;
    }

    private static Shader createShader() throws Exception {
        final Shader s = new Shader();
        s.createVertexShader(FilesLoader.readResource("/shaders/miscellaneous_vert.glsl"));
        s.createFragmentShader(FilesLoader.readResource("/shaders/miscellaneous_frag.glsl"));
        s.link();

        s.createUniform("u_instanced");
        s.createUniform("u_modelMatrix");
        s.createUniform("u_viewMatrix");
        s.createUniform("u_projectionMatrix");
        s.createUniform("u_texture");
        s.createUniform("u_textured");
        s.createUniform("u_color");

        s.bind();
        s.setUniform("u_texture", 0);

        return s;
    }

    private static final Map<Class, ObjOpenArray<GameObject>> MISCELLANEOUS = new HashMap<>();

    public static void remove(GameObject gameObject) {
        MISCELLANEOUS.get(gameObject.getClass()).remove(gameObject);
    }

    public static void clear(Class clazz) {
        ObjOpenArray<GameObject> buffer = MISCELLANEOUS.get(clazz);
        if (buffer != null) {
            buffer.clear();
        }
    }

    public static void add(GameObject gameObject) {
        ObjOpenArray<GameObject> buffer = MISCELLANEOUS.get(gameObject.getClass());
        if (buffer != null) {
            buffer.add(gameObject);
        } else {
            buffer = new ObjOpenArray<>();
            buffer.addUnchecked(gameObject);
            MISCELLANEOUS.put(gameObject.getClass(), buffer);
        }
    }

    public static void render() {
        SHADER.bind();
        SHADER.setUniform("u_projectionMatrix", getPerspectiveProjectionMatrix());
        SHADER.setUniform("u_viewMatrix", getViewMatrix());

        //for (Map.Entry<Class, ObjOpenArray<GameObject>> e : MISCELLANEOUS.entrySet()) {
        //    final ObjOpenArray<GameObject> objects = e.getValue();
        //    if (!objects.isEmpty()) {
        //        objects.get(0).initRender(SHADER);
        //        for (int i = 0; i < objects.size; i++)
        //            objects.get(i).render(SHADER);
        //        objects.get(0).endRender(SHADER);
        //    }
        //}

        SHADER.unbind();
    }
}
