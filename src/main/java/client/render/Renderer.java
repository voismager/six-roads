package client.render;

import engine.Window;
import engine.graphics.lights.DirectionalLight;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.*;

public final class Renderer {

    public static void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    public static void setup() throws Exception {
        RendererHexagon.setup();
        RendererSprite.setup();
        RendererShadow.setup();
    }

    public static void render(Vector3f ambientLight, DirectionalLight light) {
        clear();
        RendererShadow.renderDepthMap(light);
        glViewport(0, 0, Window.WIDTH, Window.HEIGHT);
        RendererHexagon.render(ambientLight, light);
        RendererSprite.render();
    }

    private Renderer() {
    }
}
