package client.render;

import engine.Window;
import engine.graphics.transformations.Camera;
import engine.graphics.transformations.PerspectiveCamera;

public class CameraHolder {
    public static final Camera CAMERA = new PerspectiveCamera((float) Math.toRadians(55.0f), Window.WIDTH, Window.HEIGHT);
}
