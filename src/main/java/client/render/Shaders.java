package client.render;

import engine.graphics.transformations.Transformations;
import engine.graphics.renderer.Shader;
import engine.support.loaders.FilesLoader;

public class Shaders {
    public static Shader BILLBOARD;

    public static void init() throws Exception {
        BILLBOARD = getBillboard();

        Transformations.Camera.setPosition(0, 0, 30);
        Transformations.Camera.setRotation(-45, 0, -45);

        Transformations.updatePerspectiveProjectionMatrix();
    }

    private static Shader getBillboard() throws Exception {
        Shader s = createShader("billboard_vert.glsl", "billboard_frag.glsl");

        s.createUniform("u_projectionMatrix");
        s.createUniform("u_viewMatrix");
        s.createUniform("u_cameraRight");
        s.createUniform("u_cameraUp");
        s.createUniform("u_center");
        s.createUniform("u_size");
        s.createUniform("u_filter");
        s.createUniform("u_texture");
        s.createUniform("u_textured");

        s.bind();
        s.setUniform("u_texture", 0);

        return s;
    }

    public static Shader createShader(String vert, String frag) throws Exception {
        Shader s = new Shader();
        s.createVertexShader(FilesLoader.readResource("/shaders/" + vert));
        s.createFragmentShader(FilesLoader.readResource("/shaders/" + frag));
        s.link();
        return s;
    }
}
