package client.render;

import client.gameobjects.Sprite;
import engine.graphics.renderer.Shader;
import engine.structures.OpenDrawBuffer;

import static engine.graphics.transformations.Transformations.*;

public class RendererSprite {
    private final static OpenDrawBuffer<Sprite> BUFFER = new OpenDrawBuffer<>(Sprite.class);
    private static Shader shader;

    public static OpenDrawBuffer<Sprite> clearAndGetBuffer() {
        BUFFER.clear();
        return BUFFER;
    }

    public static void render() {
        shader.bind();
        shader.setUniform("u_projectionMatrix", getPerspectiveProjectionMatrix());
        shader.setUniform("u_viewMatrix", getViewMatrix());

        for (int i = 0; i < BUFFER.size; i++) {
            final Sprite s = BUFFER.array[i];
            if (s.model == null) continue;
            s.model.render(shader, getModelMatrix(s.x, s.y, s.z, s.rX, s.rY, s.rZ));
        }

        shader.unbind();
    }

    public static void setup() throws Exception {
        shader = Shaders.createShader("sprite_vert.glsl", "sprite_frag.glsl");
        shader.createUniform("u_modelMatrix");
        shader.createUniform("u_viewMatrix");
        shader.createUniform("u_projectionMatrix");
        shader.createMaterialUniform("u_material");
    }
}
