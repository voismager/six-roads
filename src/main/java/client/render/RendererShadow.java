package client.render;

import engine.Window;
import engine.graphics.lights.DirectionalLight;
import engine.graphics.renderer.Shader;
import engine.graphics.shadows.ShadowBuffer;
import engine.graphics.shadows.ShadowCascade;
import org.joml.Matrix4f;

import static engine.graphics.transformations.Transformations.getViewMatrix;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

public class RendererShadow {
    public static ShadowBuffer SHADOW_BUFFER;
    public static final float[] CASCADE_SPLITS = new float[] {
            Window.z_FAR * 0.2f, Window.z_FAR
    };
    public static final int NUM_CASCADES = CASCADE_SPLITS.length;
    public static final ShadowCascade[] CASCADES = new ShadowCascade[NUM_CASCADES];
    private static Shader DEPTH_SHADER;

    public static void setup() throws Exception {
        Shader s = Shaders.createShader("depth_vert.glsl", "depth_frag.glsl");
        s.createUniform("u_instanced");
        s.createUniform("u_orthoProjectionMatrix");
        s.createUniform("u_lightViewMatrix");
        s.createUniform("u_modelMatrix");

        DEPTH_SHADER = s;
        SHADOW_BUFFER = new ShadowBuffer(NUM_CASCADES);

        float zNear = Window.z_NEAR;
        for (int i = 0; i < NUM_CASCADES; i++) {
            CASCADES[i] = new ShadowCascade(zNear, CASCADE_SPLITS[i]);
            zNear = CASCADE_SPLITS[i];
        }
    }

    private static void update(Matrix4f viewMatrix, DirectionalLight light) {
        for (int i = 0; i < NUM_CASCADES; i++) {
            ShadowCascade shadowCascade = CASCADES[i];
            shadowCascade.update(viewMatrix, light);
        }
    }

    public static void renderDepthMap(DirectionalLight light) {
        update(getViewMatrix(), light);

        glBindFramebuffer(GL_FRAMEBUFFER, SHADOW_BUFFER.getDepthMapFBO());
        glViewport(0, 0, ShadowBuffer.LINEAR_LENGTH, ShadowBuffer.LINEAR_LENGTH);
        glClear(GL_DEPTH_BUFFER_BIT);

        //glDisable(GL_CULL_FACE);

        DEPTH_SHADER.bind();

        DEPTH_SHADER.setUniform("u_instanced", 1);

        for (int i = 0; i < NUM_CASCADES; i++) {
            ShadowCascade cascade = CASCADES[i];

            DEPTH_SHADER.setUniform("u_orthoProjectionMatrix", cascade.getOrthoProjMatrix());
            DEPTH_SHADER.setUniform("u_lightViewMatrix", cascade.getLightViewMatrix());

            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, SHADOW_BUFFER.getDepthMapTexture().getIds()[i], 0);
            glClear(GL_DEPTH_BUFFER_BIT);

            //Render as usual
        }

        DEPTH_SHADER.unbind();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        //glEnable(GL_CULL_FACE);
    }

    public static void cleanup() {
        if (SHADOW_BUFFER != null) {
            SHADOW_BUFFER.cleanup();
            SHADOW_BUFFER = null;
        }
        if (DEPTH_SHADER != null) {
            DEPTH_SHADER.cleanup();
            DEPTH_SHADER = null;
        }
    }
}
