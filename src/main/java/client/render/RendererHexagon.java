package client.render;

import client.gameobjects.RenderableHexagon;
import engine.graphics.lights.DirectionalLight;
import engine.graphics.renderer.Shader;
import engine.graphics.shadows.ShadowCascade;
import engine.structures.OpenDrawBuffer;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import static engine.graphics.transformations.Transformations.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE1;

public class RendererHexagon {
    private final static DirectionalLight DIRECTIONAL_LIGHT = new DirectionalLight();
    private final static Vector4f VECTOR_4_F = new Vector4f();
    private final static OpenDrawBuffer<RenderableHexagon> BUFFER = new OpenDrawBuffer<>(RenderableHexagon.class);
    private static Shader shader;

    public static OpenDrawBuffer<RenderableHexagon> clearAndGetBuffer() {
        BUFFER.clear();
        return BUFFER;
    }

    public static void render(Vector3f ambientLight, DirectionalLight light) {
        shader.bind();
        shader.setUniform("u_projectionMatrix", getPerspectiveProjectionMatrix());
        shader.setUniform("u_viewMatrix", getViewMatrix());
        for (int i = 0; i < RendererShadow.NUM_CASCADES; i++) {
            ShadowCascade cascade = RendererShadow.CASCADES[i];
            shader.setUniform("u_orthoProjectionMatrix", cascade.getOrthoProjMatrix(), i);
            shader.setUniform("u_lightViewMatrix", cascade.getLightViewMatrix(), i);
            shader.setUniform("u_cascadeFarPlanes", RendererShadow.CASCADE_SPLITS[i], i);
        }

        renderDirectionalLight(ambientLight, light, getViewMatrix());

        shader.setUniform("u_instanced", 0);

        for (int i = 0; i < BUFFER.size; i++) {
            final RenderableHexagon hexagon = BUFFER.array[i];
            if (hexagon.model == null) continue;
            RendererShadow.SHADOW_BUFFER.bindTextures(GL_TEXTURE1);
            hexagon.model.render(shader, getModelMatrix(hexagon));
        }

        shader.unbind();
    }

    private static void renderDirectionalLight(Vector3f ambientLight, DirectionalLight light, Matrix4f viewMatrix) {
        shader.setUniform("u_ambientLight", ambientLight);
        shader.setUniform("u_specularPower", 1f);
        DIRECTIONAL_LIGHT.set(light);
        VECTOR_4_F.set(light.getDirection(), 0);
        VECTOR_4_F.mul(viewMatrix);
        DIRECTIONAL_LIGHT.setDirection(VECTOR_4_F.x, VECTOR_4_F.y, VECTOR_4_F.z);
        shader.setUniform("u_directionalLight", DIRECTIONAL_LIGHT);
    }

    public static void setup() throws Exception {
        shader = Shaders.createShader("scene_vert.glsl", "scene_frag.glsl");
        shader.createUniform("u_instanced");
        shader.createUniform("u_modelMatrix");
        shader.createUniform("u_viewMatrix");
        shader.createUniform("u_projectionMatrix");
        shader.createUniform("u_lightViewMatrix", RendererShadow.NUM_CASCADES);
        shader.createUniform("u_orthoProjectionMatrix", RendererShadow.NUM_CASCADES);
        shader.createUniform("u_texture");
        for (int i = 0; i < RendererShadow.NUM_CASCADES; i++)
            shader.createUniform("u_shadowMap_" + i);
        shader.createUniform("u_specularPower");
        shader.createMaterialUniform("u_material");
        shader.createDirectionalLightUniform("u_directionalLight");
        shader.createUniform("u_ambientLight");
        shader.createUniform("u_cascadeFarPlanes", RendererShadow.NUM_CASCADES);
        shader.bind();
        shader.setUniform("u_texture", 0);
        for (int i = 0; i < RendererShadow.NUM_CASCADES; i++)
            shader.setUniform("u_shadowMap_" + i, i + 1);
    }
}
