package client;

import client.processor.Command;
import client.processor.CommandExecutor;
import client.processor.CommandType;
import client.processor.commands.ChangeState;
import client.processor.commands.CleanupEnvironment;
import client.processor.commands.CreateEnvironment;
import client.render.Renderer;
import client.render.RendererHexagon;
import client.render.Shaders;
import client.render.RendererShadow;
import client.service.EnvironmentBuilder;
import engine.MouseInput;
import engine.Window;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumSet;
import java.util.LinkedList;
import java.util.Queue;

import static client.processor.CommandType.*;

public final class MainApplication implements CommandExecutor, Runnable {
    public static final MainApplication INSTANCE = new MainApplication();

    private static final Logger LOGGER = LogManager.getLogger(MainApplication.class);

    private final Queue<Command> commands;
    private boolean changed;
    private boolean active;
    private State state;

    private MainApplication() {
        this.commands = new LinkedList<>();
        this.changed = false;
        this.active = false;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void start(Object... args) {
        this.active = true;
        this.state = (State) args[0];
        run();
    }

    @Override
    public void run() {
        try {
            Window.init();
            Shaders.init();
            Renderer.setup();
        } catch (Exception e) {
            LOGGER.error(e);
            return;
        }

        while (active) {
            try {
                MouseInput.reset();
                State state = this.state;
                state.enter();

                while (!changed) {
                    Window.update();
                    state.update();
                    handleCommands();
                    state.render();
                }

                changed = false;
                state.leave();

            } catch (RuntimeException e) {
                LOGGER.error(e.getStackTrace(), e);
            }
        }
    }

    @Override
    public void offer(Command command) {
        this.commands.add(command);
    }

    @Override
    public EnumSet<CommandType> getAllowedCommands() {
        return EnumSet.of(CHANGE_STATE, CREATE_ENVIRONMENT, CLEANUP_ENVIRONMENT, GAME_EVENT);
    }

    @Override
    public void stop() {
        this.changeState(null);
        this.active = false;
    }

    private void handleCommands() {
        while (!commands.isEmpty()) {
            Command command = commands.poll();

            switch (command.type()) {
                case GAME_EVENT:
                    state.handle(command);
                    break;

                case CHANGE_STATE:
                    this.changeState(((ChangeState) command).state);
                    command.markAsSucceed();
                    break;

                case CREATE_ENVIRONMENT:
                    try {
                        CreateEnvironment c = (CreateEnvironment) command;

                        c.environment = EnvironmentBuilder.build(
                                c.pack.module,
                                c.pack.map,
                                c.pack.me,
                                c.pack.tools
                        );

                        command.markAsSucceed();

                    } catch (Exception e) {
                        command.markAsFailed(e);
                    }

                    break;

                case CLEANUP_ENVIRONMENT:
                    try {
                        ((CleanupEnvironment) command).environment.cleanup();
                        command.markAsSucceed();

                    } catch (RuntimeException e) {
                        command.markAsFailed();
                    }

                    break;
            }
        }
    }

    private void changeState(State state) {
        if (this.state != state) {
            this.commands.clear();
            this.changed = true;
            this.state = state;
        }
    }
}