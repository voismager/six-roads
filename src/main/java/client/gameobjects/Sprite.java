package client.gameobjects;

import engine.gameobjects.GameObject;
import engine.graphics.core.Model;
import engine.graphics.core.ModelCache;

public class Sprite extends GameObject {
    public float rX, rY, rZ;
    public final Model model;
    public final float width;
    public final float height;

    public Sprite(float width, float height, int texture) {
        this.model = ModelCache.loadQuadModel(width, height, texture);
        this.width = width;
        this.height = height;
    }

    public void rotate(float rX, float rY, float rZ) {
        this.rX += rX;
        this.rY += rY;
        this.rZ += rZ;
    }

    public void move(float x, float y, float z) {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    @Override
    public void cleanup() {

    }
}
