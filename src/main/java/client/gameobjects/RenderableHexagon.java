package client.gameobjects;

import common.Template;
import common.gameobjects.Hexagon;
import engine.graphics.core.Model;

public class RenderableHexagon extends Hexagon {
    public Model model;

    public RenderableHexagon(Template entity) {
        super(entity);
    }
}
