package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.RoomDestroyedResponse;

public class OnRoomDestroyed extends Handler {
    public OnRoomDestroyed(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.ROOM_DESTROYED_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        session.destroyRoom(((RoomDestroyedResponse) response).roomId);
    }
}
