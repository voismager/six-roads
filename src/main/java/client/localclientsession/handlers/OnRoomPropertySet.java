package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.RoomPropertySetResponse;

public class OnRoomPropertySet extends Handler {
    public OnRoomPropertySet(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.ROOM_PROPERTY_SET_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        RoomPropertySetResponse message = (RoomPropertySetResponse) response;
        session.setRoomProperty(message.room, message.property, message.value);
    }
}
