package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.PlayerJoinedResponse;

public class OnPlayerJoined extends Handler {
    public OnPlayerJoined(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.PLAYER_JOINED_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        PlayerJoinedResponse message = (PlayerJoinedResponse) response;
        session.addMember(message.room, message.player);
    }
}
