package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.PlayerPropertySetResponse;

public class OnPlayerPropertySet extends Handler {
    public OnPlayerPropertySet(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.PLAYER_PROPERTY_SET_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        PlayerPropertySetResponse message = (PlayerPropertySetResponse) response;
        session.setPlayerProp(message.player, message.property, message.value);
    }
}
