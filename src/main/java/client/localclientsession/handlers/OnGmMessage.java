package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import client.processor.Processor;
import client.processor.commands.GameEvent;
import common.server.responses.GmKillPuppeteerResponse;
import common.server.responses.GmNextTurnResponse;
import common.server.responses.GmResponseResponse;
import common.server.responses.GmRunWatcherScriptResponse;
import server.message.Header;
import server.message.Message;

public class OnGmMessage extends Handler {
    public OnGmMessage(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] {
                GmNextTurnResponse.HEADER,
                GmKillPuppeteerResponse.HEADER,
                GmResponseResponse.HEADER,
                GmRunWatcherScriptResponse.HEADER
        };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        Processor.fire(new GameEvent(response));
    }
}
