package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.message.Header;
import server.message.Message;

import java.util.function.BiConsumer;

public abstract class Handler implements BiConsumer<LocalClientSession, Message> {
    public Handler(LocalClientSession session) {
        for (Header h : headers()) {
            session.bind(h, this);
        }
    }

    public abstract Header[] headers();
}
