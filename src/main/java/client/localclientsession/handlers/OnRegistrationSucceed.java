package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import client.processor.Processor;
import client.processor.commands.ChangeState;
import client.states.Lobby;
import server.payload.FullSessionData;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.RegistrationSucceedResponse;

public class OnRegistrationSucceed extends Handler {
    public OnRegistrationSucceed(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.REGISTRATION_SUCCEED_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        FullSessionData data = ((RegistrationSucceedResponse) response).data;
        session.applyChanges(data);
    }
}
