package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.PlayerLeftResponse;

public class OnPlayerLeft extends Handler {
    public OnPlayerLeft(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.PLAYER_LEFT_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        PlayerLeftResponse message = (PlayerLeftResponse) response;
        session.removeMember(message.room, message.player);
    }
}
