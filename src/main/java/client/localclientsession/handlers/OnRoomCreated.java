package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.RoomCreatedResponse;

public class OnRoomCreated extends Handler {
    public OnRoomCreated(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.ROOM_CREATED_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        session.addRoom(((RoomCreatedResponse) response).roomId);
    }
}
