package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import client.localclientsession.Room;
import client.model.Module;
import common.MapHeader;
import client.service.ModuleRepository;
import common.server.responses.GameStartedResponse;
import common.server.properties.MapProperty;
import common.server.properties.ModuleProperty;
import engine.network.api.APIHolder;
import server.Player;
import server.message.Header;
import server.message.Message;

import java.util.Arrays;
import java.util.UUID;

public class OnGameStarted extends Handler {
    public OnGameStarted(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { GameStartedResponse.HEADER };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        GameStartedResponse message = (GameStartedResponse) response;
        Room room = session.getRoom(message.room);
        room.getMembersStream().forEach(room::addSubscriber);

        Room myRoom = session.getRoom(APIHolder.getInstance().getPlayer().id);
        
        if (myRoom != null && myRoom.getId() == room.getId()) {
            Module module = ModuleRepository.getCopy((UUID) room.getProperty(ModuleProperty.KEY));

            MapHeader map = module.maps.get((short) room.getProperty(MapProperty.KEY));

            Player[] players = Arrays.stream(message.order)
                    .mapToObj(session::getPlayer)
                    .toArray(Player[]::new);

            Player me = APIHolder.getInstance().getPlayer();

            long seed = message.seed;

            new Thread(() -> session.getGameSession().start(room, module, map, players, me, seed)).start();
        }
    }
}