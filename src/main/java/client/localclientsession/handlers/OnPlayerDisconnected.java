package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import engine.network.api.APIHolder;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.PlayerDisconnectedResponse;

public class OnPlayerDisconnected extends Handler {
    public OnPlayerDisconnected(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.PLAYER_DISCONNECTED_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession clientSession, Message response) {
        PlayerDisconnectedResponse message = (PlayerDisconnectedResponse) response;

        if (message.player == APIHolder.getInstance().getPlayer().id) {
            clientSession.disconnect();
        } else clientSession.unregisterPlayer(message.player);
    }
}
