package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import client.localclientsession.Room;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.PlayerUnsubscribedResponse;

public class OnPlayerUnsubscribed extends Handler {
    public OnPlayerUnsubscribed(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.PLAYER_UNSUBSCRIBED_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        PlayerUnsubscribedResponse message = (PlayerUnsubscribedResponse) response;
        Room room = session.getRoom(message.gameRoom);
        room.removeSubscriber(message.player);
    }
}
