package client.localclientsession.handlers;

import client.localclientsession.LocalClientSession;
import server.Player;
import server.message.Header;
import server.message.Headers;
import server.message.Message;
import server.message.PlayerConnectedResponse;

public class OnPlayerConnected extends Handler {
    public OnPlayerConnected(LocalClientSession session) {
        super(session);
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.PLAYER_CONNECTED_RESPONSE };
    }

    @Override
    public void accept(LocalClientSession session, Message response) {
        Player player = (((PlayerConnectedResponse)response).data).toPlayer();
        session.registerPlayer(player);
    }
}
