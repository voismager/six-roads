package client.localclientsession;

import client.CreateEnvironmentPack;
import client.logic.ClEnvironment;
import client.logic.GameTools;
import client.model.Module;
import client.processor.Processor;
import client.processor.commands.ChangeState;
import client.processor.commands.CleanupEnvironment;
import client.processor.commands.CreateEnvironment;
import client.processor.commands.SendRequest;
import client.states.Game;
import client.states.Lobby;
import common.MapHeader;
import common.server.requests.StopPlayingRequest;
import common.server.responses.GmRunWatcherScriptResponse;
import engine.support.RandomUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.Player;
import server.message.Header;
import server.message.Message;

import java.util.Arrays;
import java.util.Random;

public class GameSession {
    public static final GameSession INSTANCE = new GameSession();

    private static final Logger logger = LogManager.getLogger(GameSession.class);

    private final Random dice;

    private ClEnvironment environment;

    private GameSession() {
        this.dice = new Random(RandomUtil.globalNextLong());
    }

    public void start(Room room, Module module, MapHeader map, Player[] players, Player me, long seed) {
        int playerIndex = getPlayerIndex(players, me);

        dice.setSeed(seed);

        CreateEnvironmentPack e = new CreateEnvironmentPack(module, map, playerIndex, new GameTools() {
            @Override
            public boolean isSubscribed() {
                return room.hasSubscriber(me.id);
            }

            @Override
            public void stopPlaying() {
                if (isSubscribed()) Processor.fire(new SendRequest(new StopPlayingRequest()));

                Processor.fire(new ChangeState(Lobby.INSTANCE));
                Processor.fire(new CleanupEnvironment(environment));
            }

            @Override
            public Player getPlayer(int puppeteer) {
                return players[puppeteer];
            }

            @Override
            public boolean rollDice(int probability) {
                return RandomUtil.rollDice(probability, dice);
            }

            @Override
            public void unsubscribe() {
                if (isSubscribed()) Processor.fire(new SendRequest(new StopPlayingRequest()));
            }
        });

        CreateEnvironment command = new CreateEnvironment(e);
        Processor.fireAndWait(command);

        if (command.isSucceed()) {
            this.environment = command.environment;
            Processor.fire(new ChangeState(Game.INSTANCE));

            logger.info(System.lineSeparator());
            logger.info("Game Started!");
            logger.info("{}", module.meta.name);
            logger.info("{}", module.meta.id);
            logger.info(Arrays.toString(players));

        } else {
            Processor.fire(new SendRequest(new StopPlayingRequest()));
            logger.error("Couldn't create environment", command.getCause());
        }

    }

    public void processGameEvent(Message response) {
        final Header header = response.getHeader();

        if (header == GmRunWatcherScriptResponse.HEADER) {
            GmRunWatcherScriptResponse msg0 = (GmRunWatcherScriptResponse) response;
            //environment.onWatcherEvent(msg0.id, msg0.stage, msg0.puppeteer, msg0.args);
        }
    }

    public ClEnvironment getEnv() {
        return environment;
    }

    private int getPlayerIndex(Player[] players, Player player) {
        for (int i = 0; i < players.length; i++) {
            if (player.id == players[i].id) return i;
        }

        throw new IllegalArgumentException();
    }
}
