package client.localclientsession;

import client.processor.Command;
import client.processor.CommandExecutor;
import client.processor.CommandType;
import common.server.properties.LoadedModulesProperty;
import common.server.EarlyGameSession;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.UUID;

public class LocalHostSession implements CommandExecutor {
    public static final LocalHostSession INSTANCE = new LocalHostSession();

    private EarlyGameSession session;
    private InetSocketAddress address;
    private boolean active;

    public InetSocketAddress getAddress() {
        return address;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void start(Object... args) {
        try {
            if (session == null) {
                if (args.length == 2) session = (EarlyGameSession) args[1];
                else {
                    session = new EarlyGameSession();
                    session.setProperty(LoadedModulesProperty.KEY, new ArrayList<>());
                    session.addModule(UUID.fromString("0f84e2ee-e18a-422a-bd9f-4dec55454af2"));
                }
            }

            this.address = (InetSocketAddress) args[0];
            this.session.start(address);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        this.active = true;
    }

    @Override
    public void stop() {
        session.stop();
        this.active = false;
    }

    @Override
    public void offer(Command command) { }

    @Override
    public EnumSet<CommandType> getAllowedCommands() {
        return EnumSet.noneOf(CommandType.class);
    }
}
