package client.localclientsession;

import client.localclientsession.handlers.*;
import client.processor.Command;
import client.processor.CommandExecutor;
import client.processor.CommandType;
import client.processor.Processor;
import client.processor.commands.ChangeState;
import client.processor.commands.HandleResponse;
import client.processor.commands.SendRequest;
import client.service.ModuleRepository;
import client.states.Menu;
import common.server.properties.LoadedModulesProperty;
import engine.network.TCPClient;
import engine.network.api.APIHolder;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.ReferenceCountUtil;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.ObjPair;
import server.Player;
import server.message.*;
import server.payload.FullSessionData;
import server.payload.PlayerData;
import server.property.PropertyKey;

import java.net.InetSocketAddress;
import java.util.*;
import java.util.stream.Stream;

import static client.processor.CommandType.HANDLE_RESPONSE;
import static client.processor.CommandType.SEND_REQUEST;

public class LocalClientSession implements CommandExecutor {
    public final static LocalClientSession INSTANCE = new LocalClientSession();

    private final static Logger logger = LogManager.getLogger(LocalClientSession.class);

    private final Map<Header, Handler> handlers;
    private final Long2ObjectMap<Player> registered;
    private final Int2ObjectMap<Room> rooms;
    private final List<UUID> sessionModules;
    private final GameSession gameSession;
    private final TCPClient client;

    private String type;
    private InetSocketAddress address;
    private boolean active;

    private LocalClientSession() {
        this.handlers = new HashMap<>();
        this.registered = new Long2ObjectOpenHashMap<>();
        this.rooms = new Int2ObjectOpenHashMap<>();
        this.sessionModules = new ArrayList<>();
        this.gameSession = GameSession.INSTANCE;
        this.client = new TCPClient(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel ch) {
                ch.pipeline()
                        .addLast(new IdleStateHandler(17, 7, 0))
                        .addLast(new LengthFieldBasedFrameDecoder(Short.MAX_VALUE, 0, 2, 0,2))
                        .addLast(new LengthFieldPrepender(2))
                        .addLast(new RequestEncoder())
                        .addLast(new ChannelInboundHandlerAdapter() {
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) {
                                final ByteBuf buf = (ByteBuf) msg;

                                Message response = Headers.decode(buf);

                                if (response.getHeader() != Headers.PING) {
                                    Processor.fire(new HandleResponse(response));
                                }

                                ReferenceCountUtil.release(buf);
                            }

                            @Override
                            public void channelInactive(ChannelHandlerContext ctx) {
                                Processor.fire(new HandleResponse(
                                        new PlayerDisconnectedResponse(APIHolder.getInstance().getPlayer().id, (byte)0)
                                ));
                            }

                            @Override
                            public void userEventTriggered(ChannelHandlerContext ctx, Object evt) {
                                if (evt instanceof IdleStateEvent) {
                                    IdleStateEvent e = (IdleStateEvent) evt;

                                    if (e.state() == IdleState.WRITER_IDLE) {
                                        ctx.writeAndFlush(Headers.PING.getEmptyMessage());
                                    }

                                    else if (e.state() == IdleState.READER_IDLE) {
                                        logger.error("Timeout!");
                                        ctx.close();
                                    }
                                }
                            }

                            @Override
                            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
                                logger.warn("Exception caught:", cause);
                            }
                        });
            }
        });

        new OnGameStarted(this);
        new OnGmMessage(this);
        new OnPlayerConnected(this);
        new OnPlayerDisconnected(this);
        new OnPlayerJoined(this);
        new OnPlayerLeft(this);
        new OnPlayerPropertySet(this);
        new OnPlayerUnsubscribed(this);
        new OnRegistrationSucceed(this);
        new OnRoomPropertySet(this);
        new OnRoomCreated(this);
        new OnRoomDestroyed(this);
    }

    public void bind(Header header, Handler handler) {
        this.handlers.put(header, handler);
    }

    public GameSession getGameSession() {
        return gameSession;
    }

    public Player getPlayer(long id) {
        return registered.get(id);
    }

    public Collection<Room> getRooms() {
        return rooms.values();
    }

    public Room getRoom(int room) {
        return rooms.get(room);
    }

    public Room getRoom(long player) {
        return rooms.values().stream()
                .filter(r -> r.hasMember(player))
                .findFirst()
                .orElse(null);
    }

    public String getType() {
        return type;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public Stream<UUID> getModulesStream() {
        return sessionModules.stream();
    }

    public void applyChanges(FullSessionData data) {
        this.type = data.type;

        this.sessionModules.clear();

        ObjPair<PropertyKey, Object> modules = null;
        for (ObjPair<PropertyKey, Object> o : data.properties) {
            if (o.key == LoadedModulesProperty.KEY) {
                modules = o;
                break;
            }
        }

        this.sessionModules.addAll(((List<UUID>) modules.value));

        ModuleRepository.checkModules((List<UUID>) modules.value);

        registered.clear();
        Arrays.stream(data.players)
                .map(PlayerData::toPlayer)
                .forEach(p -> registered.put(p.id, p));

        rooms.clear();
        Arrays.stream(data.rooms)
                .map(Room::new)
                .forEach(r -> rooms.put(r.getId(), r));
    }

    public void setRoomProperty(int id, PropertyKey property, Object val) {
        rooms.get(id).setProperty(property, val);
    }

    public void setPlayerProp(long id, PropertyKey prop, Object value) {
        registered.get(id).setProperty(prop, value);
    }

    public void registerPlayer(Player player) {
        registered.put(player.id, player);
    }

    public void unregisterPlayer(long id) {
        registered.remove(id);
    }

    public void addMember(int room, long id) {
        rooms.get(room).addMember(id);
    }

    public void removeMember(int room, long id) {
        rooms.get(room).removeMember(id);
    }

    public void addRoom(int room) {
        rooms.put(room, new Room(room));
    }

    public void destroyRoom(int room) {
        rooms.remove(room);
    }

    public void disconnect() {
        try {
            client.disconnect();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        active = false;
        Processor.fire(new ChangeState(Menu.INSTANCE));
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void start(Object... args) {
        try {
            this.address = (InetSocketAddress) args[0];
            this.client.connect(address, new RegistrationRequest((String) args[1], (Long) args[2]));
        }

        catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        active = true;
    }

    @Override
    public void stop() {
        try {
            this.sendRequest(new DisconnectRequest((byte)1)).sync();
            disconnect();

        } catch (InterruptedException e) { throw new RuntimeException(e); }
    }

    @Override
    public void offer(Command command) {
        switch (command.type()) {
            case HANDLE_RESPONSE:
                Message r = ((HandleResponse) command).response;
                logger.info(r);

                this.handlers
                        .get(r.getHeader())
                        .accept(this, r);
                break;

            case SEND_REQUEST:
                this.sendRequest(((SendRequest) command).request);
                break;
        }
    }

    @Override
    public EnumSet<CommandType> getAllowedCommands() {
        return EnumSet.of(SEND_REQUEST, HANDLE_RESPONSE);
    }

    private ChannelFuture sendRequest(Message request) {
        return client.getChannel().writeAndFlush(request);
    }
}
