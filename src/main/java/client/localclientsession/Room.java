package client.localclientsession;

import server.payload.RoomData;
import server.property.Properties;
import server.property.PropertyKey;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Room {
    private final int id;
    private final Map<PropertyKey, Object> properties;
    private final LongSet members;
    private final LongSet subscribers;

    public Room(int id) {
        this.id = id;

        this.properties = new HashMap<>();
        for (PropertyKey key : Properties.getAllByType(PropertyKey.ROOM_PROPERTY)) {
            this.properties.put(key, key.defaultValue());
        }

        this.members = new LongOpenHashSet();
        this.subscribers = new LongOpenHashSet();
    }

    public Room(RoomData data) {
        this.id = data.id;
        this.properties = new HashMap<>(data.properties);
        this.members = new LongOpenHashSet(data.members);
        this.subscribers = new LongOpenHashSet(data.players);
    }

    public int getId() {
        return id;
    }

    public int size() {
        return members.size();
    }

    public Object getProperty(PropertyKey property) {
        return properties.get(property);
    }

    public Stream<Long> getMembersStream() {
        return members.stream();
    }

    public boolean hasMember(long player) {
        return members.contains(player);
    }

    public boolean hasSubscriber(long player) {
        return subscribers.contains(player);
    }

    public boolean isPlaying() {
        return !subscribers.isEmpty();
    }

    public void setProperty(PropertyKey prop, Object value) {
        this.properties.put(prop, value);
    }

    public void addMember(long player) {
        this.members.add(player);
    }

    public void removeMember(long player) {
        this.members.remove(player);
    }

    public void addSubscriber(long player) {
        this.subscribers.add(player);
    }

    public void removeSubscriber(long player) {
        this.subscribers.remove(player);
    }
}
