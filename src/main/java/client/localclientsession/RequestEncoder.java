package client.localclientsession;

import server.message.Message;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ChannelHandler.Sharable
public class RequestEncoder extends ChannelOutboundHandlerAdapter {
    private final ByteBufAllocator allocator = PooledByteBufAllocator.DEFAULT;

    private final Logger logger = LogManager.getLogger(RequestEncoder.class);

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) {
        Message request = (Message) msg;
        ByteBuf buffer = allocator.buffer();
        request.encode(buffer);
        ctx.write(buffer, promise);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.warn("Exception caught:", cause);
    }
}
