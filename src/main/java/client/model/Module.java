package client.model;

import common.Gamemode;
import common.MapHeader;
import client.RenderableTemplate;
import common.GameSettings;
import common.fsm.ScriptStorage;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

public final class Module {
    public final Meta meta;
    public final GameSettings settings;
    public final Short2ObjectMap<RenderableTemplate> templates;
    public final Object2ObjectMap<String, RenderableTemplate> nameToTemplate;
    public final Short2ObjectMap<MapHeader> maps;
    public final Short2ObjectMap<Gamemode> gamemode;
    public final ScriptStorage scriptStorage;

    public Module(Meta meta, GameSettings settings, Short2ObjectMap<RenderableTemplate> templates, Short2ObjectMap<MapHeader> maps, Short2ObjectMap<Gamemode> gamemode, ScriptStorage scriptStorage) {
        this.meta = meta;
        this.settings = settings;
        this.templates = templates;
        this.maps = maps;
        this.gamemode = gamemode;
        this.scriptStorage = scriptStorage;
        this.nameToTemplate = new Object2ObjectOpenHashMap<>();
        for (Short2ObjectMap.Entry<RenderableTemplate> e : templates.short2ObjectEntrySet()) {
            this.nameToTemplate.put(e.getValue().getName(), e.getValue());
        }
    }
}
