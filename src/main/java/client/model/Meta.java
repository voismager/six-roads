package client.model;

import engine.support.loaders.UnifiedResource;

import java.util.UUID;

public final class Meta {
    public final UUID id;
    public final String name;
    public final UnifiedResource resource;

    public Meta(UUID id, String name, UnifiedResource resource) {
        this.id = id;
        this.name = name;
        this.resource = resource;
    }
}
