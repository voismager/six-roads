package client.animation;

import engine.structures.OpenDrawBuffer;
import it.unimi.dsi.fastutil.ints.IntHeapPriorityQueue;
import it.unimi.dsi.fastutil.ints.IntPriorityQueue;

public class Animator {
    private static final OpenDrawBuffer<Animation> BUFFER = new OpenDrawBuffer<>(Animation.class);
    private static final IntPriorityQueue FREE_INDEXES = new IntHeapPriorityQueue();

    public static void clear() {
        BUFFER.trueClear();
        FREE_INDEXES.clear();
    }

    public static void start(Animation a) {
        if (FREE_INDEXES.isEmpty()) {
            BUFFER.add(a);
        } else {
            BUFFER.array[FREE_INDEXES.dequeueInt()] = a;
        }
    }

    public static void update() {
        for (int i = 0; i < BUFFER.size; i++) {
            final Animation a = BUFFER.array[i];

            if (a != null && a.update()) {
                BUFFER.array[i] = null;
                FREE_INDEXES.enqueue(i);
            }
        }
    }
}
