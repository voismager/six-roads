package client.animation;

public interface Animation {
    /**
     * @return <tt>true</tt> if animation is over
     */
    boolean update();

    void cancel();
}
