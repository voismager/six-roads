package client.maps;

import client.gameobjects.RenderableHexagon;
import common.gameobjects.Hexagon;
import engine.graphics.transformations.FrustumFilter;
import engine.structures.ObjArray;
import engine.structures.OpenDrawBuffer;

public class RenderableDefaultMap implements RenderableRealMap {
    private final RenderableHexagon[][][] map;

    public RenderableDefaultMap(RenderableHexagon[][][] map) {
        this.map = map;
    }

    @Override
    public void updateDrawBuffer(FrustumFilter filter, OpenDrawBuffer<RenderableHexagon> drawBuffer) {
        for (RenderableHexagon[][] ggg : map) {
            for (RenderableHexagon[] gg : ggg) {
                for (RenderableHexagon g : gg) {
                    if (g == null) continue;
                    if (filter.insideFrustum(g.x, g.y, g.z, 1.2f)) {
                        drawBuffer.add(g);
                    }
                }
            }
        }
    }

    @Override
    public Hexagon get(int q, int r, int l) {
        return this.map[q][r][l];
    }

    @Override
    public void getFlatNeighbors(int q, int r, int l, ObjArray<Hexagon> result) {

    }

    @Override
    public void put(int q, int r, int l, Hexagon cell) {
        this.map[q][r][l] = (RenderableHexagon) cell;
        cell.setPosition(q, r, l);
    }

    @Override
    public void remove(int q, int r, int l) {
        this.map[q][r][l] = null;
    }

    @Override
    public void cleanup() {

    }
}
