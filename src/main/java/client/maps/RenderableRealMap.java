package client.maps;

import client.gameobjects.RenderableHexagon;
import common.RealMap;
import engine.graphics.transformations.FrustumFilter;
import engine.structures.OpenDrawBuffer;

public interface RenderableRealMap extends RealMap {
    void updateDrawBuffer(FrustumFilter filter, OpenDrawBuffer<RenderableHexagon> drawBuffer);
}
