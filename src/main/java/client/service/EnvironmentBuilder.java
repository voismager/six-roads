package client.service;

import client.loaders.GraphicsLoader;
import client.logic.ClEnvironment;
import client.logic.GameTools;
import client.model.Module;
import com.google.gson.JsonParser;
import common.MapHeader;

import java.io.InputStreamReader;

public class EnvironmentBuilder {
    public static ClEnvironment build(Module module, MapHeader map, int me, GameTools tools) throws Exception {
        GraphicsLoader.load(module, new JsonParser().parse(new InputStreamReader(module.meta.resource.getAsStream("graphics.json"))).getAsJsonObject());

        ClEnvironment env = new ClEnvironment(module, tools, map);
        env.init(module.gamemode.get((short)0), me);
        Runtime.getRuntime().gc();
        return env;
    }
}
