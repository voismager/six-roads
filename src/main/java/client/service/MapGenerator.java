package client.service;

import client.maps.RenderableDefaultMap;
import client.gameobjects.RenderableHexagon;
import client.maps.RenderableRealMap;
import client.RenderableTemplate;
import common.Template;
import common.MapHeader;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

import java.util.Random;

import static java.lang.Math.sqrt;

public class MapGenerator {
    public static RenderableRealMap generate(MapHeader header, Short2ObjectMap<RenderableTemplate> templates) {
        switch (header.type) {
            case DEFAULT: return generateDefault(header, templates);
            default: throw new IllegalStateException();
        }
    }

    private static RenderableRealMap generateDefault(MapHeader header, Short2ObjectMap<RenderableTemplate> templates) {
        Random random = new Random(header.seed);
        int width = 30;
        int height = 30;

        int[] terrains = templates.values().stream()
                .filter(v -> v.hasLabel("terrain"))
                .mapToInt(Template::getId)
                .toArray();

        float[][] map = new float[width][height];

        for (int t : terrains) {
            int pQ = random.nextInt(width);
            int pR = random.nextInt(height);

            for (int q = 0; q < width; q++) {
                for (int r = 0; r < height; r++) {
                    double dist = sqrt((q - pQ) * (q - pQ) + (r - pR) * (r - pR));
                    dist /= sqrt(width * width + height * height);
                    map[q][r] += (Math.cos(dist) * random.nextFloat());
                }
            }
        }

        final RenderableHexagon[][][] mapToGenerate = new RenderableHexagon[width][height][2];
        for (int q = 0; q < width; q++) {
            for (int r = 0; r < height; r++) {
                RenderableTemplate template = templates.get((short)terrains[(int)map[q][r]]);
                RenderableHexagon hexagon = new RenderableHexagon(template);
                hexagon.model = template.model;
                mapToGenerate[q][r][0] = hexagon;
                hexagon.setPosition(q, r, 0);
            }
        }

        return new RenderableDefaultMap(mapToGenerate);
    }
}
