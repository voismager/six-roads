package client.service;

import client.loaders.*;
import client.model.Meta;
import client.model.Module;
import common.Gamemode;
import common.MapHeader;
import client.RenderableTemplate;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import common.GameSettings;
import common.fsm.ScriptStorage;
import common.loaders.MapLoader;
import common.loaders.SettingsLoader;
import engine.collections.CollectionsUtil;
import engine.support.loaders.FilesLoader;
import engine.support.loaders.UnifiedResource;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ModuleRepository {
    private final static Logger logger = LogManager.getLogger(ModuleRepository.class);

    private final static Map<UUID, Module> IN_MEMORY = new HashMap<>();

    public static Module get(UUID key) {
        return IN_MEMORY.get(key);
    }

    public static void checkModules(List<UUID> modules) {
        for (int i = 0; i < modules.size(); i++) {
            Module t = get(modules.get(i));

            if (t == null) {
                try {
                    loadEmbeddedModule(modules.get(i));
                } catch (Exception e) {
                    throw new RuntimeException("Exception while loading " + modules.get(i), e);
                }
            }
        }
    }

    public static Module getCopy(UUID uuid) {
        Module module = get(uuid);
        Meta meta = module.meta;
        GameSettings settings = module.settings;
        ScriptStorage scriptStorage = module.scriptStorage;
        Short2ObjectMap<RenderableTemplate> templates = CollectionsUtil.copy(module.templates, RenderableTemplate::new);
        Short2ObjectMap<Gamemode> gamemodes = CollectionsUtil.copy(module.gamemode, Gamemode::new);
        Short2ObjectMap<MapHeader> maps = CollectionsUtil.copy(module.maps, MapHeader::new);
        return new Module(meta, settings, templates, maps, gamemodes, scriptStorage);
    }

    private static void loadEmbeddedModule(UUID uuid) throws Exception {
        String prefix = "/modules/global/" + uuid.toString() + "/";

        final InputStream metaAsStream = FilesLoader.loadResource(prefix + "meta.json");
        final JsonObject metaAsJson = new JsonParser().parse(new InputStreamReader(metaAsStream)).getAsJsonObject();

        UUID foundUuid = UUID.fromString(metaAsJson.get("id").getAsString());

        if (foundUuid.equals(uuid)) {
            IN_MEMORY.put(uuid, buildEmbedded(prefix, uuid, metaAsJson));
        } else throw new IOException();
    }

    private static Module buildEmbedded(String path, UUID uuid, JsonObject header) throws Exception {
        JsonObject objectsJ = new JsonParser().parse(new InputStreamReader(FilesLoader.loadResource(path + "gameobjects.json"))).getAsJsonObject();

        Meta meta = loadMeta(new UnifiedResource(true, path), uuid, header);
        GameSettings settings = SettingsLoader.load(objectsJ.getAsJsonObject("settings"));
        ScriptStorage storage = new ScriptStorage();
        Short2ObjectMap<RenderableTemplate> templates = TemplateLoader.load(objectsJ.getAsJsonArray("templates"));
        Short2ObjectMap<Gamemode> gamemodes = GamemodeLoader.load(objectsJ.getAsJsonArray("gamemodes"), storage);
        Short2ObjectMap<MapHeader> maps = MapLoader.load(objectsJ.getAsJsonArray("maps"));

        Runtime.getRuntime().gc();
        logger.info("MODULE " + meta.name + " IS ADDED");
        return new Module(meta, settings, templates, maps, gamemodes, storage);
    }

    private static Meta loadMeta(UnifiedResource resource, UUID uuid, JsonObject header) {
        String name = header.get("name").getAsString();
        return new Meta(uuid, name, resource);
    }
}
