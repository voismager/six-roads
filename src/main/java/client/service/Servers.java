package client.service;

import common.server.properties.VersionProperty;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import server.ObjPair;
import server.message.Headers;
import server.message.SessionDataResponse;
import server.property.PropertyKey;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Servers {
    public static class Server {
        public InetSocketAddress address;
        public String version;

        public Server(InetSocketAddress address) {
            this.address = address;
        }
    }

    private final static List<Server> SERVERS = new ArrayList<>();

    public static List<Server> get() {
        return SERVERS;
    }

    public static void loadList(Reader s) throws IOException {
        try (BufferedReader br = new BufferedReader(s)) {
            String line;

            while ((line = br.readLine()) != null) {
                for (String a : line.split(",")) {
                    String address = a.substring(0, a.indexOf(':'));
                    int port = Integer.parseInt(a.substring(a.indexOf(':') + 1));
                    SERVERS.add(new Server(new InetSocketAddress(address, port)));
                }
            }
        }
    }

    public static void update() {
        final byte[] message = new byte[] { 0, 1, Headers.SESSION_DATA_REQUEST.id() };

        for (Server server : SERVERS) {
            try (Socket socket = new Socket()) {
                socket.setSoTimeout(3000);
                socket.connect(server.address, 3000);

                DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
                outToServer.write(message);
                outToServer.flush();

                DataInputStream inFromServer = new DataInputStream(socket.getInputStream());
                byte part0 = inFromServer.readByte();
                byte part1 = inFromServer.readByte();
                int length = (part0 << 1) | part1;
                byte[] b = new byte[length];

                for (int i = 0; i < length; i++) b[i] = inFromServer.readByte();

                ByteBuf buf = Unpooled.wrappedBuffer(b);

                SessionDataResponse response = (SessionDataResponse) Headers.decode(buf);

                for (ObjPair<PropertyKey, Object> p : response.data.properties) {
                    if (p.key == VersionProperty.KEY) {
                        server.version = (String) p.value;
                    }
                }

            } catch (IOException | RuntimeException e) {
                server.version = "-";
            }
        }
    }
}
