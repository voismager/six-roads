package client.gamemaster;

import client.processor.Processor;
import client.processor.commands.SendRequest;
import common.server.requests.*;

public class NetCommunicator implements Communicator {
    @Override
    public void runServerScript(short action, byte stage, int[] args, boolean blocking) {
        Processor.fire(new SendRequest(new RunGameScript(action, stage, args, blocking)));
    }

    @Override
    public void reportInnerTransition(long stageAndFlags, long key, int Identifier) {
        Processor.fire(new SendRequest(new GmInnerTransitionRequest(stageAndFlags, key, Identifier)));
    }

    @Override
    public void reportOuterTransition(long key, int Identifier) {
        Processor.fire(new SendRequest(new GmOuterTransitionRequest(key, Identifier)));
    }

    @Override
    public void requestNextTurn() {
        Processor.fire(new SendRequest(new GmNextTurnRequest()));
    }

    @Override
    public void cleanup() {

    }
}
