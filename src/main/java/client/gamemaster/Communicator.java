package client.gamemaster;

public interface Communicator {
    void runServerScript(short action, byte stage, int[] args, boolean blocking);

    void reportInnerTransition(long stageAndFlags, long key, int Identifier);

    void reportOuterTransition(long key, int Identifier);

    void requestNextTurn();

    void cleanup();
}
