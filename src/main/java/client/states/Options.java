package client.states;

import client.processor.Command;
import engine.Window;
import imgui.ImGui;
import imgui.impl.LwjglGL3;
import client.Settings;
import client.State;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;

public class Options implements State {
    public static Options INSTANCE = new Options();

    private boolean[] vSyncEnabled = new boolean[1];

    @Override
    public void handle(Command command) {

    }

    @Override
    public void enter() {
        vSyncEnabled[0] = Settings.V_SYNC;
    }

    @Override
    public void leave() {

    }

    @Override
    public void update() {
        LwjglGL3.INSTANCE.newFrame();

        ImGui imgui = ImGui.INSTANCE;

        if (imgui.checkbox("V-Sync", vSyncEnabled)) {
            Settings.V_SYNC = !Settings.V_SYNC;
            Window.toggleVSync(Settings.V_SYNC);
        }
    }

    @Override
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT);

        ImGui imGui = ImGui.INSTANCE;
        imGui.render();
        LwjglGL3.INSTANCE.renderDrawData(imGui.getDrawData());
    }
}
