package client.states;

import client.Settings;
import client.service.Servers;
import client.State;
import client.localclientsession.LocalClientSession;
import client.localclientsession.LocalHostSession;
import client.processor.Command;
import client.processor.Processor;
import client.processor.commands.ActivateExecutor;
import client.processor.commands.ChangeState;
import client.processor.commands.DeactivateExecutor;
import client.processor.commands.Shutdown;
import debug.ImGuiUtil;
import engine.network.api.APIHolder;
import engine.network.IpUtil;
import glm_.vec2.Vec2;
import imgui.ImGui;
import imgui.InputTextFlag;
import imgui.impl.LwjglGL3;
import imgui.internal.ColumnsFlag;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;

public class Menu implements State {
    public static Menu INSTANCE = new Menu();

    @Override
    public void handle(Command command) {

    }

    @Override
    public void enter() {
        opt = "Local";
    }

    @Override
    public void leave() {

    }

    private char[] buf0 = new char[96];
    private char[] buf1 = new char[96];
    private Vec2   vec0 = new Vec2();
    private String opt;
    private String error;

    private void showLoginPage() {
        ImGui.INSTANCE.inputText("Username", buf0, InputTextFlag.Null.getI());
        ImGui.INSTANCE.text("5-32 characters long");
        ImGui.INSTANCE.text("English letters, digits or underscore");
        ImGui.INSTANCE.separator();
        ImGui.INSTANCE.text(System.lineSeparator());

        ImGui.INSTANCE.inputText("Password", buf1, InputTextFlag.Null.getI());
        ImGui.INSTANCE.text("8-40 characters long without whitespaces");
        ImGui.INSTANCE.text("At least one digit and letter");
        ImGui.INSTANCE.separator();
        ImGui.INSTANCE.text(System.lineSeparator());

        if (ImGui.INSTANCE.button("Sign In / Sign Up", vec0)) {
            try {
                APIHolder.getInstance().login(ImGuiUtil.toString(buf0), ImGuiUtil.toString(buf1));

                if (!APIHolder.getInstance().isLogged()) {
                    throw new RuntimeException();
                }

                error = null;
                Servers.update();

            } catch (RuntimeException | IOException | URISyntaxException e) {
                error = "Ooops, something's gone wrong!";
            } finally {
                ImGuiUtil.clear(buf0);
                ImGuiUtil.clear(buf1);
            }
        }
    }

    @Override
    public void update() {
        LwjglGL3.INSTANCE.newFrame();

        ImGui imgui = ImGui.INSTANCE;

        if (imgui.beginMainMenuBar()) {
            if (imgui.beginMenu("Connect to public server", true)) {
                if (error != null) imgui.text(error);

                if (!APIHolder.getInstance().isLogged()) {
                    showLoginPage();
                } else {
                    imgui.beginColumns("5", 3, ColumnsFlag.GrowParentContentsSize.getI());
                    imgui.text("Address"); imgui.nextColumn();
                    imgui.text("Version"); imgui.nextColumn();
                    imgui.text("Join");    imgui.nextColumn();
                    imgui.separator();

                    for (Servers.Server server : Servers.get()) {
                        imgui.text(server.address.toString()); imgui.nextColumn();
                        imgui.text(server.version); imgui.nextColumn();

                        if (server.version.equals(Settings.VERSION)) {
                            if (imgui.button("Join", vec0)) {
                                Command command = new ActivateExecutor(
                                        LocalClientSession.INSTANCE,
                                        server.address,
                                        ImGuiUtil.toString(buf1),
                                        APIHolder.getInstance().getPlayer().id
                                );

                                Processor.fireAndWait(command);

                                if (command.isSucceed()) {
                                    Processor.fire(new ChangeState(Lobby.INSTANCE));
                                } else {
                                    throw new RuntimeException("Couldn't connect to server");
                                }
                            }
                        }

                        else {
                            imgui.text("Versions are not the same");
                        }

                        imgui.nextColumn();
                    }

                    imgui.endColumns();
                }

                imgui.endMenu();
            }

            if (imgui.beginMenu("Connect to private server", true)) {
                if (error != null) imgui.text(error);

                if (!APIHolder.getInstance().isLogged()) {
                    showLoginPage();
                } else {
                    imgui.inputText("Server Address", buf0, InputTextFlag.Null.getI());
                    //imgui.inputText("Server Password", buf1, InputTextFlag.Null.getI());

                    if (imgui.button("Connect", vec0)) {
                        try {
                            String address = ImGuiUtil.toString(buf0);
                            String host = address.substring(0, address.indexOf(':'));
                            int port = Integer.parseInt(address.substring(address.indexOf(':') + 1));

                            Command command0 = new ActivateExecutor(
                                    LocalClientSession.INSTANCE,
                                    new InetSocketAddress(host, port),
                                    ImGuiUtil.toString(buf1),
                                    APIHolder.getInstance().getPlayer().id
                            );

                            Processor.fireAndWait(command0);

                            if (command0.isSucceed()) Processor.fire(new ChangeState(Lobby.INSTANCE));
                            else {
                                throw new RuntimeException("Couldn't connect to server");
                            }

                        } catch (RuntimeException e) {
                            error = "Error: " + e.getMessage();
                        } finally {
                            ImGuiUtil.clear(buf0);
                            ImGuiUtil.clear(buf1);
                        }
                    }

                }

                imgui.endMenu();
            }

            if (imgui.beginMenu("Start Server", true)) {
                if (error != null) imgui.text(error);

                if (!APIHolder.getInstance().isLogged()) {
                    showLoginPage();
                } else {
                    imgui.inputText("Server Port", buf0, InputTextFlag.Null.getI());
                    //imgui.inputText("Server Password", buf1, InputTextFlag.Null.getI());

                    if (imgui.beginCombo("##combo", opt, 0)) {
                        if (imgui.selectable("Local", opt.equals("Local"), 0, new Vec2())) opt = "Local";
                        if (imgui.selectable("LAN", opt.equals("LAN"), 0, new Vec2())) opt = "LAN";
                        imgui.endCombo();
                    }

                    if (imgui.button("Start", vec0)) {
                        try {
                            int port = Integer.parseInt(ImGuiUtil.toString(buf0));

                            InetSocketAddress address = null;

                            switch (opt) {
                                case "Local":
                                    address = new InetSocketAddress(IpUtil.getLocal(), port);
                                    break;
                                case "LAN":
                                    address = new InetSocketAddress(IpUtil.getLAN(), port);
                                    break;
                            }

                            Command command0 = new ActivateExecutor(
                                    LocalHostSession.INSTANCE,
                                    address
                            );

                            Processor.fireAndWait(command0);

                            if (command0.isSucceed()) {
                                Command command1 = new ActivateExecutor(
                                        LocalClientSession.INSTANCE,
                                        address,
                                        "", APIHolder.getInstance().getPlayer().id
                                );

                                Processor.fireAndWait(command1);

                                if (command1.isSucceed()) Processor.fire(new ChangeState(Lobby.INSTANCE));
                                else {
                                    Processor.fire(new DeactivateExecutor(LocalHostSession.INSTANCE));
                                    throw new RuntimeException("Couldn't start server");
                                }
                            }

                        } catch (IOException | RuntimeException e) {
                            error = "Error: " + e.getMessage();
                        } finally {
                            ImGuiUtil.clear(buf0);
                            ImGuiUtil.clear(buf1);
                        }
                    }
                }

                imgui.endMenu();
            }

            if (imgui.button("Exit", new Vec2()))
                Processor.fire(new Shutdown());

            imgui.endMainMenuBar();
        }
    }

    @Override
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT);

        ImGui imGui = ImGui.INSTANCE;
        imGui.render();
        LwjglGL3.INSTANCE.renderDrawData(imGui.getDrawData());
    }
}
