package client.states;

import client.localclientsession.GameSession;
import debug.ASCIIArt;
import debug.ImGuiUtil;
import engine.support.IOUtil;
import glm_.vec2.Vec2;
import imgui.Cond;
import imgui.ImGui;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class EscapeMenu {
    private final boolean[] open;
    private final char[] body;
    private final Vec2 size;
    private final Vec2 buttonSize;

    private String msg;

    public EscapeMenu() {
        this.open = new boolean[1];
        this.body = new char[2048];
        this.size = new Vec2(400, 400);
        this.buttonSize = new Vec2();
    }

    public void draw(ImGui imGui)  {
        imGui.setNextWindowSize(size, Cond.Once);

        if (imGui.begin("menu", open, 0)) {
            if (imGui.button("Exit", new Vec2())) {
                GameSession.INSTANCE.getEnv().stopPlaying();
                return;
            }

            imGui.separator();

            //drawBugReport(imGui);

            if (msg != null) {
                imGui.newLine();
                imGui.text(msg);
            }

            imGui.end();
        }
    }

    private void drawBugReport(ImGui imGui) {
        imGui.text("Describe your bug here.");
        imGui.text("P.S. Actually single line text area is also a bug. Sorry for inconvenience.");

        imGui.inputText("##report", body, 0);

        if (imGui.button("Send Report", buttonSize)) {
            try {
                String body = ImGuiUtil.toString(this.body);

                LoggerContext context = (LoggerContext) LogManager.getContext();
                RollingFileAppender a = context.getConfiguration().getAppender("File");

                StringBuilder console = new StringBuilder();
                console.append(IOUtil.readFile(Charset.forName("UTF-8"), a.getFileName()));

                int i = 0;
                while (true) {
                    String s = String.format(a.getFilePattern().replaceAll("%i", "%d"), ++i);
                    Path path = Paths.get(s);
                    if (Files.exists(path)) console.append(IOUtil.readFile(Charset.forName("UTF-8"), path));
                    else break;
                }

                //ExternalAPI.sendReport(body, console.toString());

                Arrays.fill(this.body, '\0');
                msg = "Thank you for your report! " + ASCIIArt.getRandom();

            } catch (IOException e) {
                msg = "Ooops! Something went wrong: " + e.getMessage();
            }
        }
    }
}
