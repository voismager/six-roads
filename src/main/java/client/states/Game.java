package client.states;

import client.State;
import client.inputhandlers.InputHandler;
import client.localclientsession.GameSession;
import client.logic.ClEnvironment;
import client.logic.GameObjectsStorage;
import client.maps.RenderableRealMap;
import client.processor.Command;
import client.processor.commands.GameEvent;
import client.render.Renderer;
import client.render.RendererHexagon;
import client.render.RendererSprite;
import engine.MouseInput;
import engine.Timer;
import engine.graphics.transformations.FrustumFilter;
import engine.graphics.lights.DirectionalLight;
import imgui.ImGui;
import imgui.impl.LwjglGL3;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import static engine.graphics.transformations.Transformations.*;

public class Game implements State {
    public static final Game INSTANCE = new Game();

    private final DirectionalLight light;
    private final Vector3f ambientColor;
    private final FrustumFilter frustumFilter;

    private RenderableRealMap map;
    private InputHandler inputHandler;

    private EscapeMenu menu;
    private boolean showMenu;

    private Game() {
        this.light = new DirectionalLight();
        this.ambientColor = new Vector3f(0.75f, 0.75f, 0.75f);
        this.frustumFilter = new FrustumFilter();
    }

    @Override
    public void handle(Command command) {
        GameSession.INSTANCE.processGameEvent(((GameEvent) command).response);
    }

    @Override
    public void enter() {
        setLight(45f);
        ClEnvironment env = GameSession.INSTANCE.getEnv();
        inputHandler = env.getInputHandler();
        map = env.getRealMap();
        menu = new EscapeMenu();
        showMenu = false;
    }

    @Override
    public void leave() {
        inputHandler = null;
        map = null;
        menu = null;
    }

    @Override
    public void update() {
        updateViewMatrix();
        Timer.update();

        frustumFilter.updateFrustum(getPerspectiveProjectionMatrix(), getViewMatrix());

        LwjglGL3.INSTANCE.newFrame();

        ImGui imGui = ImGui.INSTANCE;

        if (imGui.isKeyPressed(GLFW.GLFW_KEY_ESCAPE, false)) {
            showMenu = !showMenu;
        }

        if (showMenu) {
            menu.draw(imGui);
        } else {
            //Widget clicked = env.aspectManager.update();

            //if (clicked != null)
                //env.getInterpreter().onEntityClick(clicked);

            //if (!ImGui.INSTANCE.getIo().getWantCaptureMouse()) {
                final byte mouseState = MouseInput.updateAndGetState();
                inputHandler.onLoopUpdate(mouseState);
            //}
        }
    }

    @Override
    public void render() {
        map.updateDrawBuffer(frustumFilter, RendererHexagon.clearAndGetBuffer());
        GameObjectsStorage.updateSpriteDrawBuffer(frustumFilter, RendererSprite.clearAndGetBuffer());
        Renderer.render(ambientColor, light);
        ImGui.INSTANCE.render();
        LwjglGL3.INSTANCE.renderDrawData(ImGui.INSTANCE.getDrawData());
    }

    private void setLight(float lightAngle) {
        if (lightAngle > 90) {
            lightAngle = 0;
        }

        if (lightAngle >= 80) {
            float factor = 1 - (Math.abs(lightAngle) - 80) / 10.0f;
            light.intensity = factor;
            light.getColour().y = Math.max(factor, 0.9f);
            light.getColour().z = Math.max(factor, 0.5f);
        } else {
            light.intensity = 1;
            light.setColour(1, 0.7f, 0.5f);
        }

        double angRad = Math.toRadians(lightAngle);

        light.setDirection(0, (float) Math.sin(angRad), (float) Math.cos(angRad));
    }
}
