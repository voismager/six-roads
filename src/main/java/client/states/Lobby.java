package client.states;

import client.State;
import client.localclientsession.LocalClientSession;
import client.localclientsession.LocalHostSession;
import client.localclientsession.Room;
import client.model.Module;
import client.processor.Command;
import client.processor.Processor;
import client.processor.commands.ChangeState;
import client.processor.commands.DeactivateExecutor;
import client.processor.commands.SendRequest;
import client.service.ModuleRepository;
import common.MapHeader;
import common.server.properties.MapProperty;
import common.server.properties.MaxPlayersProperty;
import common.server.properties.ModuleProperty;
import common.server.requests.StartGameRequest;
import engine.network.api.APIHolder;
import glm_.vec2.Vec2;
import imgui.ImGui;
import imgui.impl.LwjglGL3;
import server.Player;
import server.message.*;
import server.property.Properties;

import java.util.UUID;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;

public class Lobby implements State {
    public static final Lobby INSTANCE = new Lobby();

    @Override
    public void handle(Command command) {

    }

    @Override
    public void enter() {
    }

    @Override
    public void leave() {

    }

    @Override
    public void update() {
        LwjglGL3.INSTANCE.newFrame();

        if (LocalClientSession.INSTANCE.getRoom(APIHolder.getInstance().getPlayer().id) == null) {
            drawLobby();
        } else {
            drawRoom();
        }
    }

    private void drawRoom() {
        ImGui imgui = ImGui.INSTANCE;
        LocalClientSession session = LocalClientSession.INSTANCE;
        long playerId = APIHolder.getInstance().getPlayer().id;

        Long host = (Long) session.getRoom(playerId).getProperty(Properties.ROOM_HOST);
        boolean isHost = host == playerId;

        Module module = ModuleRepository.get((UUID) session.getRoom(playerId).getProperty(ModuleProperty.KEY));
        MapHeader map = module == null ? null : module.maps.get((short) session.getRoom(playerId).getProperty(MapProperty.KEY));

        String modulePreview = module == null ? "Not selected" : module.meta.name;
        String mapPreview = map == null ? "Not selected" : "";

        imgui.columns(3, "0", true);

        //////////////////

        if (isHost) {
            imgui.text("Module = "); imgui.sameLine(0);

            if (imgui.beginCombo("##1", modulePreview, 0)) {
                session.getModulesStream()
                        .forEach(uuid -> {
                            Module t = ModuleRepository.get(uuid);

                            if (ImGui.INSTANCE.selectable(t.meta.name, false, 0, new Vec2()))
                                Processor.fire(new SendRequest(new SetRoomPropertyRequest(ModuleProperty.KEY, t.meta.id)));
                        });

                imgui.endCombo();
            }

        } else imgui.text("Module = " + modulePreview);

        imgui.nextColumn();

        //////////////////

        if (module == null) imgui.text("Map = None selected");
        else {
            if (isHost) {
                imgui.text("Map = "); imgui.sameLine(0);

                if (imgui.beginCombo("##2", mapPreview, 0)) {
                    module.maps.values()
                            .forEach(m -> {
                                //if (ImGui.INSTANCE.selectable(getNameOr(module, m,"Map " + m.id()), false, 0, new Vec2()))
                                //    Processor.fire(new SendRequest(new SetRoomPropertyRequest(MapProperty.KEY, m.id())));
                            });

                    imgui.endCombo();
                }
            } else imgui.text("Map = " + mapPreview);
        }

        imgui.nextColumn();

        //////////////////

        imgui.text("Map players: " + session.getRoom(playerId).getProperty(MaxPlayersProperty.KEY));

        //////////////////

        imgui.separator();
        imgui.endColumns();

        if (isHost) {
            if (!session.getRoom(playerId).isPlaying())
                if (imgui.button("Start", new Vec2()))
                    Processor.fire(new SendRequest(new StartGameRequest()));
        }

        if (imgui.button("Leave", new Vec2())) {
            Processor.fire(new SendRequest(new LeaveRoomRequest(session.getRoom(playerId).getId())));
        }

        imgui.separator();

        //////////////////

        imgui.columns(3, "1", true);

        imgui.text("Name"); imgui.nextColumn();
        imgui.text("Status"); imgui.nextColumn();
        imgui.text("Fraction"); imgui.nextColumn();
        imgui.separator();

        session.getRoom(playerId).getMembersStream()
                .forEach(id -> {
                    Player p = session.getPlayer(id);

                    //////////////////

                    ImGui.INSTANCE.text(p.getUsername());
                    ImGui.INSTANCE.nextColumn();

                    //////////////////

                    if (p.id == host) ImGui.INSTANCE.text("Host");
                    else if (session.getRoom(playerId).hasSubscriber(p.id)) ImGui.INSTANCE.text("In Game");
                    else if (p.id != playerId) ImGui.INSTANCE.text((Boolean) p.getProperty(Properties.PLAYER_READY_FLAG) ? "Ready" : "Not ready");
                    else {
                        boolean readyFlag = (Boolean) p.getProperty(Properties.PLAYER_READY_FLAG);

                        if (map == null) ImGui.INSTANCE.text(readyFlag ? "Ready" : "Not ready");
                        else {
                            if (ImGui.INSTANCE.beginCombo("##2", readyFlag ? "Ready" : "Not ready", 0)) {
                                if (ImGui.INSTANCE.selectable("Not ready", false, 0, new Vec2())) {
                                    Processor.fire(new SendRequest(new SetPlayerPropertyRequest(Properties.PLAYER_READY_FLAG, false)));
                                }

                                if (ImGui.INSTANCE.selectable("Ready", false, 0, new Vec2())) {
                                    Processor.fire(new SendRequest(new SetPlayerPropertyRequest(Properties.PLAYER_READY_FLAG, true)));
                                }

                                ImGui.INSTANCE.endCombo();
                            }
                        }
                    }

                    ImGui.INSTANCE.nextColumn();

                    //////////////////

                    if (module == null) ImGui.INSTANCE.text("Not selected");
                    else {
                        /*
                        Faction faction = module.factions.get(((Short) p.getProperty(FactionProperty.KEY)).shortValue());
                        String factionPreview = faction == null ?
                                "Not selected" :
                                getNameOr(module, faction,  "Faction " + faction.id());

                        if (p.id != playerId || map == null) ImGui.INSTANCE.text(factionPreview);
                        else {
                            if (ImGui.INSTANCE.beginCombo("##3", factionPreview, 0)) {
                                module.factions.values().forEach(fr -> {
                                    if (ImGui.INSTANCE.selectable(getNameOr(module, fr,"Noname faction"), false, 0, new Vec2()))
                                        Processor.fire(new SendRequest(new SetPlayerPropertyRequest(FactionProperty.KEY, fr.id())));
                                });

                                ImGui.INSTANCE.endCombo();
                            }
                        }

                         */
                    }

                    ImGui.INSTANCE.nextColumn();
                    ImGui.INSTANCE.separator();
                });

        imgui.endColumns();
    }

    private void drawLobby() {
        ImGui imgui = ImGui.INSTANCE;
        LocalClientSession session = LocalClientSession.INSTANCE;

        imgui.text("IP: " + session.getAddress().getAddress().getHostAddress() + ":" + session.getAddress().getPort());

        if (imgui.button("Disconnect", new Vec2())) {
            Processor.fire(new DeactivateExecutor(LocalClientSession.INSTANCE));
            Processor.fire(new ChangeState(Menu.INSTANCE));

            if (LocalHostSession.INSTANCE.isActive()) {
                if (LocalClientSession.INSTANCE.getAddress().equals(LocalHostSession.INSTANCE.getAddress())) {
                    Processor.fire(new DeactivateExecutor(LocalHostSession.INSTANCE));
                }
            }

        } imgui.sameLine(0);

        if (imgui.button("Create Room", new Vec2())) {
            Processor.fire(new SendRequest(new CreateRoomRequest()));
        }

        imgui.columns(5, "2", true);
        imgui.text("Room Id"); imgui.nextColumn();
        imgui.text("Players"); imgui.nextColumn();
        imgui.text("Host"); imgui.nextColumn();
        imgui.text("Status"); imgui.nextColumn();
        imgui.text("---"); imgui.nextColumn();
        imgui.separator();

        for (Room room : session.getRooms()) {
            int currSize = room.size();
            int maxSize = (int) room.getProperty(MaxPlayersProperty.KEY);

            imgui.text(String.valueOf(room.getId())); imgui.nextColumn();
            imgui.text(currSize + " / " + maxSize); imgui.nextColumn();
            imgui.text(room.getProperty(Properties.ROOM_HOST) == Properties.ROOM_HOST.defaultValue() ?
                    "-" : session.getPlayer((long)room.getProperty(Properties.ROOM_HOST)).getUsername()); imgui.nextColumn();
            imgui.text(room.isPlaying() ? "In Game" : "Waiting"); imgui.nextColumn();

            if (currSize < maxSize) {
                if (imgui.button("Join", new Vec2())) {
                    Processor.fire(new SendRequest(new JoinRoomRequest(room.getId())));
                }

            } else {
                imgui.text("Full");
            }

            imgui.nextColumn();
        }
    }

    @Override
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT);

        ImGui imGui = ImGui.INSTANCE;
        imGui.render();
        LwjglGL3.INSTANCE.renderDrawData(imGui.getDrawData());
    }
}
