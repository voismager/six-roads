package client.logic;

import server.Player;

public interface GameTools {
    boolean isSubscribed();

    void stopPlaying();

    Player getPlayer(int puppeteer);

    boolean rollDice(int probability);

    void unsubscribe();
}
