package client.logic;

import common.Entity;
import constants.EntityType;
import engine.structures.ObjOpenArray;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import visuals.Widget;
import visuals.billboards.BillboardModel;
import visuals.templates.DrawableModel;

public final class AspectManager {
    private final Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> templates;
    private final Long2ObjectMap<BillboardModel[]> billboards;
    private final Short2ObjectMap<Widget> widgets;
    private final ObjOpenArray<Widget> opened;

    public AspectManager(Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> templates,
                         Long2ObjectMap<BillboardModel[]> billboards,
                         Short2ObjectMap<Widget> widgets) {

        this.templates = templates;
        this.billboards = billboards;
        this.widgets = widgets;
        this.opened = new ObjOpenArray<>();
    }

    public void cleanup() {
        opened.clear();
        widgets.forEach((s, widget) -> widget.clear());
        widgets.clear();
        billboards.clear();
        templates.forEach((i, map) -> map.clear());
        templates.clear();
    }

    public Widget update() {
        Widget clicked = null;

        for (int i = 0; i < opened.size; i++) {
            Widget w = opened.get(i);
            w.draw();

            if (w.isClicked()) clicked = w;
        }

        return clicked;
    }

    public void open(short id) {
        Widget widget = widgets.get(id);

        if (!widget.isOpened()) {
            widget.setIndex(opened.size);
            opened.add(widget);
        }
    }

    public DrawableModel getTemplate(Entity entity, String name) {
        return templates.get(entity.Identifier()).get(name);
    }

    public DrawableModel getTemplate(EntityType type, short id, String name) {
        return templates.get(EntityType.Identifier(type, id)).get(name);
    }

    public boolean hasTemplate(EntityType type, short id, String name) {
        Object2ObjectMap<String, DrawableModel> map = templates.get(EntityType.Identifier(type, id));

        if (map != null) {
            return map.containsKey(name);
        }

        return false;
    }

    public void open(short id, Entity entity, String name) {
        this.open(id, entity, name, Widget.DEFAULT);
    }

    public void open(short id, Entity entity, String name, Object[] blanks) {
        this.open(id, entity, name, blanks, Widget.DEFAULT);
    }

    public void open(short id, Entity entity, String name, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, getTemplate(entity, name).unpack(new Object[0]), flags);

        if (!widget.isOpened()) {
            widget.setIndex(opened.size);
            opened.add(widget);
        }
    }

    public void open(short id, Entity entity, String name, Object[] blanks, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, getTemplate(entity, name).unpack(blanks, new Object[0]), flags);

        if (!widget.isOpened()) {
            widget.setIndex(opened.size);
            opened.add(widget);
        }
    }

    public void open(short id, Entity entity, DrawableModel template, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, template.unpack(new Object[0]), flags);

        if (!widget.isOpened()) {
            widget.setIndex(opened.size);
            opened.add(widget);
        }
    }

    public void open(short id, Entity entity, DrawableModel template, Object[] blanks, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, template.unpack(blanks, new Object[0]), flags);

        if (!widget.isOpened()) {
            widget.setIndex(opened.size);
            opened.add(widget);
        }
    }

    public void add(short id, Entity entity, String name) {
        this.add(id, entity, name, Widget.DEFAULT);
    }

    public void add(short id, Entity entity, String name, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, getTemplate(entity, name).unpack(new Object[0]), flags);
    }

    public void add(short id, Entity entity, DrawableModel drawable, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, drawable.unpack(new Object[0]), flags);
    }

    public void add(short id, Entity entity, String name, Object[] blanks, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, getTemplate(entity, name).unpack(blanks, new Object[0]), flags);
    }

    public void add(short id, Entity entity, DrawableModel template, Object[] blanks, int flags) {
        Widget widget = widgets.get(id);
        widget.add(entity, template.unpack(blanks, new Object[0]), flags);
    }

    public void remove(short id, Entity entity) {
        Widget widget = widgets.get(id);
        widget.remove(entity);
    }

    public void apply(short id, int flags) {
        Widget widget = widgets.get(id);
        widget.apply(flags);
    }

    public void close(short id) {
        Widget widget = widgets.get(id);

        if (widget.isOpened()) {
            widget.clear();
            opened.remove(widget.getIndex());
            opened.get(widget.getIndex()).setIndex(widget.getIndex());
            widget.close();
        }
    }

    /*
    public void bind(Ground cell) {
        BillboardModel[] models = this.billboards.get(TD00_BSSs_L(HEXAGON_GROUND.toByte(), cell.getTerrain().id(), cell.getArea().id()));

        if (models != null) {
            Billboard[] billboards = new Billboard[models.length];

            for (int i = 0; i < models.length; i++) {
                billboards[i] = models[i].unwrap(new Object[] { cell });
                billboards[i].move(cell.x, cell.y, cell.z + 0.75f);
            }

            cell.setBillboards(billboards);
        }
    }

    public void unbind(Ground cell) {
        Billboard[] billboards = cell.getBillboards();

        if (billboards != null) {
            for (Billboard billboard : billboards) {
                billboard.clear();
            }

            cell.setBillboards(null);
        }
    }
     */

}
