package client.logic;

import client.gameobjects.Sprite;
import engine.graphics.transformations.FrustumFilter;
import engine.structures.OpenDrawBuffer;

public class GameObjectsStorage {
    private final static OpenDrawBuffer<Sprite> SPRITES = new OpenDrawBuffer<>(Sprite.class);

    public static void putSprite(Sprite sprite) {
        SPRITES.add(sprite);
    }

    public static void updateSpriteDrawBuffer(FrustumFilter filter, OpenDrawBuffer<Sprite> drawBuffer) {
        for (int i = 0; i < SPRITES.size; i++) {
            if (SPRITES.array[i] != null) {
                Sprite r = SPRITES.array[i];
                if (filter.insideFrustum(r.x - r.width / 2, r.y - r.height / 2, r.z, r.x + r.width / 2, r.y + r.height / 2, r.z)) {
                    drawBuffer.add(r);
                }
            }
        }
    }
}
