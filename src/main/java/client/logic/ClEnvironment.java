package client.logic;

import client.animation.Animator;
import client.gameobjects.Sprite;
import client.model.Module;
import client.gameobjects.RenderableHexagon;
import client.maps.RenderableRealMap;
import client.RenderableTemplate;
import client.service.MapGenerator;
import common.Template;
import common.Gamemode;
import common.MapHeader;
import common.fsm.FSM;
import common.fsm.Memory;
import common.fsm.MemoryImpl;
import common.gameobjects.Hexagon;
import common.gameobjects.Puppeteer;
import client.inputhandlers.InputHandler;
import client.inputhandlers.InputHandlers;
import common.PuppeteerStorage;
import common.PainterInterface;
import common.PainterService;
import engine.Cleanable;
import engine.graphics.transformations.Transformations;
import engine.graphics.core.TextureCache;
import engine.support.loaders.UnifiedResource;
import engine.types.vectors.Vector;
import scripting.GlobalEnvironment;
import server.Player;

public final class ClEnvironment implements Cleanable, GlobalEnvironment {
    private final GameTools tools;
    private final PainterService painterService;
    private final MemoryImpl globalMemory;
    private final FSM fsm;
    private final InputHandler inputHandler;
    private final RenderableRealMap realMap;
    private final PuppeteerStorage puppeteerStorage;
    private Puppeteer me;

    public final Module module;
    public AspectManager aspectManager;

    public ClEnvironment(Module module, GameTools tools, MapHeader header) {
        this.module = module;
        this.tools = tools;
        this.puppeteerStorage = new PuppeteerStorage();
        this.fsm = new FSM(module.scriptStorage, this);
        this.inputHandler = InputHandlers.get(module.settings.inputHandler, fsm, this);
        this.painterService = new PainterService();
        this.globalMemory = new MemoryImpl();
        this.realMap = MapGenerator.generate(header, module.templates);
    }

    public InputHandler getInputHandler() {
        return inputHandler;
    }

    public RenderableRealMap getRealMap() {
        return realMap;
    }

    public void init(Gamemode gamemode, int myIndex) {
        Puppeteer puppeteer = new Puppeteer(myIndex, gamemode);
        this.me = puppeteer;
        this.puppeteerStorage.add(puppeteer);
        this.inputHandler.init(puppeteer);
    }

    public boolean rollDice(int probability) {
        return tools.rollDice(probability);
    }

    @Override
    public Memory memory() {
        return globalMemory;
    }

    @Override
    public Player getPlayer(int puppeteer) {
        return this.tools.getPlayer(puppeteer);
    }

    @Override
    public void stopPlaying() {
        this.tools.stopPlaying();
    }

    @Override
    public void unsubscribe() {
        this.tools.unsubscribe();
    }

    @Override
    public int totalPuppeteers() {
        return puppeteerStorage.total();
    }

    @Override
    public PainterInterface getPainter(byte paint) {
        return this.painterService.get(paint, realMap);
    }

    @Override
    public PainterInterface getFreePainter() {
        return this.painterService.getFree(realMap);
    }

    @Override
    public int puppeteer() {
        return me.getId();
    }

    @Override
    public int loadTexture(String name) {
        return TextureCache.getInstance().getTexture(UnifiedResource.of(module, "/textures/" + name));
    }

    @Override
    public void moveCamera(float x, float y, float z) {
        Transformations.Camera.translate(x, y, z);
    }

    @Override
    public void setCamera(float x, float y, float z) {
        Transformations.Camera.setPosition(x, y, z);
    }

    @Override
    public void removeComponent(short id, short componentId) {
        //Component component = module.components.get(componentId);
        //aspectManager.remove(id, component);
    }

    @Override
    public void addComponent(short id, short componentId, String drawableId, int flags) {
        //Component component = module.components.get(componentId);
        //aspectManager.add(id, component, drawableId, flags);
    }

    @Override
    public void openComponent(short id, short componentId, String drawableId, int flags) {
        //Component component = module.components.get(componentId);
        //aspectManager.open(id, component, drawableId, flags);
    }

    @Override
    public Sprite createSprite(float width, float height, int texture) {
        Sprite sprite = new Sprite(width, height, texture);
        GameObjectsStorage.putSprite(sprite);
        return sprite;
    }

    @Override
    public Hexagon createCell(Template template) {
        RenderableHexagon cell = new RenderableHexagon(template);
        cell.model = ((RenderableTemplate) template).model;
        return cell;
    }

    @Override
    public Hexagon getCell(int q, int r, int l) {
        return realMap.get(q, r, l);
    }

    @Override
    public void putCell(int q, int r, int l, Hexagon cell) {
        realMap.put(q, r, l, cell);
    }

    @Override
    public void removeCell(int q, int r, int l) {
        realMap.remove(q, r, l);
    }

    @Override
    public Vector getAttribute(Hexagon hexagon, String name) {
        return hexagon.getAttribute(name);
    }

    @Override
    public Vector getAttribute(Template template, String name) {
        return template.getAttribute(name);
    }

    @Override
    public Vector getAttribute(Puppeteer puppeteer, String name) {
        return puppeteer.getAttribute(name);
    }

    @Override
    public void setAttribute(Hexagon hexagon, String name, Vector value) {
        hexagon.setAttribute(name, value);
    }

    @Override
    public void setAttribute(Template template, String name, Vector value) {
        template.setAttribute(name, value);
    }

    @Override
    public void setAttribute(Puppeteer puppeteer, String name, Vector value) {
        puppeteer.setAttribute(name, value);
    }

    @Override
    public Template getTemplate(short id) {
        return module.templates.get(id);
    }

    @Override
    public Template getTemplate(String name) {
        return module.nameToTemplate.get(name);
    }

    @Override
    public void cleanup() {
        aspectManager.cleanup();
        Animator.clear();
        globalMemory.cleanup();
        inputHandler.cleanup();
    }
}
