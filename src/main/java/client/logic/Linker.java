package client.logic;

import common.entities.Callable;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Prototype;
import org.luaj.vm2.compiler.LuaC;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.luajc.LuaJC;
import scripting.BuiltinScripts;
import scripting.runners.JavaRunner;
import scripting.runners.ScriptRunner;

import java.util.Collection;

import static scripting.runners.ScriptRunner.*;

public class Linker {
    public static void delink(Collection<Callable> prototypes) {
        for (Callable c : prototypes) {
            ScriptRunner runner = c.runner;

            if (runner instanceof JavaRunner) {
                ((JavaRunner) runner).unchain();
            }

            c.setRunner(null);
        }
    }

    public static void link(ClEnvironment javaEnv,
                            Collection<Callable> prototypes,
                            Short2ObjectMap<Object> sources,
                            Short2ObjectMap<Object2ObjectMap<String, Object>> parameters
    ) {
        Globals luaEnv = new Globals();

        LuaC.install(luaEnv);
        LuaJC.install(luaEnv);

        luaEnv.set("NOBODY", -1);
        luaEnv.set("DEFAULT_COLOR", -1);

        luaEnv.set("ENTER_STAGE", ENTER_STAGE);
        luaEnv.set("EXIT_STAGE", EXIT_STAGE);
        luaEnv.set("STAGE_0", STAGE_0);
        luaEnv.set("STAGE_1", STAGE_1);
        luaEnv.set("STAGE_2", STAGE_2);
        luaEnv.set("STAGE_3", STAGE_3);

        //luaEnv.set("Puppeteer", CoerceJavaToLua.coerce(new Lua_Puppeteer(javaEnv)));
        //luaEnv.set("Log", CoerceJavaToLua.coerce(new Lua_Log(javaEnv)));
        //luaEnv.set("Gui", CoerceJavaToLua.coerce(new Lua_Gui(javaEnv)));

        for (Callable script : prototypes) {
            Object src = sources.get(script.id());

            if (src instanceof String) {
                final JavaRunner runner = BuiltinScripts.getByName((String)src, javaEnv, null, parameters.get(script.id()));
                runner.cl_postConstruct();
                script.setRunner(runner);
            }

            else if (src instanceof Prototype[]) {
                Prototype[] proto = (Prototype[]) src;
            }

            else throw new IllegalArgumentException();
        }
    }
}
