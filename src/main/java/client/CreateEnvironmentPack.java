package client;

import client.logic.GameTools;
import client.model.Module;
import common.MapHeader;

public class CreateEnvironmentPack {
    public final Module module;
    public final MapHeader map;
    public final int me;
    public final GameTools tools;

    public CreateEnvironmentPack(Module module, MapHeader map, int me, GameTools tools) {
        this.module = module;
        this.map = map;
        this.me = me;
        this.tools = tools;
    }
}
