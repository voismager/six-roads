package visuals.billboards.billboards;

import engine.graphics.meshes.BillboardMesh;
import engine.graphics.renderer.Shader;
import engine.types.observables.Observable;
import org.joml.Vector4f;
import visuals.billboards.Billboard;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;

public class RectangleIconBillboard extends Billboard {
    private final static BillboardMesh MESH = BillboardMesh.BUILDER
            .point(-1, -1, 1).color(0, 0)
            .point(1, -1, 1).color(1, 0)
            .point(1, 1, 1).color(1, 1)
            .point(-1, 1, 1).color(0, 1)
            .quad(0, 1, 2, 3)
            .build(GL_STATIC_DRAW, 2);

    private final int texture;

    public RectangleIconBillboard(boolean open, float x, float y, float z, float w, float h, Vector4f filter, int texture) {
        super(open, x, y, z, w, h, filter.w, filter.x, filter.y, filter.z);
        this.texture = texture;
    }

    @Override
    public void render(Shader shader) {
        super.render(shader);
        shader.setUniform("u_textured", 1);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        MESH.initRender();
        MESH.render();
        MESH.endRender();
    }

    @Override
    public void clear() {

    }

    @Override
    public void update(Observable caller, Object meta) {

    }
}
