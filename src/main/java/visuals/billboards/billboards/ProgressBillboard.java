package visuals.billboards.billboards;

import engine.graphics.meshes.BillboardMesh;
import engine.graphics.renderer.Shader;
import engine.types.observables.Observable;
import org.joml.Vector3f;
import org.joml.Vector4f;
import visuals.billboards.Billboard;

import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;

public class ProgressBillboard extends Billboard {
    private final Observable<Integer> current, total;
    private final BillboardMesh mesh;

    public ProgressBillboard(boolean open, float x, float y, float z, float w, float h, Vector4f filter, Observable<Integer> current, Observable<Integer> total, Vector3f colour) {
        super(open, x, y, z, w, h, filter.w, filter.x, filter.y, filter.z);
        this.current = current;
        this.total = total;
        this.current.bindObserver(this);
        this.total.bindObserver(this);
        this.mesh = BillboardMesh.BUILDER
                .point(-1, -1, 1).color(colour.x, colour.y, colour.z)
                .point(1, -1, 1).color(colour.x, colour.y, colour.z)
                .point(1, 1, 1).color(colour.x, colour.y, colour.z)
                .point(-1, 1, 1).color(colour.x, colour.y, colour.z)
                .quad(0, 1, 2, 3)
                .build(GL_STATIC_DRAW, 3);
    }

    @Override
    public void render(Shader shader) {
        super.render(shader);
        shader.setUniform("u_textured", 0);
        mesh.initRender();
        mesh.render();
        mesh.endRender();
    }

    @Override
    public void clear() {
        this.mesh.clear();
        this.current.removeObserver(this);
        this.total.removeObserver(this);
    }

    @Override
    public void update(Observable caller, Object meta) {
        final float length = (current.value().floatValue() / total.value() - 0.5f) * 2;
        this.mesh.updatePositions(2 * Float.BYTES, new float[] {length, -1.f, length, 1.f});
    }
}
