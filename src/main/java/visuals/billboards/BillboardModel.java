package visuals.billboards;

import org.joml.Vector4f;

public abstract class BillboardModel {
    protected final float x, y, z, w, h;
    protected final Vector4f filter;
    protected final byte flags;

    public BillboardModel(float x, float y, float z, float w, float h, Vector4f filter, byte flags) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.filter = filter;
        this.flags = flags;
    }

    public abstract Billboard unwrap(Object[] blanks);
}
