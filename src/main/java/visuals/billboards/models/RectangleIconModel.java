package visuals.billboards.models;

import constants.BillboardStatus;
import org.joml.Vector4f;
import visuals.billboards.Billboard;
import visuals.billboards.BillboardModel;
import visuals.billboards.billboards.RectangleIconBillboard;

public class RectangleIconModel extends BillboardModel {
    private final int texture;

    public RectangleIconModel(float x, float y, float z, float w, float h, Vector4f filter, byte flags, int texture) {
        super(x, y, z, w, h, filter, flags);
        this.texture = texture;
    }

    @Override
    public Billboard unwrap(Object[] blanks) {
        return new RectangleIconBillboard(
                BillboardStatus.checkStatus(flags),
                x, y, z, w, h, filter,
                texture
        );
    }
}
