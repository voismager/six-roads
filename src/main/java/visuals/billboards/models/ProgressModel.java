package visuals.billboards.models;

import constants.BillboardStatus;
import engine.types.observables.Observable;
import org.joml.Vector3f;
import org.joml.Vector4f;
import visuals.billboards.Billboard;
import visuals.billboards.BillboardModel;
import visuals.billboards.billboards.ProgressBillboard;
import visuals.templates.UpdatableModel;

public class ProgressModel extends BillboardModel {
    private final UpdatableModel<Integer> current, total;
    private final Vector3f color;

    public ProgressModel(float x, float y, float z, float w, float h, Vector4f filter, byte flags, UpdatableModel<Integer> current, UpdatableModel<Integer> total, Vector3f color) {
        super(x, y, z, w, h, filter, flags);
        this.current = current;
        this.total = total;
        this.color = color;
    }

    @Override
    public Billboard unwrap(Object[] blanks) {
        Object[] vars = new Object[0];
        Observable<Integer> current = this.current.unpack(blanks, vars);
        Observable<Integer> total = this.total.unpack(blanks, vars);

        return new ProgressBillboard(
                BillboardStatus.checkStatus(flags),
                x, y, z, w, h, filter,
                current, total,
                color
        );
    }
}
