package visuals.billboards;

import engine.graphics.renderer.Shader;
import engine.types.observables.Observer;

public abstract class Billboard implements Observer {
    protected float x, y, z, w, h;
    protected float alpha;
    protected float r, g, b;
    private boolean open;

    public Billboard(boolean open, float x, float y, float z, float w, float h, float alpha, float r, float g, float b) {
        this.open = open;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.alpha = alpha;
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public void render(Shader shader) {
        shader.setUniform("u_center", x, y, z);
        shader.setUniform("u_size", w, h);
        shader.setUniform("u_filter", r, g, b, alpha);
    }

    public void setFilter(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public boolean isOpen() {
        return open;
    }

    public void open() {
        this.open = true;
    }

    public void close() {
        this.open = false;
    }

    public void move(float dx, float dy, float dz) {
        x += dx;
        y += dy;
        z += dz;
    }

    public abstract void clear();
}
