package visuals.drawable;

import engine.types.observables.Observable;
import visuals.Drawable;

public class Ref implements Drawable {
    private Observable<Drawable> val0;

    public Ref(Observable val0) {
        this.val0 = val0;
    }

    @Override
    public boolean draw() {
        return val0.value().draw();
    }

    @Override
    public boolean draw(int imguiId) {
        return val0.value().draw();
    }

    @Override
    public void cleanup() {
        this.val0.removeObserver(this);
        this.val0 = null;
    }

    @Override
    public void update(Observable caller, Object meta) {
    }
}
