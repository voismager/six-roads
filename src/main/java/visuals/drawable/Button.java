package visuals.drawable;

import glm_.vec2.Vec2;
import imgui.ImGui;
import engine.types.observables.Observable;
import visuals.Drawable;

public class Button implements Drawable {
    private Observable val0;

    private Vec2 size = new Vec2();

    public Button(Observable arg) {
        this.val0 = arg;
    }

    @Override
    public boolean draw() {
        return ImGui.INSTANCE.button(val0.value().toString(), size);
    }

    @Override
    public boolean draw(int imguiId) {
        return ImGui.INSTANCE.button(val0.value().toString() + "##" + imguiId, size);
    }

    @Override
    public void cleanup() {
        this.val0.removeObserver(this);
        this.val0 = null;
        this.size = null;
    }

    @Override
    public String toString() {
        return "Button{" +
                "val0=" + val0 +
                ", size=" + size +
                '}';
    }

    @Override
    public void update(Observable caller, Object meta) {
        //ImGui does it for us
    }
}
