package visuals.drawable;

import engine.types.observables.Observable;
import visuals.Drawable;

public class Group implements Drawable {
    private Drawable[] content;

    public Group(Drawable[] content) {
        this.content = content;
    }

    @Override
    public boolean draw() {
        boolean b = false;

        for (int i = 0; i < content.length; i++) {
            if (content[i].draw()) b = true;
        }

        return b;
    }

    @Override
    public boolean draw(int imguiId) {
        boolean b = false;

        for (int i = 0; i < content.length; i++) {
            if (content[i].draw(imguiId)) b = true;
        }

        return b;
    }

    @Override
    public void cleanup() {
        for (int i = 0; i < content.length; i++) {
            content[i].cleanup();
            content[i] = null;
        }

        content = null;
    }

    @Override
    public void update(Observable caller, Object meta) {

    }
}
