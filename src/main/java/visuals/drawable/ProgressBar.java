package visuals.drawable;

import glm_.vec2.Vec2;
import imgui.ImGui;
import engine.types.observables.Observable;
import visuals.Drawable;

public class ProgressBar implements Drawable {
    private Observable<Integer> num0, num1;

    private Vec2 imguiStuff0 = new Vec2();

    public ProgressBar(Observable arg0, Observable arg1) {
        this.num0 = arg0;
        this.num1 = arg1;
    }

    @Override
    public boolean draw() {
        int n0 = num0.value();
        int n1 = num1.value();
        float r = (float)n0 / n1;
        ImGui.INSTANCE.progressBar(r, imguiStuff0, num0.value() + " / " + num1.value());
        return false;
    }

    @Override
    public boolean draw(int imguiId) {
        int n0 = num0.value();
        int n1 = num1.value();
        float r = (float)n0 / n1;
        ImGui.INSTANCE.progressBar(r, imguiStuff0, num0.value() + " / " + num1.value());
        return false;
    }

    @Override
    public void cleanup() {
        this.num0.removeObserver(this);
        this.num1.removeObserver(this);
        this.num0 = null;
        this.num1 = null;
        this.imguiStuff0 = null;
    }

    @Override
    public void update(Observable caller, Object meta) {
        //ImGui does it for us
    }
}
