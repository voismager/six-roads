package visuals.drawable;

import engine.types.observables.Observable;
import imgui.ImGui;
import visuals.Drawable;

public class Tooltip implements Drawable {
    private Observable[] values;

    public Tooltip(Observable[] arguments) {
        this.values = arguments;
    }

    @Override
    public boolean draw() {
        ImGui.INSTANCE.beginTooltip();

        for (int i = 0; i < values.length; i++)
            ImGui.INSTANCE.text(values[i].value().toString());

        ImGui.INSTANCE.endTooltip();
        return false;
    }

    @Override
    public boolean draw(int imguiId) {
        ImGui.INSTANCE.beginTooltip();

        for (int i = 0; i < values.length; i++)
            ImGui.INSTANCE.text(values[i].value().toString());

        ImGui.INSTANCE.endTooltip();
        return false;
    }

    @Override
    public void cleanup() {
        for (int i = 0; i < values.length; i++) {
            values[i].removeObserver(this);
            values[i] = null;
        }

        values = null;
    }

    @Override
    public void update(Observable caller, Object meta) {
        //ImGui does it for us
    }
}
