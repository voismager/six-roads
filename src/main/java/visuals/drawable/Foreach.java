package visuals.drawable;

import engine.types.observables.Observable;
import engine.structures.IntArray;
import visuals.Drawable;
import visuals.templates.DrawableModel;
import engine.support.keys.Mapper;

import java.util.ArrayList;
import java.util.List;

import static engine.types.observables.Observable.*;

public class Foreach implements Drawable {
    private List<Drawable> content;
    private Observable<IntArray> observableList;
    private DrawableModel branch;
    private Object[] blanks;

    public Foreach(Observable<IntArray> observableList, DrawableModel branch, Object[] blanks) {
        this.observableList = observableList;
        this.branch = branch;
        this.blanks = blanks;

        observableList.bindObserver(this);

        IntArray list = observableList.value();

        this.content = new ArrayList<>(list.size());

        Object[] variables = new Object[1];

        for (int i = 0; i < list.size(); i++) {
            variables[0] = list.get(i);
            content.add(branch.unpack(blanks, variables));
        }
    }

    public Foreach(Observable<IntArray> observableList, DrawableModel branch) {
        this.observableList = observableList;
        this.branch = branch;

        observableList.bindObserver(this);

        IntArray list = observableList.value();
        int len = list.size();

        this.content = new ArrayList<>(len);

        Object[] variables = new Object[1];

        for (int i = 0; i < len; i++) {
            variables[0] = list.get(i);
            content.add(branch.unpack(variables));
        }
    }

    @Override
    public boolean draw() {
        boolean b = false;

        for (int i = 0; i < content.size(); i++) {
            if (content.get(i).draw()) b = true;
        }

        return b;
    }

    @Override
    public boolean draw(int imguiId) {
        return content.get(imguiId).draw(imguiId);
    }

    @Override
    public void cleanup() {
        observableList.removeObserver(this);

        for (int i = 0; i < content.size(); i++) {
            content.get(i).cleanup();
        }

        content.clear();
        content = null;
        branch = null;
        blanks = null;
    }

    @Override
    public void update(Observable caller, Object meta) {
        int m = (int) meta;

        switch (Mapper.TD01_I_B(m)) {
            case SET_ALL_CODE:
                IntArray list0 = observableList.value();

                for (int i = 0; i < content.size(); i++)
                    content.get(i).cleanup();

                content.clear();

                Object[] vars0 = new Object[1];

                if (null == blanks) {
                    for (int i = 0; i < list0.size(); i++) {
                        vars0[0] = list0.get(i);
                        content.add(branch.unpack(vars0));
                    }
                }

                else {
                    for (int i = 0; i < list0.size(); i++) {
                        vars0[0] = list0.get(i);
                        content.add(branch.unpack(blanks, vars0));
                    }
                }

                break;

            case SET_CODE:
                IntArray list1 = observableList.value();
                int index0 = Mapper.TD01_I_S(m);

                content.get(index0).cleanup();

                Object[] vars1 = new Object[] { list1.get(index0) };
                
                if (null == blanks) {
                    content.set(index0, branch.unpack(vars1));
                }

                else {
                    content.set(index0, branch.unpack(blanks, vars1));
                }

                break;

            case ADD_CODE:
                IntArray list2 = observableList.value();

                Object[] vars2 = new Object[] { list2.get(list2.size() - 1) };

                if (null == blanks) {
                    content.add(branch.unpack(vars2));
                }

                else {
                    content.add(branch.unpack(blanks, vars2));
                }

                break;

            case REMOVE_CODE:
                int index1 = Mapper.TD01_I_S(m);
                content.remove(index1).cleanup();

                break;
        }
    }
}
