package visuals.drawable;

import imgui.ImGui;
import engine.types.observables.Observable;
import visuals.Drawable;

public class TextField implements Drawable {
    private Observable val0;

    public TextField(Observable arg) {
        this.val0 = arg;
    }

    @Override
    public boolean draw() {
        ImGui.INSTANCE.text(val0.value().toString());
        return false;
    }

    @Override
    public boolean draw(int imguiId) {
        ImGui.INSTANCE.text(val0.value().toString());
        return false;
    }

    @Override
    public void cleanup() {
        this.val0.removeObserver(this);
        this.val0 = null;
    }

    @Override
    public void update(Observable caller, Object meta) {
        //ImGui does it for us
    }
}
