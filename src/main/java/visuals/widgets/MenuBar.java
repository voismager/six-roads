package visuals.widgets;

import imgui.ImGui;
import common.Entity;
import visuals.Drawable;
import visuals.Widget;

import java.util.Arrays;

public class MenuBar extends Widget {
    private Drawable[] content;
    private Entity[] linked;
    private int size;
    private Entity selected;

    public MenuBar(short id, Drawable[] content, Entity[] linked) {
        super(id);
        this.content = content;
        this.linked = linked;
        this.size = content.length;
    }

    @Override
    public void add(Entity entity, Drawable drawable, int flags) {
        if ((flags & REPLACE_UNIQUE_ENTITY) == REPLACE_UNIQUE_ENTITY) {
            for (int i = 0; i < size; i++) {
                if (linked[i] == entity) {
                    content[i] = drawable;
                    return;
                }
            }
        }

        if ((flags & UNIQUE_ENTITY) == UNIQUE_ENTITY) {
            for (int i = 0; i < size; i++) {
                if (linked[i] == entity) {
                    return;
                }
            }
        }

        if (size == content.length) {
            int newLength = content.length * 2 + 1;
            content = Arrays.copyOf(content, newLength);
            linked = Arrays.copyOf(linked, newLength);
        }

        content[size] = drawable;
        linked[size] = entity;
        size++;
    }

    @Override
    public void remove(Entity entity) {
        for (int i = 0; i < size; i++) {
            if (linked[i] == entity) {
                content[i].cleanup();
                System.arraycopy(content, i + 1, content, i, size - 1 - i);
                System.arraycopy(linked, i + 1, linked, i, size - 1 - i);
                size--;
                i--;
            }
        }
    }

    @Override
    public void apply(int flags) {

    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            Drawable el = content[i];
            content[i] = null;

            el.cleanup();
        }

        size = 0;
    }

    @Override
    public void draw() {
        ImGui imGui = ImGui.INSTANCE;
        selected = null;

        if (imGui.beginMainMenuBar()) {
            for (int i = 0; i < size; i++) {
                content[i].draw();

                if (imGui.isItemClicked(0)) {
                    selected = linked[i];
                }
            }

            imGui.endMainMenuBar();
        }
    }

    @Override
    public Entity selected() {
        return selected;
    }
}
