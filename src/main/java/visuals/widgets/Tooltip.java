package visuals.widgets;

import imgui.ImGui;
import common.Entity;
import visuals.Drawable;
import visuals.Widget;

import java.util.Arrays;

public class Tooltip extends Widget {
    private Drawable[] content;
    private int size;

    public Tooltip(short id, Drawable[] content) {
        super(id);
        this.content = content;
        this.size = content.length;
    }

    @Override
    public void add(Entity entity, Drawable drawable, int flags) {
        if (size == content.length)
            content = Arrays.copyOf(content, content.length * 2 + 1);

        content[size++] = drawable;
    }

    @Override
    public void remove(Entity entity) {

    }

    @Override
    public void apply(int flags) {

    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            Drawable el = content[i];
            content[i] = null;
            el.cleanup();
        }

        size = 0;
    }

    @Override
    public void draw() {
        ImGui imGui = ImGui.INSTANCE;

        imGui.beginTooltip();

        for (int i = 0; i < content.length; i++) {
            content[i].draw();
        }

        imGui.endTooltip();
    }

    @Override
    public Entity selected() {
        return null;
    }
}
