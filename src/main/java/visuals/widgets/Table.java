package visuals.widgets;

import engine.structures.ObjArray;
import glm_.vec2.Vec2;
import imgui.ImGui;
import imgui.WindowFlag;
import common.Entity;
import visuals.Drawable;
import visuals.Widget;

import java.util.Arrays;

public class Table extends Widget {
    private ObjArray<Drawable>[] content;
    private Entity[] linked;
    private int size;
    private Entity selected;

    private final String label;
    private String[] columns;
    private boolean[] pOpen;
    private int flags;
    private Vec2 pixSize;

    @SuppressWarnings("unchecked")
    public Table(short id, String label, float width, float height, String[] columns, Drawable[] content, Entity[] linked) {
        super(id);
        this.content = new ObjArray[content.length];
        this.linked = new Entity[content.length / columns.length];

        for (int i = 0; i < content.length; i++)
            this.add(linked[i], content[i], NEXT_COLUMN);

        this.label = label;
        this.columns = columns;
        this.pOpen = new boolean[] { true };
        this.flags = WindowFlag.AlwaysAutoResize.or(WindowFlag.NoCollapse);
        this.pixSize = new Vec2(width, height);
    }

    @Override
    public void add(Entity entity, Drawable drawable, int flags) {
        if ((flags & NEXT_COLUMN) == NEXT_COLUMN) {
            ObjArray<Drawable> column = new ObjArray<>(1);
            column.add(drawable);

            if (size == content.length) {
                int newLength = linked.length * 2 + 1;
                content = Arrays.copyOf(content, newLength * columns.length);
                linked = Arrays.copyOf(linked, newLength);
            }

            content[size] = column;
            linked[size / columns.length] = entity;
            size++;
        }

        else {
            content[size - 1].add(drawable);
            linked[(size - 1) / columns.length] = entity;
        }
    }

    @Override
    public void remove(Entity entity) {
        for (int i = 0; i < size; i++) {
            if (linked[i] == entity) {
                ObjArray<Drawable> a = content[i];

                for (int j = 0; j < a.size(); j++) {
                    Drawable el = a.get(j);
                    el.cleanup();
                }

                a.deepClear();

                System.arraycopy(content, i + 1, content, i, size - 1 - i);
                System.arraycopy(linked, i + 1, linked, i, size - 1 - i);
                size--;
                i--;
            }
        }
    }

    @Override
    public void apply(int flags) {
        if ((flags & NEXT_COLUMN) == NEXT_COLUMN) {
            if (size == content.length) {
                int newLength = linked.length * 2 + 1;
                content = Arrays.copyOf(content, newLength * columns.length);
                linked = Arrays.copyOf(linked, newLength);
            }

            content[size] = new ObjArray<>(1);
            size++;
        }
    }

    @Override
    public void draw() {
        ImGui imGui = ImGui.INSTANCE;
        selected = null;

        imGui.setNextWindowContentSize(pixSize);

        if (imGui.begin(label, pOpen, flags)) {
            imGui.beginColumns(label, columns.length, 0);

            for (int i = 0; i < columns.length; i++) {
                imGui.text(columns[i]);
                imGui.nextColumn();
            }

            imGui.separator();

            int currIndex = 0;
            int currColumn = 0;

            while (currIndex != size) {
                while (currColumn != columns.length) {
                    for (int i = 0; i < content[currIndex].size(); i++) {
                        if (content[currIndex].get(i).draw(currIndex)) {
                            selected = linked[currIndex / columns.length];
                        }
                    }

                    imGui.nextColumn();
                    currColumn++;
                    currIndex++;
                }

                currColumn = 0;
                imGui.separator();
            }

            imGui.endColumns();
            imGui.end();
        }
    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            ObjArray<Drawable> a = content[i];
            content[i] = null;

            for (int j = 0; j < a.size(); j++) {
                Drawable el = a.get(j);
                el.cleanup();
            }

            a.deepClear();
        }

        size = 0;
    }

    @Override
    public Entity selected() {
        return selected;
    }
}
