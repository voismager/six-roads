package visuals.widgets;

import glm_.vec2.Vec2;
import glm_.vec4.Vec4;
import imgui.HoveredFlag;
import imgui.ImGui;
import imgui.WindowFlag;
import common.Entity;
import visuals.Drawable;
import visuals.Widget;

import java.util.ArrayList;
import java.util.List;

public class Graph extends Widget {
    private boolean[] opened = new boolean[1];

    Vec2 scrolling = new Vec2(0.0f, 0.0f);

    static class NodeK {
        int ID;
        String name;
        Vec2 Pos, Size = ImGui.INSTANCE.getItemRectSize();

        public NodeK(int ID, String name, Vec2 pos) {
            this.ID = ID;
            this.name = name;
            Pos = pos;
        }

        Vec2 GetInputSlotPos() {
            return new Vec2((float) Pos.getX(), Pos.getY() + Size.getY() * ((float)1) / 2);
        }

        Vec2 GetOutputSlotPos() {
            return new Vec2(Pos.getX() + Size.getX(), Pos.getY() + Size.getY() * ((float)1) / 2);
        }
    }

    static class NodeLink {
        int InputIdx, OutputIdx;

        NodeLink(int input_idx, int output_idx) {
            InputIdx = input_idx; OutputIdx = output_idx;
        }
    }

    List<NodeK> nodes = new ArrayList<>();
    List<NodeLink> links = new ArrayList<>();

    public Graph(short id, Drawable[] content, Entity[] linked) {
        super(id);
        nodes.add(new NodeK(0, "MainTex", new Vec2(40, 50)));
        nodes.add(new NodeK(1, "BumpMap", new Vec2(40, 150)));
        nodes.add(new NodeK(2, "Combine", new Vec2(270, 80)));
        links.add(new NodeLink(0, 2));
        links.add(new NodeLink(1, 2));
    }

    @Override
    public void clear() {

    }

    @Override
    public void draw() {
        ImGui i = ImGui.INSTANCE;

        if (i.begin("name", opened, 0)) {
            Vec2 offset = i.getCursorScreenPos().plus(scrolling);

            i.beginChild("scrolling_region", new Vec2(0, 0), true, WindowFlag.NoScrollbar.or(WindowFlag.NoMove));
            i.pushItemWidth(120.0f);

            for (int link_idx = 0; link_idx < links.size(); link_idx++) {
                NodeLink link = links.get(link_idx);
                NodeK node_inp = nodes.get(link.InputIdx);
                NodeK node_out = nodes.get(link.OutputIdx);

                Vec2 p1 = offset.plus(node_inp.GetOutputSlotPos());
                Vec2 p2 = offset.plus(node_out.GetInputSlotPos());

                i.getWindowDrawList().addLine(p1, p2, i.getColorU32(new Vec4(200, 200, 100, 255)), 1.0f);
            }

            for (int node_idx = 0; node_idx < nodes.size(); node_idx++) {
                NodeK node = nodes.get(node_idx);
                Vec2 node_rect_min = offset.plus(node.Pos);
                Vec2 node_rect_max = node_rect_min.plus(node.Size);

                i.setCursorScreenPos(node_rect_min);
                i.text("%s", node.name);

                i.getWindowDrawList().addRect(node_rect_min, node_rect_max, i.getColorU32(new Vec4(100, 100, 100, 255)), 0, 0, 1.0f);
            }

            if (i.isWindowHovered(HoveredFlag.Default) && !i.isAnyItemActive() && i.isMouseDragging(2, 0.0f))
                scrolling = scrolling.plus(i.getIo().getMouseDelta());

            i.end();
        }
    }

    @Override
    public Entity selected() {
        return null;
    }

    @Override
    public void add(Entity entity, Drawable drawable, int flags) {

    }

    @Override
    public void remove(Entity entity) {

    }

    @Override
    public void apply(int flags) {

    }
}
