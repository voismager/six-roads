package visuals.widgets;

import glm_.vec2.Vec2;
import imgui.Cond;
import imgui.ImGui;
import imgui.WindowFlag;
import common.Entity;
import visuals.Drawable;
import visuals.Widget;

import java.util.Arrays;

public class Popup extends Widget {
    private Drawable[] content;
    private Entity[] linked;
    private int size;
    private Entity selected;

    private String label;
    private int columns;
    private boolean[] pOpen;
    private int flags;
    private Vec2 pivot;

    public Popup(short id, String label, int columns, Drawable[] content, Entity[] linked) {
        super(id);
        this.content = content;
        this.linked = linked;
        this.size = content.length;

        this.label = label;
        this.columns = columns;
        this.pOpen = new boolean[] { true };
        this.flags = WindowFlag.AlwaysAutoResize.or(WindowFlag.NoCollapse);
        this.pivot = new Vec2();
    }

    @Override
    public void add(Entity entity, Drawable drawable, int flags) {
        if (size == content.length) {
            int newLength = content.length * 2 + 1;
            content = Arrays.copyOf(content, newLength);
            linked = Arrays.copyOf(linked, newLength);
        }

        content[size] = drawable;
        linked[size] = entity;

        size++;
    }

    @Override
    public void remove(Entity entity) {
        for (int i = 0; i < size; i++) {
            if (linked[i] == entity) {
                content[i].cleanup();
                System.arraycopy(content, i + 1, content, i, size - 1 - i);
                System.arraycopy(linked, i + 1, linked, i, size - 1 - i);
                size--;
                i--;
            }
        }
    }

    @Override
    public void apply(int flags) {

    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            Drawable el = content[i];
            content[i] = null;

            el.cleanup();
        }

        size = 0;
    }

    @Override
    public void draw() {
        ImGui imGui = ImGui.INSTANCE;
        selected = null;

        int i = 1;

        imGui.setNextWindowPos(imGui.getMousePos(), Cond.Appearing, pivot);

        if (imGui.begin(label, pOpen, flags)) {
            for (int j = 0; j < content.length; j++) {
                imGui.beginGroup();
                content[j].draw();

                if (i != columns) {
                    imGui.sameLine(0);
                    i++;
                }

                else i = 1;

                imGui.endGroup();

                if (imGui.isItemClicked(0)) {
                    selected = linked[j];
                }
            }

            imGui.end();
        }
    }

    @Override
    public Entity selected() {
        return selected;
    }
}
