package visuals.widgets;

import imgui.ImGui;
import imgui.WindowFlag;
import common.Entity;
import visuals.Drawable;
import visuals.Widget;

import java.util.Arrays;

public class HorizontalList extends Widget {
    private Drawable[] content;
    private Entity[] linked;
    private int size;
    private Entity selected;

    private String label;
    private boolean[] pOpen;
    private int flags;

    public HorizontalList(short id, String label, Drawable[] content, Entity[] linked) {
        super(id);
        this.content = content;
        this.linked = linked;
        this.size = content.length;

        this.label = label;
        this.pOpen = new boolean[] { true };
        this.flags = WindowFlag.Null.getI();
    }

    @Override
    public void add(Entity entity, Drawable drawable, int flags) {
        if ((flags & UNIQUE_ENTITY) == UNIQUE_ENTITY) {
            for (int i = 0; i < size; i++) {
                if (linked[i] == entity) {
                    return;
                }
            }
        }

        if (size == content.length) {
            int newLength = content.length * 2 + 1;
            content = Arrays.copyOf(content, newLength);
            linked = Arrays.copyOf(linked, newLength);
        }

        content[size] = drawable;
        linked[size] = entity;
        size++;
    }

    @Override
    public void remove(Entity entity) {
        for (int i = 0; i < size; i++) {
            if (linked[i] == entity) {
                content[i].cleanup();
                System.arraycopy(content, i + 1, content, i, size - 1 - i);
                System.arraycopy(linked, i + 1, linked, i, size - 1 - i);
                size--;
                i--;
            }
        }
    }

    @Override
    public void apply(int flags) {

    }

    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            Drawable el = content[i];
            content[i] = null;
            el.cleanup();
        }

        size = 0;
    }

    @Override
    public void draw() {
        ImGui imGui = ImGui.INSTANCE;
        selected = null;

        if (imGui.begin(label, pOpen, flags)) {
            for (int i = 0; i < size; i++) {
                imGui.sameLine(0);

                if (content[i].draw()) {
                    selected = linked[i];
                }
            }

            imGui.end();
        }
    }

    @Override
    public Entity selected() {
        return selected;
    }
}
