package visuals.functions;

import visuals.functions.privatefunctions.AutoStringGenerator;

import java.util.HashMap;
import java.util.Map;

public class Generators {
    private static Map<String, Generator> GENERATORS;
    private static Generator<String> AUTO_STRING;

    static {
        AUTO_STRING = new AutoStringGenerator();
        GENERATORS = new HashMap<>();
    }

    public static Generator<String> getAutoString() {
        return AUTO_STRING;
    }

    public static Generator getGenerator(String name) {
        return GENERATORS.get(name);
    }
}
