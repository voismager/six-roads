package visuals.functions;

import client.logic.ClEnvironment;
import engine.types.observables.Observable;

public interface Generator<T> {
    Observable<T> generate(ClEnvironment env, Observable[] arguments, Object... params);
}
