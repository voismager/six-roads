package visuals.functions.privatefunctions;

import engine.types.observables.Observer;
import client.logic.ClEnvironment;
import engine.types.observables.Observable;
import visuals.ObservableFunc;
import visuals.functions.Generator;

public class AutoStringGenerator implements Generator<String> {
    @Override
    public Observable<String> generate(ClEnvironment env, Observable[] arguments, Object[] params) {
        char[] body = (char[]) params[0];
        int[] positions = (int[]) params[1];
        return new Fun(arguments, body, positions);
    }

    private static class Fun extends ObservableFunc<String> {
        private Observable[] arguments;
        private char[] body;
        private int[] positions;

        private Fun(Observable[] arguments, char[] body, int[] positions) {
            this.arguments = arguments;
            this.body = body;
            this.positions = positions;

            for (Observable a : arguments)
                a.bindObserver(this);

            apply();
        }

        @Override
        protected void apply() {
            int length = body.length;

            for (int i = 0; i < positions.length; i++)
                length += arguments[i].value().toString().length();

            char[] output = new char[length];

            int i = 0, j = 0, k = 0;

            while (i != output.length) {
                if (j != positions.length && k == positions[j]) {
                    String arg = arguments[j].value().toString();
                    arg.getChars(0, arg.length(), output, i);
                    i += arg.length();
                    ++j;
                }

                else output[i++] = body[k++];
            }

            this.result = String.valueOf(output);
        }

        @Override
        public void removeObserver(Observer observer) {
            super.removeObserver(observer);

            for (int i = 0; i < arguments.length; i++) {
                arguments[i].removeObserver(this);
                arguments[i] = null;
            }

            arguments = null;
            body = null;
            positions = null;
        }
    }
}
