package visuals.templates.drawablemodels;

import visuals.Drawable;
import visuals.drawable.Button;
import visuals.templates.DrawableModel;
import visuals.templates.UpdatableModel;

public class ButtonModel implements DrawableModel {
    private final UpdatableModel arg;

    public ButtonModel(UpdatableModel arg) {
        this.arg = arg;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        return new Button(arg.unpack(variables));
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        return new Button(arg.unpack(blanks, variables));
    }
}
