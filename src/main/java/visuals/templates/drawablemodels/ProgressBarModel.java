package visuals.templates.drawablemodels;

import engine.types.observables.Observable;
import visuals.Drawable;
import visuals.drawable.ProgressBar;
import visuals.templates.DrawableModel;
import visuals.templates.UpdatableModel;

public class ProgressBarModel implements DrawableModel {
    private UpdatableModel arg0, arg1;

    public ProgressBarModel(UpdatableModel arg0, UpdatableModel arg1) {
        this.arg0 = arg0;
        this.arg1 = arg1;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        Observable a0 = arg0.unpack(variables);
        Observable a1 = arg1.unpack(variables);
        return new ProgressBar(a0, a1);
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        Observable a0 = arg0.unpack(blanks, variables);
        Observable a1 = arg1.unpack(blanks, variables);
        return new ProgressBar(a0, a1);
    }
}
