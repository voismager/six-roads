package visuals.templates.drawablemodels;

import visuals.Constant;
import visuals.Drawable;
import visuals.templates.DrawableModel;

public class ConstantTemplate implements DrawableModel {
    private final Constant constant;

    public ConstantTemplate(Constant constant) {
        this.constant = constant;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        return constant;
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        return constant;
    }
}
