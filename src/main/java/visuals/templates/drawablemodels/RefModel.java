package visuals.templates.drawablemodels;

import visuals.Drawable;
import visuals.drawable.Ref;
import visuals.templates.DrawableModel;
import visuals.templates.UpdatableModel;

public class RefModel implements DrawableModel {
    private UpdatableModel arg;

    public RefModel(UpdatableModel arg) {
        this.arg = arg;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        return new Ref(arg.unpack(variables));
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        return new Ref(arg.unpack(blanks, variables));
    }
}
