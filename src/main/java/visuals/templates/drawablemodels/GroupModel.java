package visuals.templates.drawablemodels;

import visuals.Drawable;
import visuals.drawable.Group;
import visuals.templates.DrawableModel;

public class GroupModel implements DrawableModel {
    private DrawableModel[] elements;

    public GroupModel(DrawableModel[] elements) {
        this.elements = elements;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        Drawable[] a = new Drawable[elements.length];

        for (int i = 0; i < elements.length; i++)
            a[i] = elements[i].unpack(variables);

        return new Group(a);
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        Drawable[] a = new Drawable[elements.length];

        for (int i = 0; i < elements.length; i++)
            a[i] = elements[i].unpack(blanks, variables);

        return new Group(a);
    }
}
