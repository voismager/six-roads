package visuals.templates.drawablemodels;

import engine.structures.IntArray;
import visuals.Drawable;
import visuals.drawable.Foreach;
import visuals.templates.DrawableModel;
import visuals.templates.UpdatableModel;

public class ForeachModel implements DrawableModel {
    private final UpdatableModel<IntArray> list;
    private final DrawableModel branch;

    public ForeachModel(UpdatableModel list, DrawableModel branch) {
        this.list = list;
        this.branch = branch;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        return new Foreach(list.unpack(variables), branch);
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        return new Foreach(list.unpack(blanks, variables), branch, blanks);
    }
}
