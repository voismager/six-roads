package visuals.templates.drawablemodels;

import engine.types.observables.Observable;
import visuals.Drawable;
import visuals.drawable.Tooltip;
import visuals.templates.DrawableModel;
import visuals.templates.UpdatableModel;

public class TooltipModel implements DrawableModel {
    private UpdatableModel[] args;

    public TooltipModel(UpdatableModel[] args) {
        this.args = args;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        Observable[] a = new Observable[args.length];

        for (int i = 0; i < args.length; i++) {
            a[i] = args[i].unpack(variables);
        }

        return new Tooltip(a);
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        Observable[] a = new Observable[args.length];

        for (int i = 0; i < args.length; i++) {
            a[i] = args[i].unpack(blanks, variables);
        }

        return new Tooltip(a);
    }
}
