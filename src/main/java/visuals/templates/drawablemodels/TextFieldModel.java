package visuals.templates.drawablemodels;

import visuals.Drawable;
import visuals.drawable.TextField;
import visuals.templates.DrawableModel;
import visuals.templates.UpdatableModel;

public class TextFieldModel implements DrawableModel {
    private UpdatableModel arg;

    public TextFieldModel(UpdatableModel arg) {
        this.arg = arg;
    }

    @Override
    public Drawable unpack(Object[] variables) {
        return new TextField(arg.unpack(variables));
    }

    @Override
    public Drawable unpack(Object[] blanks, Object[] variables) {
        return new TextField(arg.unpack(blanks, variables));
    }
}
