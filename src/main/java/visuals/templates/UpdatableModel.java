package visuals.templates;

import engine.types.observables.Observable;

public interface UpdatableModel<T> {
    Observable<T> unpack(Object[] variables);

    Observable<T> unpack(Object[] blanks, Object[] variables);
}
