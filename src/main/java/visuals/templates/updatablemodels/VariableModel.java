package visuals.templates.updatablemodels;

import engine.types.observables.Observable;
import visuals.Constant;
import visuals.templates.UpdatableModel;

public class VariableModel implements UpdatableModel {
    private final int index;

    public VariableModel(int index) {
        this.index = index;
    }

    @Override
    public Observable unpack(Object[] variables) {
        return new Constant<>(variables[index]);
    }

    @Override
    public Observable unpack(Object[] blanks, Object[] variables) {
        return new Constant<>(variables[index]);
    }
}
