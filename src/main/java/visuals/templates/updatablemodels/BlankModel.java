package visuals.templates.updatablemodels;

import engine.types.observables.Observable;
import visuals.Constant;
import visuals.templates.UpdatableModel;

public class BlankModel implements UpdatableModel {
    private final int index;

    public BlankModel(int index) {
        this.index = index;
    }

    @Override
    public Observable unpack(Object[] variables) {
        return new Constant<>("BLANK");
    }

    @Override
    public Observable unpack(Object[] blanks, Object[] variables) {
        int index;

        if (this.index >= blanks.length) {
            index = blanks.length - 1;
        } else index = this.index;

        Object blank = blanks[index];

        if (blank instanceof Observable)
            return (Observable) blank;

        return new Constant<>(blank);
    }
}
