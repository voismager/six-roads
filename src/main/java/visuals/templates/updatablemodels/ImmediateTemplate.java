package visuals.templates.updatablemodels;

import engine.types.observables.Observable;
import visuals.templates.UpdatableModel;

public class ImmediateTemplate implements UpdatableModel {
    private final Observable result;

    public ImmediateTemplate(Observable result) {
        this.result = result;
    }

    @Override
    public Observable unpack(Object[] variables) {
        return result;
    }

    @Override
    public Observable unpack(Object[] blanks, Object[] variables) {
        return result;
    }
}
