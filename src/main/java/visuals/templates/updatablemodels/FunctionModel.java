package visuals.templates.updatablemodels;

import client.logic.ClEnvironment;
import engine.types.observables.Observable;
import visuals.functions.Generator;
import visuals.templates.UpdatableModel;

public class FunctionModel implements UpdatableModel {
    private final ClEnvironment env;
    private final Generator src;
    private final UpdatableModel[] args;
    private final Object[] params;

    public FunctionModel(ClEnvironment env, Generator src, UpdatableModel[] args, Object... params) {
        this.env = env;
        this.src = src;
        this.args = args;
        this.params = params;
    }

    @Override
    public Observable unpack(Object[] variables) {
        Observable[] unpackedArgs = new Observable[args.length];

        for (int i = 0; i < args.length; i++)
            unpackedArgs[i] = args[i].unpack(variables);

        return src.generate(env, unpackedArgs, params);
    }

    @Override
    public Observable unpack(Object[] blanks, Object[] variables) {
        Observable[] unpackedArgs = new Observable[args.length];

        for (int i = 0; i < args.length; i++)
            unpackedArgs[i] = args[i].unpack(blanks, variables);

        return src.generate(env, unpackedArgs, params);
    }
}
