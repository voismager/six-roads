package visuals.templates;

import visuals.Drawable;

public interface DrawableModel {
    Drawable unpack(Object[] variables);

    Drawable unpack(Object[] blanks, Object[] variables);
}
