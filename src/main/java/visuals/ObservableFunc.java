package visuals;

import engine.types.observables.Observable;
import engine.types.observables.Observer;

public abstract class ObservableFunc<T> implements Observer, Observable<T> {
    protected Observer observer;
    protected T result;

    protected abstract void apply();

    @Override
    public void bindObserver(Observer observer) {
        this.observer = observer;
    }

    @Override
    public void removeObserver(Observer observer) {
        this.observer = null;
        this.result = null;
    }

    @Override
    public T value() {
        return result;
    }

    @Override
    public void update(Observable caller, Object meta) {
        apply();

        if (observer != null)
            observer.update(this, meta);
    }

    @Override
    public void cleanup() {
        this.observer = null;
        this.result = null;
    }
}
