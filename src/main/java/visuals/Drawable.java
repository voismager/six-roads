package visuals;

import engine.Cleanable;
import engine.types.observables.Observer;

public interface Drawable extends Observer, Cleanable {
    boolean draw();

    boolean draw(int imguiId);
}
