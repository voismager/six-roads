package visuals;

import engine.types.observables.Observable;
import engine.types.observables.Observer;
import imgui.ImGui;

public class Constant<T> implements Drawable, Observable<T> {
    private final T value;

    public Constant(T value) {
        this.value = value;
    }

    @Override
    public boolean draw() {
        ImGui.INSTANCE.text(value.toString());
        return false;
    }

    @Override
    public boolean draw(int imguiId) {
        ImGui.INSTANCE.text(value.toString());
        return false;
    }

    @Override
    public String toString() {
        return "Constant{" +
                "value=" + value +
                '}';
    }

    @Override
    public void cleanup() { }

    @Override
    public T value() {
        return value;
    }

    @Override
    public void removeObserver(Observer o) { }

    @Override
    public void bindObserver(Observer o) { }

    @Override
    public void update(Observable caller, Object meta) { }
}
