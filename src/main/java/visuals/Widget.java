package visuals;

import common.Entity;

public abstract class Widget {
    public static final int DEFAULT     = 1 << 0;
    public static final int NEXT_COLUMN = 1 << 1;
    public static final int UNIQUE_ENTITY = 1 << 2;
    public static final int REPLACE_UNIQUE_ENTITY = 1 << 3;

    private final short id;
    private int index;

    public Widget(short id) {
        this.id = id;
        this.index = -1;
    }

    public short getId() {
        return id;
    }

    public boolean isOpened() {
        return index != -1;
    }

    public int getIndex() {
        return index;
    }

    public void close() {
        this.index = -1;
    }

    public boolean isClicked() {
        return selected() != null;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public abstract void clear();

    public abstract void draw();

    public abstract Entity selected();

    public abstract void add(Entity entity, Drawable drawable, int flags);

    public abstract void remove(Entity entity);

    public abstract void apply(int flags);
}