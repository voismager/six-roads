package visuals.visualparser;

class ModelLoader {
    /*
    static void load(Module module, NodeList modelList, UnifiedResource resource) throws Exception {
        final UnifiedResource modelsDirectory = resource.getChild("models/");
        final UnifiedResource texturesDirectory = resource.getChild("textures/");

        for (int i = 0; i < modelList.getLength(); i++) {
            Element shadow = (Element) modelList.item(i);

            EntityType type = EntityType.valueOf(shadow.getAttribute("class").toUpperCase());

            Entity realEntity = module
                    .getMap(type)
                    .get(Short.parseShort(shadow.getAttribute("id")));

            if (type == EntityType.TERRAIN) {
                processTerrain(realEntity, shadow);
            } else if (type == EntityType.AREA) {
                processArea(realEntity, shadow, modelsDirectory, texturesDirectory);
            } else if (type == EntityType.UNIT) {
                processUnit(realEntity, shadow, modelsDirectory, texturesDirectory);
            }
        }
    }

    private static void processUnit(Entity entity, Element element, UnifiedResource models, UnifiedResource textures) throws Exception {
        MaterialInstancedMesh[] meshes = loadMeshes(element, models, textures.getPath());

        VisibleHexagons.put(new VisibleUnit(meshes), entity.id());
    }

    private static void processArea(Entity entity, Element element, UnifiedResource models, UnifiedResource textures) throws Exception {
        MaterialInstancedMesh[] meshes = loadMeshes(element, models, textures.getPath());
        boolean randomRotation;
        float height;

        NodeList randomRotationObj = element.getElementsByTagName("randomRotation");

        randomRotation = randomRotationObj.getLength() > 0 && randomRotationObj.item(0).getTextContent().equals("true");

        NodeList heightObj = element.getElementsByTagName("height");

        if (heightObj.getLength() > 0) {
            height = Float.parseFloat(heightObj.item(0).getTextContent());
        } else {
            height = 0.0f;
        }

        VisibleHexagons.put(new VisibleArea(meshes, randomRotation, height), entity.id());
    }

    private static void processTerrain(Entity entity, Element element) {
        Vector3f color = parseVector3f(element.getElementsByTagName("color").item(0).getTextContent());
        Vector3f specular = parseVector3f(element.getElementsByTagName("specular").item(0).getTextContent());
        float reflectance = Float.parseFloat(element.getElementsByTagName("reflectance").item(0).getTextContent());

        VisibleHexagons.put(new VisibleTerrain(
                HexUtil.getTerrainMesh(color, specular, reflectance), color),
                entity.id()
        );
    }

    private static Vector3f parseVector3f(String s) {
        String[] arr = s.replaceAll("\\s", "").split(",");

        if (arr.length != 3) throw new IllegalArgumentException();

        float x = Float.parseFloat(arr[0]);
        float y = Float.parseFloat(arr[1]);
        float z = Float.parseFloat(arr[2]);
        return new Vector3f(x, y, z);
    }

    private static MaterialInstancedMesh[] loadMeshes(Element element, UnifiedResource models, String textures) throws Exception {
        NodeList objObj = element.getElementsByTagName("obj");

        if (objObj.getLength() > 0) {
            return StaticMeshLoader.load(
                    models.getChild(objObj.item(0).getTextContent()),
                    textures, 100
            );
        } else {
            return new MaterialInstancedMesh[0];
        }
    }

    private static Vector4f parseVector4f(String s) {
        String[] arr = s.replaceAll("\\s", "").split(",");

        if (arr.length != 4) throw new IllegalArgumentException();

        float x = Float.parseFloat(arr[0]);
        float y = Float.parseFloat(arr[1]);
        float z = Float.parseFloat(arr[2]);
        float w = Float.parseFloat(arr[3]);
        return new Vector4f(x, y, z, w);
    }

     */
}
