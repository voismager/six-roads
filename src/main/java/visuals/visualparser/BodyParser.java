package visuals.visualparser;

import client.logic.ClEnvironment;
import engine.support.Tools;
import engine.types.observables.Observable;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import visuals.Constant;
import visuals.functions.Generators;
import visuals.templates.UpdatableModel;
import visuals.templates.updatablemodels.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

class BodyParser {
    private static final Pattern DELIMITER_PATTERN = Pattern.compile("[(),.]");
    private static Iterator<String> tokens;
    private static int indexCounter;

    public static UpdatableModel parse(String body, List<String> variables, ClEnvironment env) {
        indexCounter = -1;

        char[] chars = body.toCharArray();

        ObjectList<UpdatableModel> references = new ObjectArrayList<>();
        IntList positions = new IntArrayList();

        StringBuilder builder = new StringBuilder();
        StringBuilder refBuilder = new StringBuilder();

        int refCount = 0;

        for (char next : chars) {
            if (next == '}') {
                if (refCount != 0) {
                    refCount--;

                    String refBody = refBuilder.toString();

                    UpdatableModel ref = BodyParser.parseBody(refBody, variables, env);
                    references.add(ref);
                    positions.add(builder.length());

                    refBuilder.setLength(0);
                }

                else builder.append(next);
            }

            else if (next == '{') refCount++;
            else if (refCount != 0) refBuilder.append(next);
            else builder.append(next);
        }

        String bodyWithoutReferences = builder.toString();

        if (bodyWithoutReferences.isEmpty() && references.size() == 1) return references.get(0);
        if (references.size() == 0) return new ImmediateTemplate(new Constant<>(bodyWithoutReferences));
        return new FunctionModel(
                null,
                Generators.getAutoString(),
                references.toArray(new UpdatableModel[0]),
                bodyWithoutReferences.toCharArray(),
                positions.toIntArray()
        );
    }

    private static UpdatableModel parseBody(String body, List<String> variables, ClEnvironment env) {
        body = body.replaceAll("\\s+(?=([^\']*\'[^\']*\')*[^\']*$)", "");
        tokens = Tools.split(body, DELIMITER_PATTERN);
        return parseBodyRecurrent(next(), variables, env);
    }

    private static UpdatableModel parseBodyRecurrent(String body, List<String> variables, ClEnvironment env) throws IllegalStateException {
        if (isBlank(body))  return new BlankModel(++indexCounter);
        if (isNumber(body)) return new ImmediateTemplate(new Constant<>(Integer.parseInt(body)));
        if (isString(body)) return new ImmediateTemplate(new Constant<>(body.substring(1, body.length() - 1)));
        if (isVariable(body, variables)) return new VariableModel(variables.indexOf(body));

        if (isFunction(body)) {
            List<UpdatableModel> argsTemplates = new ArrayList<>();

            skip("(");
            String s = next();

            while (!s.equals(")")) {
                argsTemplates.add(parseBodyRecurrent(s, variables, env));

                s = next();

                if (s.equals(",")) {
                    s = next();
                }
            }

            /*
            todo
            boolean immediate = true;

            for (UpdatableModel arg : argsTemplates) {
                if (!(arg instanceof ImmediateTemplate)) {
                    immediate = false;
                    break;
                }
            }

            if (immediate) {
                Observable[] args = new Observable[argsTemplates.size()];

                for (int i = 0; i < args.length; i++)
                    args[i] = argsTemplates.get(i).unpack(null);

                Observable func = Generators
                        .getGenerator(body)
                        .generate(env, args);

                return new ImmediateTemplate(func);
            }
             */

            return new FunctionModel(env, Generators.getGenerator(body), argsTemplates.toArray(new UpdatableModel[0]));
        }

        throw new IllegalStateException();
    }

    private static boolean isBlank(String body) {
        return body.equals("?");
    }

    private static boolean isNumber(String body) {
        return body.matches("[0-9]+");
    }

    private static boolean isString(String body) {
        return body.matches("'.*'");
    }

    private static boolean isVariable(String body, List<String> variables) {
        return variables.contains(body);
    }

    private static boolean isFunction(String body) {
        return Generators.getGenerator(body) != null;
    }

    private static void skip(String str) {
        if (tokens.hasNext()) {
            String next = tokens.next();
            if (!str.equals(next)) throw new RuntimeException();
        }

        else {
            if (!str.equals("eof")) throw new RuntimeException();
        }
    }

    private static String next() {
        if (tokens.hasNext()) return tokens.next();
        throw new RuntimeException();
    }
}
