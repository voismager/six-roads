package visuals.visualparser;

import client.logic.AspectManager;
import client.logic.ClEnvironment;
import common.entities.Component;
import constants.EntityType;
import engine.support.loaders.UnifiedResource;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import visuals.Widget;
import visuals.billboards.BillboardModel;
import visuals.templates.DrawableModel;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

public class Loader {
    public static Document compileDocument(InputStream docFile) throws ParserConfigurationException, IOException, SAXException {
        return DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(docFile);
    }

    public static Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> prepare(Document document) {
        Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> templates = new Int2ObjectOpenHashMap<>();

        TemplateLoader.prepare(
                ((Element)document.getElementsByTagName("elements").item(0)).getElementsByTagName("element"),
                templates
        );

        return templates;
    }

    public static Short2ObjectMap<Component> loadComponents(Document document) {
        NodeList entities = ((Element)document.getElementsByTagName("elements").item(0)).getElementsByTagName("element");

        Short2ObjectMap<Component> output = new Short2ObjectOpenHashMap<>();

        for (int i = 0; i < entities.getLength(); i++) {
            Element e = (Element) entities.item(i);

            String[] tokens = e.getAttribute("id").replaceAll("\\s", "").split(",");

            EntityType type = EntityType.valueOf(tokens[0]);

            if (type == EntityType.COMPONENT) {
                short id = Short.parseShort(tokens[1]);
                output.put(id, new Component(id));
            }
        }

        return output;
    }

    public static void load(ClEnvironment env, Document document, UnifiedResource resource, Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> templates) throws Exception {
        NodeList modelList      = ((Element)document.getElementsByTagName("models").item(0)).getElementsByTagName("model");
        NodeList entityList     = ((Element)document.getElementsByTagName("elements").item(0)).getElementsByTagName("element");
        NodeList billboardList  = ((Element)document.getElementsByTagName("billboards").item(0)).getElementsByTagName("billboard");
        NodeList widgetList     = ((Element)document.getElementsByTagName("widgets").item(0)).getElementsByTagName("widget");

        Long2ObjectMap<BillboardModel[]> billboards = new Long2ObjectOpenHashMap<>();
        Short2ObjectMap<Widget> widgets = new Short2ObjectOpenHashMap<>();

        env.aspectManager = new AspectManager(templates, billboards, widgets);

        //ModelLoader.load(env.module, modelList, resource);
        TemplateLoader.load(entityList, env, templates);
        //BillboardLoader.load(billboardList, env, billboards, resource.getChild("textures/"));
        WidgetLoader.load(widgetList, widgets, env.aspectManager, templates, env.module);
    }

    static Object2ObjectMap<String, String> toMap(NodeList list) {
        Object2ObjectMap<String, String> map = new Object2ObjectArrayMap<>();

        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            map.put(item.getNodeName(), item.getTextContent());
        }

        return map;
    }
}
