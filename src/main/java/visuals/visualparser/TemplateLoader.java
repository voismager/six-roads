package visuals.visualparser;

import client.logic.ClEnvironment;
import constants.EntityType;
import engine.support.Tools;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import visuals.Constant;
import visuals.Drawable;
import visuals.drawable.*;
import visuals.templates.DrawableModel;
import visuals.templates.UpdatableModel;
import visuals.templates.drawablemodels.*;

import java.util.*;

class TemplateLoader {
    private final static Map<String, Class<? extends Drawable>> DRAWABLE_LITERALS;
    private final static String CONSTANT_LITERAL;
    private final static String FOREACH_ATTRIBUTE;

    static {
        DRAWABLE_LITERALS = new HashMap<>();
        DRAWABLE_LITERALS.put("tf", TextField.class);
        DRAWABLE_LITERALS.put("pb", ProgressBar.class);
        DRAWABLE_LITERALS.put("button", Button.class);
        DRAWABLE_LITERALS.put("tooltip", Tooltip.class);
        DRAWABLE_LITERALS.put("group", Group.class);
        DRAWABLE_LITERALS.put("ref", Ref.class);

        CONSTANT_LITERAL  = "v";
        FOREACH_ATTRIBUTE = "for";
    }

    static void prepare(NodeList elements, Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> map) {
        for (int i = 0; i < elements.getLength(); i++) {
            Element element = (Element) elements.item(i);

            Object2ObjectMap<String, DrawableModel> templates = new Object2ObjectOpenHashMap<>();

            NodeList constants = element.getElementsByTagName(CONSTANT_LITERAL);

            for (int k = 0; k < constants.getLength(); k++) {
                Element constant = (Element) constants.item(k);
                String text = constant.getTextContent();
                String name = constant.getAttribute("name");
                templates.put(name, new ConstantTemplate(new Constant<>(text)));
            }

            int Identifier = EntityType.parseIdentifier(element.getAttribute("id"));
            map.put(Identifier, templates);
        }
    }

    static void load(NodeList elements, ClEnvironment env, Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> map) {
        for (int i = 0; i < elements.getLength(); i++) {
            Element element = (Element) elements.item(i);

            Object2ObjectMap<String, DrawableModel> templates = map.get(EntityType.parseIdentifier(element.getAttribute("id")));

            loadTemplate(element, env, templates);
        }
    }

    private static void loadTemplate(Element element, ClEnvironment env, Object2ObjectMap<String, DrawableModel> templates) {
        for (Map.Entry<String, Class<? extends Drawable>> e : DRAWABLE_LITERALS.entrySet()) {
            NodeList nodes = element.getElementsByTagName(e.getKey());

            for (int i = 0; i < nodes.getLength(); i++) {
                Element item = (Element) nodes.item(i);

                if (item.getParentNode() == element) {
                    DrawableModel template = loadDrawableAsTemplate(e.getValue(), item, Collections.emptyList(), env);
                    String name = item.getAttribute("name");
                    templates.put(name, template);
                }
            }
        }
    }

    private static DrawableModel loadDrawableAsTemplate(Class<? extends Drawable> clazz, Element element, List<String> variables, ClEnvironment env) {
        if (element.hasAttribute(FOREACH_ATTRIBUTE)) {
            String[] attr = element.getAttribute(FOREACH_ATTRIBUTE).split(" in ");

            UpdatableModel list = BodyParser.parse(attr[1], variables, env);

            DrawableModel branch = loadSingleDrawable(clazz, element, Tools.concatImmutable(variables, attr[0]), env);

            return new ForeachModel(list, branch);
        }

        else return loadSingleDrawable(clazz, element, variables, env);
    }

    private static DrawableModel loadSingleDrawable(Class<? extends Drawable> clazz, Element element, List<String> variables, ClEnvironment env) {
        if (clazz == Group.class) {
            List<DrawableModel> o = new ArrayList<>();

            for (Map.Entry<String, Class<? extends Drawable>> e : DRAWABLE_LITERALS.entrySet()) {
                NodeList nodes = element.getElementsByTagName(e.getKey());

                for (int i = 0; i < nodes.getLength(); i++) {
                    Element item = (Element) nodes.item(i);

                    if (item.getParentNode() == element) {
                        o.add(loadDrawableAsTemplate(e.getValue(), item, variables, env));
                    }
                }
            }

            return new GroupModel(o.toArray(new DrawableModel[0]));
        }

        NodeList argList = element.getElementsByTagName("a");

        if (clazz == Button.class) {
            checkArgs(argList, 1);
            UpdatableModel arg = BodyParser.parse(argList.item(0).getTextContent(), variables, env);

            return new ButtonModel(arg);
        }

        if (clazz == Tooltip.class) {
            checkArgs(argList, -1);
            UpdatableModel[] args = new UpdatableModel[argList.getLength()];
            for (int i = 0; i < argList.getLength(); i++)
                args[i] = BodyParser.parse(argList.item(i).getTextContent(), variables, env);

            return new TooltipModel(args);
        }

        if (clazz == ProgressBar.class) {
            checkArgs(argList, 2);
            UpdatableModel arg0 = BodyParser.parse(argList.item(0).getTextContent(), variables, env);
            UpdatableModel arg1 = BodyParser.parse(argList.item(1).getTextContent(), variables, env);

            return new ProgressBarModel(arg0, arg1);
        }

        if (clazz == TextField.class) {
            checkArgs(argList, 1);
            UpdatableModel arg = BodyParser.parse(argList.item(0).getTextContent(), variables, env);

            return new TextFieldModel(arg);
        }

        if (clazz == Ref.class) {
            checkArgs(argList, 1);
            UpdatableModel arg = BodyParser.parse(argList.item(0).getTextContent(), variables, env);

            return new RefModel(arg);
        }

        throw new AssertionError();
    }

    private static void checkArgs(NodeList argList, int expected) {
        if (expected != -1 && argList.getLength() != expected)
            throw new IllegalArgumentException();
    }
}
