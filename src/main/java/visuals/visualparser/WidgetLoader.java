package visuals.visualparser;

import client.logic.AspectManager;
import client.model.Module;
import common.Entity;
import engine.structures.pairs.ObjPair;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import visuals.Drawable;
import visuals.Widget;
import visuals.templates.DrawableModel;
import visuals.widgets.*;

import java.util.Map;
import java.util.Random;

class WidgetLoader {
    static void load(NodeList widgets,
                     Short2ObjectMap<Widget> widgetMap,
                     AspectManager manager,
                     Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> templates,
                     Module module) {

        for (int i = 0; i < widgets.getLength(); i++) {
            Element e = (Element) widgets.item(i);

            Widget widget = loadWidget(e, templates, module);

            widgetMap.put(widget.getId(), widget);

            if (e.hasAttribute("open") && e.getAttribute("open").equals("true")) {
                manager.open(widget.getId());
            }
        }
    }

    private static Widget loadWidget(Element widget, Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> templates, Module module) {
        short id = Short.parseShort(widget.getAttribute("id"));
        String className = widget.getAttribute("class");
        NodeList head = ((Element)widget.getElementsByTagName("head").item(0)).getElementsByTagName("*");
        NodeList body = ((Element)widget.getElementsByTagName("body").item(0)).getElementsByTagName("item");

        Map<String, String> properties = Loader.toMap(head);

        Drawable[] content = new Drawable[body.getLength()];
        Entity[] linked = new Entity[body.getLength()];

        for (int i = 0; i < content.length; i++) {
            Element element = (Element) body.item(i);
            ObjPair<Drawable, Entity> e = ItemParser.parse(element.getTextContent(), templates, module);
            content[i] = e.first;
            linked[i] = e.second;
        }

        return widget(id, className, properties, content, linked);
    }

    private static Widget widget(short id, String className, Map<String, String> properties, Drawable[] content, Entity[] linked) {
        switch (className.toLowerCase()) {
            case "graph":
                return new Graph(id, content, linked);

            case "menubar":
                return new MenuBar(id, content, linked);

            case "tooltip":
                return new Tooltip(id, content);

            case "h-list":
                String label0 = properties.containsKey("label") ? properties.get("label") : "list_" + new Random().nextInt();
                return new HorizontalList(id, label0, content, linked);

            case "v-list":
                String label3 = properties.containsKey("label") ? properties.get("label") : "list_" + new Random().nextInt();
                return new VerticalList(id, label3, content, linked);

            case "popup":
                String label1 = properties.containsKey("label") ? properties.get("label") : "list_" + new Random().nextInt();
                int columns1 = properties.containsKey("columns") ? Integer.parseInt(properties.get("columns")) : 1;
                return new Popup(id, label1, columns1, content, linked);

            case "table":
                String label2 = properties.containsKey("label") ? properties.get("label") : "list_" + new Random().nextInt();
                int columns2 = properties.containsKey("columns") ? Integer.parseInt(properties.get("columns")) : 1;
                float width2 = properties.containsKey("width") ? Float.parseFloat(properties.get("width")) : 0F;
                float height2 = properties.containsKey("height") ? Float.parseFloat(properties.get("height")) : 0F;
                String[] columns = new String[columns2];
                for (int i = 0; i < columns2; i++)
                    columns[i] = properties.containsKey("column" + i) ? properties.get("column" + i) : "column" + new Random().nextInt();
                return new Table(id, label2, width2, height2, columns, content, linked);
        }

        throw new IllegalArgumentException();
    }
}
