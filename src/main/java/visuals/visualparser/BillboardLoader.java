package visuals.visualparser;

import client.logic.ClEnvironment;
import constants.BillboardStatus;
import engine.structures.pairs.ObjShortPair;
import engine.graphics.core.TextureCache;
import engine.support.loaders.UnifiedResource;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.shorts.ShortSet;
import it.unimi.dsi.fastutil.shorts.ShortSets;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import visuals.billboards.BillboardModel;
import visuals.billboards.models.ProgressModel;
import visuals.billboards.models.RectangleIconModel;
import visuals.templates.UpdatableModel;

import java.util.*;

import static client.Settings.CAMERA_ANGLE;
import static constants.EntityType.HEXAGON_GROUND;
import static constants.EntityType.HEXAGON_UNIT;
import static engine.support.keys.Mapper.TD00_BSSs_L;
import static engine.support.keys.Mapper.TD00_BSss_L;

class BillboardLoader {
    /*
    static void load(NodeList billboards, ClEnvironment env, Long2ObjectMap<BillboardModel[]> map, UnifiedResource textures) throws Exception {
        for (int i = 0; i < billboards.getLength(); i++) {
            Element e = (Element) billboards.item(i);

            String className = e.getAttribute("class");

            BillboardModel[] items = load(e, env, textures);

            if (className.equals(HEXAGON_GROUND.name())) {
                String[] id0 = e.getAttribute("id").replaceAll("\\s", "").split(",");
                ShortSet terrains = id0[0].equals("any") ? env.module.terrains.keySet() : ShortSets.singleton(Short.parseShort(id0[0]));
                ShortSet areas = id0[1].equals("any") ? env.module.areas.keySet() : ShortSets.singleton(Short.parseShort(id0[1]));

                for (short terrain : terrains)
                    for (short area : areas) {
                        map.put(TD00_BSSs_L(HEXAGON_GROUND.toByte(), terrain, area), items);
                    }

                continue;
            }

            if (className.equals(HEXAGON_UNIT.name())) {
                String id1 = e.getAttribute("id");
                ShortSet units = id1.equals("any") ? env.module.units.keySet() : ShortSets.singleton(Short.parseShort(id1));

                for (short unit : units) {
                    map.put(TD00_BSss_L(HEXAGON_UNIT.toByte(), unit), items);
                }

                continue;
            }
        }
    }
     */

    private static BillboardModel[] load(Element billboard, ClEnvironment env, UnifiedResource textures) throws Exception {
        NodeList nodes = billboard.getElementsByTagName("item");

        ObjShortPair<BillboardModel>[] models = new ObjShortPair[nodes.getLength()];

        for (int i = 0; i < nodes.getLength(); i++) {
            final Element item = (Element) nodes.item(i);
            final Object2ObjectMap<String, String> params = Loader.toMap(((Element) item.getElementsByTagName("params").item(0)).getElementsByTagName("*"));
            final short order = Short.parseShort(params.getOrDefault("order", "0"));

            models[i] = new ObjShortPair<>(order, getModel(
                    item.getAttribute("class"),
                    BillboardStatus.parse(item.getAttribute("flags")),
                    params,
                    loadArguments(((Element) item.getElementsByTagName("args").item(0)).getElementsByTagName("a"), env),
                    textures
            ));
        }

        Arrays.sort(models, Comparator.comparingInt(t0 -> t0.id));
        BillboardModel[] toReturn = new BillboardModel[models.length];
        for (int i = 0; i < models.length; i++)
            toReturn[i] = models[i].value;
        return toReturn;
    }

    private static BillboardModel getModel(String clazz, byte flags, Object2ObjectMap<String, String> params, UpdatableModel[] args, UnifiedResource textures) throws Exception {
        final float[] position = calculatePosition(params.get("position"), params.getOrDefault("order", "0"));
        final float[] size = parseArrayNf(params.get("size"), 2);
        final Vector4f filter = parseVector4f(params.get("filter"));

        switch (clazz) {
            default: throw new IllegalArgumentException();

            case "rectangle_icon":
                final int texture = TextureCache.getInstance().getTexture(textures.getChild(params.get("texture")));
                return new RectangleIconModel(
                        position[0], position[1], position[2],
                        size[0], size[1], filter, flags,
                        texture
                );

            case "progress":
                final Vector3f color = parseVector3f(params.get("color"));
                return new ProgressModel(
                        position[0], position[1], position[2],
                        size[0], size[1], filter, flags,
                        args[0], args[1], color
                );
        }
    }

    private static float[] calculatePosition(String positionS, String priorityS) {
        final float dp = Float.parseFloat(priorityS) * 0.005f;
        final String[] array = positionS.replaceAll("\\s", "").split(",");
        final float x = Float.parseFloat(array[0]);
        final float w = Float.parseFloat(array[1]);
        final float y = (float) (Math.sin(Math.toRadians(90 - CAMERA_ANGLE)) * w + 0.5 - dp * Math.cos(Math.toRadians(90 - CAMERA_ANGLE)));
        final float z = (float) (Math.cos(Math.toRadians(90 - CAMERA_ANGLE)) * w + dp * Math.sin(Math.toRadians(90 - CAMERA_ANGLE)));
        return new float[] { x, y, z };
    }

    private static float[] parseArrayNf(String s, int n) {
        String[] arr = s.replaceAll("\\s", "").split(",");

        if (arr.length != n) throw new IllegalArgumentException();

        float[] out = new float[arr.length];

        for (int i = 0; i < arr.length; i++) {
            out[i] = Float.parseFloat(arr[i]);
        }

        return out;
    }

    private static Vector3f parseVector3f(String s) {
        String[] arr = s.replaceAll("\\s", "").split(",");

        if (arr.length != 3) throw new IllegalArgumentException();

        float x = Float.parseFloat(arr[0]);
        float y = Float.parseFloat(arr[1]);
        float z = Float.parseFloat(arr[2]);
        return new Vector3f(x, y, z);
    }

    private static Vector4f parseVector4f(String s) {
        String[] arr = s.replaceAll("\\s", "").split(",");

        if (arr.length != 4) throw new IllegalArgumentException();

        float x = Float.parseFloat(arr[0]);
        float y = Float.parseFloat(arr[1]);
        float z = Float.parseFloat(arr[2]);
        float w = Float.parseFloat(arr[3]);
        return new Vector4f(x, y, z, w);
    }

    private static UpdatableModel[] loadArguments(NodeList args, ClEnvironment env) {
        UpdatableModel[] a = new UpdatableModel[args.getLength()];

        for (int i = 0; i < a.length; i++)
            a[i] = BodyParser.parse(args.item(i).getTextContent(), Collections.emptyList(), env);

        return a;
    }
}
