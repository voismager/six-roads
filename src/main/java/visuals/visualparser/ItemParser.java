package visuals.visualparser;

import client.model.Module;
import common.Entity;
import constants.EntityType;
import engine.structures.pairs.ObjPair;
import engine.support.Tools;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import visuals.Drawable;
import visuals.templates.DrawableModel;

import java.util.Iterator;
import java.util.regex.Pattern;

class ItemParser {
    private static final Pattern DELIMITER_PATTERN = Pattern.compile("([,.]|(->))");
    private static Iterator<String> tokens;

    static ObjPair<Drawable, Entity> parse(String body, Int2ObjectMap<Object2ObjectMap<String, DrawableModel>> templates, Module module) {
        body = body.trim().replaceAll("\\s+", "");
        tokens = Tools.split(body, DELIMITER_PATTERN);

        String part0 = next();
        skip(",");
        String part1 = next();
        skip("->");
        String name = next();

        byte type = EntityType.parseType(part0);
        short id = Short.parseShort(part1);

        DrawableModel template = templates
                .get(EntityType.Identifier(type, id))
                .get(name);

        if (EntityType.isType(type))
            return new ObjPair<>(
                    template.unpack(new Object[0]), null
             //       module.getMap(EntityType.fromByte(type)).get(id)
            );

        else
            return new ObjPair<>(
                    template.unpack(new Object[0]),
                    null
            );
    }

    private static void skip(String str) {
        if (tokens.hasNext()) {
            String next = tokens.next();
            if (!str.equals(next)) throw new RuntimeException();
        }

        else {
            if (!str.equals("eof")) throw new RuntimeException();
        }
    }

    private static String next() {
        if (tokens.hasNext()) return tokens.next();
        return "eof";
    }
}
