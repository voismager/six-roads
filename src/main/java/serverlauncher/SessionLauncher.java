package serverlauncher;

import client.Settings;
import common.server.EarlyGameSession;
import common.server.properties.LoadedModulesProperty;
import common.server.properties.VersionProperty;
import engine.network.api.APIHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.UUID;

import static engine.support.loaders.FilesLoader.loadResource;

public class SessionLauncher {
    private final static Logger logger = LogManager.getLogger(SessionLauncher.class);

    public static void main(String[] args) {
        try {
            Settings.init(loadResource("/app.properties"));
            Settings.SERVICE_ADDRESS = "localhost";
            APIHolder.getInstance().login(args[0], args[1]);

            EarlyGameSession session = new EarlyGameSession();
            session.setProperty(LoadedModulesProperty.KEY, new ArrayList<>());
            session.addModule(UUID.fromString("0f84e2ee-e18a-422a-bd9f-4dec55454af2"));
            session.setProperty(VersionProperty.KEY, Settings.VERSION);
            session.start(new InetSocketAddress(args[2], Integer.parseInt(args[3])));
        } catch (Exception e) {
            logger.error(e);
        }
    }
}
