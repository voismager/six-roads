package launcher;

import client.Settings;
import client.processor.Processor;
import client.service.Servers;
import client.states.Menu;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static engine.support.loaders.FilesLoader.loadResource;

public final class Launcher {
    public static void main(String[] args) {
        try {
            Settings.init(loadResource("/app.properties"));
            Servers.loadList(new InputStreamReader(loadResource("/servers.csv"), StandardCharsets.UTF_8));
        }

        catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        Processor.launch(Menu.INSTANCE);
    }
}
