package common;

import common.gameobjects.Hexagon;
import engine.Cleanable;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

public interface PainterInterface extends Cleanable {
    byte paint();

    PainterInterface erase();

    boolean paintedAny();

    int getCost(int q, int r);

    PainterInterface execute(Consumer<Hexagon> consumer);

    PainterInterface paintHexagon(int q, int r, int radius, boolean paintCenter);

    PainterInterface paintDijkstra(int q, int r, int radius, boolean paintCenter, ToIntFunction<Hexagon> costFunction);

    void executeOnHexagon(int q, int r, int radius, boolean paintCenter, Consumer<Hexagon> consumer);

    void executeOnDijkstra(int q, int r, int radius, boolean paintCenter, Consumer<Hexagon> consumer, ToIntFunction<Hexagon> costFunction);

    PainterInterface predicate(Predicate<Hexagon> predicate);
}
