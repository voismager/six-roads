package common;

import common.fsm.Script;

public class Gamemode {
    public final short key;
    public final Script rootScript;

    public Gamemode(short key, Script rootScript) {
        this.key = key;
        this.rootScript = rootScript;
    }

    public Gamemode(Gamemode gamemode) {
        this.key = gamemode.key;
        this.rootScript = gamemode.rootScript;
    }
}
