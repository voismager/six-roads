package common;

import constants.EntityType;

public abstract class Entity {
    protected final short id;
    private final EntityType type;

    public Entity(short id, EntityType type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public int hashCode() {
        return Identifier();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return id == entity.id &&
                type == entity.type;
    }

    public int Identifier() {
        return EntityType.Identifier(type, id);
    }

    public short id() {
        return id;
    }

    public EntityType getType() {
        return type;
    }
}