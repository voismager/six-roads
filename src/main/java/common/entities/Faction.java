package common.entities;

import common.Entity;
import constants.EntityType;

public class Faction extends Entity {
    public final AttributeSet attributes;

    public Faction(short id, AttributeSet attributes) {
        super(id, EntityType.FACTION);
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }

    public Faction copy() {
        return new Faction(id, new AttributeSet(attributes));
    }
}
