package common.entities;

import common.Entity;
import constants.EntityType;

public class Component extends Entity {
    public Component(short id) {
        super(id, EntityType.COMPONENT);
    }

    public Component copy() {
        return this;
    }
}
