package common.entities;

import it.unimi.dsi.fastutil.objects.Object2IntMap;

import static constants.Triggers.ADD_EFFECT_FLAG;
import static constants.Triggers.REMOVE_EFFECT_FLAG;

public class Effect extends Callable {
    private final int priority;

    public Effect(short id, int priority, AttributeSet attributes) {
        super(id, attributes);
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public long[] genClTriggers(int Identifier, Object2IntMap<String> args) {
        return runner.cl_call_longs(Identifier, args);
    }

    public long[] genSvTriggers(int Identifier, Object2IntMap<String> args) {
        return runner.sv_call_longs(Identifier, args);
    }

    public boolean isRunnableOnAdd() {
        return (runner.cl_call_int() & ADD_EFFECT_FLAG) == ADD_EFFECT_FLAG;
    }

    public boolean isRunnableOnRemove() {
        return (runner.cl_call_int() & REMOVE_EFFECT_FLAG) == REMOVE_EFFECT_FLAG;
    }
}
