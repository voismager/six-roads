package common.entities;

import common.Entity;
import constants.EntityType;

public class Quest extends Entity {
    public final AttributeSet attributes;
    public final Callable ending;
    public final Callable condition;

    public Quest(short id, Callable condition, Callable ending, AttributeSet attributes) {
        super(id, EntityType.QUEST);
        this.condition = condition;
        this.ending = ending;
        this.attributes = attributes;
    }

    public Quest copy() {
        return new Quest(id, condition, ending, new AttributeSet(attributes));
    }
}
