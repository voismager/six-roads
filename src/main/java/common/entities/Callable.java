package common.entities;

import common.Entity;
import scripting.runners.ScriptRunner;

import static constants.EntityType.SCRIPT;

public class Callable extends Entity {
    public final AttributeSet attributes;
    public ScriptRunner runner;

    public Callable(short id, AttributeSet attributes) {
        super(id, SCRIPT);
        this.attributes = attributes;
    }

    public Callable copy() {
        return this;
    }

    public void setRunner(ScriptRunner runner) {
        this.runner = runner;
    }
}
