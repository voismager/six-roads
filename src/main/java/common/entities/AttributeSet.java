package common.entities;

import engine.structures.ObjArray;
import engine.structures.pairs.ObjShortPair;
import engine.types.integers.IntType;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;

public class AttributeSet {
    private final Short2ObjectMap<IntType> attributes;
    private final ObjArray<ObjShortPair<IntType>> instAttributes;

    public AttributeSet() {
        this.attributes = new Short2ObjectOpenHashMap<>();
        this.instAttributes = new ObjArray<>();
    }

    public AttributeSet(AttributeSet set) {
        this.attributes = new Short2ObjectOpenHashMap<>(set.attributes.size());
        this.instAttributes = new ObjArray<>(set.instAttributes.size());

        for (Short2ObjectMap.Entry<IntType> e : set.attributes.short2ObjectEntrySet()) {
            this.attributes.put(e.getShortKey(), e.getValue().copy());
        }

        for (int i = 0; i < set.instAttributes.size(); i++) {
            ObjShortPair<IntType> p = set.instAttributes.get(i);
            this.instAttributes.add(new ObjShortPair<>(p.id, this.attributes.get(p.id)));
        }
    }

    public void add(short id, IntType attr, boolean isStatic) {
        this.attributes.put(id, attr);

        if (!isStatic) {
            this.instAttributes.add(new ObjShortPair<>(id, attr));
        }
    }

    public IntType get(short id) {
        return attributes.get(id);
    }

    public int getInt(short id) {
        return attributes.get(id).asInt();
    }

    public int getIntOrZero(short id) {
        IntType t = attributes.get(id);

        if (t == null) return 0;

        return t.asInt();
    }

    public ObjArray<ObjShortPair<IntType>> getInstAttributes() {
        return instAttributes;
    }
}
