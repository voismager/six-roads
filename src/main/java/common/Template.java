package common;

import engine.types.vectors.Vector;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;

import java.util.Arrays;
import java.util.Objects;

public class Template {
    private final short id;
    private final String name;
    private final ObjectSet<String> labels;
    private final Object2ObjectMap<String, Vector> attributes;

    public Template(short id, String name, ObjectSet<String> labels, Object2ObjectMap<String, Vector> attributes) {
        this.id = id;
        this.name = name;
        this.labels = new ObjectOpenHashSet<>(labels);
        this.attributes = new Object2ObjectOpenHashMap<>(attributes);
    }

    public Template(Template template) {
        this.id = template.id;
        this.name = template.name;
        this.labels = new ObjectOpenHashSet<>(template.labels);
        this.attributes = new Object2ObjectOpenHashMap<>(template.attributes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Template template = (Template) o;
        return id == template.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public short getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean hasLabel(String label) {
        return labels.contains(label);
    }

    public Vector getAttribute(String id) {
        return attributes.get(id);
    }

    public void setAttribute(String id, Vector attr) {
        attributes.put(id, attr);
    }

    public void clearAttributes() {
        attributes.clear();
    }
}
