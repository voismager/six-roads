package common.fsm;

import it.unimi.dsi.fastutil.objects.ObjectList;
import scripting.GlobalEnvironment;

public interface Script {
    void run(int event, ObjectList<Object> stack, LocalContext context, GlobalEnvironment env);
}
