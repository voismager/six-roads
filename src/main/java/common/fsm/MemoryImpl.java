package common.fsm;

import engine.Cleanable;
import it.unimi.dsi.fastutil.objects.AbstractObjectList;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.util.function.Supplier;

public class MemoryImpl implements Memory, Cleanable {
    private final Object2ObjectMap<String, Object> map;

    public MemoryImpl() {
        this.map = new Object2ObjectArrayMap<>();
    }

    @Override
    public <T> T saveOrGet(String key, Supplier<T> supplier) {
        final Object inMap = this.map.get(key);
        if (inMap == null) {
            final T val = supplier.get();
            this.map.put(key, val);
            return val;
        }
        return (T) inMap;
    }

    @Override
    public void save(String key, Object o) {
        this.map.put(key, o);
    }

    @Override
    public <T> T get(String key, Class<T> clazz) {
        return (T) this.map.get(key);
    }

    @Override
    public void cleanup() {
        this.map.clear();
    }
}
