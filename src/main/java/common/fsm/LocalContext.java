package common.fsm;

import java.util.Map;

public interface LocalContext {
    int id();

    void activateParallel(String scriptName, int pid);

    void activateParallel(String scriptName, int pid, Map<?, ?> params);

    void deactivateParallel(int pid);

    void jump(String scriptName, int key);

    Object parameter(Object key);

    Memory memory();
}
