package common.fsm;

import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongSet;
import it.unimi.dsi.fastutil.objects.ObjectCollection;
import it.unimi.dsi.fastutil.objects.ObjectSet;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Map;

import static engine.support.keys.Mapper.IDENTITY_LONG;

public class TransitionMap implements Long2ObjectMap<String> {
    private Long2ObjectMap<String> simpleCases;
    private String[] values;
    private long[] keys;
    private long[] masks;
    private int size;

    public TransitionMap() {
        this.values = new String[16];
        this.keys = new long[16];
        this.masks = new long[16];
        this.simpleCases = new Long2ObjectOpenHashMap<>();
        this.simpleCases.defaultReturnValue(null);
    }

    @Override
    public String put(long key, String value) {
        long mask = EventKeys.getMask(key);
        if (mask == IDENTITY_LONG) simpleCases.put(key, value);
        else addHardCase(key, mask, value);
        return value;
    }

    private void addHardCase(long key, long mask, String value) {
        if (size == keys.length) {
            int newLength = keys.length * 2 + 1;
            keys = Arrays.copyOf(keys, newLength);
            masks = Arrays.copyOf(masks, newLength);
            values = Arrays.copyOf(values, newLength);
        }

        keys[size] = key;
        masks[size] = mask;
        values[size] = value;

        size++;
    }

    @Override
    public String get(long key) {
        String v = simpleCases.get(key);

        if (null == v) {
            for (int i = 0; i < size; i++) {
                if ((key & masks[i]) == keys[i]) {
                    return values[i];
                }
            }
        }

        return v;
    }

    @Override
    public void clear() {
        simpleCases.clear();
        size = 0;
    }

    @Override
    public int size() {
        return size + simpleCases.size();
    }

    @Override
    public boolean isEmpty() {
        return (size + simpleCases.size()) == 0;
    }

    @Override
    public void defaultReturnValue(String rv) { throw new UnsupportedOperationException(); }
    @Override
    public String defaultReturnValue() { throw new UnsupportedOperationException(); }
    @Override
    public void putAll(@NotNull Map<? extends Long, ? extends String> map) { throw new UnsupportedOperationException(); }
    @Override
    public ObjectSet<Entry<String>> long2ObjectEntrySet() { throw new UnsupportedOperationException(); }
    @Override
    public LongSet keySet() { throw new UnsupportedOperationException(); }
    @Override
    public ObjectCollection<String> values() { throw new UnsupportedOperationException(); }
    @Override
    public boolean containsKey(long key) { throw new UnsupportedOperationException(); }
    @Override
    public boolean containsValue(Object o) { throw new UnsupportedOperationException(); }
}
