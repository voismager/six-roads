package common.fsm;

import constants.EntityType;
import engine.support.keys.TD02;

import static constants.EventsFlags.*;
import static engine.support.keys.TD02.*;
import static java.lang.Byte.parseByte;

public class EventKeys {
    public static final byte EMPTY_HOVERED      = 1;
    public static final byte CELL_HOVERED       = 2;
    public static final byte LEFT_CELL_CLICK    = 3;
    public static final byte RIGHT_CELL_CLICK   = 4;
    public static final byte LEFT_EMPTY_CLICK   = 5;
    public static final byte RIGHT_EMPTY_CLICK  = 6;
    public static final byte LEFT_ENTITY_CLICK  = 7;
    public static final byte BUTTON_PUSH        = 8;

    public static byte getEvent(long key) {
        return L_1B(key);
    }

    public static long getMask(long v) {
        return TD02.getMask(v);
    }

    public static long EMPTY_HOVERED(byte mode) {
        return BBbbss_L(EMPTY_HOVERED, mode);
    }

    public static long CELL_HOVERED(byte layer, byte mode, byte paint) {
        return BBBbSs_L(CELL_HOVERED, layer, mode, paint);
    }

    public static long LEFT_CELL_CLICK(int layer, int paint) {
        return BBBbss_L(LEFT_CELL_CLICK, (byte) layer, (byte) paint);
    }

    public static long LEFT_CELL_CLICK(byte layer, byte paint) {
        return BBBbss_L(LEFT_CELL_CLICK, layer, paint);
    }

    public static long RIGHT_CELL_CLICK(int layer, int paint) {
        return BBBbss_L(RIGHT_CELL_CLICK, (byte) layer, (byte) paint);
    }

    public static long RIGHT_CELL_CLICK(byte layer, byte paint) {
        return BBBbss_L(RIGHT_CELL_CLICK, layer, paint);
    }

    public static long LEFT_EMPTY_CLICK() {
        return Bbbbss_L(LEFT_EMPTY_CLICK);
    }

    public static long RIGHT_EMPTY_CLICK() {
        return Bbbbss_L(RIGHT_EMPTY_CLICK);
    }

    public static long LEFT_ENTITY_CLICK(byte type, short entity, short widget) {
        return BbbBSS_L(LEFT_ENTITY_CLICK, type, entity, widget);
    }

    public static long BUTTON_PUSH(int buttonCode) {
        return BbbbSs_L(BUTTON_PUSH, buttonCode);
    }

    public static long BUTTON_PUSH(short buttonCode) {
        return BbbbSs_L(BUTTON_PUSH, buttonCode);
    }

    public static long parse(String stringKey) {
        final String[] tokens = stringKey.split(" ");

        switch (tokens[0]) {
            case "EMPTY_HOVERED": return EMPTY_HOVERED(parseMouseMode(tokens[1]));
            case "CELL_HOVERED":
                return CELL_HOVERED(
                        parseByte(tokens[1]),
                        parseMouseMode(tokens[2]),
                        parsePaint(tokens[3])
                );
            case "LEFT_CELL_CLICK":
                return LEFT_CELL_CLICK(
                        parseByte(tokens[1]),
                        parsePaint(tokens[2])
                );
            case "RIGHT_CELL_CLICK":
                return RIGHT_CELL_CLICK(
                        parseByte(tokens[1]),
                        parsePaint(tokens[2])
                );
            case "LEFT_EMPTY_CLICK": return LEFT_EMPTY_CLICK();
            case "RIGHT_EMPTY_CLICK": return RIGHT_EMPTY_CLICK();
            case "LEFT_ENTITY_CLICK":
                return LEFT_ENTITY_CLICK(
                        EntityType.valueOf(tokens[1]).toByte(),
                        parseId(tokens[2]),
                        parseId(tokens[3])
                );
            case "BUTTON_PUSH": return BUTTON_PUSH(Short.parseShort(tokens[1]));
            default: throw new IllegalArgumentException();
        }
    }

    private static byte parseMouseMode(String str) {
        switch (str) {
            case "MODE_ANY": return MODE_ANY;
            case "MODE_NONE": return MODE_NONE;
            case "MODE_LONG_LMB": return MODE_LONG_LMB;
            case "MODE_SHORT_LMB": return MODE_SHORT_LMB;
            case "MODE_LONG_RMB": return MODE_LONG_RMB;
            case "MODE_SHORT_RMB": return MODE_SHORT_RMB;
            default: throw new IllegalArgumentException();
        }
    }

    private static byte parsePaint(String str) {
        switch (str) {
            case "PAINT_ANY":
                return 0;
            default:
                try {
                    return Byte.parseByte(str);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }
        }
    }

    private static short parseId(String str) {
        switch (str) {
            case "ID_ANY":
                return ID_ANY;
            default:
                try {
                    return Short.parseShort(str);
                } catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }
        }
    }
}
