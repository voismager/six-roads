package common.fsm;

import engine.Cleanable;
import engine.structures.ObjArray;
import it.unimi.dsi.fastutil.ints.*;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import scripting.GlobalEnvironment;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FSM {
    private final ScriptStorage scriptStorage;
    private final GlobalEnvironment environment;
    private final ObjArray<Context> activeSchemes;
    private final Set<Context> toActivate;
    private final IntSet toDeactivate;
    private final ObjectList<Object> stack;

    private class Context implements LocalContext, Cleanable {
        final int pid;
        final Map<?, ?> params;
        final MemoryImpl memory;
        final Int2ObjectMap<String> transitions;
        Script currentScript;
        Context(int pid, Script script, Map<?, ?> params, MemoryImpl memory) {
            this.pid = pid;
            this.currentScript = script;
            this.params = new Object2ObjectArrayMap<>(params);
            this.memory = memory;
            this.transitions = new Int2ObjectArrayMap<>();
        }
        public boolean equals(Object o) { return pid == ((Context)o).pid; }
        public int hashCode() { return pid; }
        public int id() { return pid; }
        public void activateParallel(String scriptName, int pid) { activateParallel(scriptName, pid, Collections.emptyMap()); }
        public void activateParallel(String scriptName, int pid, Map<?, ?> params) {
            final Script script = scriptStorage.getScript(scriptName);
            if (script != null) toActivate.add(new Context(pid, script, params, new MemoryImpl()));
        }
        public void deactivateParallel(int pid) { toDeactivate.add(pid); }
        public void jump(String scriptName, int key) { transitions.put(key, scriptName); }
        public Object parameter(Object key) { return params.get(key); }
        public Memory memory() { return memory; }
        public void cleanup() { params.clear(); memory.cleanup(); }
    }

    public FSM(ScriptStorage scriptStorage, GlobalEnvironment environment) {
        this.scriptStorage = scriptStorage;
        this.environment = environment;
        this.activeSchemes = new ObjArray<>(4);
        this.toActivate = new HashSet<>();
        this.toDeactivate = new IntArraySet();
        this.stack = new ObjectArrayList<>(3);
    }

    public void init(Script start, Map<?, ?> params) {
        final Context context = new Context(0, start, params, new MemoryImpl());
        this.activate(context);
        this.checkDeactivation();
        this.checkActivation();
    }

    public void init(Script start) {
        this.init(start, Collections.emptyMap());
    }

    public void pushToStack(Object o) {
        this.stack.add(o);
    }

    public void clearStack() {
        this.stack.clear();
    }

    public void onEvent(int event) {
        this.runSchemes(event);
        this.checkDeactivation();
        this.checkActivation();
    }

    public void onSimultaneousEvents(int[] events) {
        this.runSchemes(events);
        this.checkDeactivation();
        this.checkActivation();
    }

    private void runSchemes(int[] events) {
        for (int i = 0; i < this.activeSchemes.size(); i++) {
            final Context context = this.activeSchemes.get(i);
            for (int event : events) {
                final String next = context.transitions.get(event);
                if (next != null) {
                    context.transitions.clear();
                    Script script = this.scriptStorage.getScript(next);
                    script.run(event, stack, context, environment);
                    context.currentScript = script;
                    break;
                }
            }
        }
    }

    private void runSchemes(int event) {
        for (int i = 0; i < this.activeSchemes.size(); i++) {
            final Context context = this.activeSchemes.get(i);
            final String next = context.transitions.get(event);
            if (next != null) {
                context.transitions.clear();
                Script script = this.scriptStorage.getScript(next);
                script.run(event, stack, context, environment);
                context.currentScript = script;
            }
        }
    }

    private void checkDeactivation() {
        if (!toDeactivate.isEmpty()) {
            for (int id : toDeactivate) {
                for (int i = 0; i < activeSchemes.size(); i++) {
                    final Context context = activeSchemes.get(i);
                    if (context.pid == id) {
                        this.deactivate(context);
                        break;
                    }
                }
            }

            toDeactivate.clear();
        }
    }

    private void checkActivation() {
        if (!toActivate.isEmpty()) {
            for (Context context : toActivate) this.activate(context);
            toActivate.clear();
        }
    }

    private void deactivate(Context context) {
        context.cleanup();
        this.activeSchemes.removeElement(context);
    }

    private void activate(Context context) {
        context.currentScript.run(0, stack, context, environment);
        this.activeSchemes.add(context);
    }
}
