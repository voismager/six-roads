package common.fsm;

import java.util.function.Supplier;

public interface Memory {
    <T> T saveOrGet(String key, Supplier<T> supplier);

    void save(String key, Object o);

    <T> T get(String key, Class<T> clazz);
}
