package common.fsm;

import scripting.StandardFSMScripts;

import java.util.HashMap;
import java.util.Map;

public class ScriptStorage {
    private final Map<String, Script> scripts = new HashMap<>();

    public ScriptStorage() {
        scripts.putAll(StandardFSMScripts.get());
    }

    public Script getScript(String name) {
        return this.scripts.get(name);
    }
}
