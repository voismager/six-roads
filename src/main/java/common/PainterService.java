package common;

import engine.Cleanable;
import it.unimi.dsi.fastutil.bytes.Byte2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.bytes.Byte2ObjectMap;

public class PainterService implements Cleanable {
    private final Byte2ObjectMap<PainterInterface> painters;

    public PainterService() {
        this.painters = new Byte2ObjectAVLTreeMap<>();
    }

    public PainterInterface getFree(RealMap map) {
        for (byte i = Byte.MIN_VALUE; i < Byte.MAX_VALUE; i++) {
            final PainterInterface painterInterface = this.painters.get(i);
            if (painterInterface == null || !painterInterface.paintedAny()) {
                return get(i, map);
            }
        }

        return get(Byte.MIN_VALUE, map);
    }

    public PainterInterface get(byte paint, RealMap map) {
        return this.painters.computeIfAbsent(paint, p -> new DefaultPainter((byte) p, map));
    }

    @Override
    public void cleanup() {
        this.painters.forEach((key, painter) -> painter.cleanup());
        this.painters.clear();
    }
}
