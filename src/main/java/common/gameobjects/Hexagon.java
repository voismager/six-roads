package common.gameobjects;

import common.Template;
import engine.gameobjects.GameObject;
import engine.support.HexagonUtils;
import engine.types.vectors.Vector;
import it.unimi.dsi.fastutil.bytes.ByteArrayList;
import it.unimi.dsi.fastutil.bytes.ByteList;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMaps;

public class Hexagon extends GameObject {
    public int q, r, l;
    protected Template template;
    protected ByteList paints;
    private Object2ObjectMap<String, Vector> attributes;

    public Hexagon(Template template) {
        this.template = template;
        this.paints = new ByteArrayList();
        this.attributes = Object2ObjectMaps.emptyMap();
    }

    public void setPosition(int q, int r, int l) {
        this.q = q;
        this.r = r;
        this.l = l;
        this.x = HexagonUtils.qrToX(q, r);
        this.y = HexagonUtils.qrToY(q, r);
        this.z = l;
    }

    public Template getTemplate() {
        return template;
    }

    public Vector getAttribute(String id) {
        return attributes.get(id);
    }

    public void setAttribute(String id, Vector attr) {
        attributes.put(id, attr);
    }

    public void clearAttributes() {
        attributes.clear();
    }

    public void erasePaint(byte paint) {
        this.paints.rem(paint);
    }

    public void paint(byte paint) {
        this.paints.add(paint);
    }

    public boolean isPainted(byte paint) {
        return this.paints.contains(paint);
    }

    @Override
    public void cleanup() {
        attributes.clear();
        paints.clear();
        attributes = null;
        paints = null;
    }
}
