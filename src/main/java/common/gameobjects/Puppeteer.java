package common.gameobjects;

import common.Gamemode;
import engine.types.vectors.Vector;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

/**
 * i.e. Player
 */
public class Puppeteer {
    private final int id;
    private final Gamemode gamemode;
    private Object2ObjectMap<String, Vector> attributes;

    public Puppeteer(int id, Gamemode gamemode) {
        this.id = id;
        this.gamemode = gamemode;
        this.attributes = new Object2ObjectOpenHashMap<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Puppeteer puppeteer = (Puppeteer) o;
        return id == puppeteer.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getId() {
        return id;
    }

    public Gamemode getGamemode() {
        return gamemode;
    }

    public Vector getAttribute(String id) {
        return attributes.get(id);
    }

    public void setAttribute(String id, Vector attr) {
        attributes.put(id, attr);
    }

    public void clearAttributes() {
        attributes.clear();
    }
}
