package common;

import constants.MapGenerationType;

public class MapHeader {
    public final short id;
    public final MapGenerationType type;
    public final long seed;

    public MapHeader(short id, MapGenerationType type, long seed) {
        this.id = id;
        this.type = type;
        this.seed = seed;
    }

    public MapHeader(MapHeader mapHeader) {
        this.id = mapHeader.id;
        this.type = mapHeader.type;
        this.seed = mapHeader.seed;
    }
}
