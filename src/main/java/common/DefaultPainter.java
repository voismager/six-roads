package common;

import common.gameobjects.Hexagon;
import engine.structures.ObjArray;
import engine.structures.ObjOpenArray;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

public class DefaultPainter implements PainterInterface {
    private final byte paint;
    private final RealMap src;

    private ObjOpenArray<Hexagon> cache;
    private Object2IntMap<Hexagon> weights;
    private Set<Hexagon> visited;
    private PriorityQueue<Hexagon> queue;
    private List<Predicate<Hexagon>> predicates;

    public DefaultPainter(byte paint, RealMap src) {
        this.paint = paint;
        this.src = src;

        this.cache = new ObjOpenArray<>(36);
        this.weights = new Object2IntOpenHashMap<>();
        this.weights.defaultReturnValue(Integer.MAX_VALUE);
        this.visited = new HashSet<>();
        this.queue = new PriorityQueue<>(Comparator.comparingInt(weights::getInt));
        this.predicates = new ArrayList<>(6);
    }

    @Override
    public byte paint() {
        return paint;
    }

    @Override
    public PainterInterface erase() {
        if (cache.size != 0) {
            for (int i = 0; i < cache.size; i++) {
                final Hexagon cell = cache.get(i);
                cell.erasePaint(paint);
            }

            cache.clear();
            weights.clear();
        }

        return this;
    }

    @Override
    public boolean paintedAny() {
        return !cache.isEmpty();
    }

    @Override
    public int getCost(int q, int r) {
        return weights.getInt(src.get(q, r, 0));
    }

    @Override
    public PainterInterface execute(Consumer<Hexagon> consumer) {
        for (int i = 0; i < cache.size; i++) {
            Hexagon c = cache.get(i);
            consumer.accept(c);
        }

        return this;
    }

    @Override
    public PainterInterface paintHexagon(int centerQ, int centerR, int radius, boolean paintCenter) {
        final Hexagon center = src.get(centerQ, centerR, 0);

        for (int q = -paint; q <= paint; q++) {
            int r1 = Math.max(-paint, -q - paint);
            int r2 = Math.min(paint, -q + paint);

            for (int r = r1; r <= r2; r++) {
                final int groundQ = q + centerQ;
                final int groundR = r + centerR;

                Hexagon ground = src.get(groundQ, groundR, 0);

                if (ground != null) {
                    if (!paintCenter && ground == center) continue;
                    if (test(ground)) paint(ground);
                }
            }
        }

        return this;
    }

    @Override
    public PainterInterface paintDijkstra(int q, int r, int radius, boolean paintCenter, ToIntFunction<Hexagon> costFunction) {
        ObjArray<Hexagon> neighbors = new ObjArray<>(6);
        visited.clear();
        queue.clear();

        ///////////////////////////

        Hexagon start = src.get(q, r, 0);
        weights.put(start, 0);
        visited.add(start);
        src.getFlatNeighbors(q, r, 0, neighbors);

        for (int i = 0; i < neighbors.size(); i++) {
            Hexagon neighbor = neighbors.get(i);

            if (!test(neighbor)) visited.add(neighbor);
            if (!visited.contains(neighbor)) {
                int newWeight = costFunction.applyAsInt(neighbor);
                int oldWeight = weights.getInt(neighbor);

                if (oldWeight > newWeight) weights.put(neighbor, newWeight);

                queue.add(neighbor);
            }
        }

        if (paintCenter && test(start)) paint(start);

        ///////////////////////////

        while (!queue.isEmpty() && weights.getInt(queue.peek()) <= radius) {
            Hexagon element = queue.poll();
            int elementWeight = weights.getInt(element);

            neighbors.clear();
            src.getFlatNeighbors(element.q, element.r, 0, neighbors);

            for (int i = 0; i < neighbors.size(); i++) {
                Hexagon neighbor = neighbors.get(i);

                if (!test(neighbor)) visited.add(neighbor);

                if (!visited.contains(neighbor)) {
                    int newWeight = elementWeight + costFunction.applyAsInt(neighbor);
                    int oldWeight = weights.getInt(neighbor);

                    if (oldWeight > newWeight) weights.put(neighbor, newWeight);

                    queue.remove(neighbor);
                    queue.add(neighbor);
                }
            }

            visited.add(element);
            paint(element);
        }

        return this;
    }

    @Override
    public void executeOnHexagon(int centerQ, int centerR, int radius, boolean paintCenter, Consumer<Hexagon> consumer) {
        final Hexagon center = src.get(centerQ, centerR, 0);

        for (int q = -paint; q <= paint; q++) {
            int r1 = Math.max(-paint, -q - paint);
            int r2 = Math.min(paint, -q + paint);

            for (int r = r1; r <= r2; r++) {
                final int groundQ = q + centerQ;
                final int groundR = r + centerR;

                Hexagon cell = src.get(groundQ, groundR, 0);

                if (cell != null) {
                    if (!paintCenter && cell == center) continue;
                    if (test(cell)) consumer.accept(cell);
                }
            }
        }
    }

    @Override
    public void executeOnDijkstra(int q, int r, int radius, boolean paintCenter, Consumer<Hexagon> consumer, ToIntFunction<Hexagon> costFunction) {
        ObjArray<Hexagon> neighbors = new ObjArray<>(6);
        visited.clear();
        queue.clear();

        ///////////////////////////

        Hexagon start = src.get(q, r, 0);
        weights.put(start, 0);
        visited.add(start);
        src.getFlatNeighbors(q, r, 0, neighbors);

        for (int i = 0; i < neighbors.size(); i++) {
            Hexagon neighbor = neighbors.get(i);

            if (!test(neighbor)) visited.add(neighbor);
            if (!visited.contains(neighbor)) {
                int newWeight = costFunction.applyAsInt(neighbor);
                int oldWeight = weights.getInt(neighbor);

                if (oldWeight > newWeight) weights.put(neighbor, newWeight);

                queue.add(neighbor);
            }
        }

        if (paintCenter && test(start)) consumer.accept(start);

        ///////////////////////////

        while (!queue.isEmpty() && weights.getInt(queue.peek()) <= radius) {
            Hexagon element = queue.poll();
            int elementWeight = weights.getInt(element);

            neighbors.clear();
            src.getFlatNeighbors(element.q, element.r, 0, neighbors);

            for (int i = 0; i < neighbors.size(); i++) {
                Hexagon neighbor = neighbors.get(i);

                if (!test(neighbor)) visited.add(neighbor);

                if (!visited.contains(neighbor)) {
                    int newWeight = elementWeight + costFunction.applyAsInt(neighbor);
                    int oldWeight = weights.getInt(neighbor);

                    if (oldWeight > newWeight) weights.put(neighbor, newWeight);

                    queue.remove(neighbor);
                    queue.add(neighbor);
                }
            }

            visited.add(element);
            consumer.accept(element);
        }
    }

    @Override
    public PainterInterface predicate(Predicate<Hexagon> predicate) {
        predicates.add(predicate);
        return this;
    }

    private boolean test(Hexagon ground) {
        for (int i = 0; i < predicates.size(); i++)
            if (!predicates.get(i).test(ground)) return false;
        return true;
    }

    private void paint(Hexagon cell) {
        cell.paint(paint);
        cache.add(cell);
    }

    @Override
    public void cleanup() {
        cache.clear();
        weights.clear();
        visited.clear();
        queue.clear();
        predicates.clear();
    }
}
