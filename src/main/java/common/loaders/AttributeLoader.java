package common.loaders;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import engine.types.vectors.Vector;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;

import static constants.ConstIdentifiers.NULL_IDENTIFIER;

public class AttributeLoader {
    public static Object2ObjectMap<String, Vector> load(JsonArray jsonArray) {
        Object2ObjectMap<String, Vector> map = new Object2ObjectArrayMap<>();

        for (JsonElement jsonElement : jsonArray) {
            JsonObject jsonObject = (JsonObject) jsonElement;
            String key = jsonObject.get("key").getAsString();
            JsonElement value = jsonObject.get("value");

            if (value.isJsonArray()) {
                map.put(key, Vector.of(toIntArray(value.getAsJsonArray())));
            } else if (value.isJsonPrimitive()) {
                map.put(key, Vector.of(value.getAsInt()));
            }
        }

        return map;
    }

    private static int[] toIntArray(JsonArray array) {
        int i = 0;
        int[] ints = new int[array.size()];

        for (JsonElement element : array) {
            JsonPrimitive p = element.getAsJsonPrimitive();
            if (p.isNumber()) {
                ints[i++] = element.getAsInt();
            } else if (p.isString() && p.getAsString().equals("null_identifier")) {
                ints[i++] = NULL_IDENTIFIER;
            }
        }

        return ints;
    }
}
