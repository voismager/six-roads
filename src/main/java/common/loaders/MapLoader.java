package common.loaders;

import common.MapHeader;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import constants.MapGenerationType;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;

public class MapLoader {
    public static Short2ObjectMap<MapHeader> load(JsonArray val1) {
        Short2ObjectMap<MapHeader> output = new Short2ObjectOpenHashMap<>();

        for (JsonElement jsonElement : val1) {
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            short id = jsonObject.get("id").getAsShort();
            MapGenerationType type = MapGenerationType.valueOf(jsonObject.get("generation").getAsString().toUpperCase());
            long seed = jsonObject.get("seed").getAsLong();
            output.put(id, new MapHeader(id, type, seed));
        }

        return output;
    }

}
