package common.loaders;

import com.google.gson.JsonObject;
import common.GameSettings;

public class SettingsLoader {
    public static GameSettings load(JsonObject o) {
        return new GameSettings(
                o.get("inputHandler").getAsString()
        );
    }
}
