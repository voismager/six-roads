package common;

import common.gameobjects.Hexagon;
import engine.Cleanable;
import engine.structures.ObjArray;

public interface RealMap extends Cleanable {
    Hexagon get(int q, int r, int l);

    void getFlatNeighbors(int q, int r, int l, ObjArray<Hexagon> result);

    void put(int q, int r, int l, Hexagon cell);

    void remove(int q, int r, int l);
}
