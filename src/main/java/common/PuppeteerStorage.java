package common;

import common.gameobjects.Puppeteer;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;

public class PuppeteerStorage {
    private final Int2ObjectMap<Puppeteer> map;

    public PuppeteerStorage() {
        this.map = new Int2ObjectOpenHashMap<>();
    }

    public void add(Puppeteer puppeteer) {
        this.map.put(puppeteer.getId(), puppeteer);
    }

    public Puppeteer get(int id) {
        return this.map.get(id);
    }

    public void remove(int id) {
        this.map.remove(id);
    }

    public int total() {
        return map.size();
    }
}
