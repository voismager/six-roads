package common.server.responses;

import server.BufferTools;
import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

import java.util.Arrays;

public class GameStartedResponse extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(GameStartedResponse::new);

    public long[] order;
    public long seed;
    public int room;

    public GameStartedResponse(long[] order, long seed, int room) {
        this.order = order;
        this.seed = seed;
        this.room = room;
    }

    private GameStartedResponse() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) {
        order = BufferTools.readLongArray(buffer);
        seed = buffer.readLong();
        room = buffer.readInt();
    }

    @Override
    protected void writeToBuffer(ByteBuf buffer) {
        BufferTools.writeLongArray(order, buffer);
        buffer.writeLong(seed);
        buffer.writeInt(room);
    }

    @Override
    public String toString() {
        return "GameStartedResponse{" +
                "order=" + Arrays.toString(order) +
                ", seed=" + seed +
                ", room=" + room +
                '}';
    }
}
