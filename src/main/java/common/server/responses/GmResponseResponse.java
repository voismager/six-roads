package common.server.responses;

import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

public class GmResponseResponse extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(GmResponseResponse::new);

    public byte status;

    public GmResponseResponse(byte status) {
        this.status = status;
    }

    private GmResponseResponse() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) {
        status = buffer.readByte();
    }

    @Override
    protected void writeToBuffer(ByteBuf buffer) {
        buffer.writeByte(status);
    }

    @Override
    public String toString() {
        return "GmResponseResponse{" +
                "status=" + status +
                '}';
    }
}
