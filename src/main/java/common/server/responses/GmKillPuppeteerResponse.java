package common.server.responses;

import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

public class GmKillPuppeteerResponse extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(GmKillPuppeteerResponse::new);

    public int puppeteer;

    public GmKillPuppeteerResponse(int puppeteer) {
        this.puppeteer = puppeteer;
    }

    private GmKillPuppeteerResponse() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) {
        puppeteer = buffer.readInt();
    }

    @Override
    protected void writeToBuffer(ByteBuf buffer) {
        buffer.writeInt(puppeteer);
    }

    @Override
    public String toString() {
        return "GmKillPuppeteerResponse{" +
                "puppeteer=" + puppeteer +
                '}';
    }
}
