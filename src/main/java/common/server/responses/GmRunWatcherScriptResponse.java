package common.server.responses;

import server.BufferTools;
import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

import java.util.Arrays;

public class GmRunWatcherScriptResponse extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(GmRunWatcherScriptResponse::new);

    public short id;
    public int stage;
    public int puppeteer;
    public int[] args;

    public GmRunWatcherScriptResponse(short id, int stage, int puppeteer, int[] args) {
        this.id = id;
        this.stage = stage;
        this.puppeteer = puppeteer;
        this.args = args;
    }

    private GmRunWatcherScriptResponse() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) {
        id = buffer.readShort();
        stage = buffer.readInt();
        puppeteer = buffer.readInt();
        args = BufferTools.readIntArray(buffer);
    }

    @Override
    protected void writeToBuffer(ByteBuf buffer) {
        buffer.writeShort(id);
        buffer.writeInt(stage);
        buffer.writeInt(puppeteer);
        BufferTools.writeIntArray(args, buffer);
    }

    @Override
    public String toString() {
        return "GmRunWatcherScriptResponse{" +
                "id=" + id +
                ", stage=" + stage +
                ", puppeteer=" + puppeteer +
                ", args=" + Arrays.toString(args) +
                '}';
    }
}
