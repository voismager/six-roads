package common.server.responses;

import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

public class GmNextTurnResponse extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(GmNextTurnResponse::new);

    public GmNextTurnResponse() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) { }

    @Override
    protected void writeToBuffer(ByteBuf buffer) { }
}
