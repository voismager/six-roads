package common.server.gamelogic.logic.effects;

import common.entities.Effect;
import constants.ConstIdentifiers;

import java.util.Arrays;

public class Buffer {
    private Effect[] writeEffects;
    private int[] writeIdentifiers;
    private long[] writeTriggers;
    private int writeSize;

    private Effect[] readEffects;
    private int[] readIdentifiers;
    private long[] readTriggers;
    private int readSize;

    private Effect lastReadEffect;
    private int lastReadIdentifier;
    private int caret;

    public Buffer() {
        writeEffects = new Effect[4];
        writeIdentifiers = new int[4];
        writeTriggers = new long[4];

        readEffects = new Effect[4];
        readIdentifiers = new int[4];
        readTriggers = new long[4];

        writeSize = 0;
        readSize = 0;
        caret = 0;
        lastReadEffect = null;
        lastReadIdentifier = ConstIdentifiers.NULL_IDENTIFIER;
    }

    public void write(Effect effect, int Identifier, long trigger) {
        if (writeSize == writeEffects.length) {
            int newLength = writeEffects.length * 2 + 1;

            writeEffects = Arrays.copyOf(writeEffects, newLength);
            writeIdentifiers = Arrays.copyOf(writeIdentifiers, newLength);
            writeTriggers = Arrays.copyOf(writeTriggers, newLength);
        }

        writeEffects[writeSize] = effect;
        writeIdentifiers[writeSize] = Identifier;
        writeTriggers[writeSize] = trigger;
        writeSize++;
    }

    public void rewind() {
        readEffects = Arrays.copyOf(writeEffects, writeSize);
        readIdentifiers = Arrays.copyOf(writeIdentifiers, writeSize);
        readTriggers = Arrays.copyOf(writeTriggers, writeSize);
        readSize = writeSize;

        for(int i = 0; i < readSize; ++i) {
            for(int j = i; j > 0 && readEffects[j - 1].getPriority() - readEffects[j].getPriority() > 0; --j) {
                Effect e = readEffects[j]; readEffects[j] = readEffects[j-1]; readEffects[j-1] = e;
                int id = readIdentifiers[j]; readIdentifiers[j] = readIdentifiers[j-1]; readIdentifiers[j-1] = id;
                long trigger = readTriggers[j]; readTriggers[j] = readTriggers[j-1]; readTriggers[j-1] = trigger;
            }
        }
    }

    public boolean isEmpty() {
        return writeSize == 0;
    }

    public boolean hasNext() {
        return caret != readSize;
    }

    public boolean skip() {
        return lastReadEffect == readEffects[caret] && lastReadIdentifier == readIdentifiers[caret];
    }

    public Effect nextEffect() {
        lastReadEffect = readEffects[caret];
        return readEffects[caret];
    }

    public int nextIdentifier() {
        lastReadIdentifier = readIdentifiers[caret];
        return readIdentifiers[caret];
    }

    public long nextTrigger() {
        return readTriggers[caret++];
    }

    public void clear() {
        writeSize = 0;
        caret = 0;
        lastReadEffect = null;
        lastReadIdentifier = ConstIdentifiers.NULL_IDENTIFIER;
    }
}
