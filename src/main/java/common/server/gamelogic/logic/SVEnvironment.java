package common.server.gamelogic.logic;

import common.gameobjects.Puppeteer;
import common.server.gamelogic.model.Module;
import common.server.gamelogic.service.Linker;
import engine.Cleanable;

public final class SVEnvironment implements Cleanable {
    private final GameTools tools;

    public Module module;

    public SVEnvironment(GameTools tools) {
        this.tools = tools;
    }

    public void unsubscribeAll() {
        this.tools.unsubscribeAll();
    }

    public void unsubscribe(Puppeteer puppeteer) {
        this.tools.unsubscribe(puppeteer);
    }

    public boolean rollDice(int probability) {
        return tools.rollDice(probability);
    }

    /////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////

    @Override
    public void cleanup() {
        //Linker.delink(module.scripts.values());
    }
}
