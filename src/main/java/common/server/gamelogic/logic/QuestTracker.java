package common.server.gamelogic.logic;

import common.server.gamelogic.model.gameobjects.SVActiveQuest;
import engine.annotations.APIUnsafe;
import engine.collections.WriteReadBuffer;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;
import it.unimi.dsi.fastutil.objects.ObjectLists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static constants.Triggers.*;
import static engine.support.keys.GameUtil.getTriggerKey;
import static engine.support.keys.Mapper.TD03_SI_L;

public class QuestTracker {
    private final Long2ObjectMap<ObjectList<SVActiveQuest>> questsPerTrigger;
    private final Long2ObjectMap<long[]> triggers;
    private final Int2ObjectMap<ObjectList<SVActiveQuest>> active;
    private final WriteReadBuffer<SVActiveQuest> buffer;

    private SVActiveQuest running;

    private Logger logger = LogManager.getLogger(QuestTracker.class);

    public QuestTracker() {
        this.questsPerTrigger = new Long2ObjectOpenHashMap<>();
        this.buffer = new WriteReadBuffer<>(SVActiveQuest.class);
        this.active = new Int2ObjectOpenHashMap<>();
        this.triggers = new Long2ObjectOpenHashMap<>();
    }

    public void cleanup() {
        questsPerTrigger.clear();
        triggers.clear();
        active.clear();
        buffer.cleanup();
    }

    public SVActiveQuest getActiveQuest(int puppeteer, short id) {
        ObjectList<SVActiveQuest> list = active.getOrDefault(puppeteer, ObjectLists.emptyList());

        int indexOf = -1;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).quest.id() == id) {
                indexOf = i;
                break;
            }
        }

        if (indexOf != -1) {
            return list.get(indexOf);
        }

        return null;
    }

    public ObjectList<SVActiveQuest> getActiveQuests(int puppeteer) {
        return active.get(puppeteer);
    }

    //////////////////////////////////////////////////////////////////////////////

    public void onPuppeteerAttributeChanged(int puppeteer, short attr, int oldValue) {
        this.runQuestsOnEvent(
                getTriggerKey(PUPPETEER_ATTRIBUTE_CHANGED, attr, puppeteer), oldValue
        );
    }

    public void onNewTurn(int puppeteer, int oldPuppeteer) {
        this.runQuestsOnEvent(
                getTriggerKey(NEW_TURN, puppeteer),
                oldPuppeteer
        );
    }

    @APIUnsafe
    public void onPuppeteerDeath(int puppeteer) {
        this.runQuestsOnEvent(
                getTriggerKey(PUPPETEER_DIED, puppeteer), 0
        );
    }

    //////////////////////////////////////////////////////////////////////////////

    public void untrack(int puppeteer) {
        ObjectList<SVActiveQuest> quests = this.active.getOrDefault(puppeteer, ObjectLists.emptyList());

        for (int i = 0; i < quests.size(); i++) {
            SVActiveQuest quest = quests.get(i);

            long key = TD03_SI_L(quest.quest.id(), puppeteer);

            long[] triggers = this.triggers.remove(key);

            for (long trigger : triggers) {
                this.questsPerTrigger.get(trigger).remove(quest);
            }

            quest.clearAttributes();
        }

        quests.clear();
    }

    public void untrack(short id, int puppeteer) {
        ObjectList<SVActiveQuest> quests = this.active.getOrDefault(puppeteer, ObjectLists.emptyList());

        SVActiveQuest quest = null;

        for (int i = 0; i < quests.size(); i++) {
            if (quests.get(i).quest.id() == id) {
                quest = quests.remove(i);
                break;
            }
        }

        if (quest != null) {
            long key = TD03_SI_L(id, puppeteer);

            long[] triggers = this.triggers.remove(key);

            for (long trigger : triggers) {
                this.questsPerTrigger.get(trigger).remove(quest);
            }

            quest.clearAttributes();
        }
    }

    public void track(SVActiveQuest activeQuest) {
        long[] triggers = activeQuest.triggers();

        for (long trigger : triggers) {
            this.questsPerTrigger.computeIfAbsent(trigger, t -> new ObjectArrayList<>())
                    .add(activeQuest);
        }

        long key = TD03_SI_L(activeQuest.quest.id(), activeQuest.puppeteer);

        this.active
                .computeIfAbsent(activeQuest.puppeteer, i -> new ObjectArrayList<>())
                .add(activeQuest);

        this.triggers.put(key, triggers);
    }

    private void runQuestsOnEvent(long trigger, int oldState) {
        final ObjectList<SVActiveQuest> triggered = this.questsPerTrigger.get(trigger);

        if (null != triggered && !triggered.isEmpty()) {
            this.writeToBuffer(triggered, trigger, oldState);
            this.runQuests();
        }
    }

    private void writeToBuffer(ObjectList<SVActiveQuest> triggered, long trigger, int oldState) {
        for (int i = 0; i < triggered.size(); i++) {
            this.writeToBuffer(triggered.get(i), trigger, oldState);
        }
    }

    private void writeToBuffer(SVActiveQuest quest, long trigger, int oldState) {
        if (!quest.equals(running)) {
            quest.currTrigger = trigger;
            quest.oldState = oldState;
            this.buffer.write(quest);
        }
    }

    private void runQuests() {
        if (running == null) {
            while (!buffer.isEmpty()) {
                buffer.fillRead();
                buffer.clearWrite();

                while (buffer.hasNext()) {
                    SVActiveQuest quest = buffer.next();
                    running = quest;
                    logger.info("Q-C: {}, {}", quest.quest.id(), quest.puppeteer);
                    if (quest.check()) {
                        quest.runEnding();
                        this.untrack(quest.quest.id(), quest.puppeteer);
                    }
                }
            }

            running = null;
        }
    }
}