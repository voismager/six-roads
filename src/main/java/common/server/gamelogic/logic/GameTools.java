package common.server.gamelogic.logic;

import common.gameobjects.Puppeteer;

public interface GameTools {
    void unsubscribeAll();

    void unsubscribe(Puppeteer p);

    boolean rollDice(int probability);
}
