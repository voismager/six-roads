package common.server.gamelogic.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import common.server.gamelogic.model.Meta;
import common.server.gamelogic.model.Module;
import engine.support.loaders.FilesLoader;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ModuleRepository {
    private final static Logger logger = LogManager.getLogger(ModuleRepository.class);

    private final Map<UUID, Module> IN_MEMORY = new HashMap<>();
    private final JsonParser JSON_PARSER = new JsonParser();

    public Module get(UUID key) {
        return IN_MEMORY.get(key);
    }

    public Collection<UUID> getKeys() {
        return IN_MEMORY.keySet();
    }

    public Module getCopy(UUID uuid) {
        return null;
    }

    public void loadEmbeddedModule(UUID uuid) throws Exception {
        String prefix = "/modules/global/" + uuid.toString() + "/";

        final InputStream metaAsStream = FilesLoader.loadResource(prefix + "meta.json");
        final JsonObject metaAsJson = JSON_PARSER.parse(new InputStreamReader(metaAsStream)).getAsJsonObject();

        UUID foundUuid = UUID.fromString(metaAsJson.get("id").getAsString());

        if (foundUuid.equals(uuid)) {
            IN_MEMORY.put(uuid, buildEmbedded(prefix, uuid, metaAsJson));
        } else throw new IOException();
    }

    private Module buildEmbedded(String path, UUID uuid, JsonObject header) throws Exception {
        return null;
    }

    private static Meta loadMeta(UUID uuid, JsonObject header) {
        String name = header.get("name").getAsString();

        return new Meta(uuid, name,
                new Short2ObjectOpenHashMap<>(),
                new Short2ObjectOpenHashMap<>()
        );
    }
}
