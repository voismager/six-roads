package common.server.gamelogic.model.module;

import common.entities.Quest;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

public class SVGamemode {
    public final short key;
    public final int maxTurnDuration;
    public final Quest[] quests;

    public SVGamemode(short key, int maxTurnDuration, Quest[] quests) {
        this.key = key;
        this.maxTurnDuration = maxTurnDuration;
        this.quests = quests;
    }

    public SVGamemode copy(Short2ObjectMap<Quest> quests) {
        Quest[] a = new Quest[this.quests.length];

        for (int i = 0; i < this.quests.length; i++) {
            a[i] = quests.get(this.quests[i].id());
        }

        return new SVGamemode(key, maxTurnDuration, a);
    }
}
