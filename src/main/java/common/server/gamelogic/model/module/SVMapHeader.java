package common.server.gamelogic.model.module;

import common.entities.Callable;
import common.entities.Faction;
import common.entities.Quest;
import constants.EntityType;
import constants.MapGenerationType;
import engine.support.loaders.UnifiedResource;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import common.Entity;

import java.util.ArrayList;
import java.util.List;

public class SVMapHeader extends Entity {
    public final List<SVFactionRule> rules;
    public final MapGenerationType type;
    public final UnifiedResource source;
    public final Callable init;

    public SVMapHeader(
            MapGenerationType type,
            short id,
            UnifiedResource source,
            List<SVFactionRule> rules, Callable init) {

        super(id, EntityType.MAP);

        this.type = type;
        this.rules = rules;
        this.source = source;
        this.init = init;
    }

    public SVMapHeader copy(Short2ObjectMap<Quest> quests, Short2ObjectMap<Faction> fractions) {
        List<SVFactionRule> rules = new ArrayList<>(this.rules.size());

        for (int i = 0; i < this.rules.size(); i++) {
            rules.add(this.rules.get(i).copy(quests, fractions));
        }

        return new SVMapHeader(type, id, source, rules, init);
    }

    public final int maxPlayers() {
        return rules.size();
    }
}
