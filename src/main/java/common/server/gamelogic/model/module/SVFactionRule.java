package common.server.gamelogic.model.module;

import common.entities.Faction;
import common.entities.AttributeSet;
import common.entities.Callable;
import common.entities.Quest;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

public class SVFactionRule {
    public final Faction faction;
    public final Callable initScript;
    public final AttributeSet attributes;
    public final SVGamemode gamemode;

    public SVFactionRule(
            Faction faction,
            Callable initScript,
            AttributeSet attributes,
            SVGamemode gamemode) {

        this.faction = faction;
        this.initScript = initScript;
        this.attributes = attributes;
        this.gamemode = gamemode;
    }

    public SVFactionRule copy(Short2ObjectMap<Quest> quests, Short2ObjectMap<Faction> factions) {
        Faction fraction = factions.get(this.faction.id());
        AttributeSet set = new AttributeSet(attributes);
        SVGamemode gamemode = this.gamemode.copy(quests);
        return new SVFactionRule(fraction, initScript, set, gamemode);
    }
}
