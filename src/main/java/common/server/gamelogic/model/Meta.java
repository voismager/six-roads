package common.server.gamelogic.model;

import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

import java.util.UUID;

public final class Meta {
    public final UUID id;
    public final String name;
    public final Short2ObjectMap<Object> sources;
    public final Short2ObjectMap<Object2ObjectMap<String, Object>> parameters;

    public Meta(UUID id, String name, Short2ObjectMap<Object> sources, Short2ObjectMap<Object2ObjectMap<String, Object>> parameters) {
        this.id = id;
        this.name = name;
        this.sources = sources;
        this.parameters = parameters;
    }
}
