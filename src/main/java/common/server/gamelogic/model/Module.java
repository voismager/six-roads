package common.server.gamelogic.model;

import common.GameSettings;
import common.entities.*;
import common.server.gamelogic.model.module.SVMapHeader;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

public final class Module {
    public final Meta meta;
    public final GameSettings settings;
    public final Short2ObjectMap<Callable> scripts;
    public final Short2ObjectMap<Quest> quests;
    public final Short2ObjectMap<SVMapHeader> maps;
    public final Short2ObjectMap<Component> components;

    public Module(
            Meta meta,
            GameSettings settings, Short2ObjectMap<Callable> scripts,
            Short2ObjectMap<Quest> quests,
            Short2ObjectMap<SVMapHeader> maps,
            Short2ObjectMap<Component> components
    ) {
        this.meta = meta;
        this.settings = settings;
        this.scripts = scripts;
        this.quests = quests;
        this.maps = maps;
        this.components = components;
    }

}
