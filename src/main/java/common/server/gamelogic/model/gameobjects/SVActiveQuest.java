package common.server.gamelogic.model.gameobjects;

import common.entities.Quest;
import engine.structures.ObjArray;
import engine.structures.pairs.ObjShortPair;
import engine.types.integers.IntType;
import it.unimi.dsi.fastutil.shorts.Short2ObjectArrayMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;

public class SVActiveQuest {
    public final int puppeteer;
    public final Quest quest;
    public long currTrigger;
    public int oldState;

    private Short2ObjectMap<IntType> attributes;

    public SVActiveQuest(Quest quest, int puppeteer) {
        this.puppeteer = puppeteer;
        this.quest = quest;

        ObjArray<ObjShortPair<IntType>> instAttributes = quest.attributes.getInstAttributes();

        for (int i = 0; i < instAttributes.size(); i++) {
            ObjShortPair<IntType> attr = instAttributes.get(i);
            addAttribute(attr.id, attr.value.copy());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SVActiveQuest quest1 = (SVActiveQuest) o;
        return puppeteer == quest1.puppeteer &&
                quest.id() == quest1.quest.id();
    }

    @Override
    public int hashCode() {
        return puppeteer + quest.id() * 31;
    }

    public void runEnding() {
        quest.ending.runner.sv_call(puppeteer);
    }

    public boolean check() {
        return quest.condition.runner.sv_call_boolean(puppeteer, currTrigger, oldState);
    }

    public long[] triggers() {
        return quest.condition.runner.sv_call_longs(puppeteer);
    }

    public IntType getAttribute(short id) {
        if (null != this.attributes) {
            return this.attributes.get(id);
        }

        return null;
    }

    public void addAttribute(short id, IntType value) {
        if (null == this.attributes) {
            this.attributes = new Short2ObjectArrayMap<>();
        }

        else {
            IntType attr = this.attributes.get(id);

            if (null != attr) {
                attr.set(value);
                return;
            }
        }

        this.attributes.put(id, value.copy());
    }

    public void clearAttributes() {
        if (null != this.attributes) {
            this.attributes.clear();
            this.attributes = null;
        }
    }
}
