package common.server.requests;

import io.netty.buffer.ByteBuf;
import server.BufferTools;
import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;

import java.util.Arrays;

public class RunGameScript extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(RunGameScript::new);

    public short script;
    public byte innerScriptIndex;
    public int[] args;
    public boolean blocking;

    public RunGameScript(short script, byte innerScriptIndex, int[] args, boolean blocking) {
        this.script = script;
        this.innerScriptIndex = innerScriptIndex;
        this.args = args;
        this.blocking = blocking;
    }

    private RunGameScript() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf byteBuf) {
        this.script = byteBuf.readShort();
        this.innerScriptIndex = byteBuf.readByte();
        this.args = BufferTools.readIntArray(byteBuf);
    }

    @Override
    protected void writeToBuffer(ByteBuf buffer) {
        buffer.writeShort(script);
        buffer.writeByte(innerScriptIndex);
        BufferTools.writeIntArray(args, buffer);
    }

    @Override
    public String toString() {
        return "GmRunScript{" +
                "action=" + script +
                ", stage=" + innerScriptIndex +
                ", args=" + Arrays.toString(args) +
                '}';
    }
}
