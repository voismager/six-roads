package common.server.requests;

import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

import java.util.function.Supplier;

public class StopPlayingRequest extends CustomMessage {
    public final static Header HEADER = Headers.registerMessage(new Supplier<CustomMessage>() {
        final StopPlayingRequest INSTANCE = new StopPlayingRequest();
        public CustomMessage get() { return INSTANCE; }
    });

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) { }
    @Override
    public void writeToBuffer(ByteBuf buffer) { }
}
