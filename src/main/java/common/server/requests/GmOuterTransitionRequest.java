package common.server.requests;

import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

public class GmOuterTransitionRequest extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(GmOuterTransitionRequest::new);

    public long key;
    public int arg0;

    public GmOuterTransitionRequest(long key, int arg0) {
        this.key = key;
        this.arg0 = arg0;
    }

    private GmOuterTransitionRequest() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) {
        key = buffer.readLong();
        arg0 = buffer.readInt();
    }

    @Override
    public void writeToBuffer(ByteBuf buffer) {
        buffer.writeLong(key);
        buffer.writeInt(arg0);
    }

    @Override
    public String toString() {
        return "GmOuterTransitionRequest{" +
                "key=" + key +
                ", arg0=" + arg0 +
                '}';
    }
}
