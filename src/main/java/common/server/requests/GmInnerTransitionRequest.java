package common.server.requests;

import server.message.CustomMessage;
import server.message.Header;
import server.message.Headers;
import io.netty.buffer.ByteBuf;

public class GmInnerTransitionRequest extends CustomMessage {
    public static final Header HEADER = Headers.registerMessage(GmInnerTransitionRequest::new);

    public long stageAndFlags;
    public long key;
    public int arg0;

    public GmInnerTransitionRequest(long stageAndFlags, long key, int arg0) {
        this.stageAndFlags = stageAndFlags;
        this.key = key;
        this.arg0 = arg0;
    }

    private GmInnerTransitionRequest() {
    }

    @Override
    public Header getHeader() {
        return HEADER;
    }

    @Override
    public void readFromBuffer(ByteBuf buffer) {
        stageAndFlags = buffer.readLong();
        key = buffer.readLong();
        arg0 = buffer.readInt();
    }

    @Override
    public void writeToBuffer(ByteBuf buffer) {
        buffer.writeLong(stageAndFlags);
        buffer.writeLong(key);
        buffer.writeInt(arg0);
    }

    @Override
    public String toString() {
        return "GmInnerTransitionRequest{" +
                "stageAndFlags=" + stageAndFlags +
                ", key=" + key +
                ", arg0=" + arg0 +
                '}';
    }
}
