package common.server.handlers;

import common.server.EarlyGameSession;
import common.server.gamelogic.model.Module;
import common.server.properties.ModuleProperty;
import common.server.requests.StartGameRequest;
import server.RPlayer;
import server.SessionRoom;
import server.globalsession.requesthandlers.Handler;
import server.message.ChannelMessage;
import server.message.Header;
import server.message.PlayerPropertySetResponse;
import server.property.Properties;

import java.util.UUID;

public class StartGameHandler implements Handler<EarlyGameSession> {
    @Override
    public void accept(EarlyGameSession session, ChannelMessage request) {
        final RPlayer player = session.getRegistered(request.channel);
        final SessionRoom room = session.getRoomByPlayer(player);

        if (room != null && room.getProperty(Properties.ROOM_HOST).equals(player.player.id)) {
            final UUID moduleId = (UUID) room.getProperty(ModuleProperty.KEY);
            final Module module = session.getModule(moduleId);

            room.getMembersStream().forEach(pl -> {
                pl.player.setProperty(Properties.PLAYER_READY_FLAG, false);

                session.getSessionSender()
                        .addAll()
                        .sendMessage(new PlayerPropertySetResponse(pl.player.id, Properties.PLAYER_READY_FLAG, false))
                        .flush();
            });

            Module copy = session.getCopy(moduleId);

            room.getGameSession().start(
                    session.getSessionSender(),
                    room.getMembersStream().toArray(RPlayer[]::new),
                    copy,
                    null
            );

            /*
            if (module != null) {
                final short mapId = (short) room.getProperty(MapProperty.KEY);
                final SVMapHeader map = module.maps.get(mapId);


                if (map != null) {
                    if (map.rules.size() >= room.getSize()) {
                        long host = (long) room.getProperty(Properties.ROOM_HOST);

                        boolean allReady = room.getMembersStream()
                                .allMatch(p -> {
                                    if ((Short) p.player.getProperty(FactionProperty.KEY) == NULL_FACTION) return false;
                                    return p.player.id == host || (Boolean) p.player.getProperty(Properties.PLAYER_READY_FLAG);
                                });

                        if (allReady) {
                            room.getMembersStream().forEach(pl -> {
                                pl.player.setProperty(Properties.PLAYER_READY_FLAG, false);

                                session.getSessionSender()
                                        .addAll()
                                        .sendMessage(new PlayerPropertySetResponse(pl.player.id, Properties.PLAYER_READY_FLAG, false))
                                        .flush();
                            });

                            Module copy = session.getCopy(moduleId);

                            room.getGameSession().start(
                                    session.getSessionSender(),
                                    room.getMembersStream().toArray(RPlayer[]::new),
                                    copy,
                                    copy.maps.get(mapId)
                            );
                        }
                    }
                }
            }

             */
        }
    }

    @Override
    public Header[] headers() {
        return new Header[] { StartGameRequest.HEADER };
    }
}
