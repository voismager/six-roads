package common.server.handlers;

import common.server.properties.MapProperty;
import common.server.properties.MaxPlayersProperty;
import common.server.properties.ModuleProperty;
import server.RPlayer;
import server.globalsession.requesthandlers.Handler;
import server.message.*;
import server.SessionRoom;
import common.server.gamelogic.model.module.SVMapHeader;
import common.server.EarlyGameSession;
import server.globalsession.GlobalSession;
import server.property.Properties;
import server.property.PropertyKey;

import java.util.UUID;

public class SetRoomPropertyHandler implements Handler<EarlyGameSession> {
    @Override
    public void accept(EarlyGameSession session, ChannelMessage req) {
        final RPlayer player = session.getRegistered(req.channel);
        final SessionRoom room = session.getRoomByPlayer(player);

        SetRoomPropertyRequest request = (SetRoomPropertyRequest) req.request;

        if (room != null && room.getProperty(Properties.ROOM_HOST).equals(player.player.id)) {
            if (!request.value.equals(room.getProperty(request.prop)) && request.prop.validate(request.value, session, room)) {
                if (request.prop == ModuleProperty.KEY) {
                    setProperty(session, room, MapProperty.KEY, MapProperty.KEY.defaultValue());
                } else if (request.prop == MapProperty.KEY) {
                    SVMapHeader map = session
                            .getModule((UUID) room.getProperty(ModuleProperty.KEY)).maps.get((short)request.value);

                    setProperty(
                            session,
                            room,
                            MaxPlayersProperty.KEY,
                            Math.min((Integer) room.getProperty(MaxPlayersProperty.KEY), map.maxPlayers())
                    );
                }

                setProperty(session, room, request.prop, request.value);
            }
        }
    }

    private void setProperty(GlobalSession session, SessionRoom room, PropertyKey property, Object o) {
        room.setProperty(property, o);

        session.getSessionSender()
                .addAll()
                .sendMessage(new RoomPropertySetResponse(room.getId(), property, o))
                .flush();
    }

    @Override
    public Header[] headers() {
        return new Header[] { Headers.SET_ROOM_PROPERTY_REQUEST };
    }
}
