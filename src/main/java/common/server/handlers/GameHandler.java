package common.server.handlers;

import common.server.requests.GmInnerTransitionRequest;
import common.server.requests.GmNextTurnRequest;
import common.server.requests.GmOuterTransitionRequest;
import server.RPlayer;
import server.globalsession.requesthandlers.Handler;
import server.message.ChannelMessage;
import server.message.Header;
import server.SessionRoom;
import server.globalsession.GlobalSession;

public class GameHandler implements Handler<GlobalSession> {
    @Override
    public void accept(GlobalSession session, ChannelMessage request) {
        final RPlayer player = session.getRegistered(request.channel);
        final SessionRoom room = session.getRoomByPlayer(player);

        if (room != null && room.getGameSession().isSubscribed(player)) {
            room.getGameSession().processGameRequest(player, request.request);
        }
    }

    @Override
    public Header[] headers() {
        return new Header[] {
                GmNextTurnRequest.HEADER,
                GmOuterTransitionRequest.HEADER,
                GmInnerTransitionRequest.HEADER
        };
    }
}
