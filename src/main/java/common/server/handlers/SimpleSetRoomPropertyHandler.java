package common.server.handlers;

import common.server.EarlyGameSession;
import server.RPlayer;
import server.SessionRoom;
import server.globalsession.GlobalSession;
import server.globalsession.requesthandlers.Handler;
import server.message.*;
import server.property.PropertyKey;

public class SimpleSetRoomPropertyHandler implements Handler<EarlyGameSession> {
    @Override
    public Header[] headers() {
        return new Header[] { Headers.SET_ROOM_PROPERTY_REQUEST };
    }

    @Override
    public void accept(EarlyGameSession session, ChannelMessage req) {
        final RPlayer player = session.getRegistered(req.channel);
        final SessionRoom room = session.getRoomByPlayer(player);
        SetRoomPropertyRequest request = (SetRoomPropertyRequest) req.request;
        setProperty(session, room, request.prop, request.value);
    }

    private void setProperty(GlobalSession session, SessionRoom room, PropertyKey property, Object o) {
        room.setProperty(property, o);

        session.getSessionSender()
                .addAll()
                .sendMessage(new RoomPropertySetResponse(room.getId(), property, o))
                .flush();
    }
}
