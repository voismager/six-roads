package common.server.handlers;

import common.server.requests.StopPlayingRequest;
import server.RPlayer;
import server.globalsession.requesthandlers.Handler;
import server.message.ChannelMessage;
import server.message.Header;
import server.SessionRoom;
import server.globalsession.GlobalSession;

public class StopPlayingHandler implements Handler<GlobalSession> {
    @Override
    public void accept(GlobalSession session, ChannelMessage message) {
        final RPlayer player = session.getRegistered(message.channel);
        final SessionRoom room = session.getRoomByPlayer(player);

        if (room != null && room.getGameSession().isSubscribed(player)) {
            room.getGameSession().processStopPlayingRequest(player);
        }
    }

    @Override
    public Header[] headers() {
        return new Header[] { StopPlayingRequest.HEADER };
    }
}
