package common.server.gamesession;

import server.RPlayer;

import java.util.ArrayList;
import java.util.List;

public class UnsubscribeTrigger {
    private final List<RPlayer> players;
    private boolean trigger;
    private boolean all;

    public UnsubscribeTrigger() {
        this.players = new ArrayList<>();
    }

    public void triggerAll() {
        trigger = true;
        all = true;
    }

    public void triggerOne(RPlayer player) {
        trigger = true;
        all = false;
        players.add(player);
    }

    public boolean isTriggered() {
        return trigger;
    }

    public boolean isAll() {
        return all;
    }

    public List<RPlayer> getPlayers() {
        return players;
    }

    public void clear() {
        trigger = false;
        players.clear();
    }
}
