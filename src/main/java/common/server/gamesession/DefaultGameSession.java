package common.server.gamesession;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import common.gameobjects.Puppeteer;
import common.server.gamelogic.logic.GameTools;
import common.server.gamelogic.logic.SVEnvironment;
import common.server.gamelogic.model.Module;
import common.server.gamelogic.model.module.SVMapHeader;
import common.server.gamelogic.service.EnvironmentBuilder;
import common.server.requests.RunGameScript;
import common.server.responses.GameStartedResponse;
import common.server.responses.GmKillPuppeteerResponse;
import engine.support.RandomUtil;
import engine.support.Tools;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.GameSession;
import server.RPlayer;
import server.message.Message;
import server.message.PlayerUnsubscribedResponse;
import server.messagesender.GameSender;
import server.messagesender.MessageSender;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Ограничения: myTurnRoot и othersTurnRoot не должны иметь ENTER_STAGE в SV и WT
 * Это нужно для правильного функционирования processRequestForNextTurn
 */
public class DefaultGameSession implements GameSession {
    private static Logger logger = LogManager.getLogger(DefaultGameSession.class);

    private final int roomId;
    private final BiMap<RPlayer, Puppeteer> bijection;
    private final Set<RPlayer> subscribers;
    private final GameSender sender;
    private final UnsubscribeTrigger trigger;
    private final Random dice;

    private MessageSender globalSender;
    private SVEnvironment env;

    public DefaultGameSession(int roomId) {
        this.roomId = roomId;
        this.bijection = HashBiMap.create();
        this.subscribers = new HashSet<>();
        this.sender = new GameSender(this);
        this.trigger = new UnsubscribeTrigger();
        this.dice = new Random(RandomUtil.globalNextLong());
    }

    @Override
    public boolean isPlaying() {
        return !subscribers.isEmpty();
    }

    @Override
    public boolean isSubscribed(RPlayer player) {
        return subscribers.contains(player);
    }

    @Override
    public Stream<RPlayer> getSubscribersStream() {
        return subscribers.stream();
    }

    @Override
    public void start(MessageSender globalSender, RPlayer[] players, Object... args) {
        this.globalSender = globalSender;

        Module module = ((Module) args[0]);
        SVMapHeader map = ((SVMapHeader) args[1]);

        Tools.shuffleArray(players);

        try {
            this.env = EnvironmentBuilder.build(module, map, new GameTools() {
                @Override
                public void unsubscribeAll() {
                    trigger.triggerAll();
                }

                @Override
                public void unsubscribe(Puppeteer p) {
                    trigger.triggerOne(getPlayer(p));
                }

                @Override
                public boolean rollDice(int probability) {
                    return RandomUtil.rollDice(probability, dice);
                }
            });

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        bijection.clear();
        subscribers.clear();
        //for (int i = 0; i < factions.length; i++) {
            //bijection.put(players[i], env.puppeteers.get(i));
        //    subscribers.add(players[i]);
        //}

        final long[] order = Arrays.stream(players)
                .mapToLong(pl -> pl.player.id)
                .toArray();

        long seed = RandomUtil.globalNextLong();
        dice.setSeed(seed);
        this.globalSender.addAll().sendMessage(new GameStartedResponse(order, seed, roomId)).flush();
    }

    @Override
    public void unsubscribeAll() {
        if (!subscribers.isEmpty()) {
            doUnsubscribeAll();
            //env.cleanup();
        }
    }

    @Override
    public void unsubscribe(RPlayer player) {
        if (subscribers.contains(player)) {
            doUnsubscribe(player);
            //if (subscribers.isEmpty()) env.cleanup();
        }
    }

    @Override
    public void processStopPlayingRequest(RPlayer player) {
        doUnsubscribeAndKill(player);
    }

    @Override
    public void processGameRequest(RPlayer issuer, Message request) {
        if (request.getHeader() == RunGameScript.HEADER) {
            final RunGameScript r = (RunGameScript) request;
            processServerScript(issuer, r.script, r.innerScriptIndex, r.args);
        }

        checkTrigger();
    }

    private void checkTrigger() {
        if (trigger.isTriggered()) {
            try {
                if (trigger.isAll()) unsubscribeAll();
                else {
                    for (RPlayer pl : trigger.getPlayers()) unsubscribe(pl);
                }

            } finally {
                trigger.clear();
            }
        }
    }

    private void processServerScript(RPlayer issuer, short actionId, int stage, int[] args) {
        final Puppeteer puppeteer = getPuppeteer(issuer);
        //final Callable action = this.env.module.scripts.get(actionId);

        //if (action != null) {
            //action.runner.sv_call(stage, puppeteer.getId(), args);
        //}
    }

    private void doUnsubscribeAndKill(RPlayer player) {
        this.doUnsubscribe(player);

        if (subscribers.isEmpty()) {
            //env.cleanup();
        } else {
            final Puppeteer puppeteer = getPuppeteer(player);

            //env.killPuppeteer(puppeteer.getId());

            this.sender.addAll()
                    .sendMessage(new GmKillPuppeteerResponse(puppeteer.getId()))
                    .flush();

            checkTrigger();
        }
    }

    private void doUnsubscribe(RPlayer player) {
        this.globalSender.addAll()
                .sendMessage(new PlayerUnsubscribedResponse(player.player.id, roomId))
                .flush();

        this.subscribers.remove(player);
    }

    private void doUnsubscribeAll() {
        for (RPlayer player : this.subscribers) {
            this.globalSender.addAll()
                    .sendMessage(new PlayerUnsubscribedResponse(player.player.id, roomId))
                    .flush();
        }

        this.subscribers.clear();
    }

    private Puppeteer getPuppeteer(RPlayer player) {
        return bijection.get(player);
    }

    private RPlayer getPlayer(Puppeteer puppeteer) {
        return bijection.inverse().get(puppeteer);
    }
}
