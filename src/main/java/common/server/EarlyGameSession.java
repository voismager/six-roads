package common.server;

import common.server.gamelogic.model.Module;
import common.server.gamelogic.service.ModuleRepository;
import common.server.gamesession.DefaultGameSession;
import common.server.handlers.*;
import common.server.properties.LoadedModulesProperty;
import engine.network.api.APIHolder;
import server.globalsession.GlobalSession;
import server.globalsession.requesthandlers.*;

import java.util.List;
import java.util.UUID;

public class EarlyGameSession extends GlobalSession {
    private final ModuleRepository moduleRepository;

    public EarlyGameSession() {
        super(
                APIHolder.getInstance(),
                DefaultGameSession::new,
                new SessionDataRequestHandler(),
                new RegistrationHandler(),
                new DisconnectHandler(),
                new CreateRoomHandler(),
                new JoinRoomHandler(),
                new LeaveRoomHandler(),
                new GameHandler(),
                new SetPlayerPropertyHandler(),
                new SimpleSetRoomPropertyHandler(),
                new StartGameHandler(),
                new StopPlayingHandler()
        );

        this.moduleRepository = new ModuleRepository();
    }

    public synchronized void addModule(UUID uuid) throws Exception {
        this.moduleRepository.loadEmbeddedModule(uuid);
        ((List<UUID>) getProperty(LoadedModulesProperty.KEY)).add(uuid);
    }

    public Module getModule(UUID uuid) {
        return moduleRepository.get(uuid);
    }

    public Module getCopy(UUID uuid) {
        return moduleRepository.getCopy(uuid);
    }
}
