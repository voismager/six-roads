package common.server.properties;

import server.property.CustomPropertyKey;
import server.BufferTools;
import io.netty.buffer.ByteBuf;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class LoadedModulesProperty extends CustomPropertyKey {
    public static final LoadedModulesProperty KEY = new LoadedModulesProperty();

    @Override
    public byte type() {
        return SESSION_PROPERTY;
    }

    @Override
    public Object defaultValue() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public Object readFromBuffer(ByteBuf buffer) {
        return BufferTools.readUUIDList(buffer);
    }

    @Override
    public void writeToBuffer(Object value, ByteBuf buffer) {
        BufferTools.writeUUIDList((List<UUID>)value, buffer);
    }

    @Override
    public boolean validate(Object value, Object... args) {
        return true;
    }
}
