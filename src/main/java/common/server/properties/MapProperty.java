package common.server.properties;

import server.property.CustomPropertyKey;
import io.netty.buffer.ByteBuf;
import server.SessionRoom;
import common.server.gamelogic.model.Module;
import common.server.EarlyGameSession;

import java.util.UUID;

import static constants.ConstIdentifiers.NULL_MAP;

public class MapProperty extends CustomPropertyKey {
    public static final MapProperty KEY = new MapProperty();

    @Override
    public byte type() {
        return ROOM_PROPERTY;
    }

    @Override
    public Object defaultValue() {
        return NULL_MAP;
    }

    @Override
    public Object readFromBuffer(ByteBuf buffer) {
        short value = buffer.readShort();
        if (value < 0 && value != NULL_MAP) throw new IllegalArgumentException();
        return value;
    }

    @Override
    public void writeToBuffer(Object value, ByteBuf buffer) {
        buffer.writeShort((Short)value);
    }

    @Override
    public boolean validate(Object value, Object... args) {
        EarlyGameSession session = (EarlyGameSession) args[0];
        SessionRoom room = (SessionRoom) args[1];

        Module module = session.getModule((UUID)room.getProperty(ModuleProperty.KEY));
        return module != null && module.maps.containsKey((short)value);
    }
}
