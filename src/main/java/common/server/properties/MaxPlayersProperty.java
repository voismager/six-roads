package common.server.properties;

import server.property.CustomPropertyKey;
import io.netty.buffer.ByteBuf;
import server.SessionRoom;
import common.server.gamelogic.model.Module;
import common.server.gamelogic.model.module.SVMapHeader;
import common.server.EarlyGameSession;

import java.util.UUID;

public class MaxPlayersProperty extends CustomPropertyKey {
    public static final MaxPlayersProperty KEY = new MaxPlayersProperty();

    @Override
    public byte type() {
        return ROOM_PROPERTY;
    }

    @Override
    public Object defaultValue() {
        return 3;
    }

    @Override
    public Object readFromBuffer(ByteBuf buffer) {
        Integer value = buffer.readInt();
        if (value < 1) throw new IllegalArgumentException();
        return value;
    }

    @Override
    public void writeToBuffer(Object value, ByteBuf buffer) {
        buffer.writeInt((Integer)value);
    }

    @Override
    public boolean validate(Object value, Object... args) {
        EarlyGameSession session = (EarlyGameSession) args[0];
        SessionRoom room = (SessionRoom) args[1];

        Module module = session.getModule((UUID) room.getProperty(ModuleProperty.KEY));

        if (module != null) {
            SVMapHeader map = module.maps.get((short) room.getProperty(MapProperty.KEY));

            if (map != null) {
                int maxPlayers = map.rules.size();

                if (maxPlayers < (int) value) {
                    return false;
                }
            }
        }

        return room.getSize() <= (int) value;
    }
}
