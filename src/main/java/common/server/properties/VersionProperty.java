package common.server.properties;

import io.netty.buffer.ByteBuf;
import server.BufferTools;
import server.property.CustomPropertyKey;

public class VersionProperty extends CustomPropertyKey {
    public static final VersionProperty KEY = new VersionProperty();

    @Override
    public byte type() {
        return SESSION_PROPERTY;
    }

    @Override
    public Object defaultValue() {
        return "null";
    }

    @Override
    public Object readFromBuffer(ByteBuf buffer) {
        return BufferTools.readString(buffer);
    }

    @Override
    public void writeToBuffer(Object value, ByteBuf buffer) {
        BufferTools.writeString((String) value, buffer);
    }

    @Override
    public boolean validate(Object value, Object... args) {
        return true;
    }
}
