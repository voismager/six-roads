package common.server.properties;

import server.property.CustomPropertyKey;
import server.BufferTools;
import io.netty.buffer.ByteBuf;
import common.server.EarlyGameSession;

import java.util.UUID;

import static constants.net.NetConstants.NULL_MODULE;

public class ModuleProperty extends CustomPropertyKey {
    public static final ModuleProperty KEY = new ModuleProperty();

    @Override
    public byte type() {
        return ROOM_PROPERTY;
    }

    @Override
    public Object defaultValue() {
        return NULL_MODULE;
    }

    @Override
    public Object readFromBuffer(ByteBuf buffer) {
        return BufferTools.readUUID(buffer);
    }

    @Override
    public void writeToBuffer(Object value, ByteBuf buffer) {
        BufferTools.writeUUID((UUID)value, buffer);
    }

    @Override
    public boolean validate(Object value, Object... args) {
        EarlyGameSession session = (EarlyGameSession) args[0];
        return session.getModule((UUID)value) != null;
    }
}
