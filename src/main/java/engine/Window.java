package engine;

import client.Settings;
import imgui.Context;
import imgui.ImGui;
import imgui.impl.LwjglGL3;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import uno.glfw.GlfwWindow;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Window {
    public static int WIDTH;
    public static int HEIGHT;
    public static final float z_NEAR = 0.1f;
    public static final float z_FAR = 100f;
    public static final float FOV = (float) Math.toRadians(55.0f);

    private static final String title = "Six Roads";

    private static long handle;

    public static void toggleVSync(boolean vSync) {
        if (vSync) {
            glfwSwapInterval(1);
        }

        else {
            glfwSwapInterval(0);
        }
    }

    public static void init() throws Exception {
        WIDTH = Settings.WINDOW_WIDTH;
        HEIGHT = Settings.WINDOW_HEIGHT;

        GLFWErrorCallback.createPrint(System.err).set();

        if (!glfwInit())
            throw new IllegalStateException("Unable to set GLFW");

        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        glfwWindowHint(GLFW_SAMPLES, 4);

        create();

        if (Settings.V_SYNC) glfwSwapInterval(1);

        GL.createCapabilities();

        glClearColor(1f, 1f, 1f, 1f);

        glEnable(GL_MULTISAMPLE);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        input();
    }

    private static void create() {
        int width  = Settings.WINDOW_WIDTH;
        int height = Settings.WINDOW_HEIGHT;
        boolean fullscreen = Settings.FULLSCREEN;

        handle = glfwCreateWindow(
                width,
                height,
                title,
                fullscreen ? glfwGetPrimaryMonitor() : NULL,
                NULL
        );

        if (handle == NULL)
            throw new RuntimeException("Failed to create the GLFW window");

        if (!fullscreen) {
            GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            glfwSetWindowPos(
                    handle,
                    (vidMode.width() - width) / 2,
                    (vidMode.height() - height) / 2
            );

            glfwShowWindow(handle);
        }

        glfwMakeContextCurrent(handle);
    }

    private static void input() {
        Context context = new Context(null);

        GlfwWindow window = new GlfwWindow(handle);

        LwjglGL3.INSTANCE.init(window, true);

        ImGui.INSTANCE.styleColorsDark(null);

        MouseInput.init(handle);
    }

    public static long getHandle() {
        return handle;
    }

    public static void update() {
        glfwSwapBuffers(handle);
        glfwPollEvents();
    }
}