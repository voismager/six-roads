package engine;

import org.joml.Vector2f;
import org.joml.Vector2i;

//Modify <-- Before 20.08.17, oldest alive comment in project
// I just found out that comment ^^,
// found an approximate date from my old 'backup' folder (which outlives 2 Windows reinstalls and currently lies in Ubuntu :D),
// now I'm in tears <-- 27.03.19
// So I decided to start a small blog right here, in comments <-- 27.03.19
// Thank you, Java. Dead Island trailer music will give you the right vibe <-- 27.03.19
// Hi, I'm still alive <-- 08.09.19

public final class MouseSelection {
    private static final float COEFF0 = 2F / 3F;
    private static final float COEFF1 = (float) (1D / Math.sqrt(3D));

    private MouseSelection() { }

    public static Vector2i pixelToAxial(Vector2f pixel, Vector2i result) {
        final float q = pixel.x * COEFF0;
        final float r = -pixel.x / 3F - COEFF1 * pixel.y;
        final float w = -q-r;

        int rx = Math.round(q);
        int ry = Math.round(w);
        int rz = Math.round(r);

        final float qDiff = Math.abs(rx - q);
        final float wDiff = Math.abs(ry - w);
        final float rDiff = Math.abs(rz - r);

        if ((qDiff > wDiff) && (qDiff > rDiff)) rx = -ry-rz;
        else if (wDiff > rDiff) ry = -rx-rz;

        return result.set(rx, ry);
    }

    /**
     * @source http://alienryderflex.com/polygon/
     */
     public boolean pointInPolygon(float polyX[], float polyY[], float x, float y) {
        int i, j = polyX.length - 1;
        boolean oddNodes = false;

        for (i = 0; i < polyX.length; i++) {
            if ((polyY[i] < y && polyY[j] >= y || polyY[j] < y && polyY[i] >= y) && (polyX[i] <= x || polyX[j] <= x)) {
                oddNodes ^= (polyX[i] + (y - polyY[i]) / (polyY[j] - polyY[i]) * (polyX[j] - polyX[i]) < x);
            }

            j = i;
        }

        return oddNodes;
    }
}