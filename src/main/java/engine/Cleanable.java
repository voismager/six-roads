package engine;

public interface Cleanable {
    void cleanup();
}
