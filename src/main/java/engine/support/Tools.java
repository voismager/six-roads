package engine.support;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Tools {
    public static <T> T[] add(T[] a, T t) {
        a = Arrays.copyOf(a, a.length + 1);
        a[a.length-1] = t;
        return a;
    }

    public static <T> T[] merge(T t, T[] a, T[] result) {
        if (result.length > a.length) {
            System.arraycopy(a, 0, result, 0, a.length);
            result[a.length] = t;
        } else {
            result = Arrays.copyOf(a, a.length + 1);
            result[a.length] = t;
        }

        return result;
    }

    public static int indexOf(Object[] array, Object o) {
        if (o == null) {
            for (int i = 0; i < array.length; i++)
                if (array[i]==null)
                    return i;
        } else {
            for (int i = 0; i < array.length; i++)
                if (o.equals(array[i]))
                    return i;
        }
        return -1;
    }

    public static Iterator<String> split(String s, Pattern pattern) {
        Matcher m = pattern.matcher(s);
        List<String> ret = new ArrayList<>();
        int start = 0;

        while (m.find()) {
            String s0 = s.substring(start, m.start());
            String s1 = m.group();

            if (!s0.isEmpty()) ret.add(s0);
            if (!s1.isEmpty()) ret.add(s1);

            start = m.end();
        }

        if (start < s.length()) {
            ret.add(s.substring(start));
        }

        return ret.iterator();
    }

    /**
     * @source https://stackoverflow.com/a/1520212/8441830
     */
    public static void shuffleArray(int[] array) {
        int index, temp;
        ThreadLocalRandom random = ThreadLocalRandom.current();

        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
    }

    public static <T> void shuffleArray(T[] array) {
        int index;
        T temp;
        ThreadLocalRandom random = ThreadLocalRandom.current();

        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
    }

    public static void shuffleArray(long[] array) {
        int index;
        long temp;
        ThreadLocalRandom random = ThreadLocalRandom.current();

        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            temp = array[index];
            array[index] = array[i];
            array[i] = temp;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] arrayWithInit(Class<T> type, int length) {
        final T[] a = (T[]) Array.newInstance(type, length);

        try {
            for (int i = 0; i < length; i++) {
                a[i] = type.newInstance();
            }
        }

        catch (IllegalAccessException | InstantiationException ignored) { }

        return a;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] arrayWithoutInit(Class<T> type, int length) {
        return (T[]) Array.newInstance(type, length);
    }

    public static <T> List<T> concatImmutable(List<T> list, T... array) {
        List<T> n = new ArrayList<>(list);
        n.addAll(Arrays.asList(array));
        return n;
    }

    public static <T, S> int binarySearch(T[] a, S key, BiComparator<T, S> c) {
        int low = 0;
        int high = a.length - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            T midVal = a[mid];
            int cmp = c.compare(midVal, key);
            if (cmp < 0)
                low = mid + 1;
            else if (cmp > 0)
                high = mid - 1;
            else
                return mid;
        }

        return -(low + 1);
    }

    public static int sum(int[] a) {
        int s = 0;

        for (int i = 0 ; i < a.length; i++)
            s += a[i];

        return s;
    }

    public static int mul(int[] a) {
        int m = 1;

        for (int i = 0 ; i < a.length; i++)
            m *= a[i];

        return m;
    }

    public static float round(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        float tmp = number * pow;
        return ( (float) ( (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) ) ) / pow;
    }

    public static int fastIntPow(int a, int n) {
        if (n == 0) return 1;

        if (n < 0) {
            return 1 / fastIntPow(a, -n);
        } else {
            int powerOfHalfN = fastIntPow(a, n / 2);
            int[] factor = {1, a};
            return factor[n % 2] * powerOfHalfN * powerOfHalfN;
        }
    }

    public static float generateBattleFactor(int winner, int loser) {
        return round((float)(winner - loser) / winner, 3);
    }

    public static float generateGeneralFactor(int hexFactor, int freeIron, int freeWood, int freeFood) {
        return round(
                (float)Math.pow(1.04, freeIron)
                        * (float)Math.pow(1.03, freeWood)
                        * (float)Math.pow(1.02, freeFood)
                        + hexFactor * 0.01f, 2);
    }
}
