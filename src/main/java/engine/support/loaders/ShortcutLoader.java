package engine.support.loaders;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimi.dsi.fastutil.objects.Object2ShortMap;
import it.unimi.dsi.fastutil.objects.Object2ShortOpenHashMap;

import java.util.Map;

public class ShortcutLoader {
    public static Object2ShortMap<String> load(JsonObject val1) {
        return loadAttributes(val1);
    }

    private static Object2ShortMap<String> loadAttributes(JsonObject obj) {
        Object2ShortMap<String> out = new Object2ShortOpenHashMap<>();
        out.defaultReturnValue((short)-1);

        for (Map.Entry<String, JsonElement> e : obj.entrySet()) {
            String[] shortcuts = toStringArr(e.getValue().getAsJsonArray());
            short key = Short.parseShort(e.getKey());

            for (String shortcut : shortcuts) {
                out.put(shortcut, key);
            }
        }

        return out;
    }

    private static String[] toStringArr(JsonArray arr) {
        String[] out = new String[arr.size()];

        int i = 0;
        for (JsonElement e : arr) {
            out[i++] = e.getAsString();
        }

        return out;
    }
}
