package engine.support.loaders;

import client.model.Module;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class UnifiedResource {
    public static UnifiedResource of(Module module, String path) {
        return module.meta.resource.getChild(path);
    }

    private final boolean isResource;
    private final String path;

    public UnifiedResource(boolean isResource, String path) {
        this.isResource = isResource;
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnifiedResource that = (UnifiedResource) o;
        return isResource == that.isResource &&
                path.equals(that.path);
    }

    @Override
    public String toString() {
        return "UnifiedResource{" +
                "isResource=" + isResource +
                ", path='" + path + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }

    public boolean isResource() {
        return isResource;
    }

    public String getPath() {
        return path;
    }

    public UnifiedResource getChild(String subpath) {
        return new UnifiedResource(isResource, path + subpath);
    }

    public InputStream getAsStream(String subpath) {
        if (isResource) {
            return FilesLoader.loadResource(path + subpath);
        } else {
            try {
                return new FileInputStream(path + subpath);
            } catch (FileNotFoundException e) {
                throw new RuntimeException("Source file was not found");
            }
        }
    }

    public InputStream getAsStream() {
        if (isResource) {
            return FilesLoader.loadResource(path);
        } else {
            try {
                return new FileInputStream(path);
            } catch (FileNotFoundException e) {
                throw new RuntimeException("Source file was not found");
            }
        }
    }
}
