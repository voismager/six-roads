package engine.support.loaders;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class FilesLoader {
    public static boolean doesResourceExist(String name) {
        return doesResourceExist(FilesLoader.class, name);
    }

    public static boolean doesResourceExist(Class clazz, String name) {
        return null != clazz.getResource(name);
    }

    public static InputStream loadResource(String name) {
        return loadResource(FilesLoader.class, name);
    }

    public static InputStream loadResource(Class clazz, String name) {
        return clazz.getResourceAsStream(name);
    }

    public static List<String> readAllLines(InputStream stream) throws Exception {
        List<String> list = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        }

        return list;
    }

    public static String readResource(String name) throws IOException {
        return readResource(FilesLoader.class, name);
    }

    public static String readResource(Class clazz, String name) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStream s = loadResource(clazz, name);
        BufferedReader br = new BufferedReader(new InputStreamReader(s, StandardCharsets.UTF_8));

        for (int c = br.read(); c != -1; c = br.read()) {
            sb.append((char)c);
        }

        return sb.toString();
    }

    public static File findFile(File[] files, String name) throws FileNotFoundException {
        for (File file : files) {
            if (file.isFile() && name.equals(file.getName())) {
                return file;
            }
        }

        throw new FileNotFoundException();
    }

    public static File findDirectory(File[] files, String name) throws FileNotFoundException {
        for (File file : files) {
            if (file.isDirectory() && name.equals(file.getName())) {
                return file;
            }
        }

        throw new FileNotFoundException();
    }

    public static File findDirectory(File directory, String name) throws FileNotFoundException {
        for (File file : directory.listFiles()) {
            if (file.isDirectory() && name.equals(file.getName())) {
                return file;
            }
        }

        throw new FileNotFoundException();
    }

    public static File[] findFiles(File[] files, String name) throws FileNotFoundException {
        for (File file : files) {
            if (file.isDirectory() && name.equals(file.getName())) {
                return file.listFiles();
            }
        }

        throw new FileNotFoundException();
    }
}