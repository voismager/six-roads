package engine.support;

import java.util.Random;

public final class RandomUtil {
    private final static Random GLOBAL = new Random();

    private RandomUtil() {
    }

    public static long globalNextLong() {
        return GLOBAL.nextLong();
    }

    public static int globalNextInt() {
        return GLOBAL.nextInt();
    }

    public static int globalNextInt(int min, int max) {
        return GLOBAL.nextInt((max - min) + 1) + min;
    }

    public static boolean rollDice(int probability, Random dice) {
        return dice.nextInt(100) < probability;
    }
}
