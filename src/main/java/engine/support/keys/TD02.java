package engine.support.keys;

public class TD02 {
    private static final short ZERO_SHORT = 0;
    private static final byte  ONE_BYTE   = 1;

    private static final long MASK_0 = 0xFFL;
    private static final long MASK_1 = 0xFF00L;
    private static final long MASK_2 = 0xFF0000L;
    private static final long MASK_3 = 0xFF000000L;
    private static final long MASK_4 = 0xFFFF00000000L;
    private static final long MASK_5 = 0xFFFF000000000000L;

    public static long BBBBSS_L(byte v0, byte v1, byte v2, byte v3, short v4, short v5) {
        long l0 = v0 & 0xFFL;
        long l1 = (v1 & 0xFFL) << 8;
        long l2 = (v2 & 0xFFL) << 16;
        long l3 = (v3 & 0xFFL) << 24;
        long l4 = ((v4 + 1) & 0xFFFFL) << 32;
        long l5 = ((v5 + 1) & 0xFFFFL) << 48;
        return l0 | l1 | l2 | l3 | l4 | l5;
    }

    public static long BBBBSs_L(byte v0, byte v1, byte v2, byte v3, short v4) {
        return BBBBSS_L(v0, v1, v2, v3, v4, ZERO_SHORT);
    }

    public static long BBBBss_L(byte v0, byte v1, byte v2, byte v3) {
        return BBBBSS_L(v0, v1, v2, v3, ZERO_SHORT, ZERO_SHORT);
    }

    public static long BBBbSS_L(byte v0, byte v1, byte v2, short v4, short v5) {
        return BBBBSS_L(v0, v1, v2, ONE_BYTE, v4, v5);
    }

    public static long BbBBSs_L(byte v0, byte v2, byte v3, short v4) {
        return BBBBSS_L(v0, ONE_BYTE, v2, v3, v4, ZERO_SHORT);
    }

    public static long BbBBSS_L(byte v0, byte v2, byte v3, short v4, short v5) {
        return BBBBSS_L(v0, ONE_BYTE, v2, v3, v4, v5);
    }

    public static long BBBbSs_L(byte v0, byte v1, byte v2, short v4) {
        return BBBBSS_L(v0, v1, v2, ONE_BYTE, v4, ZERO_SHORT);
    }

    public static long BBBbss_L(byte v0, byte v1, byte v2) {
        return BBBBSS_L(v0, v1, v2, ONE_BYTE, ZERO_SHORT, ZERO_SHORT);
    }

    public static long BbBbss_L(byte v0, byte v2) {
        return BBBBSS_L(v0, ONE_BYTE, v2, ONE_BYTE, ZERO_SHORT, ZERO_SHORT);
    }

    public static long BBbbss_L(byte v0, byte v1) {
        return BBBBSS_L(v0, v1, ONE_BYTE, ONE_BYTE, ZERO_SHORT, ZERO_SHORT);
    }

    public static long BBbbSs_L(byte v0, byte v1, short v3) {
        return BBBBSS_L(v0, v1, ONE_BYTE, ONE_BYTE, v3, ZERO_SHORT);
    }

    public static long BBbBSS_L(byte v0, byte v1, byte v3, short v4, short v5) {
        return BBBBSS_L(v0, v1, ONE_BYTE, v3, v4, v5);
    }

    public static long BbbBSS_L(byte v0, byte v3, short v4, short v5) {
        return BBBBSS_L(v0, ONE_BYTE, ONE_BYTE, v3, v4, v5);
    }

    public static long Bbbbss_L(byte v0) {
        return BBBBSS_L(v0, ONE_BYTE, ONE_BYTE, ONE_BYTE, ZERO_SHORT, ZERO_SHORT);
    }

    public static long BbbbSs_L(byte v0, short v4) {
        return BBBBSS_L(v0, ONE_BYTE, ONE_BYTE, ONE_BYTE, v4, ZERO_SHORT);
    }

    public static long BbbbSs_L(byte v0, int v4) {
        return BBBBSS_L(v0, ONE_BYTE, ONE_BYTE, ONE_BYTE, (short) v4, ZERO_SHORT);
    }

    public static long getMask(long v) {
        long mask = 0x0L;
        if ((v & MASK_0) != 0) mask |= MASK_0;
        if ((v & MASK_1) != 0) mask |= MASK_1;
        if ((v & MASK_2) != 0) mask |= MASK_2;
        if ((v & MASK_3) != 0) mask |= MASK_3;
        if ((v & MASK_4) != 0) mask |= MASK_4;
        if ((v & MASK_5) != 0) mask |= MASK_5;
        return mask;
    }

    public static byte L_1B(long v) {
        return (byte) v;
    }

    public static byte L_2B(long v) {
        return (byte) ((v >>> 8) & 0xFF);
    }

    public static byte L_3B(long v) {
        return (byte) ((v >>> 16) & 0xFF);
    }

    public static byte L_4B(long v) {
        return (byte) ((v >>> 24) & 0xFF);
    }

    public static short L_1S(long v) {
        return (short) (((v >>> 32) & 0xFFFF) - 1);
    }

    public static short L_2S(long v) {
        return (short) (((v >>> 48) & 0xFFFF) - 1);
    }
}
