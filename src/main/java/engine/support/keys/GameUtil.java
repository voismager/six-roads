package engine.support.keys;

import static constants.ConstIdentifiers.NULL_ID;
import static constants.ConstIdentifiers.NULL_IDENTIFIER;
import static constants.EventsFlags.*;
import static engine.support.keys.Mapper.*;

public class GameUtil {
    public static boolean effectsEqual(short effectId0, int boundIdentifier0, short effectId1, int boundIdentifier1) {
        return effectId0 == effectId1 && boundIdentifier0 == boundIdentifier1;
    }

    public static long getEffectKey(short effectId, int boundIdentifier) {
        return TD03_SI_L(effectId, boundIdentifier);
    }

    public static int getTrigger2Arg(long trigger) {
        return TD08_L_I(trigger);
    }

    public static boolean checkEquals(long trigger, byte type) {
        return TD08_L_B(trigger) == type;
    }

    public static boolean checkEquals(long trigger, byte type, short arg0) {
        return TD08_L_B(trigger) == type && TD08_L_S(trigger) == arg0;
    }

    public static boolean checkEquals(long trigger, byte type, short arg0, int arg1) {
        return trigger == TD08_BSI_L(type, arg0, arg1);
    }

    public static long getTriggerKey(byte trigger) {
        return TD08_BSI_L(trigger, NULL_ID, NULL_IDENTIFIER);
    }

    public static long getTriggerKey(byte trigger, int arg0) {
        return TD08_BSI_L(trigger, NULL_ID, arg0);
    }

    public static long getTriggerKey(byte trigger, short arg0, int arg1) {
        return TD08_BSI_L(trigger, arg0, arg1);
    }

    public static byte getEventType(long key) {
        return TD02.L_1B(key);
    }

    public static short get1S(long key) {
        return TD02.L_1S(key);
    }

    public static long getGroundHoveredKey(byte puppeteerFlag, byte mode, short areaId, short paintId) {
        return TD02.BBbBSS_L(GROUND_HOVERED, mode, puppeteerFlag, areaId, paintId);
    }

    public static long getGroundClickKey(byte buttonFlag, byte puppeteerFlag, short areaId, short paintId) {
        return TD02.BBbBSS_L(GROUND_CLICK, buttonFlag, puppeteerFlag, areaId, paintId);
    }

    public static byte getGroundPuppeteerFlag(long key) {
        return TD02.L_4B(key);
    }

    public static short getGroundAreaId(long key) {
        return TD02.L_1S(key);
    }

    public static short getGroundPaintId(long key) {
        return TD02.L_2S(key);
    }

    public static long getUnitHoveredKey(byte puppeteerFlag, byte mode, short unitId, short paintId) {
        return TD02.BBbBSS_L(UNIT_HOVERED, mode, puppeteerFlag, unitId, paintId);
    }

    public static long getUnitClickKey(byte buttonFlag, byte puppeteerFlag, short unitId, short paintId) {
        return TD02.BBbBSS_L(UNIT_CLICK, buttonFlag, puppeteerFlag, unitId, paintId);
    }

    public static byte getUnitPuppeteerFlag(long key) {
        return TD02.L_4B(key);
    }

    public static short getUnitUnitId(long key) {
        return TD02.L_1S(key);
    }

    public static short getUnitPaintId(long key) {
        return TD02.L_2S(key);
    }

    public static long getEntityClickKey(byte buttonFlag, byte type, short entityId, short widgetId) {
        return TD02.BBbBSS_L(ENTITY_CLICK, buttonFlag, type, entityId, widgetId);
    }

    public static byte getEntityType(long key) {
        return TD02.L_4B(key);
    }

    public static short getEntityEntityId(long key) {
        return TD02.L_1S(key);
    }

    public static short getEntityWidgetId(long key) {
        return TD02.L_2S(key);
    }

    public static long getEmptyHoveredKey(byte mode) {
        return TD02.BBbbss_L(EMPTY_HOVERED, mode);
    }

    public static long getEmptyClickKey(byte buttonFlag) {
        return TD02.BBbbss_L(EMPTY_CLICK, buttonFlag);
    }

    public static long getButtonPushKey(short buttonCode) {
        return TD02.BbbbSs_L(BUTTON_PUSH, buttonCode);
    }

    public static long getOthersKey() {
        return TD02.Bbbbss_L(OTHERS);
    }

    public static long getConditionKey(short id) {
        return TD02.BbbbSs_L(CONDITION, id);
    }

    public static long getCustomTriggerKey(short id) {
        return TD02.BbbbSs_L(CUSTOM_TRIGGER, id);
    }

    public static long getExitKey(short id) {
        return TD02.BbbbSs_L(EXIT, id);
    }

    public static long[] getEndMyTurnKey() {
        return new long[] { TD02.Bbbbss_L(END_MY_TURN) };
    }
}
