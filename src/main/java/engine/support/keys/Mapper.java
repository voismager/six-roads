package engine.support.keys;

public class Mapper {
    public  static final long  IDENTITY_LONG  = 0xFFFFFFFFFFFFFFFFL;
    private static final short IDENTITY_SHORT = (short)0xFFFF;
    private static final short ZERO_SHORT     = 0;
    private static final byte  ONE_BYTE       = 1;

    private static final long TD05_MASK_1 = TD05_SSSS_L((short) 0x0000, IDENTITY_SHORT, IDENTITY_SHORT, IDENTITY_SHORT);
    private static final long TD05_MASK_2 = TD05_SSSS_L((short) 0x0000, (short) 0x0000, IDENTITY_SHORT, IDENTITY_SHORT);
    private static final long TD05_MASK_3 = TD05_SSSS_L((short) 0x0000, (short) 0x0000, (short) 0x0000, IDENTITY_SHORT);

    public static long TD00_BSSS_L(byte v0, short v1, short v2, short v3) {
        long l0 = v0 & 0xFFL;
        long l1 = (v1 & 0xFFFFL) << 8;
        long l2 = (v2 & 0xFFFFL) << 24;
        long l3 = (v3 & 0xFFFFL) << 42;
        return l0 | l1 | l2 | l3;
    }

    public static long TD00_BSSs_L(byte v0, short v1, short v2) {
        return TD00_BSSS_L(v0, v1, v2, ZERO_SHORT);
    }

    public static long TD00_BSsS_L(byte v0, short v1, short v3) {
        return TD00_BSSS_L(v0, v1, ZERO_SHORT, v3);
    }

    public static long TD00_BSss_L(byte v0, short v1) {
        return TD00_BSSS_L(v0, v1, ZERO_SHORT, ZERO_SHORT);
    }

    public static int TD01_BS_I(byte v0, short v1) {
        return v0 << 16 | v1 & 0xFFFF;
    }

    public static int TD01_BS_I(byte v0, int v1) {
        return v0 << 16 | v1 & 0xFFFF;
    }

    public static byte TD01_I_B(int v) {
        return (byte) ((v >> 16) & 0xFF);
    }

    public static short TD01_I_S(int v) {
        return (short) (v & 0xFFFF);
    }

    public static long TD03_SI_L(short v0, int v1) {
        return (v1 & 0xFFFFFFFFL) << 16 | (v0 & 0xFFFFL);
    }

    public static int TD04_SS_I(short v0, short v1) {
        return (v1 << 16) | (v0 & 0xFFFF);
    }

    public static int TD04_SS_I(int v0, int v1) {
        return (v1 << 16) | (v0 & 0xFFFF);
    }

    public static int TD04_I_1I(int v) {
        return (short) (v & 0xFFFF);
    }

    public static int TD04_I_2I(int v) {
        return v >> 16;
    }

    public static short TD04_I_1S(int v) {
        return (short) (v & 0xFFFF);
    }

    public static short TD04_I_2S(int v) {
        return (short) (v >> 16);
    }

    public static int TD05_keys(long key) {
        if ((key & TD05_MASK_1) == TD05_MASK_1) return 1;
        if ((key & TD05_MASK_2) == TD05_MASK_2) return 2;
        if ((key & TD05_MASK_3) == TD05_MASK_3) return 3;
        return 4;
    }

    public static long TD05_SSSS_L(short[] vs) {
        switch (vs.length) {
            case 1:
                return TD05_SSSS_L(vs[0], IDENTITY_SHORT, IDENTITY_SHORT, IDENTITY_SHORT);
            case 2:
                return TD05_SSSS_L(vs[0], vs[1], IDENTITY_SHORT, IDENTITY_SHORT);
            case 3:
                return TD05_SSSS_L(vs[0], vs[1], vs[2], IDENTITY_SHORT);
            case 4:
                return TD05_SSSS_L(vs[0], vs[1], vs[2], vs[3]);
            default:
                throw new IllegalArgumentException();
        }
    }

    public static long TD05_SSSS_L(short v0, short v1, short v2, short v3) {
        long l0 = v0 & 0xFFFFL;
        long l1 = (v1 & 0xFFFFL) << 16;
        long l2 = (v2 & 0xFFFFL) << 32;
        long l3 = (v3 & 0xFFFFL) << 48;
        return l0 | l1 | l2 | l3;
    }

    public static long TD05_SSSs_L(short v0, short v1, short v2) {
        return TD05_SSSS_L(v0, v1, v2, IDENTITY_SHORT);
    }

    public static long TD05_SSss_L(short v0, short v1) {
        return TD05_SSSS_L(v0, v1, IDENTITY_SHORT, IDENTITY_SHORT);
    }

    public static long TD05_Ssss_L(short v0) {
        return TD05_SSSS_L(v0, IDENTITY_SHORT, IDENTITY_SHORT, IDENTITY_SHORT);
    }

    public static short TD05_L_S(long v) {
        return (short) (v & 0xFFFFL);
    }

    public static long TD06_SSI_L(short v0, short v1, int v2) {
        return (long) (v0 | v1 << 16) << 32 | v2 & 0xFFFFFFFFL;
    }

    public static long TD08_II_L(int v0, int v1) {
        return (((long)v1) << 32) | (v0 & 0xFFFFFFFFL);
    }

    public static long TD08_Ii_L(int v0) {
        return TD08_II_L(v0, 0);
    }

    public static long TD08_BSI_L(byte v0, short v1, int i1) {
        int i0 = ((v1 & 0xFFFF) << 8) | (v0 & 0xFF);
        return (((long)i1) << 32) | (i0 & 0xFFFFFFFFL);
    }

    public static byte TD08_L_B(long v) {
        return (byte) (v & 0xFF);
    }

    public static short TD08_L_S(long v) {
        return (short) ((v >> 8) & 0xFFFF);
    }

    public static int TD08_L_I(long v) {
        return (int) ((v >> 32));
    }

    public static int TD08_BS_I(byte v0, int v1) {
        return ((v1 & 0xFFFF) << 8) | (v0 & 0xFF);
    }

    public static int TD08_BS_I(byte v0, short v1) {
        return ((v1 & 0xFFFF) << 8) | (v0 & 0xFF);
    }

    public static byte TD08_I_B(int v) {
        return (byte) (v & 0xFF);
    }

    public static int TD08_Bs_I(byte v0) {
        return TD08_BS_I(v0, ZERO_SHORT);
    }
}