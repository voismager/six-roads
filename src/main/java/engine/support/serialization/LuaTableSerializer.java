package engine.support.serialization;

import io.netty.buffer.ByteBuf;
import io.netty.util.CharsetUtil;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

public class LuaTableSerializer {
    public static void serialize(LuaTable table, ByteBuf buf) {
        int length = table.rawlen();

        buf.writeByte(LuaValue.TTABLE);
        buf.writeInt(length);

        for (int i = 1; i <= length; i++) {
            LuaValue value = table.rawget(i);
            int type = value.type();

            switch (type) {
                case LuaValue.TBOOLEAN:
                    serializeBoolean(value, buf);
                    break;

                case LuaValue.TNUMBER:
                    serializeNumber(value, buf);
                    break;

                case LuaValue.TSTRING:
                    serializeString(value, buf);
                    break;

                case LuaValue.TTABLE:
                    serialize(value.checktable(), buf);
                    break;
            }
        }
    }

    private static void serializeNumber(LuaValue number, ByteBuf buf) {
        buf.writeByte(LuaValue.TNUMBER);
        buf.writeInt(number.toint());
    }

    private static void serializeString(LuaValue string, ByteBuf buf) {
        buf.writeByte(LuaValue.TSTRING);
        String str = string.tojstring();
        buf.writeInt(str.length());
        buf.writeCharSequence(str, CharsetUtil.UTF_8);
    }

    private static void serializeBoolean(LuaValue bool, ByteBuf buf) {
        buf.writeByte(LuaValue.TBOOLEAN);
        buf.writeBoolean(bool.toboolean());
    }
}
