package engine.support;

import common.gameobjects.Hexagon;
import org.joml.Vector2i;

public final class HexagonUtils {
    public final static int[][] NEIGHBORS_DIRECTIONS = {
            new int[] { -1, 1 }, new int[] { 0, 1 }, new int[] { 1, 0 },
            new int[] { 1, -1 }, new int[] { 0, -1 }, new int[] { -1, 0 }
    };

    private final static int[] COORDINATES = new int[2];

    public final static int NW = 0;
    public final static int N = 1;
    public final static int NE = 2;
    public final static int SE = 3;
    public final static int S = 4;
    public final static int SW = 5;

    public static int[] NORTH() {
        return NEIGHBORS_DIRECTIONS[N];
    }

    public static int[] SOUTH() {
        return NEIGHBORS_DIRECTIONS[S];
    }

    public static float qrToX(int q, int r) {
        return 3.0F / 2.0F * q;
    }

    public static float qrToY(int q, int r) {
        return (float)Math.sqrt(3.0) / 2.0F * q + (float)Math.sqrt(3.0) * r;
    }

    public static int[] xyToQr(float x, float y, int[] result) {
        final float q = x * 2F / 3F;
        final float r = -x / 3F - (float) (1D / Math.sqrt(3D)) * y;
        final float w = -q-r;

        int rx = Math.round(q);
        int ry = Math.round(w);
        int rz = Math.round(r);

        final float qDiff = Math.abs(rx - q);
        final float wDiff = Math.abs(ry - w);
        final float rDiff = Math.abs(rz - r);

        if ((qDiff > wDiff) && (qDiff > rDiff)) rx = -ry-rz;
        else if (wDiff > rDiff) ry = -rx-rz;

        result[0] = rx;
        result[1] = ry;
        return result;
    }

    public static int getDirection(int q0, int r0, int q1, int r1) {
        int qt = q1 - q0;
        int rt = r1 - r0;

        if (Math.abs(qt) > 1 || Math.abs(rt) > 1) {
            int distance = hexDistance(q0, r0, q1, r1);
            float x0 = qrToX(q0, r0);
            float y0 = qrToY(q0, r0);
            float x1 = qrToX(q1, r1);
            float y1 = qrToY(q1, r1);
            float x = x0 + (x1 - x0) * 1.0f / distance;
            float y = y0 + (y1 - y0) * 1.0f / distance;
            xyToQr(x, y, COORDINATES);
            qt = COORDINATES[0] - q0;
            rt = COORDINATES[1] - r0;
        }

        for (int i = 0; i < NEIGHBORS_DIRECTIONS.length; i++) {
            int[] dir = NEIGHBORS_DIRECTIONS[i];
            if (dir[0] == qt && dir[1] == rt) return i;
        }

        throw new AssertionError();
    }

    public static int hexDistance(Hexagon cell0, Hexagon cell1) {
        return hexDistance(cell0.q, cell0.r, cell1.q, cell1.r);
    }

    public static int hexDistance(int q0, int r0, int q1, int r1) {
        return (Math.abs(q0 - q1) + Math.abs(q0 + r0 - q1 - r1) + Math.abs(r0 - r1)) / 2;
    }

    public static void rightRotation(int q, int r, int i, Vector2i result) {
        int x = q;
        int z = r;
        int y = -x-z;

        if (i < 0) {
            for (int j = 0; j < -i; j++) {
                int oX = x;
                int oY = y;
                int oZ = z;

                x = -oZ;
                y = -oX;
                z = -oY;
            }
        } else {
            for (int j = 0; j < i; j++) {
                int oX = x;
                int oY = y;
                int oZ = z;

                x = -oY;
                y = -oZ;
                z = -oX;
            }
        }

        result.set(x, z);
    }
}
