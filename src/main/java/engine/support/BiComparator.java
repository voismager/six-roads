package engine.support;

@FunctionalInterface
public interface BiComparator<T, S> {
    int compare(T o1, S o2);
}
