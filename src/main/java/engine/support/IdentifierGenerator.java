package engine.support;

public interface IdentifierGenerator {
    int getArrayPosition(int Identifier);

    int getNext();

    void free(int Identifier);
}
