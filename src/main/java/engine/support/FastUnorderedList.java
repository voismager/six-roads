package engine.support;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public class FastUnorderedList<T> implements List<T> {
    private transient Object[] backed;
    private int size;
    private int inc;

    public FastUnorderedList(int inc) {
        this.backed = new Object[2];
        this.size = 0;
        this.inc = inc;
    }

    public FastUnorderedList(int inc, int cap) {
        this.backed = new Object[cap];
        this.size = 0;
        this.inc = inc;
    }

    public FastUnorderedList<T> slice(int[] sortedIndices) {
        FastUnorderedList<T> out = new FastUnorderedList<>(0, sortedIndices.length);

        if (sortedIndices.length != 0) {
            int i = 0;
            int currIndex = 0;
            int newArrayIndex = 0;

            Object[] newArray = new Object[backed.length];

            while (currIndex != sortedIndices.length) {
                if (sortedIndices[currIndex] == i) {
                    out.backed[out.size++] = backed[i];
                    currIndex++;
                }

                else newArray[newArrayIndex++] = backed[i];
                i++;
            }

            i = sortedIndices[sortedIndices.length - 1];
            while (++i != size) {
                newArray[newArrayIndex++] = backed[i];
            }

            this.backed = newArray;
            this.size -= sortedIndices.length;
        }

        return out;
    }

    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public boolean add(T t) {
        if (size == backed.length) grow(size + inc);
        backed[size++] = t;
        return true;
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends T> c) {
        if (c.size() > backed.length - size) grow(c.size() + size);
        FastUnorderedList list = (FastUnorderedList)c;
        System.arraycopy(list.backed, 0, backed, size, list.size);
        size += list.size;
        return true;
    }

    @Override
    public T remove(int index) {
        Object last = backed[--size];
        backed[index] = last;
        backed[size] = null;
        return null;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (backed[i] == o) {
                Object last = backed[--size];
                backed[i] = last;
                backed[size] = null;
                return true;
            }
        }

        return false;
    }

    @Override
    public T get(int index) {
        return (T) backed[index];
    }

    @Override
    public int size() {
        return size;
    }

    private void ensureCapacity(int cap) {
        if (this.backed.length < cap) grow(cap);
    }

    private void grow(int cap) {
        Object[] nArray = new Object[cap];
        System.arraycopy(backed, 0, nArray, 0, backed.length);
        backed = nArray;
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        System.arraycopy(backed, 0, a, 0, size);
        return a;
    }

    @NotNull
    @Override
    public Object[] toArray() {
        Object[] a = new Object[size];
        System.arraycopy(backed, 0, a, 0, size);
        return a;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (backed[i] == o) return true;
        }

        return false;
    }

    @NotNull
    @Override
    public Iterator<T> iterator() {
        return new Itr();
    }

    private class Itr implements Iterator<T> {
        int cursor;

        public boolean hasNext() {
            return cursor != size;
        }

        public T next() {
            int i = cursor;
            cursor = i + 1;
            return (T) backed[i];
        }

        @Override
        public void remove() {
            Object last = backed[--size];
            backed[--cursor] = last;
            backed[size] = null;
        }
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, @NotNull Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException();
    }
}
