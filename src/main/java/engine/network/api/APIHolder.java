package engine.network.api;

import client.Settings;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public final class APIHolder {
    private static API INSTANCE;

    static {
        switch (Settings.SERVICE_TYPE) {
            default:
            case "default":
                try {
                    INSTANCE = new DefaultAPI();
                } catch (KeyStoreException | NoSuchAlgorithmException | KeyManagementException e) {
                    throw new RuntimeException(e);
                }
                break;
            case "debug":
                INSTANCE = new DebugAPI();
                break;
        }
    }

    public static API getInstance() {
        return INSTANCE;
    }

    private APIHolder() {
    }
}
