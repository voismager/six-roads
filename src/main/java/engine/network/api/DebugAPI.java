package engine.network.api;

import server.Player;

import java.io.IOException;
import java.net.URISyntaxException;

public class DebugAPI implements API {
    @Override
    public Player getPlayer() {
        return new Player(1, "debug");
    }

    @Override
    public boolean isLogged() {
        return false;
    }

    @Override
    public void login(String username, String password) throws IOException, URISyntaxException {

    }

    @Override
    public void logout() {

    }

    @Override
    public Player getPlayerInformation(long id) throws Exception {
        return new Player(id, "debug");
    }
}
