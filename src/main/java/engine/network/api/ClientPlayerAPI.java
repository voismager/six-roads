package engine.network.api;

import server.Player;

import java.io.IOException;
import java.net.URISyntaxException;

public interface ClientPlayerAPI {
    Player getPlayer();

    boolean isLogged();

    void login(String username, String password) throws IOException, URISyntaxException;

    void logout();
}
