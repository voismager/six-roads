package engine.network.api;

import client.Settings;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import server.Player;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

public final class DefaultAPI implements API {
    private CloseableHttpClient httpClient;
    private Player player;
    private String token;

    DefaultAPI() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext context = SSLContextBuilder
                .create()
                .loadTrustMaterial(new TrustSelfSignedStrategy())
                .build();

        HostnameVerifier allowAllHosts = new NoopHostnameVerifier();

        SSLConnectionSocketFactory connectionSocketFactory = new SSLConnectionSocketFactory(context, allowAllHosts);

        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(7 * 1000)
                .build();

        httpClient = HttpClients
                .custom()
                .setDefaultRequestConfig(config)
                .setSSLSocketFactory(connectionSocketFactory)
                .build();
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public boolean isLogged() {
        return player != null;
    }

    @Override
    public void login(String username, String password) throws IOException, URISyntaxException {
        StringWriter writer = new StringWriter();

        new JsonWriter(writer).beginObject()
                .name("username").value(username)
                .name("password").value(password)
                .endObject().close();

        URI uri = new URIBuilder()
                .setScheme(Settings.SERVICE_SCHEME)
                .setHost(Settings.SERVICE_ADDRESS)
                .setPath("/api/users/authenticate")
                .setPort(Settings.SERVICE_PORT)
                .build();

        String json = writer.toString();

        HttpPost request = new HttpPost(uri);
        request.addHeader(org.apache.http.HttpHeaders.HOST, Settings.SERVICE_ADDRESS);
        request.addHeader(org.apache.http.HttpHeaders.CACHE_CONTROL, "no-cache");
        request.addHeader(org.apache.http.HttpHeaders.CONNECTION, "close");
        request.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));

        try (CloseableHttpResponse response = httpClient.execute(request)) {
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonObject jsonResponse = new JsonParser()
                        .parse(EntityUtils.toString(response.getEntity()))
                        .getAsJsonObject();

                token = jsonResponse.get("accessToken").getAsString();
                player = new Player(
                        jsonResponse.get("id").getAsLong(),
                        jsonResponse.get("username").getAsString()
                );
            }
        }
    }

    @Override
    public void logout() {
        player = null;
    }

    @Override
    public Player getPlayerInformation(long id) throws IOException, URISyntaxException {
        if (token == null) throw new RuntimeException("Not connected");

        URI uri = new URIBuilder()
                .setScheme(Settings.SERVICE_SCHEME)
                .setHost(Settings.SERVICE_ADDRESS)
                .setPath("/api/users/" + id)
                .setPort(Settings.SERVICE_PORT)
                .build();

        HttpGet request = new HttpGet(uri);
        request.addHeader(org.apache.http.HttpHeaders.HOST, Settings.SERVICE_ADDRESS);
        request.addHeader(org.apache.http.HttpHeaders.CACHE_CONTROL, "no-cache");
        request.addHeader(org.apache.http.HttpHeaders.CONNECTION, "close");
        request.addHeader(org.apache.http.HttpHeaders.AUTHORIZATION, "Bearer " + token);

        try (CloseableHttpResponse response = httpClient.execute(request)) {
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                JsonObject json = new JsonParser()
                        .parse(EntityUtils.toString(response.getEntity()))
                        .getAsJsonObject();

                String username = json.get("username").getAsString();

                return new Player(id, username);
            }

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
                return null;
            }

            else throw new RuntimeException("Server Error");
        }
    }
}
