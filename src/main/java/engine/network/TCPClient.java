package engine.network;

import server.message.Message;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;
import java.nio.channels.AlreadyConnectedException;

/**
 * Single-threaded TCP-client
 */
public class TCPClient {
    private final ChannelInitializer<SocketChannel> initializer;
    private EventLoopGroup worker;
    private Channel channel;

    public TCPClient(ChannelInitializer<SocketChannel> initializer) {
        this.initializer = initializer;
    }

    public Channel getChannel() {
        return channel;
    }

    public boolean isConnected() {
        return channel != null && channel.isActive();
    }

    public void connect(InetSocketAddress address, Message connectionRequest) throws InterruptedException {
        if (isConnected()) throw new AlreadyConnectedException();

        this.worker = new NioEventLoopGroup(1);

        ChannelFuture result = new Bootstrap()
                .group(worker)
                .channel(NioSocketChannel.class)
                .handler(initializer)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 7500)
                .connect(address).sync();

        if (result.isSuccess()) {
            result = result.channel()
                    .writeAndFlush(connectionRequest)
                    .sync();

            if (result.isSuccess()) this.channel = result.channel();
        }
    }

    public void disconnect() throws InterruptedException {
        if (channel.isActive()) {
            channel.close().await();
        }

        worker.shutdownGracefully();
    }
}
