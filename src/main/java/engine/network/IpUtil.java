package engine.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.Enumeration;
import java.util.List;

public class IpUtil {
    public static InetAddress getExternal() throws IOException {
        URL url = new URL("http://checkip.amazonaws.com");

        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

        return InetAddress.getByName(in.readLine());
    }

    public static InetAddress getLAN() throws SocketException {
        Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();

        while (e.hasMoreElements()) {
            NetworkInterface r = e.nextElement();

            if (!r.isLoopback()) {
                List<InterfaceAddress> a = r.getInterfaceAddresses();

                for (InterfaceAddress ia : a) {
                    InetAddress address = ia.getAddress();

                    if ( !( address instanceof Inet4Address) ) {
                        continue;
                    }

                    return address;
                }
            }
        }

        throw new ConnectException();
    }

    public static InetAddress getLocal() throws UnknownHostException {
        return InetAddress.getLocalHost();
    }
}
