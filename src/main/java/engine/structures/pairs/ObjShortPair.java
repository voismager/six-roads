package engine.structures.pairs;

public class ObjShortPair<T> {
    public short id;
    public T value;

    public ObjShortPair(short id, T value) {
        this.id = id;
        this.value = value;
    }

    @Override
    public String toString() {
        return "ObjShortPair{" +
                "id=" + id +
                ", value=" + value +
                '}';
    }
}
