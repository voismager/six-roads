package engine.structures.pairs;

public class ObjPair<F, S> {
    public F first;
    public S second;

    public ObjPair(F first, S second) {
        this.first  = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
