package engine.structures.pairs;

public class ObjLongPair<T> {
    public long id;
    public T value;

    public ObjLongPair(long id, T value) {
        this.id = id;
        this.value = value;
    }
}
