package engine.structures;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class ObjArraySet<T> {
    public static class ObjIdArray<T> implements Comparable<ObjIdArray<T>> {
        public ObjArray<T> array;
        private int id;

        public ObjIdArray(int id, ObjArray<T> array) {
            this.id = id;
            this.array = array;
        }

        public int id() {
            return id;
        }

        @Override
        public int compareTo(@NotNull ObjIdArray<T> t) {
            return id - t.id;
        }
    }

    private ObjIdArray<Object> TEMP;
    private ObjIdArray[] array;
    private int size;

    public ObjArraySet() {
        this.TEMP = new ObjIdArray<>(0, null);
        this.array = new ObjIdArray[4];
        this.size = 0;
    }

    public int size() {
        return size;
    }

    public ObjIdArray<T> getByIndex(int index) {
        return array[index];
    }

    public ObjIdArray<T> get(int id) {
        TEMP.id = id;

        int index = Arrays.binarySearch(array, 0, size, TEMP);

        if (index < 0) return null;

        return array[index];
    }

    public ObjIdArray<T> getOrNew(int id) {
        TEMP.id = id;

        int index = Arrays.binarySearch(array, 0, size, TEMP);

        if (index < 0) {
            insert(-index-1, new ObjIdArray<>(id, new ObjArray<>()));
            return array[-index-1];
        }

        return array[index];
    }

    public void remove(int id) {
        TEMP.id = id;

        int index = Arrays.binarySearch(array, 0, size, TEMP);

        if (index < 0) return;

        removeByIndex(index);
    }

    public void add(ObjIdArray<T> element) {
        int index = Arrays.binarySearch(array, 0, size, element);

        if (index < 0) {
            insert(-index - 1, element);
        }
    }

    private void removeByIndex(int index) {
        System.arraycopy(array, index + 1, array, index, size - 1 - index);
        size--;
    }

    private void insert(int index, ObjIdArray<T> element) {
        if (size == array.length)
            array = Arrays.copyOf(array, array.length * 2 + 1);

        System.arraycopy(array, index, array, index + 1, array.length - index - 1);
        array[index] = element;
        size++;
    }
}
