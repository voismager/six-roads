package engine.structures;

import java.util.Arrays;

public class ShortArray {
    protected short[] array;
    protected int size;

    public ShortArray(ShortArray i) {
        this.array = Arrays.copyOf(i.array, i.size);
        this.size = i.size;
    }

    public ShortArray() {
        this(8);
    }

    public ShortArray(int length) {
        this.array = new short[length];
        this.size = 0;
    }

    public ShortArray(short[] i) {
        this.array = Arrays.copyOf(i, i.length);
        this.size = i.length;
    }

    public void clear() {
        size = 0;
    }

    public void set(short[] value) {
        if (array.length < value.length)
            array = Arrays.copyOf(value, value.length);
        else
            System.arraycopy(value, 0, array, 0, value.length);

        size = value.length;
    }

    public void set(int index, short value) {
        array[index] = value;
    }

    public void add(short value) {
        if (size == array.length)
            array = Arrays.copyOf(array, array.length * 2 + 1);

        array[size++] = value;
    }

    public boolean removeElement(short value) {
        for (int i = 0; i < array.length; i++) {
            if (value == array[i]) {
                remove(i);
                return true;
            }
        }

        return false;
    }

    public void remove(int index) {
        System.arraycopy(array, index + 1, array, index, size - 1 - index);
        size--;
    }

    public int size() {
        return size;
    }

    public short get(int index) {
        return array[index];
    }
}
