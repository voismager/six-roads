package engine.structures;

import java.util.Arrays;

public class LongArray {
    protected long[] array;
    protected int size;

    public LongArray(LongArray i) {
        this.array = Arrays.copyOf(i.array, i.size);
        this.size = i.size;
    }

    public LongArray() {
        this(8);
    }

    public LongArray(int length) {
        this.array = new long[length];
        this.size = 0;
    }

    public LongArray(long[] i) {
        this.array = Arrays.copyOf(i, i.length);
        this.size = i.length;
    }

    public void clear() {
        size = 0;
    }

    public void set(long[] value) {
        if (array.length < value.length)
            array = Arrays.copyOf(value, value.length);
        else
            System.arraycopy(value, 0, array, 0, value.length);

        size = value.length;
    }

    public void set(LongArray value) {
        if (array.length < value.size)
            array = new long[value.size];

        System.arraycopy(value.array, 0, array, 0, value.size);
        size = value.size;
    }

    public void set(int index, long value) {
        array[index] = value;
    }

    public void add(long value) {
        if (size == array.length)
            array = Arrays.copyOf(array, array.length * 2 + 1);

        array[size++] = value;
    }

    public boolean removeElement(long value) {
        for (int i = 0; i < array.length; i++) {
            if (value == array[i]) {
                remove(i);
                return true;
            }
        }

        return false;
    }

    public void remove(int index) {
        System.arraycopy(array, index + 1, array, index, size - 1 - index);
        size--;
    }

    public int size() {
        return size;
    }

    public long get(int index) {
        return array[index];
    }
}
