package engine.structures;

import java.util.Arrays;

public class IntArray {
    protected int[] array;
    protected int size;

    public IntArray(IntArray i) {
        this.array = Arrays.copyOf(i.array, i.size);
        this.size = i.size;
    }

    public IntArray() {
        this(4);
    }

    public IntArray(int length) {
        this.array = new int[length];
        this.size = 0;
    }

    public IntArray(int[] i) {
        this.array = i;
        this.size = i.length;
    }

    @Override
    public String toString() {
        return "IntArray{" +
                "array=" + Arrays.toString(array) +
                ", size=" + size +
                '}';
    }

    public int[] toArray() {
        int[] result = new int[this.size];
        System.arraycopy(array, 0, result, 0, this.size);
        return result;
    }

    public int[] toArray(int[] result) {
        if (result == null || result.length < this.size) {
            result = new int[this.size];
        }

        System.arraycopy(array, 0, result, 0, this.size);
        return result;
    }

    public void clear() {
        size = 0;
    }

    public void set(int[] value) {
        if (array.length < value.length)
            array = Arrays.copyOf(value, value.length);
        else
            System.arraycopy(value, 0, array, 0, value.length);

        size = value.length;
    }

    public void set(IntArray value) {
        if (array.length < value.size)
            array = new int[value.size];

        System.arraycopy(value.array, 0, array, 0, value.size);
        size = value.size;
    }

    public void set(int index, int value) {
        array[index] = value;
    }

    public void add(int value) {
        if (size == array.length)
            array = Arrays.copyOf(array, array.length * 2 + 1);

        array[size++] = value;
    }

    public boolean contains(int value) {
        for (int i = 0; i < array.length; i++) {
            if (value == array[i]) {
                return true;
            }
        }

        return false;
    }

    public boolean removeElement(int value) {
        for (int i = 0; i < size; i++) {
            if (value == array[i]) {
                remove(i);
                return true;
            }
        }

        return false;
    }

    public void removeFromTail(int len) {
        if (len > size) return;
        size -= len;
    }

    public void remove(int index) {
        System.arraycopy(array, index + 1, array, index, size - 1 - index);
        size--;
    }

    public int size() {
        return size;
    }

    public int get(int index) {
        return array[index];
    }
}
