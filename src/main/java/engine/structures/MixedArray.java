package engine.structures;

import constants.ValueType;

import java.util.Arrays;

public class MixedArray {
    private byte[] bytes;
    private ValueType[] types;
    private boolean bigEndian;
    private int byteSize, typeSize;
    private int next;

    public MixedArray(int bytes, int types, boolean bigEndian) {
        this.bytes = new byte[bytes];
        this.types = new ValueType[types];
        this.bigEndian = bigEndian;
        this.next = 0;
    }

    public int length() {
        return types.length;
    }

    public boolean hasNext() {
        return next < types.length;
    }

    public int remaining() {
        return types.length - next;
    }

    public void resetReading() {
        next = 0;
    }

    private void ensureCapacity(int bytes, int types) {
        if (this.bytes.length - byteSize < bytes)
            this.bytes = Arrays.copyOf(this.bytes, this.bytes.length * 2 + bytes);

        if (this.types.length - typeSize < types)
            this.types = Arrays.copyOf(this.types, this.types.length * 2 + types);
    }

    public void addByte(byte v) {
        ensureCapacity(1, 1);
        bytes[byteSize++] = v;
        types[typeSize++] = ValueType.BYTE;
    }

    public void addShort(short v) {
        ensureCapacity(2, 1);

        if (bigEndian) {
            bytes[byteSize++] = (byte) (v >> 8);
            bytes[byteSize++] = (byte) v;
        } else {
            bytes[byteSize++] = (byte) v;
            bytes[byteSize++] = (byte) (v >> 8);
        }
    }

    public void addInteger(int v) {
        ensureCapacity(4, 1);

        if (bigEndian) {
            bytes[byteSize++] = (byte) (v >> 24);
            bytes[byteSize++] = (byte) (v >> 16);
            bytes[byteSize++] = (byte) (v >> 8);
            bytes[byteSize++] = (byte) v;
        } else {
            bytes[byteSize++] = (byte) v;
            bytes[byteSize++] = (byte) (v >> 8);
            bytes[byteSize++] = (byte) (v >> 16);
            bytes[byteSize++] = (byte) (v >> 24);
        }
    }

    public void addLong(long v) {
        ensureCapacity(8, 1);

        if (bigEndian) {
            bytes[byteSize++] = (byte)((int)(v >> 56));
            bytes[byteSize++] = (byte)((int)(v >> 48));
            bytes[byteSize++] = (byte)((int)(v >> 40));
            bytes[byteSize++] = (byte)((int)(v >> 32));
            bytes[byteSize++] = (byte)((int)(v >> 24));
            bytes[byteSize++] = (byte)((int)(v >> 16));
            bytes[byteSize++] = (byte)((int)(v >> 8));
            bytes[byteSize++] = (byte)((int)(v));

        } else {
            bytes[byteSize++] = (byte)((int)(v));
            bytes[byteSize++] = (byte)((int)(v >> 8));
            bytes[byteSize++] = (byte)((int)(v >> 16));
            bytes[byteSize++] = (byte)((int)(v >> 24));
            bytes[byteSize++] = (byte)((int)(v >> 32));
            bytes[byteSize++] = (byte)((int)(v >> 40));
            bytes[byteSize++] = (byte)((int)(v >> 48));
            bytes[byteSize++] = (byte)((int)(v >> 56));
        }
    }

    public ValueType nextType() {
        return types[next];
    }

    public byte nextAsByte() {
        if (remaining() < 1) throw new IndexOutOfBoundsException();

        return bytes[next++];
    }

    public short nextAsShort() {
        if (remaining() < 2) throw new IndexOutOfBoundsException();

        byte v0; byte v1;

        if (bigEndian) {
            v0 = bytes[next];
            v1 = bytes[++next];
            next++;

        } else {
            v1 = bytes[next];
            v0 = bytes[++next];
            next++;
        }

        return (short)(v0 << 8 | v1 & 255);
    }

    public int nextAsInteger() {
        if (remaining() < 4) throw new IndexOutOfBoundsException();

        byte v0; byte v1; byte v2; byte v3;

        if (bigEndian) {
            v0 = bytes[next];
            v1 = bytes[++next];
            v2 = bytes[++next];
            v3 = bytes[++next];
            next++;

        } else {
            v3 = bytes[next];
            v2 = bytes[++next];
            v1 = bytes[++next];
            v0 = bytes[++next];
            next++;
        }

        return v0 << 24 | (v1 & 255) << 16 | (v2 & 255) << 8 | v3 & 255;
    }

    public long nextAsLong() {
        if (remaining() < 8) throw new IndexOutOfBoundsException();

        byte v0; byte v1; byte v2; byte v3;
        byte v4; byte v5; byte v6; byte v7;

        if (bigEndian) {
            v0 = bytes[next];
            v1 = bytes[++next];
            v2 = bytes[++next];
            v3 = bytes[++next];
            v4 = bytes[++next];
            v5 = bytes[++next];
            v6 = bytes[++next];
            v7 = bytes[++next];
            next++;

        } else {
            v7 = bytes[next];
            v6 = bytes[++next];
            v5 = bytes[++next];
            v4 = bytes[++next];
            v3 = bytes[++next];
            v2 = bytes[++next];
            v1 = bytes[++next];
            v0 = bytes[++next];
            next++;
        }

        return (long)v0 << 56 | ((long)v1 & 255L) << 48 | ((long)v2 & 255L) << 40 | ((long)v3 & 255L) << 32 | ((long)v4 & 255L) << 24 | ((long)v5 & 255L) << 16 | ((long)v6 & 255L) << 8 | (long)v7 & 255L;
    }
}

