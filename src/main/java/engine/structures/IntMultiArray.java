package engine.structures;

import it.unimi.dsi.fastutil.ints.IntList;

public class IntMultiArray {
    private final int delimiter;
    private IntArray array;
    private IntArray indices;

    public IntMultiArray(IntMultiArray i) {
        this.delimiter = i.delimiter;
        this.array = new IntArray(i.array);
        this.indices = new IntArray(i.indices);
    }

    public IntMultiArray(int delimiter) {
        this(8, delimiter);
    }

    public IntMultiArray(int length, int delimiter) {
        this.delimiter = delimiter;
        this.array = new IntArray(length);
        this.indices = new IntArray(1);
        this.indices.add(0);
    }

    public IntMultiArray(IntArray array, IntArray indices, int delimiter) {
        this.array = array;
        this.indices = indices;
        this.delimiter = delimiter;
    }

    public void clear() {
        this.array.clear();
        this.indices.clear();
    }

    public void writeTo(IntList list) {
        for (int i = 0; i < array.size; i++) {
            list.add(array.get(i));
        }
    }

    public int size() {
        return array.size();
    }

    public int indexOfPack(int pack) {
        if (indices.size == pack)
            return array.size;

        return indices.get(pack);
    }

    public IntArray getPack(int pack) {
        int begin = indexOfPack(pack);
        int end = indexOfPack(pack + 1);
        IntArray a = new IntArray(end - begin);
        System.arraycopy(array.array, begin, a.array, 0, end - begin);
        a.size = end - begin;
        return a;
    }

    public void add(int value) {
        if (value == delimiter)
            indices.add(array.size());

        else array.add(value);
    }

    public void set(int index, int value) {
        array.set(index, value);
    }

    public int get(int index) {
        return array.get(index);
    }
}
