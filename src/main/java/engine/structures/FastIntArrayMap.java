package engine.structures;

import java.util.Arrays;
import java.util.Iterator;

public class FastIntArrayMap<K, T> {
    private int beginIndex;
    private int endIndex;
    private K[] keys;
    private T[] values;
    private final Iter iterator = new Iter();

    public FastIntArrayMap(K[] keys, T[] values) {
        this.keys = keys;
        this.values = values;
    }

    public interface MapIter<K, T> extends Iterator<T> {
        K getKey();
    }

    public class Iter implements MapIter<K, T> {
        K key;
        T value;
        int i;

        @Override
        public boolean hasNext() {
            while (i <= endIndex) {
                if (values[i] != null) {
                    key = keys[i];
                    value = values[i];
                    return true;
                }

                ++i;
            }

            return false;
        }

        @Override
        public T next() {
            return value;
        }

        public K getKey() {
            return key;
        }
    }

    public Iter getIterator() {
        iterator.i = beginIndex;
        return iterator;
    }

    public void put(K keyT, T t) {
        int key = keyT.hashCode();

        if (key < 0) throw new IllegalArgumentException();
        if (key >= values.length) {
            keys = Arrays.copyOf(keys, key * 2 + 1);
            values = Arrays.copyOf(values, key * 2 + 1);
        }

        keys[key] = keyT;
        values[key] = t;

        updateIndexes(key);
    }

    public boolean contains(K key) {
        return key.hashCode() >= beginIndex && key.hashCode() <= endIndex && keys[key.hashCode()] != null;
    }

    public T get(K key) {
        return values[key.hashCode()];
    }

    public T get(int key) {
        return values[key];
    }

    private void updateIndexes(int key) {
        if (key < beginIndex) beginIndex = key;
        if (key > endIndex) endIndex = key;
    }
}
