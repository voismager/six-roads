package engine.structures;

import java.util.Arrays;

public class FloatArrayWrapper {
    public final float[] array;

    public FloatArrayWrapper(float... array) {
        this.array = array;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FloatArrayWrapper that = (FloatArrayWrapper) o;
        return Arrays.equals(array, that.array);
    }
}
