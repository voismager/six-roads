package engine.structures;

import java.util.Arrays;

public class ObjArrayWrapper {
    public final Object[] array;

    public ObjArrayWrapper(Object... array) {
        this.array = array;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjArrayWrapper that = (ObjArrayWrapper) o;
        return Arrays.equals(array, that.array);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(array);
    }
}
