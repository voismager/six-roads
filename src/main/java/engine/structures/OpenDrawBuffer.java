package engine.structures;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

public class OpenDrawBuffer<T> {
    public T[] array;
    public int size;

    public OpenDrawBuffer(Class<T> type) {
        this.array = (T[]) Array.newInstance(type, 8);
    }

    public void clear() {
        size = 0;
    }

    public void trueClear() {
        Arrays.fill(array, null);
        size = 0;
    }

    public void add(final T value) {
        if (size == array.length)
            array = Arrays.copyOf(array, (array.length << 2) + 1);

        array[size] = value;
        ++size;
    }

    public void add(final T[] values) {
        if (array.length - size < values.length)
            array = Arrays.copyOf(array, size + values.length);

        System.arraycopy(values, 0, array, size, values.length);
        size += values.length;
    }

    public void add(final List<T> values) {
        if (array.length - size < values.size())
            array = Arrays.copyOf(array, size + values.size());

        for (int i = 0; i < values.size(); i++) {
            array[size++] = values.get(i);
        }
    }

    public void addUnchecked(final T value) {
        array[size++] = value;
    }
}
