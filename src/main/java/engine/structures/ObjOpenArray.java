package engine.structures;

import java.util.Arrays;

public final class ObjOpenArray<T> {
    public Object[] array;
    public int size;

    public ObjOpenArray() {
        this(8);
    }

    public ObjOpenArray(final int length) {
        this.array = new Object[length];
        this.size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void clear() {
        size = 0;
    }

    public void add(final T[] values) {
        if (array.length - size < values.length)
            array = Arrays.copyOf(array, size + values.length);

        System.arraycopy(values, 0, array, size, values.length);
        size += values.length;
    }

    public void add(final ObjOpenArray<T> values) {
        if (array.length - size < values.size)
            array = Arrays.copyOf(array, size + values.size);

        System.arraycopy(values.array, 0, array, size, values.size);
        size += values.size;
    }

    public void add(final T value) {
        if (size == array.length)
            array = Arrays.copyOf(array, (array.length << 2) + 1);

        array[size++] = value;
    }

    public void addUnchecked(final T value) {
        array[size++] = value;
    }

    public void remove(final int index) {
        System.arraycopy(array, index + 1, array, index, size - 1 - index);
        --size;
    }

    public void remove(Object t) {
        for (int i = 0; i < size; i++) {
            if (t.equals(array[i])) {
                remove(i);
                return;
            }
        }
    }

    public T get(final int index) {
        return (T) array[index];
    }
}
