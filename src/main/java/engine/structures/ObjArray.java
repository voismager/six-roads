package engine.structures;

import java.util.Arrays;
import java.util.function.Predicate;

public final class ObjArray<T> {
    private Object[] array;
    private int size;

    @Override
    public String toString() {
        return "ObjArray{" +
                "array=" + Arrays.toString(array) +
                ", size=" + size +
                '}';
    }

    public ObjArray() {
        this(8);
    }

    public ObjArray(int length) {
        this.array = new Object[length];
        this.size = 0;
    }

    public ObjArray(ObjArray<T> o) {
        this.array = Arrays.copyOf(o.array, o.size);
        this.size = o.size;
    }

    public void deepClear() {
        Arrays.fill(array, 0, size,null);
        size = 0;
    }

    public void clear() {
        size = 0;
    }

    public void set(ObjArray<T> value) {
        if (array.length < value.size)
            array = Arrays.copyOf(array, value.size);

        System.arraycopy(value.array, 0, array, 0, value.size);
        size = value.size;
    }

    public void set(T[] value) {
        if (array.length < value.length)
            array = Arrays.copyOf(value, value.length);
        else
            System.arraycopy(value, 0, array, 0, value.length);

        size = value.length;
    }

    public void set(int index, T value) {
        array[index] = value;
    }

    public void add(T value) {
        if (size == array.length)
            array = Arrays.copyOf(array, array.length * 2 + 1);

        array[size++] = value;
    }

    public void add(ObjArray<T> value) {
        if (array.length - size < value.size)
            array = Arrays.copyOf(array, size + value.size);

        System.arraycopy(value.array, 0, array, size, value.size);
        size += value.size;
    }

    public boolean contains(T value) {
        for (int i = 0; i < size; i++) {
            if (value.equals(array[i])) {
                return true;
            }
        }

        return false;
    }

    public T find(Predicate<T> predicate) {
        for (int i = 0; i < size; i++) {
            T t = (T) array[i];

            if (predicate.test(t)) {
                return t;
            }
        }

        return null;
    }

    public boolean removeElement(T value) {
        for (int i = 0; i < size; i++) {
            if (value.equals(array[i])) {
                remove(i);
                return true;
            }
        }

        return false;
    }

    public void remove(int index) {
        System.arraycopy(array, index + 1, array, index, size - 1 - index);
        size--;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public T get(int index) {
        return (T) array[index];
    }
}
