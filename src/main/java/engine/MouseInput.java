package engine;

import static org.lwjgl.glfw.GLFW.*;

public class MouseInput {
    public final static byte NONE = 0;
    public final static byte LEFT_BUTTON_CLICKED = 1;
    public final static byte LEFT_BUTTON_SHORT_PRESS = 2;
    public final static byte LEFT_BUTTON_LONG_PRESS = 3;
    public final static byte RIGHT_BUTTON_CLICKED = 4;
    public final static byte RIGHT_BUTTON_LONG_PRESS = 5;
    public final static byte RIGHT_BUTTON_SHORT_PRESS = 6;
    public final static byte SCROLLED_UP = 7;
    public final static byte SCROLLED_DOWN = 8;

    private static double[] currXpos, currYpos;
    private static double prevXpos, prevYpos;
    private static boolean[] buttons;
    private static boolean[] pressed;

    private static float time;
    private static boolean scrolledUp;
    private static boolean scrolledDown;
    private static boolean q1, q2, q3, q4;

    public static void init(long window) {
        currXpos = new double[1];
        currYpos = new double[1];
        buttons  = new boolean[12];
        pressed  = new boolean[12];

        glfwSetCursorEnterCallback(window, (l, b) -> glfwGetCursorPos(window, currXpos, currYpos));

        glfwSetCursorPosCallback(window, (windowHandle, xpos, ypos) -> {
            MouseInput.currXpos[0] = xpos;
            MouseInput.currYpos[0] = ypos;
        });

        glfwSetMouseButtonCallback(window, (windowHandle, button, action, mods) -> {
            if (button == -1) return;
            buttons[button] = action != GLFW_RELEASE;
            if (action == GLFW_RELEASE && pressed[button]) pressed[button] = false;
        });

        glfwSetScrollCallback(window, (windowHandle, xoffset, yoffset) -> {
            scrolledUp = yoffset == 1.0;
            scrolledDown = yoffset == -1.0;
        });
    }

    private static double distance() {
        return Math.max(Math.abs(currXpos[0] - prevXpos), Math.abs(currYpos[0] - prevYpos));
    }

    public static byte updateAndGetState() {
        {
            if (q1) {
                time += Timer.timeUnit();

                if (isReleased(GLFW_MOUSE_BUTTON_LEFT)) {
                    q1 = false;
                    if (time < 1.0F && !q2) return LEFT_BUTTON_CLICKED;
                }

                else if (q2) {
                    prevXpos = currXpos[0];
                    prevYpos = currYpos[0];
                    return LEFT_BUTTON_LONG_PRESS;
                }

                else if (distance() > 0.5) {
                    q2 = true;
                    return LEFT_BUTTON_SHORT_PRESS;
                }
            }

            else if (isClicked(GLFW_MOUSE_BUTTON_LEFT)) {
                prevXpos = currXpos[0];
                prevYpos = currYpos[0];
                q1 = true;
                q2 = false;
                time = 0.0F;
            }
        }

        {
            if (q3) {
                time += Timer.timeUnit();

                if (isReleased(GLFW_MOUSE_BUTTON_RIGHT)) {
                    q3 = false;
                    if (time < 1.0F && !q4) return RIGHT_BUTTON_CLICKED;
                }

                else if (q4) {
                    prevXpos = currXpos[0];
                    prevYpos = currYpos[0];
                    return RIGHT_BUTTON_LONG_PRESS;
                }

                else if (distance() > 0.5) {
                    q4 = true;
                    return RIGHT_BUTTON_SHORT_PRESS;
                }
            }

            else if (isClicked(GLFW_MOUSE_BUTTON_RIGHT)) {
                prevXpos = currXpos[0];
                prevYpos = currYpos[0];
                q3 = true;
                q4 = false;
                time = 0.0F;
            }
        }

        {
            if (scrolledUp) {
                scrolledUp = false;
                return SCROLLED_UP;
            }

            if (scrolledDown) {
                scrolledDown = false;
                return SCROLLED_DOWN;
            }

            return NONE;
        }
    }

    private static boolean isClicked(int bcode) {
        if (!buttons[bcode]) return false;
        if (pressed[bcode]) return false;
        pressed[bcode] = true;
        return true;
    }

    private static boolean isReleased(int bcode) {
        return !buttons[bcode];
    }

    public static double x() {
        return currXpos[0];
    }

    public static double y() {
        return currYpos[0];
    }

    public static void reset() {
        q1 = false;
        scrolledDown = false;
        scrolledUp = false;
    }
}
