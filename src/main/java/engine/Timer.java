package engine;

public class Timer {
    private static final float INV_FPS = 1.0f / 60;

    private static float currentTime = 0;

    public static float currentTime() {
        return currentTime;
    }

    public static float timeUnit() {
        return INV_FPS;
    }

    public static void update() {
        currentTime += INV_FPS;
    }
}
