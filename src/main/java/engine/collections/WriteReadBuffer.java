package engine.collections;

import engine.Cleanable;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;

public class WriteReadBuffer<T> implements Cleanable {
    private ObjectSet<T> writeBuf;
    private T[] readBuf;
    //private int writeSize;
    private int readSize;
    private int reader;

    public WriteReadBuffer(Class<T> clazz) {
        writeBuf = new ObjectOpenHashSet<>();
        readBuf = (T[]) Array.newInstance(clazz, 0);
        //writeSize = 0;
        readSize = 0;
        reader = 0;
    }

    public void write(T t) {
        writeBuf.add(t);
    }

    public void fillRead(Comparator<T> comparator) {
        readBuf = writeBuf.toArray(readBuf);
        Arrays.sort(readBuf, 0, writeBuf.size(), comparator);
        readSize = writeBuf.size();
        reader = 0;
    }

    public void fillRead() {
        readBuf = writeBuf.toArray(readBuf);
        readSize = writeBuf.size();
        reader = 0;
    }

    public void clearWrite() {
        writeBuf.clear();
    }

    public boolean isEmpty() {
        return writeBuf.isEmpty();
    }

    public boolean hasNext() {
        return reader != readSize;
    }

    public T next() {
        return readBuf[reader++];
    }

    @Override
    public void cleanup() {
        writeBuf.clear();
        Arrays.fill(readBuf, null);
    }
}
