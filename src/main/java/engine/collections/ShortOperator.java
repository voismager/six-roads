package engine.collections;

@FunctionalInterface
public interface ShortOperator {
    short apply(short k);
}
