package engine.collections;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectMap;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;

import java.util.function.UnaryOperator;

public class CollectionsUtil {
    public static <T, V> Int2ObjectMap<Object2ObjectMap<T, V>> copy(Int2ObjectMap<Object2ObjectMap<T, V>> original) {
        Int2ObjectMap<Object2ObjectMap<T, V>> copy = new Int2ObjectOpenHashMap<>(original.size());

        for (Int2ObjectMap.Entry<Object2ObjectMap<T, V>> e : original.int2ObjectEntrySet()) {
            copy.put(e.getIntKey(), new Object2ObjectOpenHashMap<>(e.getValue()));
        }

        return copy;
    }

    public static <V> Short2ObjectMap<V> copy(Short2ObjectMap<V> original, UnaryOperator<V> copier) {
        Short2ObjectMap<V> copy = new Short2ObjectOpenHashMap<>(original.size());

        for (Short2ObjectMap.Entry<V> e : original.short2ObjectEntrySet()) {
            V value = copier.apply(e.getValue());
            copy.put(e.getShortKey(), value);
        }

        return copy;
    }

    public static <K, V> Object2ObjectMap<K, V> copy(Object2ObjectMap<K, V> original, UnaryOperator<V> copier) {
        Object2ObjectMap<K, V> copy = new Object2ObjectOpenHashMap<>(original.size());

        for (Object2ObjectMap.Entry<K, V> e : original.object2ObjectEntrySet()) {
            V value = copier.apply(e.getValue());
            copy.put(e.getKey(), value);
        }

        return copy;
    }

    public static <V> void foreach(Short2ObjectMap<V> map, ObjShortConsumer<V> consumer) {
        for (Short2ObjectMap.Entry<V> e : map.short2ObjectEntrySet()) {
            consumer.accept(e.getValue(), e.getShortKey());
        }
    }
}
