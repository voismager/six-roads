package engine.collections;

@FunctionalInterface
public interface ObjShortConsumer<T> {
    void accept(T var1, short var2);
}
