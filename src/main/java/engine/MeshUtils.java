package engine;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

public final class MeshUtils {
    public static int genIndexBuffer(int[] content, int speed) {
        int buffer = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, content, speed);
        return buffer;
    }

    public static int genIndexBuffer(int maxElements, int speed) {
        int buffer = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, maxElements * Integer.BYTES, speed);
        return buffer;
    }

    public static int genIntBuffer(int attribIndex, int[] content, int length, int speed) {
        int buffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, content, speed);
        glVertexAttribPointer(attribIndex, length, GL_INT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return buffer;
    }

    public static int genFloatBuffer(int attribIndex, float[] content, int length, int speed) {
        int buffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, content, speed);
        glVertexAttribPointer(attribIndex, length, GL_FLOAT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return buffer;
    }

    public static int genFloatBuffer(int attribIndex, int maxElements, int length, int speed) {
        int buffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, maxElements * Float.BYTES, speed);
        glVertexAttribPointer(attribIndex, length, GL_FLOAT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return buffer;
    }
}
