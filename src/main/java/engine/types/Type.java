package engine.types;

public interface Type<T> {
    T value();
}
