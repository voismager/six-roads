package engine.types.vectors;

public class Vector0 implements Vector {
    private final int value;

    public Vector0(int value) {
        this.value = value;
    }

    @Override
    public int get() {
        return value;
    }

    @Override
    public int get(int i) {
        if (i == 0) return value;
        return 0;
    }

    @Override
    public int get(int i, int j) {
        if (i == 0 && j == 0) return value;
        return 0;
    }

    @Override
    public int length(int dim) {
        return 1;
    }
}
