package engine.types.vectors;

public class EmptyVector implements Vector {
    static EmptyVector INSTANCE = new EmptyVector();

    @Override
    public int get() {
        return 0;
    }

    @Override
    public int get(int i) {
        return 0;
    }

    @Override
    public int get(int i, int j) {
        return 0;
    }

    @Override
    public int length(int dim) {
        return 0;
    }
}
