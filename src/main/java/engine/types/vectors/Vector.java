package engine.types.vectors;

public interface Vector {
    static Vector of(int value) {
        return new Vector0(value);
    }

    static Vector of(int... value) {
        if (value.length == 0) return EmptyVector.INSTANCE;
        if (value.length == 1) return new Vector0(value[0]);
        return new Vector1(value);
    }

    int get();

    int get(int i);

    int get(int i, int j);

    int length(int dim);
}
