package engine.types.vectors;

public class Vector1 implements Vector {
    private final int[] value;

    public Vector1(int[] value) {
        this.value = value;
    }

    @Override
    public int get() {
        return value[0];
    }

    @Override
    public int get(int i) {
        if (i < value.length) return value[i];
        return 0;
    }

    @Override
    public int get(int i, int j) {
        if (i < value.length && j == 0) return value[i];
        return 0;
    }

    @Override
    public int length(int dim) {
        if (dim == 0) return value.length;
        return 0;
    }
}
