package engine.types.observables.objects;

import engine.types.observables.Observable;
import engine.types.observables.Observer;

import java.util.ArrayList;
import java.util.List;

public class ObservableObject<T> implements Observable<T> {
    private List<Observer> observers;
    private T value;

    public void set(T value) {
        this.value = value;
        fireEvent();
    }

    private void fireEvent() {
        if (observers != null) {
            int len = observers.size();

            for (int i = 0; i < len; i++) {
                observers.get(i).update(this, null);
            }
        }
    }

    @Override
    public T value() {
        return value;
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void bindObserver(Observer o) {
        if (observers == null)
            observers = new ArrayList<>();

        observers.add(o);
    }

    @Override
    public void cleanup() {
        if (observers != null) {
            observers.clear();
            observers = null;
        }

        value = null;
    }
}
