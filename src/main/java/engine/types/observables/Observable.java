package engine.types.observables;

import engine.Cleanable;
import engine.types.Type;

public interface Observable<T> extends Type<T>, Cleanable {
    byte SET_ALL_CODE       = 0;
    byte SET_CODE           = 1;
    byte ADD_CODE           = 2;
    byte REMOVE_CODE        = 3;
    byte ADD_VERTEX_CODE    = 4;
    byte REMOVE_VERTEX_CODE = 5;
    byte ADD_EDGE_CODE      = 6;
    byte REMOVE_EDGE_CODE   = 7;

    void removeObserver(Observer o);

    void bindObserver(Observer o);
}
