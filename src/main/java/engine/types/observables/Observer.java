package engine.types.observables;

public interface Observer {
    void update(Observable caller, Object meta);
}
