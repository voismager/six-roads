package engine.types.observables.integers;

import engine.types.integers.Graph;
import engine.types.integers.IntType;
import engine.types.observables.Observable;
import engine.types.observables.Observer;
import engine.structures.IntMultiArray;
import engine.structures.LongArray;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import org.luaj.vm2.LuaInteger;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import engine.support.Tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

import static engine.support.keys.Mapper.*;

public class ObservableGraph extends Graph implements Observable<Long2ObjectMap<IntMultiArray>> {
    private List<Observer> observers;

    public ObservableGraph(Long2ObjectMap<IntMultiArray> value) {
        super(value);
    }

    @Override
    public String toString() {
        return "ObservableGraph{" +
                "observers=" + observers +
                ", value=" + value +
                ", keys=" + Arrays.toString(keys) +
                '}';
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void bindObserver(Observer o) {
        if (observers == null)
            observers = new ArrayList<>();

        observers.add(o);
    }

    @Override
    public void cleanup() {
        if (observers != null) {
            observers.clear();
            observers = null;
        }

        value.forEach((l, a) -> a.clear());
        value.clear();
        value = null;

        for (LongArray a : keys) a.clear();
        keys = null;
    }

    @Override
    public Observable<Long2ObjectMap<IntMultiArray>> asObservable() {
        return this;
    }

    @Override
    public IntType<Long2ObjectMap<IntMultiArray>> copy() {
        Long2ObjectMap<IntMultiArray> map = new Long2ObjectOpenHashMap<>(value.size());

        for (Long2ObjectMap.Entry<IntMultiArray> e : value.long2ObjectEntrySet()) {
            map.put(e.getLongKey(), new IntMultiArray(e.getValue()));
        }

        return new ObservableGraph(map);
    }
}
