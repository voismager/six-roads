package engine.types.observables.integers;

import engine.types.integers.IntListType;
import engine.types.integers.IntType;
import engine.types.observables.Observable;
import engine.types.observables.Observer;
import engine.structures.IntArray;

import java.util.ArrayList;
import java.util.List;

import static engine.support.keys.Mapper.TD01_BS_I;

public class ObservableIntList extends IntListType implements Observable<IntArray> {
    private List<Observer> observers;

    public ObservableIntList(IntArray value) {
        super(value);
    }

    public ObservableIntList(int... value) {
        super(value);
    }

    @Override
    public String toString() {
        return "ObservableIntList{" +
                "observers=" + observers +
                ", value=" + value +
                '}';
    }

    @Override
    public void clear() {
        super.clear();

        fireEvent(REMOVE_CODE, -1);
    }

    @Override
    public void set(IntArray value) {
        super.set(value);

        fireEvent(SET_ALL_CODE, 0);
    }

    @Override
    public void set(int[] value) {
        super.set(value);

        fireEvent(SET_ALL_CODE, 0);
    }

    @Override
    public void set(int index, int value) {
        super.set(index, value);

        fireEvent(SET_CODE, index);
    }

    @Override
    public void add(int value) {
        super.add(value);

        fireEvent(ADD_CODE, 0);
    }

    @Override
    public void removeFromTail(int len) {
        super.removeFromTail(len);

        fireEvent(REMOVE_CODE, -1);
    }

    @Override
    public void remove(int index) {
        super.remove(index);

        fireEvent(REMOVE_CODE, index);
    }

    private void fireEvent(byte code, int index) {
        if (observers != null) {
            int fullCode = TD01_BS_I(code, index);
            int len = observers.size();

            for (int i = 0; i < len; i++) {
                observers.get(i).update(this, fullCode);
            }
        }
    }

    @Override
    public Observable<IntArray> asObservable() {
        return this;
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void bindObserver(Observer o) {
        if (observers == null)
            observers = new ArrayList<>();

        observers.add(o);
    }

    @Override
    public void cleanup() {
        if (observers != null) {
            observers.clear();
            observers = null;
        }

        value.clear();
        value = null;
    }

    @Override
    public IntType<IntArray> copy() {
        return new ObservableIntList(value);
    }
}
