package engine.types.observables.integers;

import engine.types.integers.Int;
import engine.types.integers.IntType;
import engine.types.observables.Observable;
import engine.types.observables.Observer;

import java.util.ArrayList;
import java.util.List;

public class ObservableInt extends Int implements Observable<Integer> {
    private List<Observer> observers;

    public ObservableInt(int value) {
        super(value);
    }

    @Override
    public String toString() {
        return "ObservableInt{" +
                "observers=" + observers +
                ", value=" + value +
                '}';
    }

    @Override
    public void set(int value) {
        super.set(value);

        if (observers != null) {
            int len = observers.size();

            for (int i = 0; i < len; i++) {
                observers.get(i).update(this, null);
            }
        }
    }

    @Override
    public Observable<Integer> asObservable() {
        return this;
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void bindObserver(Observer o) {
        if (observers == null)
            observers = new ArrayList<>();

        observers.add(o);
    }

    @Override
    public void cleanup() {
        if (observers != null) {
            observers.clear();
            observers = null;
        }
    }

    @Override
    public IntType<Integer> copy() {
        return new ObservableInt(value);
    }
}
