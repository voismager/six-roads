package engine.types.booleans;

public class Bool {
    protected boolean value;

    public boolean get() {
        return value;
    }

    public void set(boolean value) {
        this.value = value;
    }
}
