package engine.types.integers;

import engine.types.observables.Observable;

public class Int implements IntType<Integer> {
    protected int value;

    public Int(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Int{" +
                "value=" + value +
                '}';
    }

    public int get() {
        return value;
    }

    @Override
    public int get(int i) {
        if (i == 0) return value;
        return 0;
    }

    @Override
    public int get(int i, int j) {
        if (i == 0 && j == 0) return value;
        return 0;
    }

    @Override
    public int length(int dim) {
        return 1;
    }

    public void set(int value) {
        this.value = value;
    }

    @Override
    public Integer value() {
        return value;
    }

    @Override
    public Observable<Integer> asObservable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Int toInt() {
        return this;
    }

    @Override
    public IntListType toList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Graph toGraph() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int asInt() {
        return value;
    }

    @Override
    public void set(IntType<Integer> original) {
        this.set(original.asInt());
    }

    @Override
    public IntType<Integer> copy() {
        return new Int(value);
    }
}
