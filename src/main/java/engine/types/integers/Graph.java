package engine.types.integers;

import engine.structures.IntMultiArray;
import engine.structures.LongArray;
import engine.support.Tools;
import engine.types.observables.Observable;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;

import java.util.Arrays;

import static engine.support.keys.Mapper.*;

public class Graph implements IntType<Long2ObjectMap<IntMultiArray>> {
    protected Long2ObjectMap<IntMultiArray> value;
    protected LongArray[] keys;

    public Graph(Long2ObjectMap<IntMultiArray> value) {
        this.value = value;
        this.keys = Tools.arrayWithInit(LongArray.class, 4);

        for (long key : value.keySet()) {
            keys[TD05_keys(key) - 1].add(key);
        }
    }

    @Override
    public String toString() {
        return "Graph{" +
                "value=" + value +
                ", keys=" + Arrays.toString(keys) +
                '}';
    }

    public IntMultiArray getValue(long key) {
        return value.get(key);
    }

    public IntMultiArray getValue(short key0, short key1, short key2, short key3) {
        long key = TD05_SSSS_L(key0, key1, key2, key3);
        return value.get(key);
    }

    public IntMultiArray getValue(short key0, short key1, short key2) {
        long key = TD05_SSSs_L(key0, key1, key2);
        return value.get(key);
    }

    public IntMultiArray getValue(short key0, short key1) {
        long key = TD05_SSss_L(key0, key1);
        return value.get(key);
    }

    public IntMultiArray getValue(short key0) {
        long key = TD05_Ssss_L(key0);
        return value.get(key);
    }

    public LongArray getKeys(int length) {
        return keys[length - 1];
    }

    @Override
    public Long2ObjectMap<IntMultiArray> value() {
        return value;
    }

    @Override
    public Observable<Long2ObjectMap<IntMultiArray>> asObservable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Int toInt() {
        throw new UnsupportedOperationException();
    }

    @Override
    public IntListType toList() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Graph toGraph() {
        return this;
    }

    @Override
    public int asInt() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int get() {
        return 0;
    }

    @Override
    public int get(int i) {
        return 0;
    }

    @Override
    public int get(int i, int j) {
        return 0;
    }

    @Override
    public int length(int dim) {
        return 0;
    }

    @Override
    public void set(IntType<Long2ObjectMap<IntMultiArray>> original) {

    }

    @Override
    public IntType<Long2ObjectMap<IntMultiArray>> copy() {
        Long2ObjectMap<IntMultiArray> map = new Long2ObjectOpenHashMap<>(value.size());

        for (Long2ObjectMap.Entry<IntMultiArray> e : value.long2ObjectEntrySet()) {
            map.put(e.getLongKey(), new IntMultiArray(e.getValue()));
        }

        return new Graph(map);
    }
}
