package engine.types.integers;

import engine.structures.IntArray;
import engine.types.observables.Observable;

public class IntListType implements IntType<IntArray> {
    protected IntArray value;

    public IntListType(IntArray value) {
        this.value = new IntArray(value);
    }

    public IntListType(int... value) {
        this.value = new IntArray(value);
    }

    @Override
    public String toString() {
        return "IntList{" +
                "value=" + value +
                '}';
    }

    public int[] toArray() {
        return value.toArray();
    }

    public int size() {
        return value.size();
    }

    public final boolean isEmpty() {
        return value.size() == 0;
    }

    @Override
    public int get() {
        if (value.size() > 0) return value.get(0);
        return 0;
    }

    public int get(int index) {
        return value.get(index);
    }

    @Override
    public int get(int i, int j) {
        if (j == 0 && value.size() > i) return value.get(i);
        return 0;
    }

    @Override
    public int length(int dim) {
        if (dim == 0) return value.size();
        return 1;
    }

    public int getLast(int index) {
        return value.get(value.size() - index - 1);
    }

    public void clear() {
        this.value.clear();
    }

    public void set(IntArray value) {
        this.value.clear();
        this.value.set(value);
    }

    public void set(int[] value) {
        this.value.clear();
        this.value.set(value);
    }

    public void set(int index, int value) {
        this.value.set(index, value);
    }

    public void add(int value) {
        this.value.add(value);
    }

    public void add(int x, int y) {
        this.value.add(x);
        this.value.add(y);
    }

    public void removeFromTail(int len) {
        this.value.removeFromTail(len);
    }

    public void remove(int index) {
        this.value.remove(index);
    }

    public boolean contains(int value) {
        return this.value.contains(value);
    }

    @Override
    public IntArray value() {
        return value;
    }

    @Override
    public Observable<IntArray> asObservable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Int toInt() {
        throw new UnsupportedOperationException();
    }

    @Override
    public IntListType toList() {
        return this;
    }

    @Override
    public Graph toGraph() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int asInt() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void set(IntType<IntArray> original) {
        this.set(original.value());
    }

    @Override
    public IntType<IntArray> copy() {
        return new IntListType(value);
    }
}
