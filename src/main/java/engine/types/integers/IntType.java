package engine.types.integers;

import engine.types.Type;
import engine.types.observables.Observable;
import org.luaj.vm2.LuaValue;

public interface IntType<T> extends Type<T> {
    Observable<T> asObservable();

    Int toInt();

    IntListType toList();

    Graph toGraph();

    int asInt();

    int get();

    int get(int i);

    int get(int i, int j);

    int length(int dim);

    void set(IntType<T> original);

    IntType<T> copy();
}
