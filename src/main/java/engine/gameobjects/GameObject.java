package engine.gameobjects;

import engine.Cleanable;

public abstract class GameObject implements Cleanable {
    public float x, y, z;
}
