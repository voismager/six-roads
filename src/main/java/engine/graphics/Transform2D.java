package engine.graphics;

import client.Settings;
import engine.MouseInput;
import org.joml.*;

public class Transform2D {
    private static final float Z_NEAR = 0f;
    private static final float Z_FAR = 20f;
    private static final float aspectRatio = (float) Settings.WINDOW_WIDTH / (float) Settings.WINDOW_HEIGHT;

    private static final float MAX_VERTICAL_SCALE_EXCLUSIVE = 18F;
    private static final float MIN_VERTICAL_SCALE_EXCLUSIVE = 0F;
    private static final float VERTICAL_SCALE_STEP = 3F;

    private static float verticalScale = 12F;
    private static float horizontalScale = verticalScale * aspectRatio;

    private static final Matrix4f projMatrix;
    private static final Matrix4f orthoMatrix;
    private static final Matrix4f viewMatrix;
    private static final Matrix4f outputMatrix;

    private static final Vector2f cameraPosition;
    private static final Vector2f cameraPreviousPosition;
    private static final Vector2d globalMouseWithCameraCoordinates;
    private static final Vector2d globalMouseCoordinates;

    static {
        orthoMatrix  = new Matrix4f().setOrtho2D(0, Settings.WINDOW_WIDTH, Settings.WINDOW_HEIGHT, 0);
        projMatrix   = new Matrix4f().setOrtho(-horizontalScale, horizontalScale, -verticalScale, verticalScale, Z_NEAR ,Z_FAR);
        viewMatrix   = new Matrix4f().m32(-1);
        outputMatrix = new Matrix4f();

        cameraPosition         = new Vector2f();
        cameraPreviousPosition = new Vector2f();

        globalMouseWithCameraCoordinates = new Vector2d();
        globalMouseCoordinates           = new Vector2d();
    }

    public static void reset() {
        cameraPosition.set(0, 0);
        cameraPreviousPosition.set(0, 0);
    }

    public static Vector2f getCamera() {
        return cameraPosition;
    }

    public static void translate() {
        cameraPosition.x += globalMouseCoordinates.x - cameraPreviousPosition.x;
        cameraPosition.y += cameraPreviousPosition.y - globalMouseCoordinates.y;
        cameraPreviousPosition.set(globalMouseCoordinates);
    }

    public static void setPreviousPos() {
        cameraPreviousPosition.set(globalMouseCoordinates);
    }

    public static void update() {
        updateGenericViewMatrix(cameraPosition);
        globalMouseCoordinates.x = ((MouseInput.x() / Settings.WINDOW_WIDTH) * 2F - 1F) * horizontalScale;
        globalMouseCoordinates.y = ((MouseInput.y() / Settings.WINDOW_HEIGHT) * 2F - 1F) * verticalScale;

        globalMouseWithCameraCoordinates.x = globalMouseCoordinates.x - cameraPosition.x;
        globalMouseWithCameraCoordinates.y = globalMouseCoordinates.y + cameraPosition.y;
    }

    public static Vector2d getGlobalMouseWithCameraCoordinates() {
        return globalMouseWithCameraCoordinates;
    }

    public static Vector2d getGlobalMouseCoordinates() {
        return globalMouseCoordinates;
    }

    public static void zoomOut() {
        if (MAX_VERTICAL_SCALE_EXCLUSIVE == verticalScale + VERTICAL_SCALE_STEP) return;
        verticalScale += VERTICAL_SCALE_STEP;
        horizontalScale = verticalScale * aspectRatio;
        projMatrix.setOrtho(-horizontalScale, horizontalScale, -verticalScale, verticalScale, Z_NEAR ,Z_FAR);
    }

    public static void zoomIn() {
        if (MIN_VERTICAL_SCALE_EXCLUSIVE == verticalScale - VERTICAL_SCALE_STEP) return;
        verticalScale -= VERTICAL_SCALE_STEP;
        horizontalScale = verticalScale * aspectRatio;
        projMatrix.setOrtho(-horizontalScale, horizontalScale, -verticalScale, verticalScale, Z_NEAR ,Z_FAR);
    }

    private static void updateGenericViewMatrix(Vector2f camera) {
        viewMatrix
                .m30(camera.x)
                .m31(camera.y);
    }

    public static Matrix4f getModelViewMatrix(float x, float y) {
        outputMatrix.identity()
                .m30(x)
                .m31(y);
        return viewMatrix.mulAffine(outputMatrix, outputMatrix);
    }

    public static Matrix4f getOrthoProjModelMatrix(Vector2f position) {
        outputMatrix.identity()
                .m30(position.x)
                .m31(position.y);
        return orthoMatrix.mul(outputMatrix, outputMatrix);
    }

    public static Matrix4f getOrthoProjModelMatrix(float x, float y) {
        outputMatrix.identity()
                .m30(x)
                .m31(y);
        return orthoMatrix.mul(outputMatrix, outputMatrix);
    }

    public static Matrix4f getOrthoProjModelMatrix() {
        return orthoMatrix;
    }

    public static Matrix4f getProjMatrix() {
        return projMatrix;
    }

    public static Matrix4f getViewMatrix() {
        return viewMatrix;
    }
}
