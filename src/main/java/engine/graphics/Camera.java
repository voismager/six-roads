package engine.graphics;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class Camera {
    private static float ZOOM_STEP = 1f;

    Vector2f globalMousePosition;
    Vector3f position;
    Vector3f rotation;
    Vector2f dragOrigin;

    Camera() {
        globalMousePosition = new Vector2f();
        position = new Vector3f();
        rotation = new Vector3f();
        dragOrigin = new Vector2f();
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setRotation(float x, float y, float z) {
        rotation.set(
                (float)Math.toRadians(x),
                (float)Math.toRadians(y),
                (float)Math.toRadians(z)
        );
    }

    public void setPosition(float x, float y, float z) {
        position.set(x, y, z);
    }

    public void setDragOrigin() {
        dragOrigin.set(globalMousePosition);
    }

    public void translate() {
        position.x += dragOrigin.x - globalMousePosition.x;
        position.y += dragOrigin.y - globalMousePosition.y;
        dragOrigin.set(globalMousePosition);
    }

    public void translate(float x, float y, float z) {
        position.x += x;
        position.y += y;
        position.z += z;
    }

    public void zoomIn() {
        position.y -= (float) Math.sin(rotation.x) * ZOOM_STEP;
        position.z -= (float) Math.cos(rotation.x) * ZOOM_STEP;
    }

    public void zoomOut() {
        position.y += (float) Math.sin(rotation.x) * ZOOM_STEP;
        position.z += (float) Math.cos(rotation.x) * ZOOM_STEP;
    }
}
