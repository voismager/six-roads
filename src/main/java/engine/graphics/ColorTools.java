package engine.graphics;

import engine.support.RandomUtil;
import org.joml.Vector3f;

public class ColorTools {
    public static Vector3f generateRandomColor(int mixR, int mixG, int mixB) {
        int red = RandomUtil.globalNextInt(0, 255);
        int green = RandomUtil.globalNextInt(0, 255);
        int blue = RandomUtil.globalNextInt(0, 255);

        red = (red + mixR) / 2;
        green = (green + mixG) / 2;
        blue = (blue + mixB) / 2;

        Vector3f c = new Vector3f(red, green, blue);
        normalizeRGB(c);
        return c;
    }

    public static Vector3f playerColor(int index, int size) {
        float hue = 1f / size * index;
        Vector3f color = new Vector3f(hue, 0.55f,1f);
        toRGB(color);
        normalizeRGB(color);
        return color;
    }

    public static void normalizeRGB(Vector3f RGB) {
        RGB.x /= 255f;
        RGB.y /= 255f;
        RGB.z /= 255f;
    }

    public static void toRGB(Vector3f HSB) {
        int r = 0, g = 0, b = 0;

        if (HSB.y == 0) {
            r = g = b = (int) (HSB.z * 255.0f + 0.5f);
        }

        else {
            float h = (HSB.x - (float)Math.floor(HSB.x)) * 6.0f;
            float f = h - (float) Math.floor(h);
            float p = HSB.z * (1.0f - HSB.y);
            float q = HSB.z * (1.0f - HSB.y * f);
            float t = HSB.z * (1.0f - (HSB.y * (1.0f - f)));
            switch ((int) h) {
                case 0:
                    r = (int) (HSB.z * 255.0f + 0.5f);
                    g = (int) (t * 255.0f + 0.5f);
                    b = (int) (p * 255.0f + 0.5f);
                    break;
                case 1:
                    r = (int) (q * 255.0f + 0.5f);
                    g = (int) (HSB.z * 255.0f + 0.5f);
                    b = (int) (p * 255.0f + 0.5f);
                    break;
                case 2:
                    r = (int) (p * 255.0f + 0.5f);
                    g = (int) (HSB.z * 255.0f + 0.5f);
                    b = (int) (t * 255.0f + 0.5f);
                    break;
                case 3:
                    r = (int) (p * 255.0f + 0.5f);
                    g = (int) (q * 255.0f + 0.5f);
                    b = (int) (HSB.z * 255.0f + 0.5f);
                    break;
                case 4:
                    r = (int) (t * 255.0f + 0.5f);
                    g = (int) (p * 255.0f + 0.5f);
                    b = (int) (HSB.z * 255.0f + 0.5f);
                    break;
                case 5:
                    r = (int) (HSB.z * 255.0f + 0.5f);
                    g = (int) (p * 255.0f + 0.5f);
                    b = (int) (q * 255.0f + 0.5f);
                    break;
            }
        }

        HSB.set(r, g, b);
    }
}
