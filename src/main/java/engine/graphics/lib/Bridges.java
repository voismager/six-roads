package engine.graphics.lib;

import engine.graphics.meshes.MaterialInstancedMesh;
import engine.structures.ObjArrayWrapper;
import org.joml.Vector2f;

import java.util.HashMap;
import java.util.Map;

public class Bridges {
    private final static float oH = (float)(Math.sqrt(3) / 2);
    private final static float oW = 1.0f;
    private final static float iH = oH * 0.75f;
    private final static float iW = oW * 0.75f;
    private final static Vector2f TEMP = new Vector2f();
    private final static Map<ObjArrayWrapper, MaterialInstancedMesh> BRIDGE_MAP = new HashMap<>();
    private final static MeshBuilder.ColorMeshBuilder BUILDER = MeshBuilder.getMaterialMeshBuilder();

    /*
    public static void setBridges(Ground g, VisibleHexMap map) {
        VisibleHexagon gHexagon = g.getHexagon();
        int arrayIndex = 0;

        if (map.hasNeighborGround(g.q, g.r, 0)) {
            Ground n0 = map.getVisibleNeighborGround(g.q, g.r, 0);
            VisibleHexagon n0Hexagon = n0.getHexagon();

            g.setBridge(getBridge(n0.z - g.z, 0,
                    gHexagon.terrain.mesh.getMaterial(), n0Hexagon.terrain.mesh.getMaterial(), true),
                    ++arrayIndex
            );

            if (map.hasNeighborGround(g.q, g.r, 1)) {
                Ground n1 = map.getVisibleNeighborGround(g.q, g.r, 1);
                VisibleHexagon n1Hexagon = n1.getHexagon();

                g.setBridge(getBridge(n0.z - g.z, n1.z - g.z, 0,
                        gHexagon.terrain.mesh.getMaterial(),
                        n0Hexagon.terrain.mesh.getMaterial(),
                        n1Hexagon.terrain.mesh.getMaterial(), true),
                        ++arrayIndex
                );
            }
        }

        if (map.hasNeighborGround(g.q, g.r, 1)) {
            Ground n0 = map.getVisibleNeighborGround(g.q, g.r, 1);
            VisibleHexagon n0Hexagon = n0.getHexagon();

            g.setBridge(getBridge(n0.z - g.z, 1,
                    gHexagon.terrain.mesh.getMaterial(),
                    n0Hexagon.terrain.mesh.getMaterial(), true),
                    ++arrayIndex
            );

            if (map.hasNeighborGround(g.q, g.r, 2)) {
                Ground n1 = map.getVisibleNeighborGround(g.q, g.r, 2);
                VisibleHexagon n1Hexagon = n1.getHexagon();

                g.setBridge(getBridge(n0.z - g.z, n1.z - g.z, 1,
                        gHexagon.terrain.mesh.getMaterial(),
                        n0Hexagon.terrain.mesh.getMaterial(),
                        n1Hexagon.terrain.mesh.getMaterial(), true),
                        ++arrayIndex
                );
            }
        }

        if (map.hasNeighborGround(g.q, g.r, 2)) {
            Ground n0 = map.getVisibleNeighborGround(g.q, g.r, 2);
            VisibleHexagon n0Hexagon = n0.getHexagon();

            g.setBridge(getBridge(n0.z - g.z, 2,
                    gHexagon.terrain.mesh.getMaterial(),
                    n0Hexagon.terrain.mesh.getMaterial(), true),
                    ++arrayIndex
            );
        }
    }

    public static void updateBridges(Ground g, VisibleHexMap map) {
        boolean hasNW = map.hasNeighborGround(g.q, g.r, 0);
        boolean hasN = map.hasNeighborGround(g.q, g.r, 1);
        //Paint paint = g.getFirstPaint();

        //if (isDefault(paint)) {
            Ground left, right;

            for (int i = 0; i < 6; i++) {
                if ((left = map.getVisibleNeighborGround(g.q, g.r, i)) != null) {
                    //Paint leftPaint = left.getFirstPaint();

                    //if (isDefault(leftPaint)) {
                        setQuadBridge(g, left, i,
                                g.getHexagon().terrain.mesh.getMaterial(),
                                left.getHexagon().terrain.mesh.getMaterial(),
                                true, hasNW, hasN, map
                        );
                    //}




                    if ((right = map.getVisibleNeighborGround(g.q, g.r, i + 1 == 6 ? 0 : i + 1)) != null) {
                        setTriangleBridge(g, left, right, i,
                                g.getHexagon().terrain.mesh.getMaterial(),
                                left.getHexagon().terrain.mesh.getMaterial(),
                                right.getHexagon().terrain.mesh.getMaterial(),
                                true, hasNW, hasN, map
                        );
                    }
                }
            }
        //}
    }

    private static void setTriangleBridge(Ground center, Ground left, Ground right, int i, Material cM, Material lM, Material rM, boolean blend, boolean hasNW, boolean hasN, VisibleHexMap map) {
        if (i < 2) {
            final MaterialInstancedMesh bridge = getBridge(
                    left.z - center.z, right.z - center.z, i,
                    cM, lM, rM, blend
            );
            center.setBridge(bridge, calculateBridgeIndex(i, false, hasNW, hasN));
        } else if (i < 4) {
            boolean nHasNW = map.hasNeighborGround(right.q, right.r, 0);
            boolean nHasN = map.hasNeighborGround(right.q, right.r, 1);

            final MaterialInstancedMesh bridge = getBridge(
                    center.z - right.z, left.z - right.z, i - 2,
                    rM, cM, lM, blend
            );
            right.setBridge(bridge, calculateBridgeIndex((i - 2), false, nHasNW, nHasN));
        } else {
            boolean nHasNW = map.hasNeighborGround(left.q, left.r, 0);
            boolean nHasN = map.hasNeighborGround(left.q, left.r, 1);

            final MaterialInstancedMesh bridge = getBridge(
                    right.z - left.z, center.z - left.z, i - 4,
                    lM, rM, cM, blend
            );
            left.setBridge(bridge, calculateBridgeIndex((i - 4), false, nHasNW, nHasN));
        }
    }

    private static void setQuadBridge(Ground center, Ground neighbor, int i, Material m0, Material m1, boolean blend, boolean hasNW, boolean hasN, VisibleHexMap map) {
        if (i < 3) {
            final MaterialInstancedMesh bridge = getBridge(neighbor.z - center.z, i, m0, m1, blend);
            center.setBridge(bridge, calculateBridgeIndex(i, true, hasNW, hasN));
        } else {
            final MaterialInstancedMesh bridge = getBridge(center.z - neighbor.z, i - 3, m1, m0, blend);

            boolean nHasNW = map.hasNeighborGround(neighbor.q, neighbor.r, 0);
            boolean nHasN = map.hasNeighborGround(neighbor.q, neighbor.r, 1);

            neighbor.setBridge(bridge, calculateBridgeIndex(i - 3, true, nHasNW, nHasN));
        }
    }

    private static int calculateBridgeIndex(int i, boolean quad, boolean hasNW, boolean hasN) {
        if (i == 0) {
            if (quad) return 1;
            else return 2;
        } else if (i == 1) {
            if (hasNW) {
                if (quad) return 3;
                else return 4;
            } else {
                if (quad) return 1;
                else return 2;
            }
        } else if (i == 2) {
            if (hasNW) {
                if (hasN) return 5;
                else return 3;
            } else if (hasN) return 3;
            else return 1;
        }

        throw new IllegalStateException();
    }

    private static MaterialInstancedMesh getBridge(float h, int i0, Material m0, Material m1, boolean blend) {
        final ObjArrayWrapper params = new ObjArrayWrapper(true, blend, h, i0, m0.id, m1.id, null);

        if (BRIDGE_MAP.containsKey(params)) return BRIDGE_MAP.get(params);

        innerCorner(i0, TEMP);
        float xi0 = TEMP.x;
        float yi0 = TEMP.y;

        innerCorner(i0 + 1, TEMP);
        float xi1 = TEMP.x;
        float yi1 = TEMP.y;

        Vector2f m = new Vector2f((xi0 + xi1) / 2, (yi0 + yi1) / 2);
        m.mul(1f / m.length() * oH * 0.5f);

        float xo0 = xi0 + m.x;
        float yo0 = yi0 + m.y;
        float xo1 = xi1 + m.x;
        float yo1 = yi1 + m.y;

        BUILDER.clear();

        if (blend) {
            BUILDER.addQuad(
                    xi1, yi1, 0, m0.ambientR, m0.ambientG, m0.ambientB,
                    xo1, yo1, h, m1.ambientR, m1.ambientG, m1.ambientB,
                    xo0, yo0, h, m1.ambientR, m1.ambientG, m1.ambientB,
                    xi0, yi0, 0, m0.ambientR, m0.ambientG, m0.ambientB
            );
        } else {
            BUILDER.addQuad(
                    (xo1 + xi1) * 0.5f, (yo1 + yi1) * 0.5f, h * 0.5f, m1.ambientR, m1.ambientG, m1.ambientB,
                    xo1, yo1, h, m1.ambientR, m1.ambientG, m1.ambientB,
                    xo0, yo0, h, m1.ambientR, m1.ambientG, m1.ambientB,
                    (xo0 + xi0) * 0.5f, (yo0 + yi0) * 0.5f, h * 0.5f, m1.ambientR, m1.ambientG, m1.ambientB
            );
            BUILDER.addQuad(
                    xi1, yi1, 0, m0.ambientR, m0.ambientG, m0.ambientB,
                    (xo1 + xi1) * 0.5f, (yo1 + yi1) * 0.5f, h * 0.5f, m0.ambientR, m0.ambientG, m0.ambientB,
                    (xo0 + xi0) * 0.5f, (yo0 + yi0) * 0.5f, h * 0.5f, m0.ambientR, m0.ambientG, m0.ambientB,
                    xi0, yi0, 0, m0.ambientR, m0.ambientG, m0.ambientB
            );
        }

        BUILDER.setMaterial(Materials.getBridge(
                new Vector4f(),
                new Vector4f(
                        m0.diffuseR + m1.diffuseR,
                        m0.diffuseG + m1.diffuseG,
                        m0.diffuseB + m1.diffuseB,
                        m0.diffuseA + m1.diffuseA).mul(0.5f),

                new Vector4f(
                        m0.specularR + m1.specularR,
                        m0.specularG + m1.specularG,
                        m0.specularB + m1.specularB,
                        m0.specularA + m1.specularA).mul(0.5f),

                (m0.reflectance + m1.reflectance) * 0.5f
        ));

        BUILDER.setInstances(100);

        MaterialInstancedMesh mesh = (MaterialInstancedMesh) BUILDER.bake(GL_STATIC_DRAW, 3);
        BRIDGE_MAP.put(params, mesh);
        return mesh;
    }

    private static MaterialInstancedMesh getBridge(float h1, float h2, int i0, Material m0, Material m1, Material m2, boolean blend) {
        final ObjArrayWrapper params = new ObjArrayWrapper(false, blend, h1, h2, i0, m0.id, m1.id, m2);

        if (BRIDGE_MAP.containsKey(params)) return BRIDGE_MAP.get(params);

        innerCorner(i0 + 1, TEMP);
        float xi0 = TEMP.x;
        float yi0 = TEMP.y;

        Vector2f c = new Vector2f(xi0, yi0);
        c.mul(1 + oW * 0.5f);

        Vector2f r = new Vector2f(c.y, -c.x);
        r.mul(1 / r.length() * oH * 0.25f);
        c.add(r, r);

        Vector2f l = new Vector2f(-c.y, c.x);
        l.mul(1 / l.length() * oH * 0.25f);
        c.add(l, l);

        BUILDER.clear();

        if (blend) {
            BUILDER.addTriangle(
                    xi0, yi0, 0, m0.ambientR, m0.ambientG, m0.ambientB,
                    r.x, r.y, h2, m2.ambientR, m2.ambientG, m2.ambientB,
                    l.x, l.y, h1, m1.ambientR, m1.ambientG, m1.ambientB
            );
        } else {
            float m0Rx = (xi0 + r.x) * 0.5f;
            float m0Ry = (yi0 + r.y) * 0.5f;
            float m0Rz = h2 * 0.5f;

            float m0Lx = (xi0 + l.x) * 0.5f;
            float m0Ly = (yi0 + l.y) * 0.5f;
            float m0Lz = h1 * 0.5f;

            float mLRx = (r.x + l.x) * 0.5f;
            float mLRy = (r.y + l.y) * 0.5f;
            float mLRz = (h1 + h2) * 0.5f;

            float cX = (xi0 + r.x + l.x) / 3;
            float cY = (yi0 + r.y + l.y) / 3;
            float cZ = (h1 + h2) / 3;

            BUILDER.addQuad(
                    xi0, yi0, 0, m0.ambientR, m0.ambientG, m0.ambientB,
                    m0Rx, m0Ry, m0Rz, m0.ambientR, m0.ambientG, m0.ambientB,
                    cX, cY, cZ, m0.ambientR, m0.ambientG, m0.ambientB,
                    m0Lx, m0Ly, m0Lz, m0.ambientR, m0.ambientG, m0.ambientB
            );

            BUILDER.addQuad(
                    l.x, l.y, h1, m1.ambientR, m1.ambientG, m1.ambientB,
                    m0Lx, m0Ly, m0Lz, m1.ambientR, m1.ambientG, m1.ambientB,
                    cX, cY, cZ, m1.ambientR, m1.ambientG, m1.ambientB,
                    mLRx, mLRy, mLRz, m1.ambientR, m1.ambientG, m1.ambientB
            );

            BUILDER.addQuad(
                    r.x, r.y, h2, m2.ambientR, m2.ambientG, m2.ambientB,
                    mLRx, mLRy, mLRz, m2.ambientR, m2.ambientG, m2.ambientB,
                    cX, cY, cZ, m2.ambientR, m2.ambientG, m2.ambientB,
                    m0Rx, m0Ry, m0Rz, m2.ambientR, m2.ambientG, m2.ambientB
            );
        }

        BUILDER.setMaterial(Materials.getBridge(
                new Vector4f(),
                new Vector4f(
                        m0.diffuseR + m1.diffuseR + m2.diffuseR,
                        m0.diffuseG + m1.diffuseG + m2.diffuseG,
                        m0.diffuseB + m1.diffuseB + m2.diffuseB,
                        m0.diffuseA + m1.diffuseA + m2.diffuseA).mul(0.3333f),

                new Vector4f(
                        m0.specularR + m1.specularR + m2.specularR,
                        m0.specularG + m1.specularG + m2.specularG,
                        m0.specularB + m1.specularB + m2.specularB,
                        m0.specularA + m1.specularA + m2.specularA).mul(0.3333f),

                (m0.reflectance + m1.reflectance + m2.reflectance) * 0.3333f
        ));

        BUILDER.setInstances(100);

        MaterialInstancedMesh mesh = (MaterialInstancedMesh) BUILDER.bake(GL_STATIC_DRAW, 3);
        BRIDGE_MAP.put(params, mesh);
        return mesh;
    }

    private static void innerCorner(int i, Vector2f result) {
        switch (i) {
            case 0: result.set(-iW, 0); break;
            case 1: result.set(-iW * 0.5f, iH); break;
            case 2: result.set(iW * 0.5f, iH); break;
            case 3: result.set(iW, 0); break;
            case 4: result.set(iW * 0.5f, -iH); break;
            case 5: result.set(-iW * 0.5f, -iH); break;
        }
    }
    */
}
