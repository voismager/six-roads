package engine.graphics.lib;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

public class MeshBuilding {
    private static FloatList positions = new FloatArrayList();
    private static FloatList color = new FloatArrayList();
    private static IntList indices = new IntArrayList();
    private static int uniquePoints = 0;

    public static void start() {
        positions.clear();
        color.clear();
        indices.clear();
        uniquePoints = 0;
    }

    public static float[] getPositions() {
        return positions.toFloatArray();
    }
    
    public static float[] getColour() {
        return color.toFloatArray();
    }
    
    public static int[] getIndices() {
        return indices.toIntArray();
    }

    public static void setColor(float r, float g, float b, float a) {
        for (int i = 0; i < color.size(); i+=4) {
            color.set(i, r);
            color.set(i + 1, g);
            color.set(i + 2, b);
            color.set(i + 3, a);
        }
    }

    public static void setColor(int i, float r, float g) {
        color.set(i * 2, r);
        color.set(i * 2 + 1, g);
    }

    public static void setColor(int i, float r, float g, float b) {
        color.set(i * 3, r);
        color.set(i * 3 + 1, g);
        color.set(i * 3 + 2, b);
    }

    public static void setColor(int i, float r, float g, float b, float a) {
        color.set(i * 4, r);
        color.set(i * 4 + 1, g);
        color.set(i * 4 + 2, b);
        color.set(i * 4 + 3, a);
    }

    public static void addTriangle(
            float p0X, float p0Y, float p0Z, float c0X, float c0Y,
            float p1X, float p1Y, float p1Z, float c1X, float c1Y,
            float p2X, float p2Y, float p2Z, float c2X, float c2Y
    ) {
        addTriangle(
                addPoint(p0X, p0Y, p0Z, c0X, c0Y),
                addPoint(p1X, p1Y, p1Z, c1X, c1Y),
                addPoint(p2X, p2Y, p2Z, c2X, c2Y)
        );
    }

    public static void addTriangle(
            float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z,
            float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z,
            float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z
    ) {
        addTriangle(
                addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z),
                addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z),
                addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z)
        );
    }

    public static void addTriangle(
            float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z, float c0W,
            float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z, float c1W,
            float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z, float c2W
    ) {
        addTriangle(
                addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z, c0W),
                addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z, c1W),
                addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z, c2W)
        );
    }

    public static void addQuad(
            float p0X, float p0Y, float p0Z, float c0X, float c0Y,
            float p1X, float p1Y, float p1Z, float c1X, float c1Y,
            float p2X, float p2Y, float p2Z, float c2X, float c2Y,
            float p3X, float p3Y, float p3Z, float c3X, float c3Y) {
        
        addQuad(
                addPoint(p0X, p0Y, p0Z, c0X, c0Y),
                addPoint(p1X, p1Y, p1Z, c1X, c1Y),
                addPoint(p2X, p2Y, p2Z, c2X, c2Y),
                addPoint(p3X, p3Y, p3Z, c3X, c3Y)
        );
    }

    public static void addQuad(
            float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z,
            float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z,
            float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z,
            float p3X, float p3Y, float p3Z, float c3X, float c3Y, float c3Z) {

        addQuad(
                addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z),
                addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z),
                addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z),
                addPoint(p3X, p3Y, p3Z, c3X, c3Y, c3Z)
        );
    }

    public static void addQuad(
            float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z, float c0W,
            float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z, float c1W,
            float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z, float c2W,
            float p3X, float p3Y, float p3Z, float c3X, float c3Y, float c3Z, float c3W) {
        
        addQuad(
                addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z, c0W),
                addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z, c1W),
                addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z, c2W),
                addPoint(p3X, p3Y, p3Z, c3X, c3Y, c3Z, c3W)
        );
    }

    public static void addQuad(int p0, int p1, int p2, int p3) {
        indices.add(p0);
        indices.add(p1);
        indices.add(p2);
        indices.add(p0);
        indices.add(p2);
        indices.add(p3);
    }

    public static void addTriangle(int p0, int p1, int p2) {
        indices.add(p0);
        indices.add(p1);
        indices.add(p2);
    }

    public static int addPoint(float x1, float y1, float c0, float c1) {
        positions.add(x1);
        positions.add(y1);
        color.add(c0);
        color.add(c1);
        return uniquePoints++;
    }

    public static int addPoint(float x1, float y1, float z1, float c0, float c1) {
        positions.add(x1);
        positions.add(y1);
        positions.add(z1);
        color.add(c0);
        color.add(c1);
        return uniquePoints++;
    }

    public static int addPoint(float x1, float y1, float z1, float c0, float c1, float c2) {
        positions.add(x1);
        positions.add(y1);
        positions.add(z1);
        color.add(c0);
        color.add(c1);
        color.add(c2);
        return uniquePoints++;
    }

    public static int addPoint(float x1, float y1, float z1, float c0, float c1, float c2, float c3) {
        positions.add(x1);
        positions.add(y1);
        positions.add(z1);
        color.add(c0);
        color.add(c1);
        color.add(c2);
        color.add(c3);
        return uniquePoints++;
    }

    public static Point2D start(float x1, float y1) {
        start();
        return POINT.point(x1, y1);
    }

    private static final Point2D POINT = new Point2D();

    public static class Point2D {
        public Point2D point(float x1, float y1) {
            positions.add(x1);
            positions.add(y1);
            uniquePoints++;
            return this;
        }

        public Point2D color(float c0, float c1) {
            color.add(c0);
            color.add(c1);
            return this;
        }

        public Point2D color(float c0, float c1, float c2) {
            color.add(c0);
            color.add(c1);
            color.add(c2);
            return this;
        }

        public Point2D color(float c0, float c1, float c2, float c3) {
            color.add(c0);
            color.add(c1);
            color.add(c2);
            color.add(c3);
            return this;
        }
    }
}
