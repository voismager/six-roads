package engine.graphics.lib;

import engine.graphics.meshes.*;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

public class MeshBuilder {
    public static ColorMeshBuilder getMaterialMeshBuilder() {
        return new ColorMeshBuilder();
    }

    public static SimpleMeshBuilder getSimpleMeshBuilder() {
        return new SimpleMeshBuilder();
    }

    public static class SimpleMeshBuilder {
        private FloatList positions = new FloatArrayList();
        private FloatList color = new FloatArrayList();
        private IntList indices = new IntArrayList();
        private int uniquePoints = 0;

        public void clear() {
            this.positions.clear();
            this.color.clear();
            this.indices.clear();
            uniquePoints = 0;
        }

        public SimpleMesh bake(int speed, int colourSize) {
            if (colourSize != 2 && colourSize != 3 && colourSize != 4) throw new IllegalArgumentException();

            float[] positions = this.positions.toFloatArray();
            float[] color = this.color.toFloatArray();
            int[] indices = this.indices.toIntArray();

            return new SimpleMesh(speed, positions, color, indices, colourSize);
        }

        public void setColor(float r, float g, float b, float a) {
            for (int i = 0; i < color.size(); i+=4) {
                color.set(i, r);
                color.set(i + 1, g);
                color.set(i + 2, b);
                color.set(i + 3, a);
            }
        }

        public void setColor(int i, float r, float g) {
            color.set(i * 2, r);
            color.set(i * 2 + 1, g);
        }

        public void setColor(int i, float r, float g, float b) {
            color.set(i * 3, r);
            color.set(i * 3 + 1, g);
            color.set(i * 3 + 2, b);
        }

        public void setColor(int i, float r, float g, float b, float a) {
            color.set(i * 4, r);
            color.set(i * 4 + 1, g);
            color.set(i * 4 + 2, b);
            color.set(i * 4 + 3, a);
        }

        public SimpleMeshBuilder addTriangle(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y
        ) {
            return addTriangle(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y)
            );
        }

        public SimpleMeshBuilder addTriangle(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z
        ) {
            return addTriangle(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z)
            );
        }

        public SimpleMeshBuilder addTriangle(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z, float c0W,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z, float c1W,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z, float c2W
        ) {
            return addTriangle(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z, c0W),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z, c1W),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z, c2W)
            );
        }

        public SimpleMeshBuilder addQuad(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y,
                float p3X, float p3Y, float p3Z, float c3X, float c3Y) {

            return addQuad(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y),
                    addPoint(p3X, p3Y, p3Z, c3X, c3Y)
            );
        }

        public SimpleMeshBuilder addQuad(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z,
                float p3X, float p3Y, float p3Z, float c3X, float c3Y, float c3Z) {

            return addQuad(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z),
                    addPoint(p3X, p3Y, p3Z, c3X, c3Y, c3Z)
            );
        }

        public SimpleMeshBuilder addQuad(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z, float c0W,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z, float c1W,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z, float c2W,
                float p3X, float p3Y, float p3Z, float c3X, float c3Y, float c3Z, float c3W) {

            return addQuad(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z, c0W),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z, c1W),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z, c2W),
                    addPoint(p3X, p3Y, p3Z, c3X, c3Y, c3Z, c3W)
            );
        }

        public SimpleMeshBuilder addQuad(int p0, int p1, int p2, int p3) {
            indices.add(p0);
            indices.add(p1);
            indices.add(p2);
            indices.add(p0);
            indices.add(p2);
            indices.add(p3);
            return this;
        }

        public SimpleMeshBuilder addTriangle(int p0, int p1, int p2) {
            indices.add(p0);
            indices.add(p1);
            indices.add(p2);
            return this;
        }

        public int addPoint(float x1, float y1, float z1, float c0, float c1) {
            positions.add(x1);
            positions.add(y1);
            positions.add(z1);
            color.add(c0);
            color.add(c1);
            return uniquePoints++;
        }

        public int addPoint(float x1, float y1, float z1, float c0, float c1, float c2) {
            positions.add(x1);
            positions.add(y1);
            positions.add(z1);
            color.add(c0);
            color.add(c1);
            color.add(c2);
            return uniquePoints++;
        }

        public int addPoint(float x1, float y1, float z1, float c0, float c1, float c2, float c3) {
            positions.add(x1);
            positions.add(y1);
            positions.add(z1);
            color.add(c0);
            color.add(c1);
            color.add(c2);
            color.add(c3);
            return uniquePoints++;
        }
    }

    public static class ColorMeshBuilder {
        private FloatList positions = new FloatArrayList();
        private FloatList color = new FloatArrayList();
        private FloatList normals = new FloatArrayList();
        private IntList indices = new IntArrayList();
        private int uniquePoints;
        private Material material;
        private int instances;

        private ColorMeshBuilder() {
            uniquePoints = 0;
            instances = 1;
        }

        public void clear() {
            this.positions.clear();
            this.color.clear();
            this.normals.clear();
            this.indices.clear();
            uniquePoints = 0;
            instances = 1;
            material = null;
        }

        public void setInstances(int instances) {
            this.instances = instances;
        }

        public void setMaterial(Material material) {
            this.material = material;
        }

        private float[] getNormals(float[] positions, int[] indices) {
            final Int2ObjectMap<List<Vector3f>> triangles = new Int2ObjectOpenHashMap<>();

            for (int i = 0; i < indices.length; i+=3) {
                int p0 = indices[i];
                int p1 = indices[i+1];
                int p2 = indices[i+2];

                float p0x = positions[p0*3];
                float p0y = positions[p0*3+1];
                float p0z = positions[p0*3+2];

                float p1x = positions[p1*3];
                float p1y = positions[p1*3+1];
                float p1z = positions[p1*3+2];

                float p2x = positions[p2*3];
                float p2y = positions[p2*3+1];
                float p2z = positions[p2*3+2];

                Vector3f p1v = new Vector3f(p1x - p0x, p1y - p0y, p1z - p0z);
                Vector3f p2v = new Vector3f(p2x - p0x, p2y - p0y, p2z - p0z);
                Vector3f normal = p1v.cross(p2v, new Vector3f()).normalize();

                triangles.computeIfAbsent(p0, v -> new ArrayList<>()).add(normal);
                triangles.computeIfAbsent(p1, v -> new ArrayList<>()).add(normal);
                triangles.computeIfAbsent(p2, v -> new ArrayList<>()).add(normal);
            }

            for (int v = 0; v < positions.length / 3; v++) {
                Vector3f normal = new Vector3f();

                for (Vector3f t : triangles.get(v)) {
                    normal.add(t.x, t.y, t.z);
                }

                normal.normalize();

                normals.add(normal.x);
                normals.add(normal.y);
                normals.add(normal.z);
            }

            return this.normals.toFloatArray();
        }

        public MaterialMesh bake(int speed, int colourSize) {
            float[] positions = this.positions.toFloatArray();
            float[] color = this.color.toFloatArray();
            int[] indices = this.indices.toIntArray();

            float[] normals = getNormals(positions, indices);

            if (instances == 1) {
                return new MaterialMesh(speed, positions, color, normals, indices, material);
            } else {
                return new MaterialInstancedMesh(speed, positions, color, normals, indices, material, DefaultInstanceDataInitializer.INSTANCE);
            }
        }

        public ColorMeshBuilder addTriangle(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z
        ) {
            return addTriangle(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z)
            );
        }

        public ColorMeshBuilder addQuad(
                float p0X, float p0Y, float p0Z, float c0X, float c0Y, float c0Z,
                float p1X, float p1Y, float p1Z, float c1X, float c1Y, float c1Z,
                float p2X, float p2Y, float p2Z, float c2X, float c2Y, float c2Z,
                float p3X, float p3Y, float p3Z, float c3X, float c3Y, float c3Z) {

            return addQuad(
                    addPoint(p0X, p0Y, p0Z, c0X, c0Y, c0Z),
                    addPoint(p1X, p1Y, p1Z, c1X, c1Y, c1Z),
                    addPoint(p2X, p2Y, p2Z, c2X, c2Y, c2Z),
                    addPoint(p3X, p3Y, p3Z, c3X, c3Y, c3Z)
            );
        }

        public ColorMeshBuilder addQuad(int p0, int p1, int p2, int p3) {
            indices.add(p0);
            indices.add(p1);
            indices.add(p2);
            indices.add(p0);
            indices.add(p2);
            indices.add(p3);
            return this;
        }

        public ColorMeshBuilder addTriangle(int p0, int p1, int p2) {
            indices.add(p0);
            indices.add(p1);
            indices.add(p2);
            return this;
        }

        public void setColor(int i, float r, float g, float b) {
            color.set(i * 3, r);
            color.set(i * 3 + 1, g);
            color.set(i * 3 + 2, b);
        }

        public int addPoint(float x1, float y1, float z1, float r1, float g1) {
            positions.add(x1);
            positions.add(y1);
            positions.add(z1);
            color.add(r1);
            color.add(g1);
            return uniquePoints++;
        }

        public int addPoint(float x1, float y1, float z1, float r1, float g1, float b1) {
            for (int i = 0; i < positions.size(); i+=3) {
                float x0 = positions.getFloat(i);
                float y0 = positions.getFloat(i+1);
                float z0 = positions.getFloat(i+2);

                float r0 = color.getFloat(i);
                float g0 = color.getFloat(i + 1);
                float b0 = color.getFloat(i + 2);

                if (equal(x0, y0, z0, x1, y1, z1) && equal(r0, g0, b0, r1, g1, b1)) {
                    return i / 3;
                }
            }

            positions.add(x1);
            positions.add(y1);
            positions.add(z1);
            color.add(r1);
            color.add(g1);
            color.add(b1);
            return uniquePoints++;
        }

        private boolean isWithMaterial() {
            return material != null;
        }
    }

    private static boolean equal(float x0, float y0, float z0, float x1, float y1, float z1) {
        return Math.sqrt( Math.pow((x0 - x1), 2) + Math.pow((y0 - y1), 2) + Math.pow((z0 - z1), 2) ) < 0.00001f;
    }

    private static boolean equal(float x0, float y0, float x1, float y1) {
        return Math.sqrt( Math.pow((x0 - x1), 2) + Math.pow((y0 - y1), 2) ) < 0.00001f;
    }
}
