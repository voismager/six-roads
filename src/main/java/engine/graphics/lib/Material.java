package engine.graphics.lib;

public class Material {
    public final int id;
    public final float ambientR;
    public final float ambientG;
    public final float ambientB;
    public final float ambientA;
    public final float diffuseR;
    public final float diffuseG;
    public final float diffuseB;
    public final float diffuseA;
    public final float specularR;
    public final float specularG;
    public final float specularB;
    public final float specularA;
    public final float reflectance;
    public final int texture;
    public final int mode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Material material = (Material) o;
        return Float.compare(material.ambientR, ambientR) == 0 &&
                Float.compare(material.ambientG, ambientG) == 0 &&
                Float.compare(material.ambientB, ambientB) == 0 &&
                Float.compare(material.ambientA, ambientA) == 0 &&
                Float.compare(material.diffuseR, diffuseR) == 0 &&
                Float.compare(material.diffuseG, diffuseG) == 0 &&
                Float.compare(material.diffuseB, diffuseB) == 0 &&
                Float.compare(material.diffuseA, diffuseA) == 0 &&
                Float.compare(material.specularR, specularR) == 0 &&
                Float.compare(material.specularG, specularG) == 0 &&
                Float.compare(material.specularB, specularB) == 0 &&
                Float.compare(material.specularA, specularA) == 0 &&
                Float.compare(material.reflectance, reflectance) == 0 &&
                texture == material.texture &&
                mode == material.mode;
    }

    Material(int id, float ambientR, float ambientG, float ambientB, float ambientA, float diffuseR, float diffuseG, float diffuseB, float diffuseA, float specularR, float specularG, float specularB, float specularA, float reflectance, int texture, int mode) {
        this.id = id;
        this.ambientR = ambientR;
        this.ambientG = ambientG;
        this.ambientB = ambientB;
        this.ambientA = ambientA;
        this.diffuseR = diffuseR;
        this.diffuseG = diffuseG;
        this.diffuseB = diffuseB;
        this.diffuseA = diffuseA;
        this.specularR = specularR;
        this.specularG = specularG;
        this.specularB = specularB;
        this.specularA = specularA;
        this.reflectance = reflectance;
        this.texture = texture;
        this.mode = mode;
    }

    public boolean hasTexture() {
        return texture != -1;
    }
}
