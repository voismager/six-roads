package engine.graphics.lib;

import engine.graphics.meshes.SimpleMesh;
import org.joml.Vector4f;

import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;

public class Units {
    public final static SimpleMesh UNIT_MESH;
    private final static SimpleMesh[] BORDER_MESHES;

    static {
        MeshBuilder.SimpleMeshBuilder builder = MeshBuilder.getSimpleMeshBuilder();

        float thickness = 0.8f;

        float uX0 = -0.56f; float uY0 = 0;
        float uX1 = -0.45f; float uY1 = 0.38f;
        float uX2 = 0f;     float uY2 = 0.56f;
        float uX3 = 0.45f;  float uY3 = 0.38f;
        float uX4 = 0.56f;  float uY4 = 0f;
        float uX5 = 0f;     float uY5 = -0.56f;

        builder.addPoint(uX0 * thickness, uY0 * thickness, 0, 0f, 0.5f);
        builder.addPoint(uX1 * thickness, uY1 * thickness, 0, 0.25f, 0.25f);
        builder.addPoint(uX2 * thickness, uY2 * thickness, 0, 0.5f, 0);
        builder.addPoint(uX3 * thickness, uY3 * thickness, 0, 0.75f, 0.25f);
        builder.addPoint(uX4 * thickness, uY4 * thickness, 0, 1f, 0.5f);
        builder.addPoint(uX5 * thickness, uY5 * thickness, 0, 0.5f, 1);

        builder.addQuad(0, 5, 2, 1).addQuad(2, 5, 4, 3);

        UNIT_MESH = builder.bake(GL_STATIC_DRAW, 2);

        builder.clear();

        builder.addPoint(uX0 * thickness, uY0 * thickness, 0, 0, 0, 0, 0);
        builder.addPoint(uX1 * thickness, uY1 * thickness, 0, 0, 0, 0, 0);
        builder.addPoint(uX2 * thickness, uY2 * thickness, 0, 0, 0, 0, 0);
        builder.addPoint(uX3 * thickness, uY3 * thickness, 0, 0, 0, 0, 0);
        builder.addPoint(uX4 * thickness, uY4 * thickness, 0, 0, 0, 0, 0);
        builder.addPoint(uX5 * thickness, uY5 * thickness, 0, 0, 0, 0, 0);

        builder.addPoint(uX0, uY0, 0, 0, 0, 0, 0);
        builder.addPoint(uX1, uY1, 0, 0, 0, 0, 0);
        builder.addPoint(uX2, uY2, 0, 0, 0, 0, 0);
        builder.addPoint(uX3, uY3, 0, 0, 0, 0, 0);
        builder.addPoint(uX4, uY4, 0, 0, 0, 0, 0);
        builder.addPoint(uX5, uY5, 0, 0, 0, 0, 0);

        builder.addQuad(6, 11, 5, 0);
        builder.addQuad(11, 10, 4, 5);
        builder.addQuad(10, 9, 3, 4);
        builder.addQuad(9, 8, 2, 3);
        builder.addQuad(8, 7, 1, 2);
        builder.addQuad(7, 6, 0, 1);

        Vector4f[] BORDER_MATERIALS = new Vector4f[] {
                new Vector4f(1, 0, 0, 1),
                new Vector4f(0, 1, 0, 1),
                new Vector4f(0, 0, 1, 1),
                new Vector4f(0, 0, 0, 1)
        };

        BORDER_MESHES = new SimpleMesh[BORDER_MATERIALS.length];

        for (int i = 0; i < BORDER_MATERIALS.length; i++) {
            Vector4f m = BORDER_MATERIALS[i];
            builder.setColor(m.x, m.y, m.z, m.w);
            BORDER_MESHES[i] = builder.bake(GL_STATIC_DRAW, 4);
        }

        builder.clear();
    }

    public static SimpleMesh getUnitBorderMesh(int puppeteer) {
        if (puppeteer != -1) {
            return BORDER_MESHES[puppeteer];
        } else {
            return BORDER_MESHES[BORDER_MESHES.length - 1];
        }
    }
}
