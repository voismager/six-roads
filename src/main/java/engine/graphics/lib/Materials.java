package engine.graphics.lib;

import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;

public class Materials {
    private static final List<Material> MATERIALS = new ArrayList<>();

    public static Material get(int id) {
        return MATERIALS.get(id);
    }

    public static Material getBridge(Vector4f ambient, Vector4f diffuse, Vector4f specular, float reflectance) {
        return getBridge(
                ambient.x, ambient.y, ambient.z, ambient.w,
                diffuse.x, diffuse.y, diffuse.z, diffuse.w,
                specular.x, specular.y, specular.z, specular.w,
                reflectance
        );
    }

    public static Material getBridge(float ambientR, float ambientG, float ambientB, float ambientA, float diffuseR, float diffuseG, float diffuseB, float diffuseA, float specularR, float specularG, float specularB, float specularA, float reflectance) {
        return newOrExisting(
                ambientR, ambientG, ambientB, ambientA,
                diffuseR, diffuseG, diffuseB, diffuseA,
                specularR, specularG, specularB, specularA,
                reflectance, -1, 2
        );
    }

    public static Material getUntextured(Vector4f ambient, Vector4f diffuse, Vector4f specular, float reflectance) {
        return getUntextured(
                ambient.x, ambient.y, ambient.z, ambient.w,
                diffuse.x, diffuse.y, diffuse.z, diffuse.w,
                specular.x, specular.y, specular.z, specular.w,
                reflectance
        );
    }

    public static Material getUntextured(float ambientR, float ambientG, float ambientB, float ambientA, float diffuseR, float diffuseG, float diffuseB, float diffuseA, float specularR, float specularG, float specularB, float specularA, float reflectance) {
        return newOrExisting(
                ambientR, ambientG, ambientB, ambientA,
                diffuseR, diffuseG, diffuseB, diffuseA,
                specularR, specularG, specularB, specularA,
                reflectance, -1, 1
        );
    }

    public static Material getTextured(Vector4f ambient, float reflectance, int texture) {
        return getTextured(
                ambient.x, ambient.y, ambient.z, ambient.w,
                reflectance, texture
        );
    }

    public static Material getTextured(float ambientR, float ambientG, float ambientB, float ambientA, float reflectance, int texture) {
        return newOrExisting(
                ambientR, ambientG, ambientB, ambientA,
                ambientR, ambientG, ambientB, ambientA,
                ambientR, ambientG, ambientB, ambientA,
                reflectance, texture, 0
        );
    }

    public static Material get() {
        return newOrExisting(
                0.0f, 0.0f, 0.0f, 0.0f,
                1.0f, 1.0f, 1.0f, 1.0f,
                0.0f, 0.0f, 0.0f, 0.0f,
                0, -1, 1
        );
    }

    private static Material newOrExisting(float ambientR, float ambientG, float ambientB, float ambientA, float diffuseR, float diffuseG, float diffuseB, float diffuseA, float specularR, float specularG, float specularB, float specularA, float reflectance, int texture, int mode) {
        final Material newMaterial = new Material(
                MATERIALS.size(),
                ambientR, ambientG, ambientB, ambientA,
                diffuseR, diffuseG, diffuseB, diffuseA,
                specularR, specularG, specularB, specularA,
                reflectance, texture, mode
        );

        int index;
        if ((index = MATERIALS.indexOf(newMaterial)) != -1) {
            return MATERIALS.get(index);
        } else {
            MATERIALS.add(newMaterial);
            return newMaterial;
        }
    }
}
