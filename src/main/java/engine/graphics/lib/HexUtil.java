package engine.graphics.lib;

import engine.graphics.meshes.MaterialInstancedMesh;
import engine.graphics.meshes.SimpleMesh;
import engine.structures.FloatArrayWrapper;
import engine.structures.OpenDrawBuffer;
import org.joml.Vector3f;
import org.joml.Vector4f;
import visuals.billboards.Billboard;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;

public class HexUtil {
    private static float oH = (float)(Math.sqrt(3) / 2);
    private static float oW = 1.0f;
    private static float m = 0.75f;
    private static float iH = oH * m;
    private static float iW = oW * m;

    private final static Vector4f DIFFUSE = new Vector4f(0.4f, 0.4f, 0.4f, 1f);

    private static final OpenDrawBuffer<Billboard> BILLBOARDS = new OpenDrawBuffer<>(Billboard.class);

    private static Map<FloatArrayWrapper, MaterialInstancedMesh> terrainMeshes = new HashMap<>();

    public final static SimpleMesh TERRAIN_MESH;

    private static MeshBuilder.ColorMeshBuilder hexBuilder = MeshBuilder.getMaterialMeshBuilder();

    static {
        hexBuilder.clear();

        hexBuilder.addPoint(-iW, 0.0f, 0.0f, 0f, 0f, 0f);
        hexBuilder.addPoint(-iW * 0.5f, iH, 0.0f, 0f, 0f, 0f);
        hexBuilder.addPoint(iW * 0.5f, iH, 0.0f, 0f, 0f, 0f);
        hexBuilder.addPoint(iW, 0.0f, 0.0f, 0f, 0f, 0f);
        hexBuilder.addPoint(iW * 0.5f, -iH, 0.0f, 0f, 0f, 0f);
        hexBuilder.addPoint(-iW * 0.5f, -iH, 0.0f, 0f, 0f, 0f);
        hexBuilder.addPoint(0, 0, 0, 0f, 0f, 0f);

        hexBuilder
                .addTriangle(0, 6, 1)
                .addTriangle(1, 6, 2)
                .addTriangle(2, 6, 3)
                .addTriangle(3, 6, 4)
                .addTriangle(4, 6, 5)
                .addTriangle(6, 0, 5);

        MeshBuilder.SimpleMeshBuilder builder = MeshBuilder.getSimpleMeshBuilder();

        builder.addPoint(-iW, 0.0f, 0.0f, 0f,0.5f);
        builder.addPoint(-iW * 0.5f, iH, 0.0f, 0.25f, 0f);
        builder.addPoint(iW * 0.5f, iH, 0.0f,0.75f, 0f);
        builder.addPoint(iW, 0.0f, 0.0f, 1f, 0.5f);
        builder.addPoint(iW * 0.5f, -iH, 0.0f, 0.75f, 1f);
        builder.addPoint(-iW * 0.5f, -iH, 0.0f, 0.25f, 1f);
        builder.addPoint(0, 0, 0, 0.5f, 0.5f);

        builder.addTriangle(0, 6, 1).addTriangle(1, 6, 2).addTriangle(2, 6, 3)
                .addTriangle(3, 6, 4).addTriangle(4, 6, 5).addTriangle(6, 0, 5);

        TERRAIN_MESH = builder.bake(GL_STATIC_DRAW, 2);

        builder.clear();
    }

    public static MaterialInstancedMesh getTerrainMesh(float rC, float gC, float bC, float rS, float gS, float bS, float reflectance) {
        FloatArrayWrapper key = new FloatArrayWrapper(
                rC, gC, bC,
                rS, gS, bS,
                reflectance
        );

        if (terrainMeshes.containsKey(key)) {
            return terrainMeshes.get(key);
        }

        for (int i = 0; i < 7; i++) hexBuilder.setColor(i, rC, gC, bC);

        hexBuilder.setMaterial(Materials.getBridge(
                rC, gC, bC, 1.0f,
                DIFFUSE.x, DIFFUSE.y, DIFFUSE.z, DIFFUSE.w,
                rS, gS, bS, 1.0f,
                reflectance
        ));

        hexBuilder.setInstances(8);

        MaterialInstancedMesh mesh = (MaterialInstancedMesh) hexBuilder.bake(GL_STATIC_DRAW, 3);
        terrainMeshes.put(key, mesh);
        return mesh;
    }

    public static MaterialInstancedMesh getTerrainMesh(Vector3f color, Vector3f specular, float reflectance) {
        return getTerrainMesh(color.x, color.y, color.z, specular.x, specular.y, specular.z, reflectance);
    }

    public static OpenDrawBuffer<Billboard> getBillboards() {
        return BILLBOARDS;
    }

}
