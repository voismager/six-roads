package engine.graphics.shadows;

import engine.graphics.ArrTexture;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.*;

public class ShadowBuffer {
    public static final int LINEAR_LENGTH = (int) Math.pow(65, 2);

    private final int depthMapFBO;
    private final ArrTexture textures;

    public ShadowBuffer(int cascades) throws Exception {
        this.depthMapFBO = glGenFramebuffers();
        this.textures = new ArrTexture(cascades, LINEAR_LENGTH, LINEAR_LENGTH, GL_DEPTH_COMPONENT);

        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textures.getIds()[0], 0);

        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            throw new Exception("Could not create FrameBuffer");
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    public ArrTexture getDepthMapTexture() {
        return textures;
    }

    public int getDepthMapFBO() {
        return depthMapFBO;
    }

    public void bindTextures(int start) {
        for (int i = 0; i < textures.getIds().length; i++) {
            glActiveTexture(start + i);
            glBindTexture(GL_TEXTURE_2D, textures.getIds()[i]);
        }
    }

    public void cleanup() {
        glDeleteFramebuffers(depthMapFBO);
        textures.cleanup();
    }
}
