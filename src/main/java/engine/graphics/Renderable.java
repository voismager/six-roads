package engine.graphics;

public interface Renderable {
    float x();

    float y();

    float z();
}
