package engine.graphics.transformations;

public class PerspectiveCamera extends Camera {
    public float fieldOfView;

    public PerspectiveCamera(float fieldOfView, float viewportWidth, float viewportHeight) {
        this.fieldOfView = fieldOfView;
    }

    @Override
    public void update() {
        float aspect = viewportWidth / viewportHeight;
        projection.perspective(fieldOfView, aspect, Math.abs(near), Math.abs(far));
        view.lookAt(position, tmpVec.set(position).add(direction), up);
        frustumFilter.updateFrustum(projection, view);
    }
}
