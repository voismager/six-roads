package engine.graphics.transformations;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class OrthoCamera extends Camera {
    public float zoom = 1;

    public OrthoCamera(float viewportWidth, float viewportHeight) {
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
        this.near = 0;
        update();
    }

    @Override
    public void update() {
        projection.setOrtho(zoom * -viewportWidth / 2, zoom * (viewportWidth / 2), zoom * -(viewportHeight / 2), zoom
                * viewportHeight / 2, near, far);
        view.lookAt(position, tmpVec.set(position).add(direction), up);
        frustumFilter.updateFrustum(projection, view);
    }

    public void setOrtho(float viewportWidth, float viewportHeight) {
        up.set(0, 0, 1);
        direction.set(0, -1, 0);
        position.set(zoom * viewportWidth / 2.0f, zoom * viewportHeight / 2.0f, 0);
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
        update();
    }

    public void translate(float x, float y) {
        this.translate(x, y, 0);
    }

    public void translate(Vector2f vec) {
        this.translate(vec.x, vec.y, 0);
    }
}
