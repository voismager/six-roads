package engine.graphics.transformations;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public abstract class Camera {
    public final FrustumFilter frustumFilter = new FrustumFilter();
    public final Vector3f position = new Vector3f();
    public final Vector3f direction = new Vector3f(0, -1, 0);
    public final Vector3f up = new Vector3f(0, 0, 1);
    public final Matrix4f projection = new Matrix4f();
    public final Matrix4f view = new Matrix4f();
    public float near = 1;
    public float far = 100;
    public float viewportWidth;
    public float viewportHeight;
    protected final Vector3f tmpVec = new Vector3f();

    public abstract void update();

    public void translate(float x, float y, float z) {
        position.add(x, y, z);
    }

    public void translate(Vector3f vec) {
        position.add(vec);
    }

    public void normalizeUp() {
        tmpVec.set(direction).cross(up).normalize();
        up.set(tmpVec).cross(direction).normalize();
    }

    public void rotate (float angle, float axisX, float axisY, float axisZ) {
        direction.rotateAxis(angle, axisX, axisY, axisZ);
        up.rotateAxis(angle, axisX, axisY, axisZ);
    }
}
