package engine.graphics.transformations;

import org.joml.FrustumIntersection;
import org.joml.Matrix4f;

public class FrustumFilter {
    private final Matrix4f prjViewMatrix;

    private FrustumIntersection frustumInt;

    public FrustumFilter() {
        prjViewMatrix = new Matrix4f();
        frustumInt = new FrustumIntersection();
    }

    public void updateFrustum(Matrix4f projMatrix, Matrix4f viewMatrix) {
        prjViewMatrix.set(projMatrix);
        prjViewMatrix.mul(viewMatrix);
        frustumInt.set(prjViewMatrix);
    }

    public boolean insideFrustum(float minX, float minY, float minZ, float maxX, float maxY, float maxZ) {
        return frustumInt.testAab(minX, minY, minZ, maxX, maxY, maxZ);
    }

    public boolean insideFrustum(float x0, float y0, float z0, float boundingRadius) {
        return frustumInt.testSphere(x0, y0, z0, boundingRadius);
    }
}
