package engine.graphics.transformations;

import engine.gameobjects.GameObject;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

import static engine.Window.*;

public final class Transformations {
    private static final Matrix4f PERSPECTIVE_PROJECTION_MATRIX;
    private static final Matrix4f ISOMETRIC_PROJECTION_MATRIX;
    private static final Matrix4f VIEW_MATRIX;
    private static final Matrix4f MODEL_MATRIX;
    private static final Vector3f TEMP_VEC_3_0;

    static {
        PERSPECTIVE_PROJECTION_MATRIX = new Matrix4f();
        ISOMETRIC_PROJECTION_MATRIX = new Matrix4f();
        VIEW_MATRIX = new Matrix4f();
        MODEL_MATRIX = new Matrix4f();
        TEMP_VEC_3_0 = new Vector3f();
    }

    public static class Camera {
        private static final Vector3f POSITION;
        private static final Vector3f ROTATION;

        static {
            POSITION = new Vector3f();
            ROTATION = new Vector3f();
        }

        public static Vector3f getPosition() {
            return POSITION;
        }

        public static void setRotation(float x, float y, float z) {
            ROTATION.set(
                    (float)Math.toRadians(x),
                    (float)Math.toRadians(y),
                    (float)Math.toRadians(z)
            );
        }

        public static void setPosition(float x, float y, float z) {
            POSITION.set(x, y, z);
        }

        public static void translate(float x, float y, float z) {
            POSITION.x += x;
            POSITION.y += y;
            POSITION.z += z;
        }
    }

    public static Matrix4f updateViewMatrix() {
        VIEW_MATRIX.identity()
                .rotationXYZ(Camera.ROTATION.x, Camera.ROTATION.y, Camera.ROTATION.z)
                .translate(-Camera.POSITION.x, -Camera.POSITION.y, -Camera.POSITION.z);

        return VIEW_MATRIX;
    }

    public static Matrix4f getGenericViewMatrix(float pX, float pY, float pZ, float rdegX, float rdegY, float rdegZ, Matrix4f matrix) {
        matrix.rotationX((float)Math.toRadians(rdegX))
                .rotateY((float)Math.toRadians(rdegY))
                .rotateZ((float)Math.toRadians(rdegZ))
                .translate(-pX, -pY, -pZ);

        return matrix;
    }

    public static Matrix4f updatePerspectiveProjectionMatrix() {
        float aspectRatio = (float) WIDTH / HEIGHT;
        return PERSPECTIVE_PROJECTION_MATRIX.perspective(FOV, aspectRatio, z_NEAR, z_FAR);
    }

    public static Matrix4f getPerspectiveProjectionMatrix() {
        return PERSPECTIVE_PROJECTION_MATRIX;
    }

    public static Matrix4f updateIsometricProjectionMatrix(float left, float right, float bottom, float top, float near, float far) {
        return ISOMETRIC_PROJECTION_MATRIX.ortho(left, right, bottom, top, near, far);
    }

    public static Matrix4f getIsometricProjectionMatrix() {
        return ISOMETRIC_PROJECTION_MATRIX;
    }

    public static Matrix4f getViewMatrix() {
        return VIEW_MATRIX;
    }

    public static Vector3f getCameraRight() {
        return TEMP_VEC_3_0.set(VIEW_MATRIX.m00(), VIEW_MATRIX.m10(), VIEW_MATRIX.m20());
    }

    public static Vector3f getCameraUp() {
        return TEMP_VEC_3_0.set(VIEW_MATRIX.m01(), VIEW_MATRIX.m11(), VIEW_MATRIX.m21());
    }

    public static Matrix4f getModelMatrix(GameObject o) {
        MODEL_MATRIX.translation(o.x, o.y, o.z);
        return MODEL_MATRIX;
    }

    public static Matrix4f getModelMatrix(float x, float y, float z) {
        MODEL_MATRIX.translation(x, y, z);
        return MODEL_MATRIX;
    }

    public static Matrix4f getModelMatrix(float x, float y, float z, float rX, float rY, float rZ) {
        MODEL_MATRIX.translation(x, y, z)
                .rotateXYZ(rX, rY, rZ);

        return MODEL_MATRIX;
    }

    public static Matrix4f getModelMatrix(float x, float y, float z, float rZ) {
        MODEL_MATRIX.translation(x, y, z)
                .rotateZ(-rZ);

        return MODEL_MATRIX;
    }

    private Transformations() { }
}
