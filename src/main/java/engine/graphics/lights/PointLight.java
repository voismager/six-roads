package engine.graphics.lights;

import org.joml.Vector3f;

public class PointLight {
    public final Vector3f colour;
    public final Vector3f position;
    public float intensity;
    public float constant;
    public float linear;
    public float exponent;

    public PointLight(PointLight pointLight) {
        this(new Vector3f(pointLight.colour), new Vector3f(pointLight.position),
                pointLight.intensity, pointLight.constant, pointLight.linear, pointLight.exponent);
    }

    public PointLight(Vector3f colour, Vector3f position, float intensity, float constant, float linear, float exponent) {
        this.colour = colour;
        this.position = position;
        this.intensity = intensity;
        this.constant = constant;
        this.linear = linear;
        this.exponent = exponent;
    }
}
