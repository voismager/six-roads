package engine.graphics.lights;

import org.joml.Vector3f;

public class DirectionalLight {
    private final Vector3f colour;
    private final Vector3f direction;
    public float intensity;

    public DirectionalLight(Vector3f colour, Vector3f direction, float intensity) {
        this.colour = colour;
        this.direction = direction;
        this.intensity = intensity;
    }

    public DirectionalLight() {
        this.colour = new Vector3f();
        this.direction = new Vector3f();
    }

    public DirectionalLight(DirectionalLight light) {
        this.colour = new Vector3f(light.colour);
        this.direction = new Vector3f(light.direction);
        this.intensity = light.intensity;
    }

    public void set(DirectionalLight light) {
        colour.set(light.getColour());
        direction.set(light.getDirection());
        intensity = light.intensity;
    }

    public Vector3f getColour() {
        return colour;
    }

    public Vector3f getDirection() {
        return direction;
    }

    public void setColour(float x, float y, float z) {
        this.colour.set(x, y, z);
    }

    public void setDirection(float x, float y, float z) {
        this.direction.set(x, y, z);
    }
}
