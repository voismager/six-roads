package engine.graphics;

import client.Settings;
import org.joml.*;

import java.lang.Math;

import static engine.Window.*;

@Deprecated
public class Transformation {
    public static Camera camera;

    private static Matrix4f projectionMatrix;
    private static Matrix4f viewMatrix;
    private static Matrix4f output0;

    private static Matrix4f invProjectionMatrix;
    private static Matrix4f invViewMatrix;
    private static Vector4f tmpVec4;
    private static Vector3f tmpVec3;

    public static Vector2f leftUpperVisibleCorner;
    public static Vector2f leftLowerVisibleCorner;
    public static Vector2f rightUpperVisibleCorner;
    public static Vector2f rightLowerVisibleCorner;
    public static Vector2f mouseCameraPosition;

    public static void init() {
        camera = new Camera();
        camera.position.set(0, 0, 10);
        camera.setRotation(-Settings.CAMERA_ANGLE,0.0f, 0.0f);

        float aspectRatio = (float) Settings.WINDOW_WIDTH / (float) Settings.WINDOW_HEIGHT;

        projectionMatrix = new Matrix4f().perspective(FOV, aspectRatio, z_NEAR, z_FAR);
        viewMatrix = new Matrix4f();
        output0 = new Matrix4f();

        invProjectionMatrix = new Matrix4f();
        invViewMatrix = new Matrix4f();
        tmpVec4 = new Vector4f();
        tmpVec3 = new Vector3f();

        leftUpperVisibleCorner = new Vector2f();
        leftLowerVisibleCorner = new Vector2f();
        rightUpperVisibleCorner = new Vector2f();
        rightLowerVisibleCorner = new Vector2f();
        mouseCameraPosition = new Vector2f();
    }

    public static Matrix4f updateViewMatrix() {
        viewMatrix.rotationX(camera.rotation.x)
                .rotateY(camera.rotation.y)
                .rotateZ(camera.rotation.z);

        viewMatrix.translate(-camera.position.x, -camera.position.y, -camera.position.z);

        return viewMatrix;
    }

    public static Matrix4f updateGenericViewMatrix(float pX, float pY, float pZ, float rdegX, float rdegY, float rdegZ, Matrix4f matrix) {
        matrix.rotationX((float)Math.toRadians(rdegX))
                .rotateY((float)Math.toRadians(rdegY))
                .rotateZ((float)Math.toRadians(rdegZ))
                .translate(-pX, -pY, -pZ);

        return matrix;
    }

    public static Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public static Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    public static Vector3f getCameraRight() {
        return tmpVec3.set(viewMatrix.m00(), viewMatrix.m10(), viewMatrix.m20());
    }

    public static Vector3f getCameraUp() {
        return tmpVec3.set(viewMatrix.m01(), viewMatrix.m11(), viewMatrix.m21());
    }

    public static Matrix4f getModelViewMatrix() {
        return output0;
    }

    public static Matrix4f updateModelViewMatrix(float x, float y, float z, Matrix4f view) {
        output0.identity()
                .m30(x)
                .m31(y)
                .m32(z);

        return view.mul(output0, output0);
    }

    public static Matrix4f updateModelMatrix(float x, float y, float z) {
        output0.translation(x, y, z);
        return output0;
    }

    public static Matrix4f updateModelMatrix(float x, float y, float z, float rZ) {
        output0.translation(x, y, z)
                .rotateZ(rZ);

        return output0;
    }

    public static Matrix4f updateModelViewMatrix(float x, float y, float z) {
        output0.identity()
                .m30(x)
                .m31(y)
                .m32(z);

        return viewMatrix.mul(output0, output0);
    }

    public static void moveCamera(float x, float y) {
        camera.position.set(x, y, camera.position.z);
    }

    public static void moveCamera(float x, float y, float z) {
        camera.position.set(x, y, z);
    }

    public static void update(final double mX, final double mY) {
        invProjectionMatrix.set(projectionMatrix);
        invProjectionMatrix.invert();
        invViewMatrix.set(viewMatrix);
        invViewMatrix.invert();

        float t;

        float xGen = (float)(2 * mX) / (float) Settings.WINDOW_WIDTH - 1.0f;
        float yGen = 1.0f - (float)(2 * mY) / (float) Settings.WINDOW_HEIGHT;

        tmpVec4.set(xGen, yGen, -1.0f, 1.0f);
        tmpVec4.mul(invProjectionMatrix);
        tmpVec4.z = -1.0f;
        tmpVec4.w = 0.0f;
        tmpVec4.mul(invViewMatrix);

        t = Intersectionf.intersectRayPlane(
                camera.position.x, camera.position.y, camera.position.z,
                tmpVec4.x, tmpVec4.y, tmpVec4.z,
                0, 0, 1, 0f,
                0.0f
        );

        float x = t * tmpVec4.x;
        float y = t * tmpVec4.y;

        camera.globalMousePosition.set(x, y);

        mouseCameraPosition.set(camera.position.x + x, camera.position.y + y);
    }
}
