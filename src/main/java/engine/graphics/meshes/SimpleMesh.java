package engine.graphics.meshes;

import engine.MeshUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

public class SimpleMesh extends Mesh {
    protected final int positionVbo;
    protected final int textureVbo;
    protected final int indexVbo;

    protected int positionCount;
    protected int textureCount;
    protected int indexCount;

    public SimpleMesh(int speed, int maxVertices, int colourSize) {
        this.positionCount = 0;
        this.textureCount = 0;
        this.indexCount = 0;

        glBindVertexArray(vao);
        this.positionVbo = MeshUtils.genFloatBuffer(0, maxVertices, 3, speed);
        this.textureVbo = MeshUtils.genFloatBuffer(1, maxVertices, colourSize, speed);
        this.indexVbo = MeshUtils.genIndexBuffer(maxVertices, speed);
        glBindVertexArray(0);
    }

    public SimpleMesh(int speed, float[] positions, float[] texture, int[] indices, int positionSize, int colourSize) {
        this.positionCount = positions.length;
        this.textureCount = texture.length;
        this.indexCount = indices.length;

        glBindVertexArray(vao);
        this.positionVbo = MeshUtils.genFloatBuffer(0, positions, positionSize, speed);
        this.textureVbo = MeshUtils.genFloatBuffer(1, texture, colourSize, speed);
        this.indexVbo = MeshUtils.genIndexBuffer(indices, speed);
        glBindVertexArray(0);
    }

    public SimpleMesh(int speed, float[] positions, float[] texture, int[] indices, int colourSize) {
        this(speed, positions, texture, indices, 3, colourSize);
    }

    public void add(float[] pos, float[] col, int[] ind) {
        glBindVertexArray(vao);

        glBindBuffer(GL_ARRAY_BUFFER, positionVbo);
        glBufferSubData(GL_ARRAY_BUFFER, positionCount * Float.BYTES, pos);
        positionCount += pos.length;
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ARRAY_BUFFER, textureVbo);
        glBufferSubData(GL_ARRAY_BUFFER, textureCount * Float.BYTES, col);
        textureCount += col.length;
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVbo);
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, indexCount * Integer.BYTES, ind);
        indexCount += ind.length;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        glBindVertexArray(0);
    }

    public int getTextureVbo() {
        return textureVbo;
    }

    public int getPositionVbo() {
        return positionVbo;
    }

    public int getIndexVbo() {
        return indexVbo;
    }

    public int getTextureCount() {
        return textureCount;
    }

    public int getPositionCount() {
        return positionCount;
    }

    public int getIndexCount() {
        return indexCount;
    }

    @Override
    public void initRender() {
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
    }

    @Override
    public void render() {
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }

    @Override
    public void render(FloatBuffer buffer, int size) {

    }

    @Override
    public void endRender() {
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glBindVertexArray(0);
    }

    @Override
    public void clear() {
        glDisableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(positionVbo);
        glDeleteBuffers(textureVbo);
        glDeleteBuffers(indexVbo);

        glBindVertexArray(0);
        glDeleteVertexArrays(vao);
    }
}
