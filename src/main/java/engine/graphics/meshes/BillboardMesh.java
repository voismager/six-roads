package engine.graphics.meshes;

import engine.MeshUtils;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

public class BillboardMesh extends Mesh {
    public final static Builder BUILDER = new Builder();

    protected final int positionVbo;
    protected final int textureVbo;
    protected final int filterVbo;
    protected final int indexVbo;

    protected int positionCount;
    protected int textureCount;
    protected int indexCount;

    public BillboardMesh(int speed, float[] positions, float[] filter, float[] texture, int[] indices, int colourSize) {
        this.positionCount = positions.length;
        this.textureCount = texture.length;
        this.indexCount = indices.length;

        glBindVertexArray(vao);
        this.positionVbo = MeshUtils.genFloatBuffer(0, positions, 2, speed);
        this.textureVbo = MeshUtils.genFloatBuffer(1, texture, colourSize, speed);
        this.filterVbo = MeshUtils.genFloatBuffer(2, filter, 1, speed);
        this.indexVbo = MeshUtils.genIndexBuffer(indices, speed);
        glBindVertexArray(0);
    }

    public void updatePositions(int offset, float[] data) {
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, positionVbo);
        glBufferSubData(GL_ARRAY_BUFFER, offset, data);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    @Override
    public void initRender() {
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
    }

    @Override
    public void render() {
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }

    @Override
    public void render(FloatBuffer buffer, int size) {

    }

    @Override
    public void endRender() {
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glBindVertexArray(0);
    }

    @Override
    public void clear() {
        glDisableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(positionVbo);
        glDeleteBuffers(filterVbo);
        glDeleteBuffers(textureVbo);
        glDeleteBuffers(indexVbo);
        glBindVertexArray(0);
        glDeleteVertexArrays(vao);
    }

    public static class Builder {
        private final static FloatList POSITIONS = new FloatArrayList();
        private final static FloatList COLOR = new FloatArrayList();
        private final static FloatList FILTER = new FloatArrayList();
        private final static IntList INDICES = new IntArrayList();

        public BillboardMesh build(int speed, int colourSize) {
            final BillboardMesh mesh = new BillboardMesh(
                    speed, POSITIONS.toFloatArray(), FILTER.toFloatArray(),
                    COLOR.toFloatArray(), INDICES.toIntArray(), colourSize
            );

            POSITIONS.clear();
            COLOR.clear();
            FILTER.clear();
            INDICES.clear();
            return mesh;
        }

        public Builder quad(int p0, int p1, int p2, int p3) {
            INDICES.add(p0);
            INDICES.add(p1);
            INDICES.add(p2);
            INDICES.add(p0);
            INDICES.add(p2);
            INDICES.add(p3);
            return this;
        }

        public Builder color(float c0, float c1) {
            COLOR.add(c0);
            COLOR.add(c1);
            return this;
        }

        public Builder color(float c0, float c1, float c2) {
            COLOR.add(c0);
            COLOR.add(c1);
            COLOR.add(c2);
            return this;
        }

        public Builder color(float c0, float c1, float c2, float c3) {
            COLOR.add(c0);
            COLOR.add(c1);
            COLOR.add(c2);
            COLOR.add(c3);
            return this;
        }

        public Builder point(float x, float y, float f0) {
            POSITIONS.add(x);
            POSITIONS.add(y);
            FILTER.add(f0);
            return this;
        }

        private Builder() { }
    }
}
