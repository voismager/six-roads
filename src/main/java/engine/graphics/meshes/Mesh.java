package engine.graphics.meshes;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public abstract class Mesh {
    protected final int vao;

    public Mesh() {
        this.vao = glGenVertexArrays();
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mesh mesh = (Mesh) o;
        return vao == mesh.vao;
    }

    @Override
    public final int hashCode() {
        return vao;
    }

    public final int getVao() {
        return vao;
    }

    public abstract void initRender();

    public abstract void render();

    public abstract void render(FloatBuffer buffer, int size);

    public abstract void endRender();

    public abstract void clear();
}
