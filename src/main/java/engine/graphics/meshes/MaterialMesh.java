package engine.graphics.meshes;

import engine.MeshUtils;
import engine.graphics.lib.Material;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;

public class MaterialMesh extends Mesh {
    protected final Material material;

    protected final int positionVbo;
    protected final int textureVbo;
    protected final int indexVbo;
    protected final int normalVbo;

    protected int positionCount;
    protected int textureCount;
    protected int indexCount;
    protected int normalCount;

    public MaterialMesh(int speed, float[] positions, float[] texture, float[] normals, int[] indices, Material material) {
        this.positionCount = positions.length;
        this.textureCount = texture.length;
        this.indexCount = indices.length;
        this.normalCount = normals.length;
        this.material = material;

        glBindVertexArray(vao);
        this.positionVbo = MeshUtils.genFloatBuffer(0, positions, 3, speed);
        this.textureVbo = MeshUtils.genFloatBuffer(1, texture, material.hasTexture() ? 2 : 3, speed);
        this.normalVbo = MeshUtils.genFloatBuffer(2, normals, 3, speed);
        this.indexVbo = MeshUtils.genIndexBuffer(indices, speed);
        glBindVertexArray(0);
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public void initRender() {
        if (material != null && material.hasTexture()) {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, material.texture);
        }

        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);
    }

    @Override
    public void render() {
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }

    @Override
    public void render(FloatBuffer buffer, int size) {

    }

    @Override
    public void endRender() {
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(0);
        glBindVertexArray(0);

        glBindTexture(GL_TEXTURE_2D, 0);
    }

    @Override
    public void clear() {
        glDisableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(positionVbo);
        glDeleteBuffers(textureVbo);
        glDeleteBuffers(indexVbo);
        glDeleteBuffers(normalVbo);

        glBindVertexArray(0);
        glDeleteVertexArrays(vao);
    }
}
