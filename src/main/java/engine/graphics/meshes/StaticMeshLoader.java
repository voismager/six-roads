package engine.graphics.meshes;

import engine.graphics.core.TextureCache;
import engine.graphics.lib.Materials;
import engine.graphics.meshes.DefaultInstanceDataInitializer;
import engine.graphics.meshes.MaterialInstancedMesh;
import engine.graphics.lib.Material;
import engine.graphics.meshes.MaterialMesh;
import engine.support.loaders.UnifiedResource;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.*;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static engine.support.IOUtil.ioResourceToByteBuffer;
import static org.lwjgl.assimp.Assimp.*;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.system.MemoryUtil.*;

public class StaticMeshLoader {
    public static MaterialInstancedMesh[] load(UnifiedResource resource, String texturesDir, int instances) throws Exception {
        return (MaterialInstancedMesh[]) load(
                resource, texturesDir,
                aiProcess_JoinIdenticalVertices | aiProcess_Triangulate | aiProcess_FixInfacingNormals,
                instances
        );
    }

    public static MaterialMesh[] load(UnifiedResource resource, String texturesDir) throws Exception {
        return load(
                resource, texturesDir,
                aiProcess_JoinIdenticalVertices | aiProcess_Triangulate | aiProcess_FixInfacingNormals,
                1
        );
    }

    private static MaterialMesh[] load(UnifiedResource resource, String texturesDir, int flags, int instances) throws Exception {
        if (!resource.isResource()) throw new RuntimeException("Is not resource");

        AIFileIO fileIo = AIFileIO.create();
        AIFileOpenProcI fileOpenProc = new AIFileOpenProc() {
            public long invoke(long pFileIO, long fileName, long openMode) {
                AIFile aiFile = AIFile.create();
                final ByteBuffer data;
                String fileNameUtf8 = memUTF8(fileName);
                try {
                    data = ioResourceToByteBuffer(fileNameUtf8, 8192);
                } catch (IOException e) {
                    throw new RuntimeException("Could not open file: " + fileNameUtf8);
                }
                AIFileReadProcI fileReadProc = new AIFileReadProc() {
                    public long invoke(long pFile, long pBuffer, long size, long count) {
                        long max = Math.min(data.remaining(), size * count);
                        memCopy(memAddress(data) + data.position(), pBuffer, max);
                        return max;
                    }
                };
                AIFileSeekI fileSeekProc = new AIFileSeek() {
                    public int invoke(long pFile, long offset, int origin) {
                        if (origin == Assimp.aiOrigin_CUR) {
                            data.position(data.position() + (int) offset);
                        } else if (origin == Assimp.aiOrigin_SET) {
                            data.position((int) offset);
                        } else if (origin == Assimp.aiOrigin_END) {
                            data.position(data.limit() + (int) offset);
                        }
                        return 0;
                    }
                };
                AIFileTellProcI fileTellProc = new AIFileTellProc() {
                    public long invoke(long pFile) {
                        return data.limit();
                    }
                };
                aiFile.ReadProc(fileReadProc);
                aiFile.SeekProc(fileSeekProc);
                aiFile.FileSizeProc(fileTellProc);
                return aiFile.address();
            }
        };
        AIFileCloseProcI fileCloseProc = new AIFileCloseProc() {
            public void invoke(long pFileIO, long pFile) {
                /* Nothing to do */
            }
        };

        fileIo.set(fileOpenProc, fileCloseProc, NULL);
        AIScene aiScene = aiImportFileEx(resource.getPath().substring(1), flags, fileIo);

        if (aiScene == null) {
            throw new Exception("Error loading model");
        }

        int numMaterials = aiScene.mNumMaterials();
        PointerBuffer aiMaterials = aiScene.mMaterials();
        List<Material> materials = new ArrayList<>();
        for (int i = 0; i < numMaterials; i++) {
            AIMaterial aiMaterial = AIMaterial.create(aiMaterials.get(i));
            processMaterial(aiMaterial, materials, texturesDir);
        }

        int numMeshes = aiScene.mNumMeshes();
        PointerBuffer aiMeshes = aiScene.mMeshes();

        if (instances == 1) {
            MaterialMesh[] meshes = new MaterialMesh[numMeshes];

            for (int i = 0; i < numMeshes; i++) {
                AIMesh aiMesh = AIMesh.create(aiMeshes.get(i));
                MaterialMesh mesh = processMesh(aiMesh, materials, instances);
                meshes[i] = mesh;
            }

            return meshes;

        } else {
            MaterialInstancedMesh[] meshes = new MaterialInstancedMesh[numMeshes];

            for (int i = 0; i < numMeshes; i++) {
                AIMesh aiMesh = AIMesh.create(aiMeshes.get(i));
                MaterialInstancedMesh mesh = (MaterialInstancedMesh) processMesh(aiMesh, materials, instances);
                meshes[i] = mesh;
            }

            return meshes;
        }
    }

    private static void processMaterial(AIMaterial aiMaterial, List<Material> materials, String texturesDir) throws Exception {
        AIColor4D colour = AIColor4D.create();

        AIString path = AIString.calloc();
        Assimp.aiGetMaterialTexture(aiMaterial, aiTextureType_DIFFUSE, 0, path, (IntBuffer) null, null, null, null, null, null);
        String textPath = path.dataString();

        int texture = -1;

        if (textPath.length() > 0) {
            File file = new File(textPath);
            TextureCache textCache = TextureCache.getInstance();
            texture = textCache.getTexture(new UnifiedResource(true, texturesDir + file.getName()));
        }

        //Vector4f ambient = Material.DEFAULT_COLOUR;
        //int result = aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_AMBIENT, aiTextureType_NONE, 0, colour);
        //if (result == 0) {
        //    ambient = new Vector4f(colour.r(), colour.g(), colour.b(), colour.a());
        //}

        float diffuseR = 1, diffuseG = 1, diffuseB = 1, diffuseA = 1;
        float specularR = 1, specularG = 1, specularB = 1, specularA = 1;

        int result = aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_NONE, 0, colour);
        if (result == 0) {
            diffuseR = colour.r();
            diffuseG = colour.g();
            diffuseB = colour.b();
            diffuseA = colour.a();
        }

        result = aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_SPECULAR, aiTextureType_NONE, 0, colour);
        if (result == 0) {
            specularR = colour.r();
            specularG = colour.g();
            specularB = colour.b();
            specularA = colour.a();
        }

        if (texture == -1) {
            materials.add(Materials.getUntextured(
                    diffuseR, diffuseG, diffuseB, diffuseA,
                    diffuseR, diffuseG, diffuseB, diffuseA,
                    specularR, specularG, specularB, specularA,
                    0.0f
            ));
        } else {
            materials.add(Materials.getTextured(
                    diffuseR, diffuseG, diffuseB, diffuseA,
                    0.0f, texture
            ));
        }
    }

    private static MaterialMesh processMesh(AIMesh aiMesh, List<Material> materials, int instances) {
        FloatList vertices = new FloatArrayList();
        FloatList textures = new FloatArrayList();
        FloatList normals = new FloatArrayList();
        IntList indices = new IntArrayList();

        processVertices(aiMesh, vertices);
        processNormals(aiMesh, normals);
        processTextCoords(aiMesh, textures);
        processIndices(aiMesh, indices);

        Material material;
        int materialIdx = aiMesh.mMaterialIndex();
        if (materialIdx >= 0 && materialIdx < materials.size()) {
            material = materials.get(materialIdx);
        } else {
            material = Materials.get();
        }

        if (instances == 1) {
            return new MaterialMesh(
                    GL_STATIC_DRAW,
                    vertices.toFloatArray(),
                    textures.toFloatArray(),
                    normals.toFloatArray(),
                    indices.toIntArray(),
                    material
            );
        } else {
            return new MaterialInstancedMesh(
                    GL_STATIC_DRAW,
                    vertices.toFloatArray(),
                    textures.toFloatArray(),
                    normals.toFloatArray(),
                    indices.toIntArray(),
                    material,
                    DefaultInstanceDataInitializer.INSTANCE
            );
        }
    }

    private static void processVertices(AIMesh aiMesh, List<Float> vertices) {
        AIVector3D.Buffer aiVertices = aiMesh.mVertices();
        while (aiVertices.remaining() > 0) {
            AIVector3D aiVertex = aiVertices.get();
            vertices.add(aiVertex.x());
            vertices.add(aiVertex.y());
            vertices.add(aiVertex.z());
        }
    }

    private static void processNormals(AIMesh aiMesh, List<Float> normals) {
        AIVector3D.Buffer aiNormals = aiMesh.mNormals();
        while (aiNormals != null && aiNormals.remaining() > 0) {
            AIVector3D aiNormal = aiNormals.get();
            normals.add(aiNormal.x());
            normals.add(aiNormal.y());
            normals.add(aiNormal.z());
        }
    }

    private static void processTextCoords(AIMesh aiMesh, List<Float> textures) {
        AIVector3D.Buffer textCoords = aiMesh.mTextureCoords(0);
        int numTextCoords = textCoords != null ? textCoords.remaining() : 0;
        for (int i = 0; i < numTextCoords; i++) {
            AIVector3D textCoord = textCoords.get();
            textures.add(textCoord.x());
            textures.add(1 - textCoord.y());
        }
    }

    private static void processIndices(AIMesh aiMesh, List<Integer> indices) {
        int numFaces = aiMesh.mNumFaces();
        AIFace.Buffer aiFaces = aiMesh.mFaces();
        for (int i = 0; i < numFaces; i++) {
            AIFace aiFace = aiFaces.get(i);
            IntBuffer buffer = aiFace.mIndices();
            while (buffer.remaining() > 0) {
                indices.add(buffer.get());
            }
        }
    }
}
