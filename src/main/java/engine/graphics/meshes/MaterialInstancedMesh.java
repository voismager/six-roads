package engine.graphics.meshes;

import engine.graphics.lib.Material;

import java.nio.FloatBuffer;
import java.util.function.IntSupplier;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL31.glDrawElementsInstanced;

public class MaterialInstancedMesh extends MaterialMesh {
    private final int instanceDataVbo;
    private final int elements;

    public MaterialInstancedMesh(int speed, float[] positions, float[] texture, float[] normals, int[] indices, Material material, IntSupplier instanceInitializer) {
        super(speed, positions, texture, normals, indices, material);

        glBindVertexArray(vao);
        this.instanceDataVbo = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, instanceDataVbo);
        this.elements = instanceInitializer.getAsInt();
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    @Override
    public void initRender() {
        super.initRender();

        int start = 3;
        for (int i = 0; i < elements; i++) {
            glEnableVertexAttribArray(start + i);
        }
    }

    @Override
    public void endRender() {
        int start = 3;
        for (int i = 0; i < elements; i++) {
            glDisableVertexAttribArray(start + i);
        }

        super.endRender();
    }

    @Override
    public void render(FloatBuffer buffer, int size) {
        glBindBuffer(GL_ARRAY_BUFFER, instanceDataVbo);
        glBufferData(GL_ARRAY_BUFFER, buffer, GL_DYNAMIC_READ);
        glDrawElementsInstanced(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0, size);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    @Override
    public void clear() {
        glDisableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(positionVbo);
        glDeleteBuffers(textureVbo);
        glDeleteBuffers(indexVbo);
        glDeleteBuffers(normalVbo);
        glDeleteBuffers(instanceDataVbo);

        glBindVertexArray(0);
        glDeleteVertexArrays(vao);
    }
}
