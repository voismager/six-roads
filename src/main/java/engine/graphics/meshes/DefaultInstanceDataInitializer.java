package engine.graphics.meshes;

import org.lwjgl.system.MemoryUtil;

import java.nio.FloatBuffer;
import java.util.function.IntSupplier;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL33.glVertexAttribDivisor;

public class DefaultInstanceDataInitializer implements IntSupplier {
    public static final DefaultInstanceDataInitializer INSTANCE = new DefaultInstanceDataInitializer();

    public static final int VECTOR4F_SIZE_BYTES = 4 * Float.BYTES;
    public static final int MATRIX_SIZE_FLOATS = 4 * 4;
    public static final int INSTANCE_SIZE_FLOATS = MATRIX_SIZE_FLOATS;
    public static final int INSTANCE_SIZE_BYTES = INSTANCE_SIZE_FLOATS * Float.BYTES;

    public static final int INSTANCES = 120;
    public static final FloatBuffer INSTANCE_BUFFER = MemoryUtil.memAllocFloat(INSTANCES * INSTANCE_SIZE_FLOATS);

    @Override
    public int getAsInt() {
        int start = 3;
        int strideStart = 0;

        // Model Matrix
        for (int i = 0; i < 4; i++) {
            glVertexAttribPointer(start, 4, GL_FLOAT, false, INSTANCE_SIZE_BYTES, strideStart);
            glVertexAttribDivisor(start, 1);
            start++;
            strideStart += VECTOR4F_SIZE_BYTES;
        }

        return 4;
    }
}
