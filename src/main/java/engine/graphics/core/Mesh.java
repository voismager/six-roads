package engine.graphics.core;

import engine.Cleanable;
import engine.MeshUtils;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import org.lwjgl.assimp.AIFace;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AIVector3D;

import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL30.*;

public class Mesh {
    private final static int POSITION_VERTEX_ATTR = 0;
    private final static int TEXTURE_POSITION_VERTEX_ATTR = 1;
    private final static int NORMAL_VERTEX_ATTR = 2;

    private final int vao;

    private final int positionVbo;
    private final int textureVbo;
    private final int indexVbo;
    private final int normalVbo;

    private int positionCount;
    private int textureCount;
    private int indexCount;
    private int normalCount;

    public void initRender() {
        glBindVertexArray(vao);
        glEnableVertexAttribArray(POSITION_VERTEX_ATTR);
        glEnableVertexAttribArray(TEXTURE_POSITION_VERTEX_ATTR);
        glEnableVertexAttribArray(NORMAL_VERTEX_ATTR);
    }

    public void endRender() {
        glDisableVertexAttribArray(NORMAL_VERTEX_ATTR);
        glDisableVertexAttribArray(TEXTURE_POSITION_VERTEX_ATTR);
        glDisableVertexAttribArray(POSITION_VERTEX_ATTR);
        glBindVertexArray(0);
    }

    public void render() {
        glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
    }

    public Mesh(float[] vertices, float[] textureCoords, float[] normals, int[] indices) {
        this.vao = glGenVertexArrays();

        glBindVertexArray(vao);
        this.positionCount = vertices.length;
        this.textureCount = textureCoords.length;
        this.normalCount = normals.length;
        this.indexCount = indices.length;
        this.positionVbo = MeshUtils.genFloatBuffer(POSITION_VERTEX_ATTR, vertices, 3, GL_STATIC_DRAW);
        this.textureVbo = MeshUtils.genFloatBuffer(TEXTURE_POSITION_VERTEX_ATTR, textureCoords, 2, GL_STATIC_DRAW);
        this.normalVbo = MeshUtils.genFloatBuffer(NORMAL_VERTEX_ATTR, normals, 3, GL_STATIC_DRAW);
        this.indexVbo = MeshUtils.genIndexBuffer(indices, GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    public Mesh(AIMesh aiMesh) {
        this.vao = glGenVertexArrays();

        glBindVertexArray(vao);
        final FloatList vertices = new FloatArrayList();
        final FloatList textureCoords = new FloatArrayList();
        final FloatList normals = new FloatArrayList();
        final IntList indices = new IntArrayList();

        final AIVector3D.Buffer aiVertices = aiMesh.mVertices();
        while (aiVertices.remaining() > 0) {
            AIVector3D aiVertex = aiVertices.get();
            vertices.add(aiVertex.x()); vertices.add(aiVertex.y()); vertices.add(aiVertex.z());
        }

        final AIVector3D.Buffer aiTextureCoords = aiMesh.mTextureCoords(0);
        int numTextCoords = aiTextureCoords != null ? aiTextureCoords.remaining() : 0;
        for (int i = 0; i < numTextCoords; i++) {
            AIVector3D textCoord = aiTextureCoords.get();
            textureCoords.add(textCoord.x()); textureCoords.add(1 - textCoord.y());
        }

        final AIVector3D.Buffer aiNormals = aiMesh.mNormals();
        while (aiNormals != null && aiNormals.remaining() > 0) {
            AIVector3D aiNormal = aiNormals.get();
            normals.add(aiNormal.x()); normals.add(aiNormal.y()); normals.add(aiNormal.z());
        }

        final int numFaces = aiMesh.mNumFaces();
        final AIFace.Buffer aiFaces = aiMesh.mFaces();
        for (int i = 0; i < numFaces; i++) {
            IntBuffer buffer = aiFaces.get(i).mIndices();
            while (buffer.remaining() > 0) indices.add(buffer.get());
        }

        this.positionCount = vertices.size();
        this.textureCount = textureCoords.size();
        this.normalCount = normals.size();
        this.indexCount = indices.size();
        this.positionVbo = MeshUtils.genFloatBuffer(POSITION_VERTEX_ATTR, vertices.toFloatArray(), 3, GL_STATIC_DRAW);
        this.textureVbo = MeshUtils.genFloatBuffer(TEXTURE_POSITION_VERTEX_ATTR, textureCoords.toFloatArray(), 2, GL_STATIC_DRAW);
        this.normalVbo = MeshUtils.genFloatBuffer(NORMAL_VERTEX_ATTR, normals.toFloatArray(), 3, GL_STATIC_DRAW);
        this.indexVbo = MeshUtils.genIndexBuffer(indices.toIntArray(), GL_STATIC_DRAW);

        glBindVertexArray(0);
    }
}
