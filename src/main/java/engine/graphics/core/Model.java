package engine.graphics.core;

import engine.graphics.renderer.Shader;
import engine.structures.ObjArray;
import org.joml.Matrix4f;

import static engine.graphics.transformations.Transformations.getModelMatrix;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

public class Model {
    private final ObjArray<Mesh> mesh;
    private final ObjArray<Material> materials;

    public Model(ObjArray<Mesh> mesh, ObjArray<Material> materials) {
        this.mesh = mesh;
        this.materials = materials;
    }

    public void render(Shader shader, Matrix4f transform) {
        shader.setUniform("u_modelMatrix", transform);

        int length = mesh.size();
        for (int i = 0; i < length; i++) {
            Material material = materials.get(i);
            shader.setUniform("u_material", material);
            if (material.texture != -1) {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, material.texture);
            }
            Mesh mesh = this.mesh.get(i);
            mesh.initRender();
            mesh.render();
            mesh.endRender();
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        glBindVertexArray(0);
    }
}
