package engine.graphics.core;

public class Material {
    public int texture;
    public float ambR, ambG, ambB, ambA;
    public float difR, difG, difB, difA;
    public float specR, specG, specB, specA;
    public float reflectance;

    public Material() {
        this.texture = -1;
    }
}
