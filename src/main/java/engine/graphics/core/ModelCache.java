package engine.graphics.core;

import engine.structures.ObjArray;
import org.joml.Vector3f;

public class ModelCache {
    private static final ObjArray<Mesh> HEXAGON_MESH = buildHexagonMesh();

    public static Model loadQuadModel(float width, float height, int texture) {
        Material material = new Material();
        material.texture = texture;
        ObjArray<Material> materials = new ObjArray<>(1);
        materials.add(material);
        ObjArray<Mesh> mesh = buildQuadMesh(width, height);
        return new Model(mesh, materials);
    }

    public static Model loadHexagonModel(Vector3f color) {
        Material material = new Material();
        material.difR = color.x;
        material.difG = color.y;
        material.difB = color.z;
        material.difA = 1;
        ObjArray<Material> materials = new ObjArray<>(1);
        materials.add(material);
        return new Model(HEXAGON_MESH, materials);
    }

    private static ObjArray<Mesh> buildQuadMesh(float width, float height) {
        float[] positions = new float[] {
                -width * 0.5f, height * 0.5f, 0.0f,
                width * 0.5f, height * 0.5f, 0.0f,
                width * 0.5f, -height * 0.5f, 0.0f,
                -width * 0.5f, -height * 0.5f, 0.0f
        };

        float[] textureCoords = new float[] {
                0f, 0f,
                1f, 0f,
                1f, 1f,
                0f, 1f
        };

        float[] normals = new float[] {
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
        };

        int[] indices = new int[] {
                0, 2, 1,
                0, 3, 2
        };

        ObjArray<Mesh> mesh = new ObjArray<>(1);
        mesh.add(new Mesh(positions, textureCoords, normals, indices));
        return mesh;
    }

    private static ObjArray<Mesh> buildHexagonMesh() {
        float height = (float)(Math.sqrt(3) / 2);
        float width = 1.0f;

        float[] positions = new float[] {
                -width, 0.0f, 0.0f,
                -width * 0.5f, height, 0.0f,
                width * 0.5f, height, 0.0f,
                width, 0.0f, 0.0f,
                width * 0.5f, -height, 0.0f,
                -width * 0.5f, -height, 0.0f,
                0, 0, 0
        };

        float[] textureCoords = new float[] {
                0f, 0.5f,
                0.25f, 0f,
                0.75f, 0f,
                1f, 0.5f,
                0.75f, 1f,
                0.25f, 1f,
                0.5f, 0.5f
        };

        float[] normals = new float[] {
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
                0.0f, 0.0f, 1.0f,
        };

        int[] indices = new int[] {
                0, 6, 1,
                1, 6, 2,
                2, 6, 3,
                3, 6, 4,
                4, 6, 5,
                6, 0, 5
        };

        ObjArray<Mesh> mesh = new ObjArray<>(1);
        mesh.add(new Mesh(positions, textureCoords, normals, indices));
        return mesh;
    }
}
