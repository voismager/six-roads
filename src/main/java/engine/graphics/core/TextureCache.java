package engine.graphics.core;

import engine.graphics.GlTools;
import engine.support.loaders.UnifiedResource;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.function.IntConsumer;

public class TextureCache {
    private static Logger LOGGER = LogManager.getLogger(TextureCache.class);

    private static TextureCache INSTANCE;

    private Object2IntMap<UnifiedResource> texturesMap;

    private TextureCache() {
        texturesMap = new Object2IntOpenHashMap<>();
        texturesMap.defaultReturnValue(-1);
    }

    public static synchronized TextureCache getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TextureCache();
        }

        return INSTANCE;
    }

    public int getTexture(UnifiedResource resource) {
        int texture = texturesMap.getInt(resource);

        if (texture == -1) {
            try {
                texture = GlTools.loadSingleTexture(resource);
                texturesMap.put(resource, texture);
            } catch (IOException e) {
                LOGGER.warn("No texture was found for path {}", resource.getPath());
                return -1;
            }
        }

        return texture;
    }

    public static void clear() {
        Object2IntMap<UnifiedResource> texturesMap = getInstance().texturesMap;
        texturesMap.values().forEach((IntConsumer) GL11::glDeleteTextures);
        texturesMap.clear();
    }
}
