package engine.graphics.core;

import engine.structures.ObjArray;
import engine.support.loaders.UnifiedResource;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.*;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static engine.support.IOUtil.ioResourceToByteBuffer;
import static org.lwjgl.assimp.Assimp.*;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.system.MemoryUtil.*;

public class ModelLoader {
    public static Model load(UnifiedResource resource, UnifiedResource textures, int flags) throws IOException {
        if (resource.isResource()) return loadAsResource(resource, textures, flags);
        throw new AssertionError();
    }

    private static Model loadAsResource(UnifiedResource resource, UnifiedResource textures, int flags) throws IOException {
        final AIFileIO io = AIFileIO.create();
        io.set(new ResourceOpenProcess(), new ResourceCloseProcess(), NULL);
        final AIScene scene = aiImportFileEx(resource.getPath().substring(1), flags, io);
        if (scene == null) { throw new IOException("Error loading model"); }

        final Material[] materials = loadMaterials(scene, textures);
        return loadModel(scene, materials);
    }

    private static Model loadModel(AIScene scene, Material[] materialsArr) throws IOException {
        final int size = scene.mNumMeshes();
        final PointerBuffer aiMeshes = scene.mMeshes();

        if (aiMeshes == null) throw new IOException("No meshes!");

        final ObjArray<Mesh> meshes = new ObjArray<>(size);
        final ObjArray<Material> materials = new ObjArray<>(size);

        for (int i = 0; i < size; i++) {
            final AIMesh aiMesh = AIMesh.create(aiMeshes.get(i));
            final Mesh mesh = new Mesh(aiMesh);
            meshes.add(mesh);

            int materialIdx = aiMesh.mMaterialIndex();
            if (materialIdx >= 0 && materialIdx < materialsArr.length)
                materials.add(materialsArr[materialIdx]);
            else materials.add(new Material());
        }

        glBindVertexArray(0);
        return new Model(meshes, materials);
    }

    private static Material[] loadMaterials(AIScene scene, UnifiedResource textures) {
        final int size = scene.mNumMaterials();
        final PointerBuffer aiMaterials = scene.mMaterials();

        if (aiMaterials == null) return new Material[0];

        final Material[] materials = new Material[size];
        for (int i = 0; i < size; i++) {
            final AIMaterial aiMaterial = AIMaterial.create(aiMaterials.get(i));
            final Material material = new Material();
            materials[i] = material;

            AIString path = AIString.calloc();
            Assimp.aiGetMaterialTexture(aiMaterial, aiTextureType_DIFFUSE, 0, path, (IntBuffer) null, null, null, null, null, null);
            String texturePath = path.dataString();
            if (!texturePath.isEmpty())
                material.texture = TextureCache.getInstance().getTexture(textures.getChild(new File(texturePath).getName()));

            AIColor4D colour = AIColor4D.create();
            if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_NONE, 0, colour) == 0) {
                material.difR = colour.r();
                material.difG = colour.g();
                material.difB = colour.b();
                material.difA = colour.a();
            }
            if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_SPECULAR, aiTextureType_NONE, 0, colour) == 0) {
                material.specR = colour.r();
                material.specG = colour.g();
                material.specB = colour.b();
                material.specA = colour.a();
            }
            if (aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_AMBIENT, aiTextureType_NONE, 0, colour) == 0) {
                material.ambR = colour.r();
                material.ambG = colour.g();
                material.ambB = colour.b();
                material.ambA = colour.a();
            }
        }

        return materials;
    }

    private static class ResourceOpenProcess extends AIFileOpenProc {
        @Override
        public long invoke(long pFileIO, long fileName, long openMode) {
            AIFile file = AIFile.create();
            final ByteBuffer data;
            String fileNameUtf8 = memUTF8(fileName);
            try {
                data = ioResourceToByteBuffer(fileNameUtf8, 8192);
            } catch (IOException e) {
                throw new RuntimeException("Could not open file: " + fileNameUtf8);
            }
            AIFileReadProcI fileReadProc = new AIFileReadProc() {
                public long invoke(long pFile, long pBuffer, long size, long count) {
                    long max = Math.min(data.remaining(), size * count);
                    memCopy(memAddress(data) + data.position(), pBuffer, max);
                    return max;
                }
            };
            AIFileSeekI fileSeekProc = new AIFileSeek() {
                public int invoke(long pFile, long offset, int origin) {
                    if (origin == Assimp.aiOrigin_CUR) data.position(data.position() + (int) offset);
                    else if (origin == Assimp.aiOrigin_SET) data.position((int) offset);
                    else if (origin == Assimp.aiOrigin_END) data.position(data.limit() + (int) offset);
                    return 0;
                }
            };
            AIFileTellProcI fileTellProc = new AIFileTellProc() {
                public long invoke(long pFile) { return data.limit(); }
            };
            file.ReadProc(fileReadProc);
            file.SeekProc(fileSeekProc);
            file.FileSizeProc(fileTellProc);
            return file.address();
        }
    }

    private static class ResourceCloseProcess extends AIFileCloseProc {
        @Override
        public void invoke(long pFileIO, long pFile) { }
    }
}
