package debug;

import client.localclientsession.GameSession;
import engine.support.Tools;
import glm_.vec2.Vec2;
import imgui.Cond;
import imgui.ImGui;
import imgui.InputTextFlag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class Console {
    private final List<String> history;
    private final boolean[] open;
    private final char[] command;
    private final Vec2 size;

    public Console() {
        this.history = new ArrayList<>();
        this.open = new boolean[1];
        this.command = new char[64];
        this.size = new Vec2(400, 400);
    }

    public void draw(ImGui imGui) {
        imGui.setNextWindowSize(size, Cond.Once);

        if (imGui.begin("console", open, 0)) {
            if (imGui.inputText("<<", command, InputTextFlag.EnterReturnsTrue.getI())) {
                String c = ImGuiUtil.toString(command);
                history.add(c);
                history.add(process(c));
                Arrays.fill(command, '\0');
            }

            for (int i = 0; i < history.size(); i++) imGui.text(history.get(i));

            imGui.end();
        }
    }

    private static final Pattern delimiter = Pattern.compile("[(),.]");
    private Iterator<String> tokens;

    private String process(String command) {
        command = command.replaceAll("\\s+(?=([^\']*\'[^\']*\')*[^\']*$)", "");
        tokens = Tools.split(command, delimiter);

        try {
            switch (next()) {
                default: throw new RuntimeException("Invalid command name");

                case "clear":
                    skip("("); skip(")");
                    history.clear();
                    return "Result: done";

                case "1975":
                    return new String(hexStringToByteArray("566c616469736c6176204765726173696d6f762c2031362f30372f323031392e204869203a6f"));

                //case "getCurrentPuppeteer":
                   // skip("("); skip(")");
                    //return "Result: " + GameSession.INSTANCE.getEnv().puppeteers.current().getId();

                //case "getGlobalAttribute":
                //    skip("(");
                //    short id0 = Short.parseShort(next());
                //    skip(")");
                //    return "Result: " + GameSession.INSTANCE.getEnv().module.global.attributes.get(id0).toString();

                //case "getPuppeteerAttribute":
                  //  skip("(");
                  //  int id1 = Integer.parseInt(next());
                  //  skip(",");
                   // short id2 = Short.parseShort(next());
                  //  skip(")");
                   // return "Result: " + GameSession.INSTANCE.getEnv().puppeteers.get(id1).getAttribute(id2).toString();

                //case "getCellInfo":
                    //skip("("); skip(")");
                    //return "Result: " + GameSession.INSTANCE.getEnv().memory.peekGroundFromSchemeList(0);
            }

        } catch (RuntimeException e) {
            return "Error: " + e.getMessage();
        }
    }

    private static byte[] hexStringToByteArray(String hex) {
        int l = hex.length();
        byte[] data = new byte[l/2];
        for (int i = 0; i < l; i += 2) {
            data[i/2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                    + Character.digit(hex.charAt(i+1), 16));
        }
        return data;
    }

    private void skip(String str) {
        if (tokens.hasNext()) {
            String next = tokens.next();
            if (!str.equals(next)) throw new RuntimeException();
        }

        else {
            if (!str.equals("eof")) throw new RuntimeException();
        }
    }

    private String next() {
        if (tokens.hasNext()) return tokens.next();
        throw new RuntimeException();
    }
}
