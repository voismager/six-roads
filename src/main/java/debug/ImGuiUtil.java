package debug;

import java.util.Arrays;

public class ImGuiUtil {
    public static String toString(char[] a) {
        int index = 0;

        for (int i = 0; i < a.length; i++) {
            if (a[i] == '\0') {
                index = i;
                break;
            }
        }

        return String.valueOf(a, 0, index);
    }

    public static void clear(char[] a) {
        Arrays.fill(a, '\0');
    }
}
