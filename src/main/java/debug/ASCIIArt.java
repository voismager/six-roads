package debug;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ASCIIArt {
    private static Random random = new Random();
    private static String[] arts;

    static {
        List<String> list = new ArrayList<>();
        list.add("--------{---(@");
        list.add("><>");
        list.add("¸.·´¯`·.´¯`·.¸¸.·´¯`·.¸><(((º>");
        list.add("@}}>-----");
        list.add("@('_')@");
        list.add("d[ o_0 ]b");
        list.add(")xxxxx[;;;;;;;;;>");
        arts = list.toArray(new String[0]);
    }

    public static String getRandom() {
        int index = random.nextInt(arts.length);
        return arts[index];
    }
}
