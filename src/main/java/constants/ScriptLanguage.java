package constants;

public class ScriptLanguage {
    public static final byte NONE = 0;
    public static final byte JAVA = 1;
    public static final byte LUA  = 2;
}
