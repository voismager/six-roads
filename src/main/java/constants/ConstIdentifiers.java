package constants;

public class ConstIdentifiers {
    public static final int   NULL_PUPPETEER        = -1;
    public static final short DEFAULT_AREA          = -2;
    public static final short DEFAULT_TERRAIN       = -2;
    public static final int   NULL_IDENTIFIER       = Integer.MAX_VALUE;
    public static final short NULL_MAP              = -1;
    public static final short NULL_FACTION          = -1;
    public static final short NULL_ID               = -1;
    public static final byte  NULL_PAINT            = -1;
    public static final short DEFAULT_GROUND_PAINT  = -2;
    public static final short DEFAULT_UNIT_PAINT    = -3;
}
