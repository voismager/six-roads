package constants;

import static engine.support.keys.Mapper.*;

public enum EntityType {
    ANY,
    COMPONENT,
    AREA,
    ATTRIBUTE,
    TERRAIN,
    UNIT,
    MAP,
    FACTION,
    SCRIPT,
    QUEST,
    HEXAGON_UNIT,
    HEXAGON_GROUND;

    private static final EntityType[] CACHED = values();

    public static int Identifier(EntityType type, short id) {
        return TD01_BS_I(type.toByte(), id);
    }

    public static int Identifier(byte type, short id) {
        return TD01_BS_I(type, id);
    }

    public static short IdentifierToId(int Identifier) {
        return TD01_I_S(Identifier);
    }

    public static EntityType fromIdentifier(int Identifier) {
        return fromByte(TD01_I_B(Identifier));
    }

    public static EntityType fromByte(byte b) {
        return CACHED[b];
    }

    public static boolean isType(byte b) {
        return b >= 0 && b < CACHED.length;
    }

    public static byte parseType(String s) {
        if (s.matches("[0-9]+")) {
            return Byte.parseByte(s);
        }

        return valueOf(s).toByte();
    }

    public static int parseIdentifier(String idField) {
        final String[] tokens = idField.replaceAll("\\s", "").split(",");

        if (tokens.length == 2) {
            return parseIdentifier(tokens[0], tokens[1]);
        }

        throw new IllegalArgumentException();
    }

    public static int parseIdentifier(String part0, String part1) {
        if (part0.matches("[0-9]+")) {
            return Identifier(
                    (byte) (Byte.parseByte(part0) + CACHED.length),
                    Short.parseShort(part1)
            );
        }

        return Identifier(
                valueOf(part0),
                Short.parseShort(part1)
        );
    }

    public byte toByte() {
        return (byte) ordinal();
    }
}
