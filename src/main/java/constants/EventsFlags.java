package constants;

import engine.support.keys.GameUtil;

public class EventsFlags {
    public static final char PUSH_FLAG          = 1 << 0;
    public static final char POP_UNIT_FLAG      = 1 << 1;
    public static final char POP_GROUND_FLAG    = 1 << 2;
    public static final char POP_ENTITY_FLAG    = 1 << 3;
    public static final char CLEAR_UNIT_FLAG    = 1 << 4;
    public static final char CLEAR_GROUND_FLAG  = 1 << 5;
    public static final char CLEAR_ENTITY_FLAG  = 1 << 6;
    public static final char PREFETCH_FLAG      = 1 << 7;
    public static final char CLEAN_FLAG         = 1 << 8;

    public static final byte END_MY_TURN       = 0;
    public static final byte EXIT              = 1;
    public static final byte EMPTY_HOVERED     = 2;
    public static final byte EMPTY_CLICK       = 3;
    public static final byte UNIT_CLICK        = 4;
    public static final byte GROUND_CLICK      = 5;
    public static final byte ENTITY_CLICK      = 6;
    public static final byte GROUND_HOVERED    = 7;
    public static final byte UNIT_HOVERED      = 8;
    public static final byte OTHERS            = 9;
    public static final byte CONDITION         = 10;
    public static final byte CUSTOM_TRIGGER    = 11;
    public static final byte BUTTON_PUSH       = 12;

    public static final long  A_PUSH_FLAG       = 1L << 16;
    public static final long  A_POP_GROUND_FLAG = 1L << 17;
    public static final long  A_POP_UNIT_FLAG   = 1L << 18;
    public static final long  A_POP_ENTITY_FLAG = 1L << 19;

    public static final byte  BUTTON_ANY        = 0;
    public static final byte  BUTTON_LMB        = 1;
    public static final byte  BUTTON_RMB        = 2;
    public static final byte  PUPPETEER_ANY     = 0;
    public static final byte  PUPPETEER_ME      = 1;
    public static final byte  PUPPETEER_ANOTHER = 2;
    public static final byte  PUPPETEER_NONE    = 3;
    public static final short ID_ANY            = -1;
    public static final byte  MODE_ANY          = 0;
    public static final byte  MODE_NONE         = 1;
    public static final byte  MODE_LONG_LMB     = 2;
    public static final byte  MODE_SHORT_LMB    = 3;
    public static final byte  MODE_LONG_RMB     = 4;
    public static final byte  MODE_SHORT_RMB    = 6;

    public static long parseKey(String str) {
        String[] tokens = str.replaceAll("\\s+", "").split(",");

        switch (tokens[0]) {
            case "EXIT":
                return GameUtil.getExitKey(Short.parseShort(tokens[1]));

            case "EMPTY_CLICK":
                return GameUtil.getEmptyClickKey(parseButtonFlag(tokens[1]));

            case "UNIT_CLICK":
                return GameUtil.getUnitClickKey(
                        parseButtonFlag(tokens[1]),
                        parsePuppeteerFlag(tokens[2]),
                        parseId(tokens[3]),
                        parseId(tokens[4])
                );

            case "GROUND_CLICK":
                return GameUtil.getGroundClickKey(
                        parseButtonFlag(tokens[1]),
                        parsePuppeteerFlag(tokens[2]),
                        parseId(tokens[3]),
                        parseId(tokens[4])
                );

            case "ENTITY_CLICK":
                return GameUtil.getEntityClickKey(
                        parseButtonFlag(tokens[1]),
                        parseTypeFlag(tokens[2]),
                        parseId(tokens[3]),
                        parseId(tokens[4])
                );

            case "EMPTY_HOVERED":
                return GameUtil.getEmptyHoveredKey(
                        parseMouseMode(tokens[1])
                );

            case "UNIT_HOVERED":
                return GameUtil.getUnitHoveredKey(
                        parseMouseMode(tokens[1]),
                        parsePuppeteerFlag(tokens[2]),
                        parseId(tokens[3]),
                        parseId(tokens[4])
                );

            case "GROUND_HOVERED":
                return GameUtil.getGroundHoveredKey(
                        parseMouseMode(tokens[1]),
                        parsePuppeteerFlag(tokens[2]),
                        parseId(tokens[3]),
                        parseId(tokens[4])
                );

            case "BUTTON_PUSHED":
                return GameUtil.getButtonPushKey(
                        parseButtonCode(tokens[1])
                );

            case "OTHERS":
                return GameUtil.getOthersKey();

            case "CONDITION":
                return GameUtil.getConditionKey(parseId(tokens[1]));

            case "TRIGGERS":
                return GameUtil.getCustomTriggerKey(parseId(tokens[1]));

            default:
                throw new IllegalArgumentException();
        }
    }

    private static short parseButtonCode(String str) {
        switch (str) {
            default: return Short.parseShort(str);
        }
    }

    private static short parseId(String str) {
        switch (str) {
            case "ID_ANY":
                return ID_ANY;

            default:
                try {
                    return Short.parseShort(str);
                }

                catch (NumberFormatException e) {
                    throw new IllegalArgumentException();
                }
        }
    }

    private static byte parseMouseMode(String str) {
        switch (str) {
            case "MODE_ANY": return MODE_ANY;
            case "MODE_NONE": return MODE_NONE;
            case "MODE_LONG_LMB": return MODE_LONG_LMB;
            case "MODE_SHORT_LMB": return MODE_SHORT_LMB;
            case "MODE_LONG_RMB": return MODE_LONG_RMB;
            case "MODE_SHORT_RMB": return MODE_SHORT_RMB;
            default: throw new IllegalArgumentException();
        }
    }

    private static byte parseTypeFlag(String str) {
        return EntityType.valueOf(str).toByte();
    }

    private static byte parseButtonFlag(String str) {
        switch (str) {
            case "BUTTON_ANY": return BUTTON_ANY;
            case "BUTTON_LMB": return BUTTON_LMB;
            case "BUTTON_RMB": return BUTTON_RMB;
            default: throw new IllegalArgumentException();
        }
    }

    public static String buttonFlagToString(byte flag) {
        switch (flag) {
            case BUTTON_ANY: return "BUTTON_ANY";
            case BUTTON_LMB: return "BUTTON_LMB";
            case BUTTON_RMB: return "BUTTON_RMB";
            default: throw new IllegalArgumentException();
        }
    }

    private static byte parsePuppeteerFlag(String str) {
        switch (str) {
            case "PUPPETEER_ANY": return PUPPETEER_ANY;
            case "PUPPETEER_ME": return PUPPETEER_ME;
            case "PUPPETEER_ANOTHER": return PUPPETEER_ANOTHER;
            case "PUPPETEER_NONE": return PUPPETEER_NONE;
            default: throw new IllegalArgumentException();
        }
    }

    public static String puppeteerFlagToString(byte flag) {
        switch (flag) {
            case PUPPETEER_ANY: return "PUPPETEER_ANY";
            case PUPPETEER_ME: return "PUPPETEER_ME";
            case PUPPETEER_ANOTHER: return "PUPPETEER_ANOTHER";
            case PUPPETEER_NONE: return "PUPPETEER_NONE";
            default: throw new IllegalArgumentException();
        }
    }

    public static char parseStackFlags(String str) {
        String[] tokens = str.replaceAll("\\s+", "").split(",");

        char flags = 0x00;

        for (String token : tokens) {
            switch (token) {
                case "pop_unit": flags |= POP_UNIT_FLAG; break;
                case "pop_ground": flags |= POP_GROUND_FLAG; break;
                case "pop_entity": flags |= POP_ENTITY_FLAG; break;
                case "clear_unit": flags |= CLEAR_UNIT_FLAG; break;
                case "clear_ground": flags |= CLEAR_GROUND_FLAG; break;
                case "clear_entity": flags |= CLEAR_ENTITY_FLAG; break;
                case "push": flags |= PUSH_FLAG; break;
                case "prefetch": flags |= PREFETCH_FLAG; break;
                case "clean": flags |= CLEAN_FLAG; break;
                case "": break;
                default: throw new IllegalArgumentException();
            }
        }

        return flags;
    }
}
