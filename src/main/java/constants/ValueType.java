package constants;

public enum ValueType {
    VOID, BYTE, SHORT, INTEGER, LONG;

    private static final ValueType[] CACHED = values();

    public static ValueType fromByte(byte v) {
        if (v >= 0 && v < CACHED.length) return CACHED[v];
        throw new IllegalArgumentException();
    }

    public byte toByte() {
        return (byte) ordinal();
    }
}
