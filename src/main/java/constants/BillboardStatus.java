package constants;

public class BillboardStatus {
    public static final byte CLOSED          = 0;
    public static final byte OPEN            = 1 << 0;

    public static byte parse(String str) {
        String[] tokens = str.replaceAll("\\s+", "").split(",");

        byte flags = 0;

        for (String token : tokens) {
            switch (token) {
                case "closed":
                    return CLOSED;
                case "open":
                    return OPEN;

                default: throw new IllegalArgumentException();
            }
        }

        return flags;
    }

    public static boolean checkStatus(byte flags) {
         if (flags == CLOSED) return false;
         if ((flags & OPEN) == OPEN) return true;

         throw new AssertionError();
    }
}
