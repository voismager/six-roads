package constants;

import static engine.support.keys.Mapper.TD08_BSI_L;
import static engine.support.keys.Mapper.TD08_Bs_I;

public class Triggers {
    public static final int  NO_FLAGS                   = 0;
    public static final int  ADD_EFFECT_FLAG            = 1 << 0;
    public static final int  REMOVE_EFFECT_FLAG         = 1 << 1;

    public static final byte UNIT_MOVEMENT                  = 2;
    public static final byte CELL_CAPTURED                  = 3;
    public static final byte ATTRIBUTE_CHANGED              = 4;
    public static final byte PUPPETEER_ATTRIBUTE_CHANGED    = 5;
    public static final byte NEW_TURN                       = 6;
    public static final byte EFFECT_ADDED                   = 7;
    public static final byte EFFECT_REMOVED                 = 8;
    public static final byte PUPPETEER_DIED                 = 9;

    public static final long ADD_EFFECT_TRIGGER         = TD08_BSI_L(EFFECT_ADDED, (short)0, 0);
    public static final long REMOVE_EFFECT_TRIGGER      = TD08_BSI_L(EFFECT_REMOVED, (short)0, 0);
}
