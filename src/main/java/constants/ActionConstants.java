package constants;

public class ActionConstants {
    public static final byte MY_TURN     = 0;
    public static final byte OTHERS_TURN = 1;

    public static final byte SUCCESS = 0;
    public static final byte CANCEL = 1;
}
