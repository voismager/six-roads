package constants;

import java.util.NoSuchElementException;

public class QuestType {
    public static final byte DEFEAT = 0;
    public static final byte VICTORY = 1;
    public static final byte THE_SUN_GO_ON_SHINING = 2;

    public static byte parse(String s) {
        s = s.toUpperCase();
        if (s.equals("DEFEAT")) return DEFEAT;
        if (s.equals("VICTORY")) return VICTORY;
        if (s.equals("THE_SUN_GO_ON_SHINING")) return THE_SUN_GO_ON_SHINING;
        throw new NoSuchElementException();
    }
}
