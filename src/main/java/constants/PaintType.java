package constants;

public class PaintType {
    public final static byte GROUND  = 0;
    public final static byte UNIT    = 1;

    public final static byte DEFAULT = 0;
    public final static byte DIJKSTRA = 1;
    public final static byte ALL = 2;
    public final static byte GIVEN_COORDINATES = 3;
}
