package scripting.fsm;

import client.gameobjects.Sprite;
import common.fsm.Script;

import java.util.Collections;
import java.util.Map;
import java.util.function.Consumer;

public class P2061_start implements Consumer<Map<String, Script>> {
    @Override
    public void accept(Map<String, Script> map) {
        map.put("P2061::start", (event, stack, context, env) -> {
            final Sprite hero = env.createSprite(1f, 1f, env.loadTexture("hero.png"));

            context.deactivateParallel(context.id());
            context.activateParallel("P2061::movement_init", 1, Collections.singletonMap(null, hero));
        });
    }
}
