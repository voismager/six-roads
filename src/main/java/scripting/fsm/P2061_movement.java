package scripting.fsm;

import client.gameobjects.Sprite;
import common.fsm.Script;
import common.gameobjects.Hexagon;

import java.util.Map;
import java.util.function.Consumer;

import static org.lwjgl.glfw.GLFW.*;

public class P2061_movement implements Consumer<Map<String, Script>> {
    @Override
    public void accept(Map<String, Script> map) {
        map.put("P2061::movement_init", (event, stack, context, env) -> {
            final Sprite hero = (Sprite) context.parameter(null);
            //env.setCamera(hero.x, hero.y - 7, 10);
            context.jump("P2061::movement_main", 0);
            context.jump("P2061::movement_main", 1);
            context.jump("P2061::movement_main", 2);
            context.jump("P2061::movement_main", 3);
            context.jump("P2061::movement_main", 4);
            context.jump("P2061::movement_main", 5);
            context.jump("P2061::movement_main", 6);
            context.jump("P2061::movement_main", 7);
        });

        map.put("P2061::movement_main", (event, stack, context, env) -> {
            final Sprite hero = (Sprite) context.parameter(null);
            switch (event) {
                case 0: env.moveCamera(0, 0.2f, 0); break;
                case 1: env.moveCamera(0.2f, 0.2f, 0); break;
                case 2: env.moveCamera(0.2f, 0, 0); break;
                case 3: env.moveCamera(0.2f, -0.2f, 0); break;
                case 4: env.moveCamera(0, -0.2f, 0); break;
                case 5: env.moveCamera(-0.2f, -0.2f, 0); break;
                case 6: env.moveCamera(-0.2f, 0, 0); break;
                case 7: env.moveCamera(-0.2f, 0.2f, 0); break;
            }

            //env.setCamera(hero.x, hero.y - 7, 10);

            context.jump("P2061::movement_main", 0);
            context.jump("P2061::movement_main", 1);
            context.jump("P2061::movement_main", 2);
            context.jump("P2061::movement_main", 3);
            context.jump("P2061::movement_main", 4);
            context.jump("P2061::movement_main", 5);
            context.jump("P2061::movement_main", 6);
            context.jump("P2061::movement_main", 7);
        });
    }
}
