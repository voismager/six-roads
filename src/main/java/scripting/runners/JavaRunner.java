package scripting.runners;

import engine.types.integers.IntType;
import client.logic.ClEnvironment;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import common.server.gamelogic.logic.SVEnvironment;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;

public abstract class JavaRunner implements ScriptRunner {
    protected ClEnvironment cl;
    protected SVEnvironment sv;

    public static short getId(Object2ObjectMap<String, Object> parameters) {
        return (short) parameters.get("id");
    }

    public static int[] getIntArrayOrEmpty(Object2ObjectMap<String, Object> parameters, String name) {
        Object p = parameters.get(name);

        if (p instanceof Object[]) {
            Object[] a = (Object[]) p;
            int[] res = new int[a.length];

            for (int i = 0; i < a.length; i++) {
                if (a[i] instanceof Integer) {
                    res[i] = (int) a[i];
                }
            }

            return res;
        }

        return new int[0];
    }

    public static int getIntOrDefault(Object2ObjectMap<String, Object> parameters, String name, int defaultV) {
        return (int) parameters.getOrDefault(name, defaultV);
    }

    public static short getShortOrDefault(Object2ObjectMap<String, Object> parameters, String name, int defaultV) {
        return (short) (int) parameters.getOrDefault(name, defaultV);
    }

    public static String getStringOrDefault(Object2ObjectMap<String, Object> parameters, String name, String defaultV) {
        return (String) parameters.getOrDefault(name, defaultV);
    }

    public static boolean getBooleanOrDefault(Object2ObjectMap<String, Object> parameters, String name, boolean defaultV) {
        return (boolean) parameters.getOrDefault(name, defaultV);
    }

    public JavaRunner(ClEnvironment cl, SVEnvironment sv, Object2ObjectMap<String, Object> parameters) {
        this.cl = cl;
        this.sv = sv;
    }

    public JavaRunner(ClEnvironment cl, SVEnvironment sv) {
        this.cl = cl;
        this.sv = sv;
    }

    public void cl_postConstruct() { }

    public void unchain() {
        cl = null;
        sv = null;
    }

    @Override
    public void cl_call() {

    }

    @Override
    public void cl_call(int arg0) {

    }

    @Override
    public void cl_call(long arg0) {

    }

    @Override
    public void cl_call(int arg0, short arg1) {

    }

    @Override
    public void cl_call(int arg0, int arg1) {

    }

    @Override
    public void cl_call(int arg0, IntType arg1) {

    }

    @Override
    public void cl_call(int arg0, long arg1) {

    }

    @Override
    public void cl_call(int arg0, Object2IntMap<String> params) {

    }

    @Override
    public void cl_call(int arg0, int[] arg1) {

    }

    @Override
    public void cl_call(int arg0, int arg1, int arg2) {

    }

    @Override
    public void cl_call(int arg0, int arg1, IntType arg2) {

    }

    @Override
    public void cl_call(int arg0, IntType arg1, int arg2) {

    }

    @Override
    public void cl_call(int arg0, int arg1, int[] arg2) {

    }

    @Override
    public void cl_call(int arg0, long arg1, int arg2) {

    }

    @Override
    public void cl_call(int arg0, long arg1, Object2IntMap<String> params) {

    }

    @Override
    public void cl_call(int index, int arg0, int arg1, int arg2) {

    }

    @Override
    public void cl_call(int index, int arg0, int arg1, IntType arg2) {

    }

    @Override
    public void cl_call(int index, IntType arg0, int arg1, int arg2) {

    }

    @Override
    public void cl_call(int index, IntType arg0, int arg1, IntType arg2) {

    }

    @Override
    public boolean cl_call_boolean() {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0) {
        return false;
    }

    @Override
    public boolean cl_call_boolean0(int arg0) {
        return false;
    }

    @Override
    public boolean cl_call_boolean1(int arg1) {
        return false;
    }

    @Override
    public boolean cl_call_boolean2(int arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, short arg1) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, int arg1) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, IntType arg0) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, short arg1, int arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, short arg1, long arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, int arg0, int arg1) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, int arg0, IntType arg1) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, long arg1, int arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, long arg1, IntType arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, IntType arg0, int arg1) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, IntType arg0, IntType arg1) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, int arg1, int arg2, int arg3) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, int arg0, int arg1, IntType arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, IntType arg0, int arg1, int arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int index, IntType arg0, int arg1, IntType arg2) {
        return false;
    }

    @Override
    public boolean cl_call_boolean(int arg0, int arg1, int arg2, int arg3, int arg4) {
        return false;
    }

    @Override
    public int cl_call_int() {
        return 0;
    }

    @Override
    public int cl_call_int(int index, int arg0) {
        return 0;
    }

    @Override
    public int cl_call_int(int index, IntType arg0) {
        return 0;
    }

    @Override
    public int cl_call_int(int index, int arg0, int arg1) {
        return 0;
    }

    @Override
    public int cl_call_int(int index, IntType arg0, int arg1) {
        return 0;
    }

    @Override
    public int cl_call_int(int index, int arg0, int arg1, int arg2) {
        return 0;
    }

    @Override
    public int cl_call_int(int index, IntType arg0, int arg1, int arg2) {
        return 0;
    }

    @Override
    public int[] cl_call_ints() {
        return new int[0];
    }

    @Override
    public int[] cl_call_ints(int arg0) {
        return new int[0];
    }

    @Override
    public int[] cl_call_ints(int arg0, int arg1) {
        return new int[0];
    }

    @Override
    public int[] cl_call_ints(int index, IntType arg0) {
        return new int[0];
    }

    @Override
    public int[] cl_call_ints(int index, int arg0, int arg1) {
        return new int[0];
    }

    @Override
    public int[] cl_call_ints(int index, IntType arg0, int arg1) {
        return new int[0];
    }

    @Override
    public int[] cl_call_ints(int index, int arg0, int arg1, int arg2) {
        return new int[0];
    }

    @Override
    public int[] cl_call_ints(int index, IntType arg0, int arg1, int arg2) {
        return new int[0];
    }

    @Override
    public long[] cl_call_longs() {
        return new long[0];
    }

    @Override
    public long[] cl_call_longs(int arg0) {
        return new long[0];
    }

    @Override
    public long[] cl_call_longs(int arg0, short arg1) {
        return new long[0];
    }

    @Override
    public long[] cl_call_longs(int arg0, int arg1) {
        return new long[0];
    }

    @Override
    public long[] cl_call_longs(int arg0, IntType arg1) {
        return new long[0];
    }

    @Override
    public long[] cl_call_longs(int arg0, Object2IntMap<String> args) {
        return new long[0];
    }

    @Override
    public void sv_call() {

    }

    @Override
    public void sv_call(int arg0) {

    }

    @Override
    public void sv_call(int arg0, short arg1) {

    }

    @Override
    public void sv_call(int arg0, long arg1) {

    }

    @Override
    public void sv_call(int arg0, int arg1, int arg2) {

    }

    @Override
    public void sv_call(int arg0, int arg1, long arg2) {

    }

    @Override
    public void sv_call(int arg0, long arg1, int arg2) {

    }

    @Override
    public void sv_call(int arg0, long arg1, Object2IntMap<String> params) {

    }

    @Override
    public boolean sv_call_boolean(int arg0) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, short arg1) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, int arg1) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, short arg1, int arg2) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, short arg1, long arg2) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, int arg1, int arg2) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, int arg1, IntType arg2) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, long arg1, int arg2) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, long arg1, IntType arg2) {
        return false;
    }

    @Override
    public boolean sv_call_boolean(int arg0, int arg1, int arg2, int arg3, int arg4) {
        return false;
    }

    @Override
    public int[] sv_call_ints(int arg0) {
        return new int[0];
    }

    @Override
    public long[] sv_call_longs(int arg0) {
        return new long[0];
    }

    @Override
    public long[] sv_call_longs(int arg0, short arg1) {
        return new long[0];
    }

    @Override
    public long[] sv_call_longs(int arg0, int arg1) {
        return new long[0];
    }

    @Override
    public long[] sv_call_longs(int arg0, Object2IntMap<String> args) {
        return new long[0];
    }
}
