package scripting.runners;

import client.logic.ClEnvironment;
import common.server.gamelogic.logic.SVEnvironment;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;

import java.util.Arrays;
import java.util.function.IntConsumer;
import java.util.function.IntFunction;
import java.util.function.IntPredicate;
import java.util.function.ObjIntConsumer;

public abstract class ActionRunner extends JavaRunner {
    private IntFunction<long[]>[]   cl_stages;
    private ObjIntConsumer<int[]>[] wt_stages;
    private IntPredicate[]          sv_stages;
    private IntPredicate             prefetch;
    private IntConsumer                 clean;

    @SuppressWarnings("unchecked")
    public ActionRunner(ClEnvironment cl, SVEnvironment sv, Object2ObjectMap<String, Object> parameters) {
        super(cl, sv, parameters);

        this.cl_stages = new IntFunction[2];
        this.wt_stages = new ObjIntConsumer[0];
        this.sv_stages = new IntPredicate[0];

        init(parameters);
    }

    protected abstract void init(Object2ObjectMap<String, Object> parameters);

    protected final void addPrefetch(IntPredicate prefetch, IntConsumer clean) {
        this.prefetch = prefetch;
        this.clean = clean;
    }

    protected final void addCLStage(int index, IntFunction<long[]> stage) {
        if (this.cl_stages.length <= index)
            this.cl_stages = Arrays.copyOf(cl_stages, index + 2);

        this.cl_stages[index] = stage;
    }

    protected final void addSVStage(int index, IntPredicate stage) {
        if (this.sv_stages.length <= index)
            this.sv_stages = Arrays.copyOf(sv_stages, index + 2);

        this.sv_stages[index] = stage;
    }

    protected final void addWTStage(int index, ObjIntConsumer<int[]> stage) {
        if (this.wt_stages.length <= index)
            this.wt_stages = Arrays.copyOf(wt_stages, index + 2);

        this.wt_stages[index] = stage;
    }

    @Override
    public boolean cl_call_boolean0(int index) {
        return index >= 0 && cl_stages.length > index && cl_stages[index] != null;
    }

    @Override
    public boolean cl_call_boolean1(int index) {
        return index >= 0 && sv_stages.length > index && sv_stages[index] != null;
    }

    @Override
    public boolean cl_call_boolean2(int index) {
        return index >= 0 && wt_stages.length > index && wt_stages[index] != null;
    }

    @Override
    public boolean cl_call_boolean() {
        return prefetch != null;
    }

    @Override
    public long[] cl_call_longs(int stage, int puppeteer) {
        return cl_stages[stage].apply(puppeteer);
    }

    @Override
    public void cl_call(int puppeteer) {
        clean.accept(puppeteer);
    }

    @Override
    public void cl_call(int stage, int puppeteer, int[] args) {
        wt_stages[stage].accept(args, puppeteer);
    }

    @Override
    public boolean cl_call_boolean(int puppeteer) {
        return prefetch.test(puppeteer);
    }

    @Override
    public boolean sv_call_boolean(int stage, int puppeteer) {
        if (sv_stages[stage] == null) return true;
        return sv_stages[stage].test(puppeteer);
    }
}
