package scripting.runners;

import engine.types.integers.IntType;
import it.unimi.dsi.fastutil.objects.Object2IntMap;

import java.util.function.IntFunction;

public interface ScriptRunner {
    int ENTER_STAGE = 0;
    int EXIT_STAGE = 1;
    int STAGE_0 = 2;
    int STAGE_1 = 3;
    int STAGE_2 = 4;
    int STAGE_3 = 5;

    long[] ZERO_LONGS = new long[0];

    IntFunction<long[]> DO_NOTHING = puppeteer -> ZERO_LONGS;

    void cl_call();
    void cl_call(int arg0);
    void cl_call(long arg0);
    void cl_call(int arg0, short arg1);
    void cl_call(int arg0, int arg1);
    void cl_call(int arg0, IntType arg1);
    void cl_call(int arg0, long arg1);
    void cl_call(int arg0, Object2IntMap<String> params);
    void cl_call(int arg0, int[] arg1);
    void cl_call(int arg0, int arg1, int arg2);
    void cl_call(int arg0, int arg1, IntType arg2);
    void cl_call(int arg0, IntType arg1, int arg2);
    void cl_call(int arg0, int arg1, int[] arg2);
    void cl_call(int arg0, long arg1, int arg2);
    void cl_call(int arg0, long arg1, Object2IntMap<String> params);
    void cl_call(int index, int arg0, int arg1, int arg2);
    void cl_call(int index, int arg0, int arg1, IntType arg2);
    void cl_call(int index, IntType arg0, int arg1, int arg2);
    void cl_call(int index, IntType arg0, int arg1, IntType arg2);

    boolean cl_call_boolean();
    boolean cl_call_boolean(int arg0);
    boolean cl_call_boolean0(int arg0);
    boolean cl_call_boolean1(int arg1);
    boolean cl_call_boolean2(int arg2);
    boolean cl_call_boolean(int arg0, short arg1);
    boolean cl_call_boolean(int arg0, int arg1);
    boolean cl_call_boolean(int index, IntType arg0);
    boolean cl_call_boolean(int arg0, short arg1, int arg2);
    boolean cl_call_boolean(int arg0, short arg1, long arg2);
    boolean cl_call_boolean(int index, int arg0, int arg1);
    boolean cl_call_boolean(int index, int arg0, IntType arg1);
    boolean cl_call_boolean(int arg0, long arg1, int arg2);
    boolean cl_call_boolean(int arg0, long arg1, IntType arg2);
    boolean cl_call_boolean(int index, IntType arg0, int arg1);
    boolean cl_call_boolean(int index, IntType arg0, IntType arg1);
    boolean cl_call_boolean(int arg0, int arg1, int arg2, int arg3);
    boolean cl_call_boolean(int index, int arg0, int arg1, IntType arg2);
    boolean cl_call_boolean(int index, IntType arg0, int arg1, int arg2);
    boolean cl_call_boolean(int index, IntType arg0, int arg1, IntType arg2);
    boolean cl_call_boolean(int arg0, int arg1, int arg2, int arg3, int arg4);

    int cl_call_int();
    int cl_call_int(int index, int arg0);
    int cl_call_int(int index, IntType arg0);
    int cl_call_int(int index, int arg0, int arg1);
    int cl_call_int(int index, IntType arg0, int arg1);
    int cl_call_int(int index, int arg0, int arg1, int arg2);
    int cl_call_int(int index, IntType arg0, int arg1, int arg2);

    int[] cl_call_ints();
    int[] cl_call_ints(int arg0);
    int[] cl_call_ints(int arg0, int arg1);
    int[] cl_call_ints(int index, IntType arg0);
    int[] cl_call_ints(int index, int arg0, int arg1);
    int[] cl_call_ints(int index, IntType arg0, int arg1);
    int[] cl_call_ints(int index, int arg0, int arg1, int arg2);
    int[] cl_call_ints(int index, IntType arg0, int arg1, int arg2);

    long[] cl_call_longs();
    long[] cl_call_longs(int arg0);
    long[] cl_call_longs(int arg0, short arg1);
    long[] cl_call_longs(int arg0, int arg1);
    long[] cl_call_longs(int arg0, IntType arg1);
    long[] cl_call_longs(int arg0, Object2IntMap<String> args);

    void sv_call();
    void sv_call(int arg0);
    void sv_call(int arg0, short arg1);
    void sv_call(int arg0, long arg1);
    void sv_call(int arg0, int arg1, int arg2);
    void sv_call(int arg0, int arg1, long arg2);
    void sv_call(int arg0, long arg1, int arg2);
    void sv_call(int arg0, long arg1, Object2IntMap<String> params);

    boolean sv_call_boolean(int arg0);
    boolean sv_call_boolean(int arg0, short arg1);
    boolean sv_call_boolean(int arg0, int arg1);
    boolean sv_call_boolean(int arg0, short arg1, int arg2);
    boolean sv_call_boolean(int arg0, short arg1, long arg2);
    boolean sv_call_boolean(int arg0, int arg1, int arg2);
    boolean sv_call_boolean(int arg0, int arg1, IntType arg2);
    boolean sv_call_boolean(int arg0, long arg1, int arg2);
    boolean sv_call_boolean(int arg0, long arg1, IntType arg2);
    boolean sv_call_boolean(int arg0, int arg1, int arg2, int arg3, int arg4);

    int[] sv_call_ints(int arg0);

    long[] sv_call_longs(int arg0);
    long[] sv_call_longs(int arg0, short arg1);
    long[] sv_call_longs(int arg0, int arg1);
    long[] sv_call_longs(int arg0, Object2IntMap<String> args);
}
