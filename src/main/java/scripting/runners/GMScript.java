package scripting.runners;

import scripting.GlobalEnvironment;

public interface GMScript {
    void run(LocalContext context, GlobalEnvironment environment, MessageQueue queue);
}
