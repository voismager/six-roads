package scripting.runners;

import client.logic.ClEnvironment;
import common.server.gamelogic.logic.SVEnvironment;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class JavaActionScript {
    private final PLScript[] playerStages;
    private final PLScript[] watchersStages;
    private final GMScript[] gameMasterStages;
    private final Predicate<LocalContext> prefetchStage;
    private final Consumer<LocalContext> cleanStage;

    JavaActionScript(ClEnvironment cl, SVEnvironment sv,
                     PLScript[] playerStages,
                     PLScript[] watchersStages,
                     GMScript[] gameMasterStages,
                     Predicate<LocalContext> prefetchStage,
                     Consumer<LocalContext> cleanStage) {

        this.playerStages = playerStages;
        this.watchersStages = watchersStages;
        this.gameMasterStages = gameMasterStages;
        this.prefetchStage = prefetchStage;
        this.cleanStage = cleanStage;
    }
}
