package scripting.runners;

import it.unimi.dsi.fastutil.ints.IntList;

public interface MessageQueue {
    ActionQueue pushRunScript(int puppeteer, short script, int innerScriptIndex, IntList args);
}
