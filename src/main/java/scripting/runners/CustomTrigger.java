package scripting.runners;

import client.logic.ClEnvironment;
import common.server.gamelogic.logic.SVEnvironment;
import engine.support.keys.GameUtil;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;

import static constants.EventsFlags.CUSTOM_TRIGGER;
import static constants.Triggers.NO_FLAGS;

public class CustomTrigger extends JavaRunner {
    protected long key;

    public CustomTrigger(ClEnvironment cl, SVEnvironment sv, Object2ObjectMap<String, Object> parameters) {
        super(cl, sv, parameters);
    }

    @Override
    public final void cl_call(long key) {
        this.key = key;
    }
}
