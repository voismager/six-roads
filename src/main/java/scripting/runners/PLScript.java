package scripting.runners;

import scripting.GlobalEnvironment;

public interface PLScript {
    void run(LocalContext context, GlobalEnvironment environment, ActionQueue queue);
}
