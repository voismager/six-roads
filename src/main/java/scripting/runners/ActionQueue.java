package scripting.runners;

public interface ActionQueue {
    ActionQueue pushJumpToAction(short actionId);

    ActionQueue pushTriggerToStage(long trigger, int stage);

    ActionQueue withFlag(char flag);
}
