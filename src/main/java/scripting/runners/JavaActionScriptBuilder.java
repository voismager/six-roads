package scripting.runners;

import client.logic.ClEnvironment;
import common.server.gamelogic.logic.SVEnvironment;

import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class JavaActionScriptBuilder {
    private ArrayList<PLScript> playerStages = new ArrayList<>();
    private ArrayList<PLScript> watchersStages = new ArrayList<>();
    private ArrayList<GMScript> gameMasterStages = new ArrayList<>();
    private Predicate<LocalContext> prefetchStage;
    private Consumer<LocalContext> cleanStage;

    private final Builder0 builder0 = new Builder0();

    public class Builder0 {
        public Builder0 gameMasterShouldReact(GMScript script) {
            int index = playerStages.size() - 1;
            gameMasterStages.ensureCapacity(index + 1);
            gameMasterStages.set(index, script);
            return this;
        }

        public Builder0 watcherShouldReact(PLScript script) {
            int index = playerStages.size() - 1;
            watchersStages.ensureCapacity(index + 1);
            watchersStages.set(index, script);
            return this;
        }

        public JavaActionScriptBuilder completeThisStage() {
            return JavaActionScriptBuilder.this;
        }
    }

    public Builder0 setStageScript(PLScript stage) {
        playerStages.add(stage);
        return builder0;
    }

    public JavaActionScriptBuilder setPrefetchScript(Predicate<LocalContext> stage) {
        this.prefetchStage = stage;
        return this;
    }

    public JavaActionScriptBuilder setCleanScript(Consumer<LocalContext> stage) {
        this.cleanStage = stage;
        return this;
    }

    public JavaActionScript complete(ClEnvironment cl, SVEnvironment sv) {
        return new JavaActionScript(
                cl, sv,
                playerStages.toArray(new PLScript[0]),
                watchersStages.toArray(new PLScript[0]),
                gameMasterStages.toArray(new GMScript[0]),
                prefetchStage, cleanStage
        );
    }
}
