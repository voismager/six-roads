package scripting;

import client.logic.ClEnvironment;
import common.server.gamelogic.logic.SVEnvironment;
import it.unimi.dsi.fastutil.objects.Object2ObjectMap;
import scripting.runners.JavaRunner;

import java.util.HashMap;
import java.util.Map;

public class BuiltinScripts {
    private static final Map<String, TripleFunction<ClEnvironment, SVEnvironment, Object2ObjectMap<String, Object>, JavaRunner>> runners;

    private interface TripleFunction<F, S, T, R> {
        R apply(F f, S s, T t);
    }

    static {
        runners = new HashMap<>();
    }

    public static JavaRunner getByName(String name, ClEnvironment cl, SVEnvironment sv, Object2ObjectMap<String, Object> params) {
        return runners.get(name).apply(cl, sv, params);
    }
}
