package scripting;

import client.gameobjects.Sprite;
import common.Template;
import common.fsm.Memory;
import common.gameobjects.Hexagon;
import common.gameobjects.Puppeteer;
import common.PainterInterface;
import engine.types.vectors.Vector;
import server.Player;

public interface GlobalEnvironment {
    Memory memory();

    Player getPlayer(int puppeteer);

    void stopPlaying();
    void unsubscribe();

    int totalPuppeteers();

    PainterInterface getPainter(byte paint);
    PainterInterface getFreePainter();

    int puppeteer();
    int loadTexture(String name);
    void moveCamera(float x, float y, float z);
    void setCamera(float x, float y, float z);

    void removeComponent(short id, short componentId);
    void addComponent(short id, short componentId, String drawableId, int flags);
    void openComponent(short id, short componentId, String drawableId, int flags);

    Sprite createSprite(float width, float height, int texture);

    Hexagon createCell(Template template);
    Hexagon getCell(int q, int r, int l);
    void putCell(int q, int r, int l, Hexagon cell);
    void removeCell(int q, int r, int l);

    Vector getAttribute(Hexagon hexagon, String name);
    Vector getAttribute(Template template, String name);
    Vector getAttribute(Puppeteer puppeteer, String name);

    void setAttribute(Hexagon hexagon, String name, Vector value);
    void setAttribute(Template template, String name, Vector value);
    void setAttribute(Puppeteer puppeteer, String name, Vector value);

    Template getTemplate(short id);
    Template getTemplate(String name);
}
