package scripting;

import common.fsm.Script;
import scripting.fsm.*;

import java.util.HashMap;
import java.util.Map;

public class StandardFSMScripts {
    private final static Map<String, Script> SCRIPTS = new HashMap<>();

    static {
        SCRIPTS.put("std::empty", (event, stack, context, env) -> {});
        new P2061_start().accept(SCRIPTS);
        new P2061_movement().accept(SCRIPTS);
    }

    public static Map<String, Script> get() {
        return SCRIPTS;
    }
}
