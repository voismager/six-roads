import engine.network.IpUtil;

import java.io.IOException;
import java.net.InetAddress;

public class GetMyIpTest {
    public static void main(String[] args) throws IOException {
        InetAddress a = IpUtil.getLAN();
        System.out.println(a);

        InetAddress aa = IpUtil.getExternal();
        System.out.println(aa);
    }
}
