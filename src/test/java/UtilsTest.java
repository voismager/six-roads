import engine.support.HexagonUtils;

import java.util.Random;

public class UtilsTest {
    public static void main(String[] args) {
        Random random = new Random();
        int k = 0;

        for (int i = 0; i < 100000; i++) {
            int q0 = random.nextInt(500000);
            int r0 = random.nextInt(500000);
            int q1 = random.nextInt(500000);
            int r1 = random.nextInt(500000);

            try {
                HexagonUtils.getDirection(q0, r0, q1, r1);
            } catch (AssertionError e) {
                System.out.println(q0 + " " + r0);
                System.out.println(q1 + " " + r1);
                System.out.println();
                k++;
            }
        }

        System.out.println(k);
    }
}
