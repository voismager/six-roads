import client.Settings;
import engine.network.api.APIHolder;

import static engine.support.loaders.FilesLoader.loadResource;

public class HttpTest {
    public static void main(String[] args) throws Exception {
        Settings.init(loadResource("/app.properties"));
        APIHolder.getInstance().login("admin", "admin");
        APIHolder.getInstance().getPlayerInformation(1);
    }
}
