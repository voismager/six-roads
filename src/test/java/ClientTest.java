import client.Settings;
import client.localclientsession.LocalClientSession;
import client.processor.Command;
import client.processor.Processor;
import client.processor.commands.ActivateExecutor;
import client.processor.commands.SendRequest;
import client.states.Lobby;
import engine.network.IpUtil;
import engine.network.api.APIHolder;
import server.message.JoinRoomRequest;
import server.message.SetPlayerPropertyRequest;
import server.property.Properties;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import static engine.support.loaders.FilesLoader.loadResource;

public class ClientTest {
    public static void main(String[] args) throws Exception {
        try {
            Settings.init(loadResource("/app.properties"));
        }

        catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        new Thread(() -> {
            sleep();

            InetSocketAddress address = null;
            try {
                address = new InetSocketAddress(IpUtil.getLocal(), 5555);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            //Command command0 = new ActivateExecutor(LocalHostSession.INSTANCE, address, session);

            //Processor.fireAndWait(command0);

            Command command1 = new ActivateExecutor(LocalClientSession.INSTANCE, address, "", APIHolder.getInstance().getPlayer().id);

            Processor.fireAndWait(command1);

            sleep();
            Processor.fire(new SendRequest(new JoinRoomRequest(1)));
            sleep();
            Processor.fire(new SendRequest(new SetPlayerPropertyRequest(Properties.PLAYER_READY_FLAG, true)));

        }).start();

        Processor.launch(Lobby.INSTANCE);
    }

    private static void sleep() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
