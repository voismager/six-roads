import client.Settings;
import client.localclientsession.LocalClientSession;
import client.localclientsession.LocalHostSession;
import client.processor.Command;
import client.processor.Processor;
import client.processor.commands.ActivateExecutor;
import client.processor.commands.SendRequest;
import client.states.Menu;
import common.server.EarlyGameSession;
import common.server.properties.LoadedModulesProperty;
import common.server.properties.MapProperty;
import common.server.properties.ModuleProperty;
import common.server.requests.StartGameRequest;
import engine.network.IpUtil;
import engine.network.api.APIHolder;
import server.message.CreateRoomRequest;
import server.message.SetPlayerPropertyRequest;
import server.message.SetRoomPropertyRequest;
import server.property.Properties;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.UUID;

import static engine.support.loaders.FilesLoader.loadResource;

public class LaunchTest {
    public static void main(String[] args) throws Exception {
        try {
            Settings.init(loadResource("/app.properties"));
        }

        catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        EarlyGameSession session = new EarlyGameSession();
        session.setProperty(LoadedModulesProperty.KEY, new ArrayList<>());
        session.addModule(UUID.fromString("0f84e2ee-e18a-422a-bd9f-4dec55454af2"));

        new Thread(() -> {
            sleep();

            InetSocketAddress address = null;
            try {
                address = new InetSocketAddress(IpUtil.getLocal(), 5555);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            Command command0 = new ActivateExecutor(LocalHostSession.INSTANCE, address, session);

            Processor.fireAndWait(command0);

            Command command1 = new ActivateExecutor(LocalClientSession.INSTANCE, address, "", APIHolder.getInstance().getPlayer().id);

            Processor.fireAndWait(command1);

            sleep();
            Processor.fire(new SendRequest(new CreateRoomRequest()));
            sleep();
            Processor.fire(new SendRequest(new SetRoomPropertyRequest(ModuleProperty.KEY, UUID.fromString("0f84e2ee-e18a-422a-bd9f-4dec55454af2"))));
            sleep();
            Processor.fire(new SendRequest(new SetRoomPropertyRequest(MapProperty.KEY, (short)0)));
            sleep();
            Processor.fire(new SendRequest(new SetPlayerPropertyRequest(Properties.PLAYER_READY_FLAG, true)));
            sleep();
            Processor.fire(new SendRequest(new StartGameRequest()));

        }).start();

        Processor.launch(Menu.INSTANCE);
    }

    private static void sleep() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
