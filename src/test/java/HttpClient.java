import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.SSLException;
import java.net.InetAddress;

public class HttpClient {
    private final static Logger logger = LogManager.getLogger(HttpClient.class);

    private final ChannelInitializer<SocketChannel> initializer;
    private EventLoopGroup worker;
    private Channel channel;

    public HttpClient(boolean ssl, ChannelHandler... handlers) throws SSLException {
        if (ssl) {
            final SslContext sslContext = SslContextBuilder.forClient()
                    .trustManager(InsecureTrustManagerFactory.INSTANCE).build();

            this.initializer = new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) {
                    ch.pipeline()
                            .addLast(sslContext.newHandler(ch.alloc()))
                            .addLast(new HttpClientCodec())
                            .addLast(new HttpObjectAggregator(512*16))
                            .addLast(handlers);
                }
            };

        } else {
            this.initializer = new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) {
                    ch.pipeline()
                            .addLast(new HttpClientCodec())
                            .addLast(new HttpObjectAggregator(512*16))
                            .addLast(handlers);
                }
            };
        }
    }

    public ChannelFuture send(HttpRequest request) {
        return channel.writeAndFlush(request);
    }

    public boolean connect(InetAddress address, int port) {
        if (channel != null && channel.isOpen()) return true;

        this.worker = new NioEventLoopGroup(1);

        try {
            ChannelFuture result = new Bootstrap()
                    .group(worker)
                    .channel(NioSocketChannel.class)
                    .handler(initializer)
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 7500)
                    .connect(address, port).await();

            if (result.isSuccess()) {
                this.channel = result.channel();
                return true;
            }

            else {
                this.worker.shutdownGracefully();
                throw new RuntimeException();
            }

        } catch (InterruptedException | RuntimeException e) {
            logger.error("Http connection exception: {}", e.getMessage());
            return false;
        }
    }

    public void disconnect() throws InterruptedException {
        channel.closeFuture().sync();
        worker.shutdownGracefully();
    }
}
