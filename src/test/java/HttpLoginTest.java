import client.Settings;
import engine.network.api.APIHolder;

import java.io.IOException;
import java.net.URISyntaxException;

import static engine.support.loaders.FilesLoader.loadResource;

public class HttpLoginTest {
    public static void main(String[] args) throws IOException, URISyntaxException {
        Settings.init(loadResource("/app.properties"));

        APIHolder.getInstance().login("admin", "admin");

        System.out.println(APIHolder.getInstance().getPlayer());
    }

}
