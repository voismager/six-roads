package msg;

import io.netty.buffer.ByteBuf;
import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;

import java.util.function.Function;

public class Properties {
    private static final Object2IntMap<Class> PROP_CODES;

    static {
        PROP_CODES = new Object2IntOpenHashMap<>();
        PROP_CODES.put(Boolean.class, 0);
        PROP_CODES.put(Byte.class, 1);
        PROP_CODES.put(Short.class, 2);
        PROP_CODES.put(Integer.class, 3);
        PROP_CODES.put(Long.class, 4);
        PROP_CODES.put(String.class, 5);
    }

    public static Function<ByteBuf, Object> getDecoder(Class clazz) {
        return null;
    }

    public static int getProperties(Class[] properties) {
        if (properties.length > 4) throw new IllegalStateException();

        int code = (byte) properties.length;
        int pointer = 2;

        for (Class prop : properties) {
            code = code | (PROP_CODES.getInt(prop) << pointer);
            pointer += 2;
        }

        return code;
    }
}
